//
//  RProfileViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface RProfileViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *RallyPointLbl;
@property (strong, nonatomic) IBOutlet UIButton *aboutBtn;
@property (strong, nonatomic) IBOutlet UILabel *schoolLbl;
@property (strong, nonatomic) IBOutlet UIButton *packBtn;
@property (strong, nonatomic) IBOutlet UILabel *HomeLbl;
@property (strong, nonatomic) IBOutlet UITextView *BioLbl;
@property (strong, nonatomic) IBOutlet UILabel *packLbl;
@property (strong, nonatomic) IBOutlet  AsyncImageView  *profileImg;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSString *searchuserid;
@property (strong, nonatomic) IBOutlet NSString *frienduserId;
@property (strong, nonatomic) IBOutlet NSString *JoinRallyersuserId;
@property (strong, nonatomic) IBOutlet NSString *hubUserIDStr;
@property (strong, nonatomic) IBOutlet UIButton *addUser;
@property(nonatomic) BOOL  serachView1;
@property(nonatomic) BOOL  homeView1;
- (IBAction)aboutAction:(id)sender;
@property (nonatomic, strong) UITableView *packTableview;
- (IBAction)packAction:(id)sender;
- (IBAction)addUserAction:(id)sender;
- (IBAction)photoAlbumAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *arrowImg;
@property (strong, nonatomic) IBOutlet UILabel *ageLbl;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLbl;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;
- (IBAction)removeUserFromPackAction:(id)sender;
@property ( strong , nonatomic ) IBOutlet UIButton *photoAlbumBtn;
@property ( strong , nonatomic ) IBOutlet UILabel *countryAddress;


@property (strong, nonatomic) IBOutlet AsyncImageView *profileCircleImage;
- (IBAction)FullImageAction:(id)sender;

@end
