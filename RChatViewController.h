//
//  RChatViewController.h
//  Rally
//
//  Created by Ambika on 4/9/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"
#import "HPGrowingTextView.h"


@interface RChatViewController : UIViewController<UIBubbleTableViewDataSource,UITextViewDelegate,UIGestureRecognizerDelegate,HPGrowingTextViewDelegate>
{
    NSMutableArray *bubbleData;
    HPGrowingTextView *growingtextView;
    UIView *customView;
}
@property (strong, nonatomic) NSString *RallyIDStr;
@property (strong, nonatomic) NSString *rallyTitleName;
@property (weak, nonatomic) IBOutlet UIBubbleTableView *bubbleTable;
- (IBAction)sayPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *chatTextField;
@property (weak, nonatomic) IBOutlet UIView *textInputView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end
