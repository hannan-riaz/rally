//
//  RLogInViewController.h
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "STTwitter.h"
@interface RLogInViewController : UIViewController<FBLoginViewDelegate>
{
    NSDictionary *userInfoDict;
}
- (IBAction)SigninAction:(id)sender;
- (IBAction)FacebookAction:(id)sender;
- (IBAction)TwitterAction:(id)sender;
- (IBAction)MessageAction:(id)sender;
@property(nonatomic,strong) NSString * urlStr;
- (IBAction)SignUpAction:(id)sender;
//-(void)userdetail;
@property(nonatomic)BOOL FirstTimelogout;
@property (strong, nonatomic) IBOutlet UIImageView *imageVew;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
-(void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verfier;
@property ( strong , nonatomic ) IBOutlet UIButton *signupBtn;
@property ( strong , nonatomic ) IBOutlet UIButton *signinBtn;
@property ( strong , nonatomic ) IBOutlet UILabel *alreadySignupLbl;

@property (weak, nonatomic) IBOutlet UILabel *tagLbl;
@end
