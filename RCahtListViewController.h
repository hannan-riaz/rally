//
//  RCahtListViewController.h
//  Rally
//
//  Created by Ambika on 6/14/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCahtListViewController : UIViewController<UIScrollViewDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *chatScrollView;
@property (strong, nonatomic)NSString * dateLastView;
@end
