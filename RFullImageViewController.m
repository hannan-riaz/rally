//
//  RFullImageViewController.m
//  Rally
//
//  Created by Ambika on 6/12/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 240.0
#define kOFFSET_FOR_KEYBOARD1 75.0


#import "RFullImageViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "RProfileViewController.h"
#import "RAlbumViewController.h"
@interface RFullImageViewController ()

{
    MBProgressHUD *ProgressHUD;
    NSString * currentdate_Str;
    NSMutableArray * commentArray;
    NSMutableArray * userFriendsArray;
    NSMutableArray *searchResultsArray;
    NSMutableArray * serachArray;
    int txt ;
    CGRect labelFrame ;
    UIButton * userNameBtn;
    UIScrollView * imageScrollView;
    AsyncImageView * fullScreenImage;
    int x;
    UIBarButtonItem *rightBarButton ;
    int imageTag;
    NSString *selectedImageId;
    NSString * selectedImageIdForlike;
    NSString * selectedImageIdForScroll;
    NSMutableArray * albumStautsArray;
    NSString * selectedImageIdForComment;
}
@end

@implementation RFullImageViewController
@synthesize rallyImagesDic,LikesLbl,commentsLbl,friendUserId_Image,likesCheckLbl,commentview,postBtn,closeBtn,commentTableView,searchTableView,fullImageview,fullImgView,imageNames,hubrallyImagesDic,likeImageBtn,commentBtn,lineView,grayBoxView,commnetImageView,rImageView,albumDataArray,albumIdString,rallyId_string,deleteImageBtn,trashGrayImge,PushDataDic,userid_str;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(iOSVersion>=7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    deleteImageBtn.hidden = YES;
    if ([PushDataDic  objectForKey:@"commenttcount"])
    {
    commentview.hidden = NO;
         imageScrollView.hidden = YES;
        [self commentAction :nil];
    }
    else
    {
         commentview.hidden = YES;
         imageScrollView.hidden = NO;
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    

       if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-107)];
            
            //  fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
            commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-450, commentview.frame.size.width, commentview.frame.size.height);
            // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
        }
        else
        {
            imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-193)];
            // fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+84,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
            commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-310, commentview.frame.size.width, commentview.frame.size.height);
      }

    }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-107)];
                
                //  fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
                commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-450, commentview.frame.size.width, commentview.frame.size.height);
                // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
            }
            else
            {
                imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-193)];
                // fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+84,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
                commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-310, commentview.frame.size.width, commentview.frame.size.height);
            }

        }
    else
    {
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-107)];

    //  fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
      commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-450, commentview.frame.size.width, commentview.frame.size.height);
        // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
    }
    else
    {
         imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-193)];
       // fullImageview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+84,  fullImageview.frame.size.width,  fullImageview.frame.size.height);
        commentview.frame = CGRectMake(0, imageScrollView.frame.origin.y+imageScrollView.frame.size.height-310, commentview.frame.size.width, commentview.frame.size.height);
    }
    
    }
    if (hubrallyImagesDic)
    {
//        CGFloat height = [UIScreen mainScreen].bounds.size.height;
//        if (height== 568)
//        {
//           imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-64)];
//        }
//        else
//        {
//            imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, 320, self.view.frame.size.height-152)];
//        }
    }
    imageScrollView.backgroundColor = [UIColor blackColor];
    imageScrollView.pagingEnabled = YES;
    
    [self.view addSubview:imageScrollView];
    

       if (imageNames)
    {
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[rallyImagesDic  objectForKey:@"imagename"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        //fullImageview.imageURL=imageURL;
    }
       albumStautsArray = [[NSMutableArray alloc]init];
    fullImageview.hidden = YES;
      commentArray = [[NSMutableArray alloc]init];
    userFriendsArray = [[NSMutableArray alloc]init];
    serachArray = [[NSMutableArray alloc]init];
    searchResultsArray = [[NSMutableArray alloc]init];
    postBtn.layer.cornerRadius = 5.0f;
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets = UIEdgeInsetsMake(0.5, 0.5, 0, 0);
    closeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;

    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
      UITapGestureRecognizer *lpgr = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self action:@selector(handleLongPress:)];
 
    [commentTableView addGestureRecognizer:lpgr];
    userNameBtn = [[UIButton alloc]init];

           // Do any additional setup after loading the view from its nib.
}

-(void)scrollImage
{  x = 0;
    imageScrollView.scrollEnabled = YES;
    imageScrollView.showsVerticalScrollIndicator = YES;
    imageScrollView.showsHorizontalScrollIndicator=NO;
    imageScrollView.delegate = self;
        if(hubrallyImagesDic)
    {
       // [self fullImageFrom_Hub];
//        deleteImageBtn.hidden = YES;
//        trashGrayImge.hidden = YES;
//        likesCheckLbl.hidden = YES;
//        LikesLbl.hidden = YES;
//        commentsLbl.hidden = YES;
//        likeImageBtn.hidden = YES;
//        commentBtn.hidden = YES;
//        lineView.hidden = YES;
//        grayBoxView.hidden = YES;
//        commnetImageView.hidden = YES;
//        rImageView.hidden = YES;
//        grayBoxView.hidden = YES;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            
            fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320, imageScrollView.frame.size.height)];
        }
        else
        {
            fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320, imageScrollView.frame.size.height)];
        }

        if ([[hubrallyImagesDic  objectForKey:@"userID"] isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            deleteImageBtn.hidden = NO;
            trashGrayImge.hidden = NO;

        }
        else
        {
            deleteImageBtn.hidden = YES;
            trashGrayImge.hidden = YES;
        }
        if ( [[hubrallyImagesDic  objectForKey:@"sentto"]isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            deleteImageBtn.hidden = NO;
            trashGrayImge.hidden = NO;
        }

        //deleteImageBtn.hidden = NO;
       // trashGrayImge.hidden = NO;
        likesCheckLbl.hidden = NO;
        LikesLbl.hidden = NO;
        commentsLbl.hidden = NO;
        likeImageBtn.hidden = NO;
        commentBtn.hidden = NO;
        lineView.hidden = YES;
        grayBoxView.hidden = NO;
        commnetImageView.hidden = NO;
        rImageView.hidden = NO;
        grayBoxView.hidden = NO;

    }
else
{
      for (int i = 0; i <[albumDataArray count]; i++)
      {  CGFloat height = [UIScreen mainScreen].bounds.size.height;
          if (height== 568)
          {
              
              fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(x, 0, 320,  imageScrollView.frame.size.height)];
          }
          else
          {
              fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(x, 0, 320,  imageScrollView.frame.size.height)];
          }

        
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[albumDataArray objectAtIndex:i]objectForKey:@"imagename"] ];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        fullScreenImage.imageURL=imageURL;
        fullScreenImage.tag = i;
        fullScreenImage.contentMode =  UIViewContentModeScaleAspectFit;

        fullScreenImage.backgroundColor = [UIColor clearColor];
        [imageScrollView addSubview:fullScreenImage];
        
        x = x + fullScreenImage.frame.size.width;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self callWebService_getalbumimages];
   imageScrollView.contentSize = CGSizeMake(x,imageScrollView.frame.size.height);
   [imageScrollView setContentOffset:CGPointMake(320*self.index, 0) animated:YES];
    
    CGPoint scrollViewContent=   imageScrollView.contentOffset;
    CGFloat contentOffsetX =  scrollViewContent.x;
    imageTag = contentOffsetX /320 ;
    selectedImageIdForScroll =[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"];
}
}
-(void)fullImageFrom_Hub // change imageName or imagename to userIMAGE

{
    imageScrollView.scrollEnabled = NO;
    NSString *imgStr;
    
    if ([hubrallyImagesDic  objectForKey:@"imageNAME"])
    {
        imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[hubrallyImagesDic  objectForKey:@"imageNAME"]];
    }
    else if ([hubrallyImagesDic  objectForKey:@"imagename"])
    {
        imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[hubrallyImagesDic  objectForKey:@"imagename"]];
    }
    NSURL *imageURL = [NSURL URLWithString:imgStr];
    //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        
        fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320,  imageScrollView.frame.size.height)];
    }
    else
    {
        fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320, imageScrollView.frame.size.height)];
    }
    fullScreenImage.imageURL=imageURL;
    fullScreenImage.contentMode =  UIViewContentModeScaleAspectFit;
    fullScreenImage.backgroundColor = [UIColor clearColor];
    [imageScrollView addSubview:fullScreenImage];
//    deleteImageBtn.hidden = YES;
//    trashGrayImge.hidden = YES;
//    likesCheckLbl.hidden = YES;
//    LikesLbl.hidden = YES;
//    commentsLbl.hidden = YES;
//    likeImageBtn.hidden = YES;
//    commentBtn.hidden = YES;
//    lineView.hidden = YES;
//    grayBoxView.hidden = YES;
//    commnetImageView.hidden = YES;
//    rImageView.hidden = YES;
//    grayBoxView.hidden = YES;
    
//    
    if ([[hubrallyImagesDic  objectForKey:@"userID"] isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        deleteImageBtn.hidden = NO;
         trashGrayImge.hidden = NO;
    }
    else
    {
       deleteImageBtn.hidden = YES;
         trashGrayImge.hidden = YES;
    }
    
    if ( [[hubrallyImagesDic  objectForKey:@"sentto"]isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        deleteImageBtn.hidden = NO;
        trashGrayImge.hidden = NO;
    }
    likesCheckLbl.hidden = NO;
    LikesLbl.hidden = NO;
    commentsLbl.hidden = NO;
    likeImageBtn.hidden = NO;
    commentBtn.hidden = NO;
    lineView.hidden = YES;
    grayBoxView.hidden = NO;
    commnetImageView.hidden = NO;
    rImageView.hidden = NO;
    grayBoxView.hidden = NO;

}
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    
      [growingtextView resignFirstResponder];
    growingtextView.text = @"";
}
-(void)callWebService_getalbumimages
{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    if (PushDataDic)
    {
        NSString * albm_id = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"albumid"]];
        NSString * rally_id = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"rallyid"]];
         urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getalbumimages.php?rallyid=%@&albumid=%@&userid=%@",rally_id,albm_id,[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
    }
    else if(hubrallyImagesDic)
    {
        NSString * albm_id;
        NSString * rally_id;
        NSString * user_id_str ;
        if([hubrallyImagesDic objectForKey:@"albumID"]||[hubrallyImagesDic objectForKey:@"rallyID"])
        {
            albm_id = [NSString stringWithFormat:@"%@",[hubrallyImagesDic objectForKey:@"albumID"]];
            rally_id = [NSString stringWithFormat:@"%@",[hubrallyImagesDic objectForKey:@"rallyID"]];
            user_id_str = [NSString stringWithFormat:@"%@", [hubrallyImagesDic  objectForKey:@"userID"]];
        }
        else
        {
            albm_id = [NSString stringWithFormat:@"%@",[hubrallyImagesDic objectForKey:@"albumid"]];
            rally_id = [NSString stringWithFormat:@"%@",[hubrallyImagesDic objectForKey:@"rallyid"]];
        user_id_str = [NSString stringWithFormat:@"%@", [hubrallyImagesDic  objectForKey:@"sentto"]];
        }
    
       
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getalbumimages.php?rallyid=%@&albumid=%@&userid=%@",rally_id,albm_id,user_id_str];
    }
    else
    {
     
         urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getalbumimages.php?rallyid=%@&albumid=%@&userid=%@",rallyId_string,albumIdString,userid_str];
    }
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved_getalbumimages:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}


-(void)JSONRecieved_getalbumimages:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //    NSLog(@"response12%@",response);
    
    if ([dict objectForKey:@"albumimages"]== [NSNull null]  )
    {
        
    }
    else
    {
       
     if(hubrallyImagesDic)
        {
 albumDataArray = [dict objectForKey:@"albumimages"];

           }
        else
        {
             albumDataArray = [dict objectForKey:@"albumimages"];
        }
    }
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case  APIResponseStatus_GetAlbumImagesFailed:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Somthing went wrong.Please Try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
        case  APIResponseStatus_GetAlbumImagesSuccessfull:
        {
            
            break;
        }
            
            
        default:
            break;
            
    }
}


-(void)callWebSercvice_getrallyalbum
{
    
    NSString * urlStr;
    if (PushDataDic)
    {
         urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@&friendid=",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    }
    else
    {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@&friendid=",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    }
    else
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@&friendid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image];
    }
    }
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_getrallyalbum:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_getrallyalbum:(NSDictionary *)response
{
    NSDictionary *dict = response;
    // NSLog(@"response12%@",response);
    
    if (![[dict objectForKey:@"useralbum"] isEqual:[NSNull null]] )
    {
       
       
    }
    switch ([dict[kRAPIResult] integerValue])
    {
        case APIResponseStatus_GetUserAlbumSuccessfull:
        {
            if (hubrallyImagesDic)
            {
                //NSURL *imageURL = [NSURL URLWithString:imgStr];
                //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
              //  fullImageview.imageURL=imageURL;
                [self fullImageFrom_Hub];
                

            }
        
            break;
        }
        case APIResponseStatus_GetUserAlbumFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_FriendNotInPack:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This person isn’t in your pack, send them a request to view their pic." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            if ([PushDataDic  objectForKey:@"commenttcount"])
            {
            commentview.hidden = NO;
                imageScrollView.hidden = YES;
            }
            else
            {
                commentview.hidden = YES;
                imageScrollView.hidden = NO;
              
            }
            likesCheckLbl.hidden = YES;
            LikesLbl.hidden = YES;
            commnetImageView.hidden = YES;
             commentsLbl.hidden = YES;
            commentBtn.hidden = NO;
            likeImageBtn.hidden = YES;
            commentBtn.hidden = YES;
            lineView.hidden = YES;
            grayBoxView.hidden = YES;
           
            rImageView.hidden = YES;
            break;
        }
        default:
            break;
            
    }
}

-(void)callWebSercvice_DeletRallyImage
{
    
    NSString * urlStr;
    if (PushDataDic)
    {
        NSString * img_id = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"imageid"]];
       NSString * album_id = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"albumid"]];
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterallyimage.php?imageid=%@&albumid=%@",img_id,album_id];

    }
    else
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterallyimage.php?imageid=%@&albumid=%@",selectedImageId,albumIdString];

    }
    
       NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_DeletRallyImage:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_DeletRallyImage:(NSDictionary *)response
{
    NSDictionary *dict = response;
    // NSLog(@"response12%@",response);
    
       switch ([dict[kRAPIResult] integerValue])
    {
            
        case APIResponseStatus_RallyImageDeletedSuccessfully:
        {
//            RAlbumViewController *ImageViewController = [[RAlbumViewController alloc]initWithNibName:@"RAlbumViewController" bundle:nil];
//            [self.navigationController pushViewController:ImageViewController animated:NO];
            [self.navigationController popViewControllerAnimated:YES];

            break;
        }
        case APIResponseStatus_DeleteRallyImageFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
               default:
            break;
            
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
    {
        deleteImageBtn.hidden = NO;
        trashGrayImge.hidden = NO;
    }
    else
    {
        deleteImageBtn.hidden = YES;
        trashGrayImge.hidden = YES;
    }
    
    

    [self scrollImage];
    if (PushDataDic)
    {
        if (PushDataDic)
        { if ([PushDataDic  objectForKey:@"commenttcount"])
        {

            commentview.hidden = NO;
            imageScrollView.hidden = YES;
        }
            else
            {
                commentview.hidden = YES;
                imageScrollView.hidden = NO;

            }
            [self callWebSercvice_getrallyalbum];
            imageScrollView.scrollEnabled = NO;
            NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[PushDataDic  objectForKey:@"imagename"]];
            
            NSURL *imageURL = [NSURL URLWithString:imgStr];
            //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                
                fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320, imageScrollView.frame.size.height)];
            }
            else
            {
                fullScreenImage = [[ AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 320, imageScrollView.frame.size.height)];
            }
            fullScreenImage.imageURL=imageURL;
            fullScreenImage.contentMode =  UIViewContentModeScaleAspectFit;

            fullScreenImage.backgroundColor = [UIColor clearColor];
            [imageScrollView addSubview:fullScreenImage];
             LikesLbl.text = [NSString stringWithFormat:@"%@",[PushDataDic  objectForKey:@"likecount"]];
            commentsLbl.text =[NSString stringWithFormat:@"%@",[PushDataDic  objectForKey:@"commenttcount"]];
            if ([PushDataDic  objectForKey:@"like"]) {
                rImageView.image = [UIImage imageNamed:@"startRally.png"];

           }
            
//            NSString * likeStatus_str = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"likestatus"]];
//            NSString * imgidStatus_str1 = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"]];
//            
//            
//            NSString *imgid_str = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"imageid"]];
//            if ([imgidStatus_str1 isEqualToString:imgid_str]) {
//                
//                if ([likeStatus_str isEqualToString:@"0"]) {
//                    
//                    likesCheckLbl.text = @"Like";
//                    rImageView.image = [UIImage imageNamed:@"grayRallyIcon.png"];
//                }
//                else
//                {
//                    likesCheckLbl.text = @"Unlike";
//                    rImageView.image = [UIImage imageNamed:@"startRally.png"];
//                }
            
   //         }

            deleteImageBtn.hidden = NO;
            trashGrayImge.hidden = NO;
            likesCheckLbl.hidden = NO;
            LikesLbl.hidden = NO;
            commentsLbl.hidden = NO;
            likeImageBtn.hidden = NO;
            commentBtn.hidden = NO;
            lineView.hidden = YES;
            grayBoxView.hidden = NO;
            commnetImageView.hidden = NO;
            rImageView.hidden = NO;
            grayBoxView.hidden = NO;
        }

    }
    else
    {
         [self callWebSercvice_getrallyalbum];
    }
   
    [self callWebService_likeView ];
    
       //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackBg.png"]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    if ([PushDataDic  objectForKey:@"commenttcount"])
    {commentview.hidden = NO;
         imageScrollView.hidden = YES;
    }
    else
    {
        commentview.hidden = YES;
         imageScrollView.hidden = NO;

    }
         likesCheckLbl.text = @"Like";
    postBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//    self.navigationItem.title = @"Photo";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Photo";
    self.navigationItem.titleView = lblTitle;
    
     
       }

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];

    
    //self.navigationController.navigationBarHidden = NO;
    
}

-(void)keyboardWillShow:(NSNotification*)aNotification
{
   
    NSDictionary* info = [aNotification userInfo];
    CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if (kbRect.size.height==216)
        {
        [self setViewMovedUp:NO];
        [self setViewMovedUp:YES];
        }
        else if (kbRect.size.height==224)
        {
        [self setViewMovedUp:NO];
        [self setViewMovedUp:YES];
        }
        else if (kbRect.size.height==253)
        {
        [self setViewMovedUp:NO];
        [self setViewMovedUp:YES];
        }
        else
        {
        [self setViewMovedUp:NO];
        }
    }
}

-(void)keyboardWillHide:(NSNotification*)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
          if (IS_OS_8_OR_LATER)
          {
          rect.origin.y -= kOFFSET_FOR_KEYBOARD1+30;
          rect.size.height += kOFFSET_FOR_KEYBOARD1+30;
          }
          else
          {
            rect.origin.y -= kOFFSET_FOR_KEYBOARD1;
            rect.size.height += kOFFSET_FOR_KEYBOARD1;
           }
        }
        else
        {
            if (IS_OS_8_OR_LATER)
            {
                rect.origin.y -= kOFFSET_FOR_KEYBOARD+50;
                rect.size.height += kOFFSET_FOR_KEYBOARD+50;
            }
            else
            {
                rect.origin.y -= kOFFSET_FOR_KEYBOARD;
                rect.size.height += kOFFSET_FOR_KEYBOARD;
            }
            
        }
    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
        if (IS_OS_8_OR_LATER)
        {
        rect.origin.y += kOFFSET_FOR_KEYBOARD1+30;
        rect.size.height -= kOFFSET_FOR_KEYBOARD1+30;
        }
        else
        {
        rect.origin.y += kOFFSET_FOR_KEYBOARD1;
        rect.size.height -= kOFFSET_FOR_KEYBOARD1;
        }
        }
        else
        {
            if (IS_OS_8_OR_LATER)
            {
                rect.origin.y += kOFFSET_FOR_KEYBOARD+50;
                rect.size.height -= kOFFSET_FOR_KEYBOARD+50;
            }
            else
            {
                rect.origin.y += kOFFSET_FOR_KEYBOARD;
                rect.size.height -= kOFFSET_FOR_KEYBOARD;
            }
            //rect.origin.y += kOFFSET_FOR_KEYBOARD;
            //rect.size.height -= kOFFSET_FOR_KEYBOARD;
        }
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}




-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callWebService_likeView
{
    
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
       NSString * urlStr;
    if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
        
    { urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=0",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,selectedImageIdForScroll];
        
        
    }
    else
    {
        
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=0",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],selectedImageIdForScroll];
        
        
        
    }
    if (hubrallyImagesDic)
        
    {
        NSString *    friendImageId_Image;
        if ([hubrallyImagesDic  objectForKey:@"userID"] || [hubrallyImagesDic  objectForKey:@"imageID"])
        {
            friendUserId_Image = [hubrallyImagesDic  objectForKey:@"userID"];
             friendImageId_Image = [hubrallyImagesDic  objectForKey:@"imageID"];
        }
        else if([hubrallyImagesDic  objectForKey:@"sentfrom"])
        {
            friendUserId_Image = [hubrallyImagesDic  objectForKey:@"sentfrom"];
             friendImageId_Image = [hubrallyImagesDic  objectForKey:@"imageid"];
        }
        
        
    if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
        
    { urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=0",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,friendImageId_Image];
        
        
    }
    else
    {
        
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=0",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image];
        
            }
   
    }
    if (PushDataDic)
        
    {
        NSString *  img_id = [NSString stringWithFormat:@"%@",[PushDataDic  objectForKey:@"imageid"]];
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=0",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],img_id];

    }
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

- (IBAction)likesBtn:(id)sender
{
   /* if (![sender isSelected])
    {
        likesCheckLbl.text = @"likes";
        [sender setSelected:YES]; }
    else
    {
        likesCheckLbl.text = @"Dislikes";
        [sender setSelected:NO];
    }
*/
    //likeImageBtn.userInteractionEnabled = NO;
    CGPoint scrollViewContent=   imageScrollView.contentOffset;
    CGFloat contentOffsetX =  scrollViewContent.x;
    imageTag = contentOffsetX /320 ;
   
         selectedImageIdForlike =[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"];
 
   
    
    [self callWebService_like];
}

-(void)callWebService_like
{
    
//    if (ProgressHUD == nil)
//    {
//        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        ProgressHUD.labelText = @"Loading...";
//        ProgressHUD.dimBackground = YES;
//    }
    NSString * urlStr;
    if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])

    {
        if ([likesCheckLbl.text isEqualToString:@"Unlike"])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=2",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,selectedImageIdForlike];
            
        }
        else if([likesCheckLbl.text isEqualToString:@"Like"])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,selectedImageIdForlike];
            
        }
        }
    else
    {
        if ([likesCheckLbl.text isEqualToString:@"Unlike"])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=2",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],selectedImageIdForlike];
            
        }
        else if([likesCheckLbl.text isEqualToString:@"Like"])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=1",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],selectedImageIdForlike];
            
        }
            }
    
    
    if (hubrallyImagesDic)
        
    {
        NSString *    friendImageId_Image;
        if([hubrallyImagesDic objectForKey:@"albumID"]||[hubrallyImagesDic objectForKey:@"rallyID"]|| [hubrallyImagesDic objectForKey:@"userID"]|| [hubrallyImagesDic objectForKey:@"imageID"])
        {
         
            friendUserId_Image = [hubrallyImagesDic  objectForKey:@"userID"];
            
           friendImageId_Image = [hubrallyImagesDic  objectForKey:@"imageID"];
        }
        else
        {
        friendUserId_Image = [hubrallyImagesDic  objectForKey:@"userID"];
    
       friendImageId_Image = [hubrallyImagesDic  objectForKey:@"imageid"];
        }
        if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
            
        {
            if ([likesCheckLbl.text isEqualToString:@"Unlike"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=2",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,friendImageId_Image];
                
            }
            else if([likesCheckLbl.text isEqualToString:@"Like"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,friendImageId_Image];
                
            }
        }
        else
        {
            if ([likesCheckLbl.text isEqualToString:@"Unlike"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=2",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image];
                
            }
            else if([likesCheckLbl.text isEqualToString:@"Like"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=1",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image];
                
            }
        }
    }
        if (PushDataDic)
        {
           
            
            NSString *    friendImageId_Image = [PushDataDic  objectForKey:@"imageid"];
            
           
            if ([likesCheckLbl.text isEqualToString:@"Unlike"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=2",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image];
                
            }
            else if([likesCheckLbl.text isEqualToString:@"Like"])
            {
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/imagelike.php?userid=%@&friendid=%@&imageid=%@&likecheck=1",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image];
                
            }

            
            
        }

        
  
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}


-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"likes"] == [NSNull null] && [dict objectForKey:@"commentcount"] == [NSNull null]&&[dict objectForKey:@"likestatus"] == [NSNull null])
    {
        
        // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else
    {
        LikesLbl.text = [dict objectForKey:@"likes"];
        commentsLbl.text =[NSString stringWithFormat:@"%@", [dict objectForKey:@"commentcount"]];
        NSString * likesStatusStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"likestatus"]];
        if ([likesStatusStr isEqualToString:@"0"])
        {
            likesCheckLbl.text = @"Like";
            rImageView.image = [UIImage imageNamed:@"grayRallyIcon.png"];
        }
        else
        {
            likesCheckLbl.text = @"Unlike";
            rImageView.image = [UIImage imageNamed:@"startRally.png"];
        }

        
    }
    [self callWebService_getalbumimages];
        switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_ImageLikeViewSuccessfully:
            
        {
//            likeImageBtn.userInteractionEnabled = YES;

            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Please Try again later..." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
        case  APIResponseStatus_ImageLikeViewFailed:
            
        {
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Please Try again later..." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }

         case  APIResponseStatus_ImageLikeViewSuccessfullWithLike:
        {
            
            likesCheckLbl.text = @"Unlike";
            //        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User Not Addd In Your Pack" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //        [alert show];
            
            
            break;
        }
        case  APIResponseStatus_ImageLikeViewFailedWithLike:
        {
            
            //        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User Not Addd In Your Pack" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //        [alert show];
            
            
            break;
        }

        case  APIResponseStatus_ImageLikeViewSuccessfullWithDislike:
        {
            likesCheckLbl.text = @"Like";
            //        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User Not Addd In Your Pack" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //        [alert show];
            
            
            break;
        }

            
            
        default:
            
            break;
            
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    CGPoint scrollViewContent=   imageScrollView.contentOffset;
    CGFloat contentOffsetX =  scrollViewContent.x;
    imageTag = contentOffsetX /320 ;
    
    if (PushDataDic)
    {
        LikesLbl.text = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"likecount"]];
        commentsLbl.text = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"commenttcount"]];
        NSString * likeStatus_str = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"likestatus"]];
        NSString * imgidStatus_str1 = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"]];
        
        
        NSString *imgid_str = [NSString stringWithFormat:@"%@",[PushDataDic objectForKey:@"imageid"]];
        if ([imgidStatus_str1 isEqualToString:imgid_str]) {
       
        if ([likeStatus_str isEqualToString:@"0"])
        {
            
            likesCheckLbl.text = @"Like";
            rImageView.image = [UIImage imageNamed:@"grayRallyIcon.png"];
        }
        else
        {
            likesCheckLbl.text = @"Unlike";
            rImageView.image = [UIImage imageNamed:@"startRally.png"];
        }
           
        }
    }
    else if(hubrallyImagesDic)
    {
           }
    else
    {
    LikesLbl.text = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"likecount"]];
    commentsLbl.text = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"commentcount"]];
    NSString * likeStatus_str = [NSString stringWithFormat:@"%@",[[albumDataArray objectAtIndex:imageTag]objectForKey:@"likestatus"]];
    
    if ([likeStatus_str isEqualToString:@"0"]) {
        
        likesCheckLbl.text = @"Like";
        rImageView.image = [UIImage imageNamed:@"grayRallyIcon.png"];
    }
    else
    {
        likesCheckLbl.text = @"Unlike";
        rImageView.image = [UIImage imageNamed:@"startRally.png"];
    }
    }

             
      }
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}
- (IBAction)commentAction:(id)sender

{
   
    //[commentTxtView becomeFirstResponder];
    growingtextView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(5, 307, 230, 53)];
    growingtextView.isScrollable = NO;
    growingtextView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	growingtextView.minNumberOfLines = 1;
	growingtextView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    growingtextView.keyboardType = UIKeyboardTypeDefault;
	growingtextView.returnKeyType = UIReturnKeyNext; //just as an example
	growingtextView.font = [UIFont systemFontOfSize:15.0f];
	growingtextView.delegate = self;
    growingtextView.autocorrectionType = UITextAutocorrectionTypeYes;
    growingtextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    growingtextView.backgroundColor = [UIColor whiteColor];
    //growingtextView.placeholder = @"Type to see the textView grow!";
    growingtextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    growingtextView.layer.cornerRadius = 4.2f;
    growingtextView.layer.borderWidth = 0.2f;
    [commentview addSubview:growingtextView];

    commentview.hidden = NO;
    imageScrollView.hidden = YES;
    CGPoint scrollViewContent=   imageScrollView.contentOffset;
    CGFloat contentOffsetX =  scrollViewContent.x;
    imageTag = contentOffsetX /320 ;
    if (albumDataArray.count>0) {
        selectedImageIdForComment =[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"];
    }
    [self callWebservice_Comment];
    //[commentTableView reloadData];
}

- (IBAction)sendAction:(id)sender
{
    
    [growingtextView resignFirstResponder];
    [self callWebservice_Comment];
}
-(void)callWebservice_Comment
    {
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
    currentdate_Str = [currentdate_Str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
       
        NSString *commentStr = [NSString stringWithFormat:@"%@",growingtextView.text];
        commentStr = [commentStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        commentStr = [commentStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
        NSString *uniText = [NSString stringWithUTF8String:[commentStr UTF8String]];
        NSData *goodMsg = [uniText dataUsingEncoding:NSUTF8StringEncoding];

     NSString *  urlStr;
        if (rallyImagesDic)
        {
            if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
            {
                
                
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/galleryimagecomment.php?friendid=%@&imageid=%@&commentdate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],selectedImageIdForComment,currentdate_Str];
            }
    else
        
    {
        
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/galleryimagecomment.php?friendid=%@&imageid=%@&commentdate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],selectedImageIdForComment,currentdate_Str];
        
      }
        }
        if (hubrallyImagesDic)
            
        {
            NSString *    friendImageId_Image;
            if([hubrallyImagesDic objectForKey:@"albumID"]||[hubrallyImagesDic objectForKey:@"rallyID"]|| [hubrallyImagesDic objectForKey:@"userID"]|| [hubrallyImagesDic objectForKey:@"imageID"])
            {
                friendImageId_Image = [hubrallyImagesDic  objectForKey:@"imageID"];

            }
            else
            {
                friendImageId_Image  = [hubrallyImagesDic  objectForKey:@"imageid"];

            }
            if  ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_Image])
            {
             urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/galleryimagecomment.php?userid=%@&friendid=%@&imageid=%@&commentdate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendUserId_Image,friendImageId_Image,currentdate_Str];
            }
            else
                
            {
               
               urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/galleryimagecomment.php?userid=%@&friendid=%@&imageid=%@&commentdate=%@",friendUserId_Image,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image,currentdate_Str];
                
            }
        }
            if (PushDataDic)
                
            {
                NSString * friendImageId_Image = [PushDataDic  objectForKey:@"imageid"];
                  urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/galleryimagecomment.php?userid=%@&friendid=%@&imageid=%@&commentdate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],friendImageId_Image,currentdate_Str];
               }
       
    
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"commenttext\";"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:goodMsg]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        
       
        

    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved_comment:responseObject];
          //[commentTableView scrollBubbleViewToBottomAnimated:YES];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Error", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
/*- (void) scrollBubbleViewToBottomAnimated:(BOOL)animated
{
    NSInteger lastSectionIdx = [self numberOfSections] - 1;
    
    if (lastSectionIdx >= 0)
    {
    	[self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([self numberOfRowsInSection:lastSectionIdx] - 1) inSection:lastSectionIdx] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
    
}*/
-(void)JSONRecieved_comment:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"comments"] != [NSNull null])
    {
        commentArray = [[dict objectForKey:@"comments"]mutableCopy];
        if (commentArray.count>0) {
        
        
      commentsLbl.text =[NSString stringWithFormat:@"%lu", (unsigned long)commentArray.count];
        }

    }
    if ([dict objectForKey:@"userfriends"] != [NSNull null])
    {
    userFriendsArray =  [dict objectForKey:@"userfriends"];
    }
       [commentTableView reloadData];
    growingtextView.text =@"";
    [self callWebService_getalbumimages];
    switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_CommentAddedSuccessfully:
            
        {
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Please Try again later..." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
        case    APIResponseStatus_GetCommentSuccessfully:
        {
            
              break;
        }
       
        default:
            
            break;
            
    }
}
#pragma mark - HPGrowingTextViewDataSource implementation

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = commentview.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	commentview.frame = r;
    commentTableView.frame  = CGRectMake(commentTableView.frame.origin.x, growingTextView.frame.origin.y-commentTableView.frame.size.height-10,  commentTableView.frame.size.width, commentTableView.frame.size.height);
    searchTableView.frame  = CGRectMake(searchTableView.frame.origin.x, growingTextView.frame.origin.y-searchTableView.frame.size.height-10,  searchTableView.frame.size.width, searchTableView.frame.size.height);

}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
   
    [searchResultsArray removeAllObjects];
    [serachArray removeAllObjects];
    if ([text isEqualToString:@"\n"])
    {
        [growingTextView resignFirstResponder];
    }
    
    if ( [text  isEqualToString:@"@"])
    {
        txt  = 1;
    }
    if (txt == 1)
    {
       // NSString *str = text;
        NSRange equalRange = [growingtextView.text rangeOfString:@"@"];
        if (equalRange.location != NSNotFound) {
            NSString *result = [growingtextView.text substringFromIndex:equalRange.location+equalRange.length];
            result = [NSString stringWithFormat:@"%@%@",result,text];
             // if you need case sensitive search avoid '[c]' in the predicate
            if (userFriendsArray.count >0) {
           
            for (int j= 0 ; j < [userFriendsArray count]; j++)
            {
                NSString *str = [[userFriendsArray objectAtIndex:j]objectForKey:@"friendNAME"];
                [serachArray addObject:str];
                
            }
                
            }
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",result];
            searchResultsArray =[NSMutableArray arrayWithArray:[serachArray filteredArrayUsingPredicate:predicate]];
            if (searchResultsArray.count > 0 ) {
                searchTableView.hidden = NO;
                [searchTableView reloadData];
            }
            
        } else {
//            NSLog(@"There is no = in the string");
        }
    }
    if (searchResultsArray.count == 0)
    {
        searchTableView.hidden = YES;
        
    }
    return YES;
}

- (IBAction)cancelAction:(id)sender
{
    commentview.hidden = YES;
    imageScrollView.hidden = NO;
    [growingtextView resignFirstResponder];
    [self callWebService_getalbumimages ];
    [self callWebservice_Comment];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    textView.text = @"";
    textView.textColor = [UIColor blackColor];
    
//    if ([textView.text isEqualToString:@"@"])
//    {
//        UITableView *friendsTableView = [[UITableView alloc] init];
//        friendsTableView.frame = CGRectMake(0, 50, 100,100);
//        friendsTableView.delegate = self;
//        friendsTableView.dataSource = self;
//        [self.view addSubview:friendsTableView];
//    }
    return YES;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == commentTableView)
    {
         if (commentArray.count>0) {
         return [commentArray count];
         }
    }
    else
        if (searchResultsArray.count>0) {
             return [searchResultsArray count];
        }
    
    
    return 1;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString* CellIdentifier1 = nil;
    if (tableView == commentTableView)
        CellIdentifier1 = @"Cell1";
    else if(tableView == searchTableView)
        CellIdentifier1 = @"Cell";
    
    UILabel * userNameLbl;
    UILabel * CommentLbl;
    
    AsyncImageView *userImage;
    
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
  //  if (cell == nil)
  //  {
  //      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
   // }
    if (tableView == commentTableView)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            cell.indentationLevel = 6;
            //userNameBtn = [[UILabel alloc]initWithFrame:CGRectMake(60,2,cell.frame.size.width ,15 )];
                       userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            userNameBtn.frame = CGRectMake(60,2,cell.frame.size.width ,15);
            userNameBtn.tag = -4;
            userNameBtn.backgroundColor = [UIColor clearColor];
            userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
            [userNameBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:userNameBtn];
            
            CommentLbl = [[UILabel alloc]initWithFrame:CGRectMake(60,userNameLbl.frame.origin.y+userNameLbl.frame.size.height,cell.frame.size.width-80 ,200 )];
            CommentLbl.textColor = [UIColor blackColor];
            CommentLbl.numberOfLines = 0;
            // CommentLbl.lineBreakMode = NSLineBreakByWordWrapping;
            CommentLbl.adjustsLetterSpacingToFitWidth = YES;
            CommentLbl.adjustsFontSizeToFitWidth = YES;
            CommentLbl.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
            [cell.contentView addSubview:CommentLbl];
            CommentLbl .tag = -2;
            userImage = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 8, 35, 35)];
            userImage .tag = -1;
            [cell addSubview:userImage];

            //userNameBtn =[[UIButton alloc]init];
                             }
       
   if(! commentArray.count== 0)
    {
    
        if ([[[commentArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
        {
            userImage.image = [UIImage imageNamed:@""];
        }
        else
        {
            NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[commentArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"]];
            AsyncImageView *img = (AsyncImageView*)[cell viewWithTag:-1];
            NSURL *imageURL = [NSURL URLWithString:imgStr];
            //  NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
            img.imageURL = imageURL;
            // cell.imageView.imageURL = imageURL;
        }
        
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
        if ([[[commentArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"] isEqual:[NSNull null]])
        {
            [userNameBtn setTitle:@"" forState:UIControlStateNormal];
            
        }
        else
            
        {
           NSString * commetUserName =[NSString stringWithFormat:@"%@",[[commentArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"]];
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
            {
                

    commetUserName = [commetUserName   stringByReplacingOccurrencesOfString:@"_" withString:@" "];            }
            
            UIButton *btn = (UIButton*) [cell viewWithTag:-4];
 commetUserName = [commetUserName  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
             commetUserName = [commetUserName  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            [btn setTitle:commetUserName forState:UIControlStateNormal];
           // btn.titleLabel.text = commetUserName;
        }
        if ([[[commentArray objectAtIndex:indexPath.row] objectForKey:@"commenttext"] isEqual:[NSNull null]])
        {
            cell.textLabel.text = @"";
            
        }
        else
            
        {
            NSString * commentTextStr  =[NSString stringWithFormat:@"%@",[[commentArray objectAtIndex:indexPath.row] objectForKey:@"commenttext"]];
            cell.textLabel.numberOfLines = 0;
            commentTextStr = [commentTextStr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            NSData *data = [commentTextStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *commentValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            if (commentValue == nil||[commentValue isEqualToString:@""])
            {
                cell.textLabel.text = commentTextStr ;
            }
            else
            {
                cell.textLabel.text = commentValue ;
            }

           
             //labelFrame = CommentLbl.frame;
          // labelFrame.size = [commentTextStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17] constrainedToSize:CGSizeMake(CommentLbl.frame.size.width, 999) lineBreakMode:NSLineBreakByWordWrapping];
           // CommentLbl.frame = labelFrame;
           // [CommentLbl sizeToFit];

        }
    }
         return cell;

    }
    else if( tableView == searchTableView)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];

        }
        if (searchResultsArray.count>0)
        {
       
        cell.textLabel.text = [searchResultsArray objectAtIndex:indexPath.row];
        }
            return cell;
    }
  
    
      return NO;
}

- (void)userBtnAction:(UIButton*)sender
{
    commentview.hidden = YES;
    imageScrollView.hidden = NO;
    CGRect buttonFrameInTableView = [sender convertRect:userNameBtn.bounds toView:commentTableView];
    NSIndexPath *indexPath = [commentTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    
    rowNumber += indexPath.row;
    
    [userNameBtn setTag:rowNumber];
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    ProfileViewController.hubUserIDStr = [[commentArray objectAtIndex:rowNumber] objectForKey:@"userID"];
    [self.navigationController pushViewController:ProfileViewController animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str ;
    if (commentArray.count>0) {
 
   str  = [NSString stringWithFormat:@"%@",[[commentArray objectAtIndex:indexPath.row] objectForKey:@"commenttext"]];
    }
    CGSize size = [str sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17] constrainedToSize:CGSizeMake(280, 2999) lineBreakMode:NSLineBreakByCharWrapping];
    return size.height + 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == searchTableView) {
        searchTableView.hidden = YES;
        NSString *textViewStr = growingtextView.text;
        
        NSRange equalRange = [growingtextView.text rangeOfString:@"@"];
        if (equalRange.location != NSNotFound)
        {
            NSString *result = [growingtextView.text substringFromIndex:equalRange.location+equalRange.length];
            textViewStr = [textViewStr stringByReplacingOccurrencesOfString:result
                                                 withString:[searchResultsArray objectAtIndex:indexPath.row]];
            growingtextView.text = textViewStr;
        }
        
       
           }
    else
    {
        [growingtextView resignFirstResponder];
       
    }
    

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (PushDataDic)
    {
         return YES;
    }
    if ([friendUserId_Image isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        return YES;
    }
    else if(hubrallyImagesDic)
      
    {
        if ([hubrallyImagesDic  objectForKey:@"userID"])
        {
            friendUserId_Image = [hubrallyImagesDic  objectForKey:@"userID"];
        }
        else if([hubrallyImagesDic  objectForKey:@"sentto"])
        {
            friendUserId_Image = [hubrallyImagesDic  objectForKey:@"sentto"];
        }
        

        if ([friendUserId_Image isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        return YES;
    }
    }
    else
    {
        return NO;
    }
    return  NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
   
   NSString * commentId_str = [NSString stringWithFormat:@"%@",[[commentArray objectAtIndex:indexPath.row] objectForKey:@"commentid"]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/delete_image_comment.php?userid=%@&commentid=%@",[defaults objectForKey:@"id"],commentId_str];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecievedEdit_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    [commentArray removeObjectAtIndex:indexPath.row];
    // [packTableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [commentTableView reloadData];
    
}

-(void)JSONRecievedEdit_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //   NSLog(@"response12%@",response);
    
    //packArr = [dict objectForKey:@"userfriends"];
   
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_NotAuthorisedToDeleteThisComment:
        {
            
            [[[UIAlertView alloc]initWithTitle:@"Whoops" message:@"You are not authorised to delete comment" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];

            //
           
            break;
        }
            
        case APIResponseStatus_CommentDeletedSuccessfully:
        {
            
            [self callWebservice_Comment];
            [commentTableView reloadData];
            break;
        }
        default:
            
            break;
            
    }
  
    
}


- (IBAction)deleteImageAction:(id)sender
{
    CGPoint scrollViewContent=   imageScrollView.contentOffset;
    CGFloat contentOffsetX =  scrollViewContent.x;
    imageTag = contentOffsetX /320 ;
    selectedImageId =[[albumDataArray objectAtIndex:imageTag]objectForKey:@"imageid"];
    
    UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Delete" message:@"Are you want to delete this photo?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    alert.tag = -3;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == -3)
    {
        if (buttonIndex == 0)
        {
            [self callWebSercvice_DeletRallyImage];
        }
        else
        {
            
        }
    }
}
@end
