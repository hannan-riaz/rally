//
//  RCalenderViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <TapkuLibrary/TapkuLibrary.h>
//#import "TKCalendarMonthView.h"
#import "CalendarKit.h"
#import "CKCalendarViewController.h"
#import "CKCalendarView.h"
#import "RMapViewController.h"


@interface RCalenderViewController : CKCalendarViewController<CKCalendarViewDelegate, CKCalendarViewDataSource>


@property(nonatomic,strong)CKCalendarEvent * aCKCalendarEvent;
@property(nonatomic,strong)CKCalendarView *calendarView2;
@property(nonatomic,strong)CKCalendarViewController *calendarView1;

//@property (nonatomic,strong) NSMutableArray *arryCalender;
//@property (nonatomic,strong) NSMutableDictionary *dataDictionary;
//
//- (void) generateRandomDataForStartDate:(NSDate*)start endDate:(NSDate*)end;
@end
