//
//  RSearchViewController.m
//  Rally
//
//  Created by Ambika on 5/22/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RSearchViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RProfileViewController.h"
#import "RJoinRallyViewController.h"
#import "AsyncImageView.h"
@interface RSearchViewController ()

{
    NSMutableArray * ralyNameArray;
    NSMutableArray * searchResultArray1;
    NSMutableArray * userNameArray;
    NSMutableArray * nameArray;
    NSMutableArray * serachArray;
    NSDictionary * rallyDic;
     NSDictionary * nameDic;
    NSMutableArray * name;
    NSMutableArray * multipleArray;
    NSString *searchString;
    int r;
    int j;
    MBProgressHUD * ProgressHUD;
    AsyncImageView *userImagView;
    UIImageView * rallyLogoImagView;
   

    
}
@end

@implementation RSearchViewController
@synthesize searchTableview,searchResultArray,serachBar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

   
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        
        //serachBar .frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, serachBar.frame.size.width, serachBar.frame.size.height);
        // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);

    }
       else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {

//        serachBar .frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, serachBar.frame.size.width, serachBar.frame.size.height);
//             searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
        }else
        {
//            serachBar .frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-44, serachBar.frame.size.width, serachBar.frame.size.height);
//            searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
//
        }
        

    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
           // self.searchRView.frame = CGRectMake(0, 0,  self.searchRView.frame.size.width,  self.searchRView.frame.size.height);
       // serachBar.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, serachBar.frame.size.width, serachBar.frame.size.height);
      // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
        }
        else
        {
           // serachBar.frame = CGRectMake(0,self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, serachBar.frame.size.width, serachBar.frame.size.height);
           // searchTableview.frame = CGRectMake(0, serachBar.frame.origin.y+serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
        }

        
    }

   // searchTableview .frame = CGRectMake(0, self.serachBar.frame.origin.y+self.serachBar.frame.size.height, searchTableview.frame.size.width, searchTableview.frame.size.height);
    
    ralyNameArray = [[NSMutableArray alloc] init];
    userNameArray = [[NSMutableArray alloc] init];
    serachArray = [[NSMutableArray alloc] init];
    searchResultArray= [[NSMutableArray alloc] init];
    searchResultArray1 = [[NSMutableArray alloc] init];
    nameArray = [[NSMutableArray alloc] init];
    name = [[NSMutableArray alloc] init];
    multipleArray = [[NSMutableArray alloc] init];
    rallyDic = [[NSDictionary alloc]init];
    nameDic = [[NSDictionary alloc]init];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    //searchTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth |
  //  UIViewAutoresizingFlexibleHeight;
 // [searchTableview setContentInset:UIEdgeInsetsZero];
   // [self handleSearch:serachBar];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationItem.title = NSLocalizedString(@"Search", @"Search");
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];

    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Search";
    self.navigationItem.titleView = lblTitle;
    
        UIImage *serchImage = [UIImage imageNamed:@"greenSearch.png"];
    [self.serachBar setImage:serchImage forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    //self.serachBar.searchTextPositionAdjustment = UIOffsetMake(-19, 0);
}

-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"rallies"] != [NSNull null])
    {
        ralyNameArray = [dict objectForKey:@"rallies"];
    }
    if ([dict objectForKey:@"users"] != [NSNull null])
    {
        userNameArray = [dict objectForKey:@"users"];
    }
    
    searchResultArray = [[ralyNameArray arrayByAddingObjectsFromArray:userNameArray] mutableCopy];
   
  //  twoArray = [NSMutableArray arrayWithArrays:ralyNameArray,userNameArray, nil];
       [searchTableview reloadData];
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_SearchFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong with search. Please try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

            break;
        }
        case  APIResponseStatus_NameRequired:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
            break;
        }
        case  APIResponseStatus_SearchSuccessfull:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally is not Start....please try  again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
//            
            
            break;
        }
   
        default:
            
            break;
            
    }
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - ------------ Table View Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
      return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    //get the count from the array
    if ([tableView isEqual:searchTableview])
    {
        numberOfRows = searchResultArray.count;
    }
    if(numberOfRows == 0 && [searchString length] > 0)
    {
        numberOfRows = 1;
    }
    return numberOfRows;
   
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell *cell = nil;
    
//    if ([tableView isEqual:searchTableview])
//    {
    
        static NSString *TableViewCellIdentifier = @"Cell";
       static NSString *TableViewCellIdentifier1 = @"Cell1";
    
        if (cell == nil)
        {
            if ([[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"userNAME"])
            {
                cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
              cell = [[UITableViewCell alloc]
                          initWithStyle:UITableViewCellStyleDefault
                          reuseIdentifier:TableViewCellIdentifier];
                
            if (iOSVersion>=8.0)
            {
                cell.indentationLevel = 3.4; //Added by swati 27Oct
            }
            else
            {
                cell.indentationLevel = 4.5;
            }

            
            userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(7,8,30 ,30 )];
            userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
            userImagView.clipsToBounds = YES;
            userImagView.image = [UIImage imageNamed:@""];
            userImagView.tag = 101;
            userImagView.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:userImagView];
            }
            else if ([[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"rallyNAME"])
            {
                cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier1];
                cell = [[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault
                        reuseIdentifier:TableViewCellIdentifier1];
               
                if (iOSVersion>=8.0)
                {
                    cell.indentationLevel = 3.4; //Added by swati 27Oct
                }
                else
                {
                    cell.indentationLevel = 4.5;
                }


            rallyLogoImagView = [[UIImageView alloc]initWithFrame:CGRectMake(7,10,30 ,30 )];
            rallyLogoImagView.tag = 102;
            rallyLogoImagView.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:rallyLogoImagView];
            }
        }
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        cell.textLabel.numberOfLines = 0;
        if(searchResultArray.count > 0)
        {
        if ([[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"rallyNAME"])
            {
                cell.tag =1;
                NSString *  chat_Str = [NSString stringWithFormat:@"%@",[[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"rallyNAME"]];
                chat_Str = [chat_Str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                chat_Str = [chat_Str  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
                NSData *data = [chat_Str dataUsingEncoding:NSUTF8StringEncoding];
                NSString *chatDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (chatDataEncode == nil||[chatDataEncode isEqualToString:@""])
                {
                   cell.textLabel.text = chat_Str;
                }
                else
                {
                    cell.textLabel.text = chatDataEncode;
                }

                
                UIImageView * rallyImaView = (AsyncImageView *)[cell viewWithTag:102];
                rallyImaView.image = [UIImage imageNamed:@"startRally.png"];
               
            }
            else
            {
                
                NSString *  nameStr2 = [NSString stringWithFormat:@"%@",[[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"userNAME"]];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
                {
                    
                 nameStr2 = [nameStr2   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                }
                 nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"  " withString:@" "];

               cell.textLabel.text = nameStr2;
               cell.tag =2;
               
                if ([[[searchResultArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
                {
                    userImagView.image =[UIImage imageNamed:@""];
                }
                else
                { AsyncImageView * imaview = (AsyncImageView *)[cell viewWithTag:101];
                    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[searchResultArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"]];
                    NSURL *imageURL = [NSURL URLWithString:imgStr];
                    // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
                    imaview.imageURL = imageURL;
                    
                }


            }
            
        }
        //display message to user
        else
        {
            cell.textLabel.text = @"No Results found, try again!";
        }
        
        
    
    return cell;
    
   }

- (UITableViewCell*)tableView:(UITableView*)tableView  didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(searchResultArray.count > 0)
    {
        NSString *useridstr = [NSString stringWithFormat:@"%ld",(long)[[[searchResultArray objectAtIndex:indexPath.row]objectForKey:@"userID"]integerValue]] ;
        NSString *rallyidstr = [NSString stringWithFormat:@"%ld",(long)[[[searchResultArray objectAtIndex:indexPath.row]   objectForKey:@"rallyID"]integerValue]] ;
        NSString *rallyPrivateStr = [NSString stringWithFormat:@"%ld",(long)[[[searchResultArray objectAtIndex:indexPath.row]   objectForKey:@"rallyPRIVATE"]integerValue]] ;

    UITableViewCell *cell =[self tableView:self.searchTableview cellForRowAtIndexPath:indexPath];

        if (cell.tag == 1 )
        {
            RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
            JoinRallyViewController.rallyid =rallyidstr;
            JoinRallyViewController.rallyPrivateFromSearch =rallyPrivateStr;
            [self.navigationController pushViewController:JoinRallyViewController animated:NO];
        }
        else
        {
            RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
            ProfileViewController.searchuserid =useridstr;
            ProfileViewController.serachView1 = YES;
            [self.navigationController pushViewController:ProfileViewController animated:NO];
        }
  
    }
       return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cell =[self tableView:self.searchTableview cellForRowAtIndexPath:indexPath];
    if (cell.tag == 1 )
    {
        
        NSString *cellTextStr = [NSString stringWithFormat:@"%@", [[searchResultArray objectAtIndex:indexPath.row] objectForKey:@"rallyNAME"]];
        cellTextStr = [cellTextStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        cellTextStr = [cellTextStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
        NSData *data1 = [cellTextStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *chatDataEncode1 = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
        if (chatDataEncode1 == nil||[chatDataEncode1 isEqualToString:@""])
        {
            
            CGSize size = [cellTextStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
            //        NSLog(@"%f",size.height);
            return size.height + 30;
        }
        else
        {
            
            CGSize size = [chatDataEncode1 sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
            //        NSLog(@"%f",size.height);
            return size.height + 30;
        }
        
        
    }
    
    else
    {
        NSString *cellTextStr = [NSString stringWithFormat:@"%@", [[searchResultArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"]];
        CGSize size = [cellTextStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
        //        NSLog(@"%f",size.height);
        return size.height + 30;
    }
    
}

#pragma mark - ------------ Searchbar Delegate Methods ------------

//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];

   // [self handleSearch:searchBar];
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
  //  [self handleSearch:searchBar];
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
      if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    if (serachBar.text)
    {
        NSString*serachStr = [NSString stringWithFormat:@"%@",serachBar.text];
        serachStr = [serachStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        serachStr = [serachStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];

        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/search.php?name=%@",serachStr];

    }
    else if (text)
    {
        NSString*textStr = [NSString stringWithFormat:@"%@",text];
        textStr = [textStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        textStr = [textStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/search.php?name=%@",textStr];
    }
    
    //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    return  YES;
}
//do our search on the remote server using HTTP request
- (void)handleSearch:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
   /*// NSLog(@"User searched for %@", searchBar.text);
    searchString = searchBar.text;
    [searchBar resignFirstResponder];
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/search.php?name=%@",searchString];
    
    //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];*/
 }
//user tapped on the cancel button
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    
    [searchBar resignFirstResponder];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
