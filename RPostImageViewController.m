//
//  RPostImageViewController.m
//  Rally
//
//  Created by Ambika on 6/14/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RPostImageViewController.h"

@interface RPostImageViewController ()

@end

@implementation RPostImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
