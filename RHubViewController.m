 //
//  RHubViewController.m
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RHubViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RProfileViewController.h"
#import "RJoinRallyViewController.h"
#import "AsyncImageView.h"
#import "RImageViewController.h"
#import "RAlbumViewController.h"
#import "RFullImageViewController.h"
static NSString *TableViewCellIdentifier;
static NSString *TableViewCellIdentifier1;
@interface RHubViewController ()

{
    UIButton *joinBtn;
    UIButton *ImageBtn;
    UIButton *rallyBtn;
    UIButton * userImageBtn1;
    UIButton * userImage1Btn1;
    AsyncImageView * userImage;
    UIButton *userNameBtn;
    UIButton *addressBtn;
    UILabel *startRallyLbl;
    UILabel *timeLbl;
    AsyncImageView * userImage1;
    UIButton *userNameBtn1;
    UIButton *addressBtn1;
    UILabel *startRallyLbl1;
    UILabel *timeLbl1;
    AsyncImageView * imageView1;
    UILabel *likesLbl;
    UILabel *joinGoingLbl;
    BOOL user;
    BOOL UploadPhoto;
    UIScrollView * photoScrolview;
    NSMutableArray *photoArray1;
    int x;
    NSString *  rallyIdStrFor_likes;
    UIView *  cellView;
    UIView *  PhotoView;
    NSMutableArray * sorteddataArray;
    NSDate  * componentDateStr;
    NSMutableArray * urlimageArray;
    UIRefreshControl *refreshControl_MyPack;
    UIRefreshControl *refreshControl_Around;
    NSString * rallyIdStr ;
    UIAlertView * alert1;
     UIAlertView * alert2;
    UIAlertView * alert3;
    NSString * privateRallyid;
    NSInteger  buttonIndex1;
    UILabel * getLbl;
    NSTimer *timmerRepeatFor_hub;
    int showload;
}

@property(strong,nonatomic)MBProgressHUD *ProgressHUD;
@end

@implementation RHubViewController
@synthesize hubTableview,ProgressHUD,pageControllerObj,scrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.everyOneTableView.hidden = YES;
    [self.aroundyouLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    [joinBtn setTitleColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0] forState:UIControlStateNormal];
    [self.myPackLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:18.0]];
    [self.aroundyouLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:18.0]];
    
    
    lastOffset_x = 0;
    
    [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"OnHubPage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
//    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"RadiusDistance"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
    
  //  self.automaticallyAdjustsScrollViewInsets=TRUE;

    
    locationManager = [[CLLocationManager alloc] init];
    
    if ( [CLLocationManager locationServicesEnabled] )
    {
        locationManager.delegate = self;
        //        self.locationManager.distanceFilter = kdistance;
        if(IS_OS_8_OR_LATER)
        {
            [locationManager requestAlwaysAuthorization];
        }
        [locationManager startUpdatingLocation];
        
        CLLocation *location = locationManager.location;
        Currlat = location.coordinate.latitude;
        Currlon = location.coordinate.longitude;
        
        CLLocationCoordinate2D loc;
        loc.latitude =Currlat;
        loc.longitude=Currlon;
    }

    refreshControl_MyPack = [[UIRefreshControl alloc]
                                        init];
    refreshControl_MyPack.tintColor = [UIColor grayColor];
   
    [refreshControl_MyPack addTarget:self action:@selector(updateTable) forControlEvents:UIControlEventValueChanged];
    //hubTableview = refreshControl;
   
    [hubTableview addSubview:refreshControl_MyPack];
    
   
    refreshControl_Around = [[UIRefreshControl alloc]
                             init];
    refreshControl_Around.tintColor = [UIColor grayColor];
    
    [refreshControl_Around addTarget:self action:@selector(updateTable1) forControlEvents:UIControlEventValueChanged];

    
    [self.everyOneTableView addSubview:refreshControl_Around];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    urlimageArray = [[NSMutableArray alloc]init];
     // Do any additional setup after loading the view from its nib.
  /*  self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];*/
    
    
   
    //CGPoint origin = CGPointMake( 320/2, 44/2 );

    
   
    
    
    photoArray1 = [[NSMutableArray alloc]init];
   // photoArray1 = @[@"image_two.png",@"image_three.png",@"image_four.png"];
    
   sorteddataArray = [[NSMutableArray alloc]init];
    }
- (void)updateTable
{
    //sorteddataArray = nil;
    [self callWebService];
    [hubTableview reloadData];
    [refreshControl_MyPack endRefreshing];

   //[self performSelector:@selector(stopRefresh) withObject:nil afterDelay:2.5];
}
- (void)updateTable1
{
    //sorteddataArray = nil;
    [self callWebService_aroundYou];
    [self.everyOneTableView reloadData];
    [refreshControl_Around endRefreshing];
}
-(void)stopRefresh
{

    
}

-(void)viewWillAppear:(BOOL)animated
{
    showload =0;
    
    //pageVal=0;
    
    if (!IS_IPHONE_5)
    {
        [self.hubTableview setFrame:CGRectMake(0,0,320,420)];
        [self.everyOneTableView setFrame:CGRectMake(320,0,320,420)];
    }
    self.scrollView.contentSize=CGSizeMake(640,568);
    
    self.pageControllerObj = [[UIPageControl alloc] initWithFrame:CGRectMake(160,28,0,0)];
    
    self.pageControllerObj.pageIndicatorTintColor=[UIColor grayColor];
    
    self.pageControllerObj.currentPageIndicatorTintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    self.pageControllerObj.enabled=TRUE;
    
    [self.pageControllerObj setNumberOfPages:2];
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    //    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"The Hub";
    self.navigationItem.titleView = lblTitle;
    
//    [self.navigationController.navigationBar addSubview:self.pageControllerObj];
    
    self.navigationItem.hidesBackButton=TRUE;
    
    customView=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,44)];
    
    customView.backgroundColor=[UIColor clearColor];
   
    labelforNavigationTitle = [[UILabel alloc] initWithFrame:CGRectMake(40,-10,237,40)];
    
    labelforNavigationTitle.backgroundColor = [UIColor clearColor];
    
    labelforNavigationTitle.adjustsFontSizeToFitWidth=TRUE;
    
    labelforNavigationTitle.numberOfLines = 0;
    
    labelforNavigationTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    labelforNavigationTitle.font = [UIFont boldSystemFontOfSize:17.0];
    
    
    labelforNavigationTitle.textAlignment = NSTextAlignmentCenter;
    
//    [customView addSubview:labelforNavigationTitle];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn addTarget:self action:@selector(popToView)
     
      forControlEvents:UIControlEventTouchUpInside];
    
    leftBtn.frame = CGRectMake(14,15,22,18);
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"menu-new.png"]  forState:UIControlStateNormal];
    
    [customView addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [rightBtn addTarget:self action:@selector(addSettingView)
     
       forControlEvents:UIControlEventTouchUpInside];
    
    rightBtn.frame = CGRectMake(275,13,28,22);
    
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"chat.png"]  forState:UIControlStateNormal];
    
    [customView addSubview:rightBtn];
    
    
    
    [self.navigationController.navigationBar addSubview:customView];
    
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"OnHubPage"]==0)
    {
        [self callWebService];
        labelforNavigationTitle.text =@"Your Pack";
        self.pageControllerObj.currentPage=0;
        rightBtn.hidden=NO;
    }
    else
    {
        [self callWebService_aroundYou];
         labelforNavigationTitle.text =@"Around You";
         self.pageControllerObj.currentPage=1;
        rightBtn.hidden=NO;
    }
   
   

    
//    UILabel *lblTitle = [[UILabel alloc] init];
//    lblTitle.frame = CGRectMake(0,0,230,40) ;
//    lblTitle.adjustsFontSizeToFitWidth=TRUE;
//    lblTitle.text =@"The Hub";
//    lblTitle.textAlignment = NSTextAlignmentCenter;
//    lblTitle.numberOfLines = 0;
//    lblTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//    lblTitle.font = [UIFont boldSystemFontOfSize:17.0];
//    
//    self.navigationItem.titleView = lblTitle;
    
   // self.navigationItem.title = @"The Hub";
   // self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16.0],NSFontAttributeName , [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] ,NSForegroundColorAttributeName,nil];

    hubTableview.backgroundColor = [UIColor clearColor] ;
    
    self.everyOneTableView.backgroundColor = [UIColor clearColor] ;
    
    timmerRepeatFor_hub =  [NSTimer scheduledTimerWithTimeInterval:5  target:self selector:@selector(timerCallFor_hub:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timmerRepeatFor_hub forMode:NSRunLoopCommonModes];
   //[self timerFired:[NSTimer scheduledTimerWithTimeInterval:0.1 invocation:nil repeats:YES]];
    

    
}
-(void)addSettingView
{
    RAppDelegate *appDelegate=(RAppDelegate *)[[UIApplication sharedApplication]delegate];
    settingView=[[UIView alloc]init];
    tempView=[[UIView alloc]init];
    if (IS_IPHONE_5)
    {
        settingView.frame=CGRectMake(0,0,320,568);
        tempView.frame=CGRectMake(0,0,320,568);
    }
    else
    {
        settingView.frame=CGRectMake(0,0,320,480);
        tempView.frame=CGRectMake(0,0,320,480);
    }
    tempView.backgroundColor=[UIColor clearColor];
    
    settingView.backgroundColor=[UIColor lightGrayColor];
    settingView.alpha=0.8;
    [[appDelegate window] addSubview:settingView];
    
    [[appDelegate window]addSubview:tempView];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [tempView addGestureRecognizer:tap];
    
   
    
    UIImageView *radiusPopUp=[[UIImageView alloc]initWithFrame:CGRectMake(49,210,225, 105)];
    radiusPopUp.image=[UIImage imageNamed:@"search-radius.png"];
    radiusPopUp.userInteractionEnabled=TRUE;
//    radiusPopUp.opaque=TRUE;
//    radiusPopUp.alpha=1.0f;
  //  radiusPopUp.layer.cornerRadius=10.0f;
    [tempView addSubview:radiusPopUp];
    
//    [self.view bringSubviewToFront:radiusPopUp];
    
    CGRect frame = CGRectMake(10.0, 70.0, 200.0, 10.0);
    UISlider *slider = [[UISlider alloc] initWithFrame:frame];
    [slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
   // [slider setThumbImage:[UIImage imageNamed:@"scroller.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackTintColor:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0]];
    [slider setMaximumTrackTintColor:[UIColor grayColor]];
    slider.minimumValue = 0.0;
    slider.maximumValue = 50.0;
    slider.continuous = YES;
//    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//    slider.transform = transform;
    
    showRadiusLbl=[[UILabel alloc]initWithFrame:CGRectMake(170,14,80,20)];
    showRadiusLbl.textColor=[UIColor darkGrayColor];
    //showRadiusLbl.text=@"0mi.";
    
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"RadiusDistance"])
    {
    [slider setValue:0.0 animated:YES];
    showRadiusLbl.text=@"0mi.";
    }
    else
    {
    NSString *str=[[NSUserDefaults standardUserDefaults]valueForKey:@"RadiusDistance"];
    [slider setValue:[str floatValue] animated:YES];
    showRadiusLbl.text=[NSString stringWithFormat:@"%@mi.",str];
    }
    
    [radiusPopUp addSubview:slider];
    [radiusPopUp addSubview:showRadiusLbl];
    

}
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.view==tempView || recognizer.view==settingView)
    {
     showload=0;
    [self callWebService_aroundYou];
    [tempView removeFromSuperview];
    [settingView removeFromSuperview];
    }
    
}
-(void)sliderAction:(id)sender
{
    UISlider *slider = (UISlider*)sender;
    float value = slider.value;
    //NSLog(@"Value %f",value);
    showRadiusLbl.text=[NSString stringWithFormat:@"%dmi.",(int)value];
    strRadiusVal=[NSString stringWithFormat:@"%d",(int)value];
    
    [[NSUserDefaults standardUserDefaults]setValue:strRadiusVal forKey:@"RadiusDistance"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //-- Do further actions
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)timerCallFor_hub:(NSTimer *)timer
{
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"OnHubPage"]==0)
    {
    [self callWebService];
    }
    else
    {
    [self callWebService_aroundYou];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.pageControllerObj removeFromSuperview];
    [customView removeFromSuperview];
}
-(void)viewDidDisappear:(BOOL)animated
{
    cellView = nil;
    userImage = nil;
    userNameBtn = nil;
    startRallyLbl = nil;
    timeLbl = nil;
    likesLbl = nil;
    joinGoingLbl= nil;
    addressBtn = nil;
    rallyBtn = nil;
    joinBtn = nil;
    PhotoView = nil;
    userImage1 = nil;
    
    startRallyLbl1  = nil;
    timeLbl1 = nil;
    addressBtn1 = nil;
    photoScrolview = nil;
    imageView1 = nil;
    ImageBtn = nil;

    [timmerRepeatFor_hub invalidate];
    timmerRepeatFor_hub = nil;
}
-(void)callWebService
{
    if (showload == 0)
    {
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    }
   // NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/hub.php?userid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
     NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/hub.php?userid=%@&selected_hub=my",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    operation.responseSerializer = [AFJSONResponseSerializer serializer];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self JSONRecieved:responseObject];
        if (showload == 0)
        {

        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
            showload = 1;
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [timmerRepeatFor_hub invalidate];
        timmerRepeatFor_hub = nil;
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        kRAlert(@"Whoops",error.localizedDescription);
    }];

    [[NSOperationQueue mainQueue] addOperation:operation];

}

-(void)callWebService_aroundYou
{
    NSString * urlStr;
    if (showload == 0)
    {
        
        if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
     }
    // NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/hub.php?userid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
    
     
    if(Currlat==0.0&&Currlon==0.0)
    {
       // [locationManager startUpdatingLocation];
       
//        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Unable to find your current location.Try Again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//       [alert show];
    }
    else
    {
        if ([strRadiusVal length]==0)
        {
            strRadiusVal=@"50";
        }
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/hub.php?userid=%@&selected_hub=all&distance_filter=%@&user_latitude=%@&user_longitude=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],strRadiusVal,[NSString stringWithFormat:@"%f",Currlat],[NSString stringWithFormat:@"%f",Currlon]];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             if (showload == 0)
             {
                 
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 showload = 1;
             }
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [timmerRepeatFor_hub invalidate];
             timmerRepeatFor_hub = nil;
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops",error.localizedDescription);
         }];
        
        [[NSOperationQueue mainQueue] addOperation:operation];
    }
}


-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *Dict = response;
    //NSLog(@"response12%@",response);
    sorteddataArray = nil;
    if ([Dict objectForKey:@"sorteddata"]!= [NSNull null])
    {
        sorteddataArray = [Dict objectForKey:@"sorteddata"];
        [hubTableview reloadData];
        [self.everyOneTableView reloadData];
    }
    
//    for (int i =0; i<=[ralliesEventArr count]; i++)
//    {
//        //calendar.eventStr=[[ralliesEventArr objectAtIndex:i]objectForKey:@"rallyNAME"];
//    }
    
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_HubViewFailed: //changed alert message_7 Nov
        {
           [timmerRepeatFor_hub invalidate];
            timmerRepeatFor_hub = nil;
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"No Rallies have been started yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_HubViewSuccessfull:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Hub View Successfull!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }

        default:
            
            break;
    }
}


#pragma mark - ------------ Table View Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sorteddataArray count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell *cell = nil;
    
    //refernceTableView=tableView;
    
    
    static NSString *TableViewCellIdentifier = @"Cell";
  //  static NSString *TableViewCellIdentifier1 = @"Cell1";
    // cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    
//    if (tableView==hubTableview)
//    {
    
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];/// change size as you need.
    separatorLineView.backgroundColor = [UIColor whiteColor];// you can also put image here
    
        if (cell == nil)
        {
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
                
            {
                cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
                
                cellView = [[UIView alloc]initWithFrame: CGRectMake(0,0,320,150)];
                cellView.backgroundColor = [UIColor clearColor];
                cellView.userInteractionEnabled = YES;
                
                UIImageView *backImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 304, 142)];
                backImg.image = [UIImage imageNamed:@"join_butn_bkgd.png"];
                [cellView addSubview:backImg];
                
                userImage = [[AsyncImageView alloc]init];
                //userImage.image = [UIImage imageNamed:@""];
                userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
                userImage.backgroundColor = [UIColor clearColor];
                userImage.tag = -1;
                userImage.frame = CGRectMake(10, 22, 50, 50);
                
                userImage.layer.cornerRadius = userImage.frame.size.height /2;
                userImage.layer.masksToBounds = YES;
                userImage.layer.borderWidth = 0;
                
                [cellView addSubview:userImage];
                
                userNameBtn =[[UIButton alloc]initWithFrame:CGRectMake(32, 53, 200, 40)];
                userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                 userNameBtn.frame = CGRectMake(55,2,0,30);
                userNameBtn.tag =-2;
                if (tableView==hubTableview)
                {
                     [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
                    
                }
                else
                {
                   [userNameBtn addTarget:self  action:@selector(userBtnAction1:)  forControlEvents:UIControlEventTouchUpInside];
                   // userNameBtn.tag =2000;
                }
               
               
                // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
//                userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//                userNameBtn.frame = CGRectMake(25, 50, 200, 20);
//                userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                userNameBtn.titleLabel.font = [UIFont fontWithName:@"Raleway-SemiBold" size:18.0f];
                [userNameBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
                // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
                [cellView addSubview:userNameBtn];
                
                startRallyLbl = [[UILabel alloc]init];
                startRallyLbl.frame =CGRectMake(71,37,130,30);
                
                startRallyLbl.textColor = [UIColor blackColor];
                startRallyLbl.tag = -3;
                startRallyLbl.font = [UIFont fontWithName:@"Raleway-Regular" size:13.0f];
                [cellView addSubview:startRallyLbl];
                
                timeLbl = [[UILabel alloc]init];
                timeLbl.frame = CGRectMake(5, 106, 120, 40);
                timeLbl.textColor = [UIColor darkGrayColor];
                timeLbl.font = [UIFont fontWithName:@"Raleway-Regular" size:10.0f];
                timeLbl.tag = 1000+indexPath.row;
                //timeLbl.text = @"22 seconds ago";
                [cellView addSubview:timeLbl];
                
                likesLbl = [[UILabel alloc]init];
                likesLbl.frame = CGRectMake(55,60, 30, 40);
                likesLbl.textColor = [UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1.0];
                likesLbl.tag = indexPath.row+2000;
                likesLbl.textAlignment = NSTextAlignmentLeft;
                likesLbl.adjustsFontSizeToFitWidth = YES;
                likesLbl.adjustsLetterSpacingToFitWidth = YES;
                likesLbl.font = [UIFont fontWithName:@"Raleway-SemiBold" size:12.0f];
                //likesLbl.text = @"Like";
                //  [cellView addSubview:likesLbl];
                
                
                
                joinGoingLbl = [[UILabel alloc]init];
                joinGoingLbl.frame = CGRectMake(261, 106, 60, 40);
                joinGoingLbl.textColor = [UIColor darkGrayColor];
                joinGoingLbl.font = [UIFont fontWithName:@"Raleway-Regular" size:10.0f];
                joinGoingLbl.tag = -4;
                joinGoingLbl.adjustsFontSizeToFitWidth = YES;
                joinGoingLbl.adjustsLetterSpacingToFitWidth = YES;
                //joinGoingLbl.text = @"45 going";
                //    [cellView addSubview:joinGoingLbl];
                
                //addressBtn = [[UIButton alloc]init];
                addressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                //    addressBtn.backgroundColor=[UIColor redColor];
                //    addressBtn.frame = CGRectMake(55,20,265,30);
                addressBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                addressBtn.titleLabel.numberOfLines =2;
                addressBtn.frame = CGRectMake(52, 202, 200, 20);
                addressBtn.titleLabel.font = [UIFont fontWithName:@"Raleway-SemiBold" size:20.0f];
                [addressBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                 addressBtn.tag = -5;
                if (tableView==hubTableview)
                {
                      [addressBtn addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    [addressBtn addTarget:self  action:@selector(addressAction1:)  forControlEvents:UIControlEventTouchUpInside];
                   // addressBtn.tag=5000;
                }
                
                addressBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
                [addressBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
                addressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//                [addressBtn setTitle:@"My Birthday Party" forState:UIControlStateNormal];
                [cellView addSubview:addressBtn];
                
                addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
                // addressBtn.frame = CGRectMake(55,20,290, 20);
              
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Raleway-SemiBold" size:20.0f];
                addressBtn1.frame = CGRectMake(30, 202, 200, 20);
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                addressBtn1.tag = -15;
                if (tableView==hubTableview)
                {
                      [addressBtn1 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                      [addressBtn1 addTarget:self  action:@selector(addressAction1:)  forControlEvents:UIControlEventTouchUpInside];
                    // addressBtn1.tag = 15000;
                }

               
                addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
                addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                //[cellView addSubview:addressBtn1];
                
                
                
                rallyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                //[rallyBtn setTitle:@"X" forState:UIControlStateNormal];
                // [rallyBtn setImage:[UIImage imageNamed:@"startRally.png"]  forState:UIControlStateNormal];
                rallyBtn.frame = CGRectMake(10, 60, 40, 40);
                
                rallyBtn.tag =indexPath.row+200;
                
                if (tableView==hubTableview)
                {
                    [rallyBtn addTarget:self  action:@selector(rallyLikesAction:)  forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    [rallyBtn addTarget:self  action:@selector(rallyLikesAction1:)  forControlEvents:UIControlEventTouchUpInside];
                }
                
                // [cellView addSubview:rallyBtn];
                
                joinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                joinBtn.frame = CGRectMake(60,105,190,27);
                [joinBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-SemiBold" size:16.0]];
                [joinBtn setBackgroundImage:[UIImage imageNamed:@"join_button-real.png"]  forState:UIControlStateNormal];
                [joinBtn setTitleColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0] forState:UIControlStateNormal];
                [joinBtn setTitle:@"JOIN" forState:UIControlStateNormal];
                joinBtn.tag = -7;
               
                if (tableView==hubTableview)
                {
                    [joinBtn addTarget:self action:@selector(addressAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    [joinBtn addTarget:self action:@selector(addressAction1:) forControlEvents:UIControlEventTouchUpInside];
                     //joinBtn.tag = 7000;
                }
                [joinBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                // button.backgroundColor= [UIColor lightGrayColor];
                // [cellView addSubview:joinBtn];
                
                
                userImageBtn1 =[[UIButton alloc]init];
                userImageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
                userImageBtn1.frame = CGRectMake(10, 10, 40, 40);
                userImageBtn1.tag = -8;
                if (tableView==hubTableview)
                {
                     [userImageBtn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                     [userImageBtn1 addTarget:self  action:@selector(userBtnAction1:)  forControlEvents:UIControlEventTouchUpInside];
                   //userImageBtn1.tag = 8000;
                }
                
               
                userImageBtn1.backgroundColor = [UIColor clearColor];
                [cellView addSubview:userImageBtn1];
                
                imageView1 = [[AsyncImageView alloc] init ];
                imageView1.frame= CGRectMake(10, 60, 70,80) ;
                imageView1.userInteractionEnabled = YES;
                imageView1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
                imageView1.tag = -9;
                imageView1.contentMode =  UIViewContentModeScaleToFill;
                //  [cellView addSubview:imageView1];
                ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                ImageBtn.frame = CGRectMake(imageView1.frame.origin.x,imageView1.frame.origin.y,imageView1.frame.size.width, imageView1.frame.size.height);
               
                
                 ImageBtn.tag = -10;
               
                if (tableView==hubTableview)
                {
                    [ImageBtn addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                      [ImageBtn addTarget:self  action:@selector(UploadPhotoAction1:)  forControlEvents:UIControlEventTouchUpInside];
                }
                // [imageView1 addSubview:ImageBtn];
                
                
                [cell.contentView  addSubview:cellView];
                
                
            }
            
            
            /*else if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
             {
             cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier1];
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier1];
             PhotoView = [[UIView alloc]initWithFrame: CGRectMake(0,0,320,110)];
             PhotoView.backgroundColor = [UIColor clearColor];
             PhotoView.userInteractionEnabled = YES;
             PhotoView.tag = 159;
             
             userImage1 = [[AsyncImageView alloc]init];
             userImage1.image = [UIImage imageNamed:@""];
             userImage1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
             userImage1.tag = 160;
             userImage1.backgroundColor = [UIColor clearColor];
             userImage1.frame = CGRectMake(10, 9, 40, 40);
             [PhotoView addSubview:userImage1];
             
             
             userNameBtn1 =[[UIButton alloc]init];
             userNameBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
             //userNameBtn1.frame = CGRectMake(55,0,80,30);
             userNameBtn1.tag = 161;
             [userNameBtn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
             // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
             userNameBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
             userNameBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
             userNameBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
             [userNameBtn1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
             //    userNameBtn1.titleLabel.adjustsLetterSpacingToFitWidth = YES;
             //    userNameBtn1.titleLabel.adjustsFontSizeToFitWidth = YES;
             [PhotoView addSubview:userNameBtn1];
             
             startRallyLbl1 = [[UILabel alloc]init];
             //startRallyLbl1.frame = CGRectMake(130, 0, 110, 30);
             startRallyLbl1.textColor = [UIColor darkGrayColor];
             startRallyLbl1.tag = 162;
             //    startRallyLbl1.adjustsLetterSpacingToFitWidth = YES;
             //    startRallyLbl1.adjustsFontSizeToFitWidth = YES;
             startRallyLbl1.textAlignment = NSTextAlignmentCenter;
             startRallyLbl1.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
             //startRallyLbl.text = @"started a Rally at";
             [PhotoView addSubview:startRallyLbl1];
             
             timeLbl1 = [[UILabel alloc]init];
             timeLbl1.frame = CGRectMake(230,105, 120, 30);
             timeLbl1.tag = 1000+indexPath.row;
             timeLbl1.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
             timeLbl1.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
             timeLbl1.text = @"22 seconds ago";
             [PhotoView addSubview:timeLbl1];
             
             addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
             addressBtn1.frame = CGRectMake(55,20,290, 20);
             [addressBtn1 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
             addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:11.0f];
             [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
             addressBtn1.tag = 164;
             addressBtn1.titleLabel.adjustsLetterSpacingToFitWidth = YES;
             addressBtn1.titleLabel.adjustsFontSizeToFitWidth = YES;
             addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
             addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
             [PhotoView addSubview:addressBtn1];
             
             
             
             photoScrolview = [[UIScrollView alloc]init];
             photoScrolview.frame = CGRectMake(10, 60,310, 80);
             photoScrolview.scrollEnabled = YES;
             photoScrolview.tag = 165;
             photoScrolview.showsVerticalScrollIndicator = NO;
             photoScrolview.showsHorizontalScrollIndicator=YES;
             photoScrolview.delegate = self;
             [PhotoView addSubview:photoScrolview];
             
             
             imageView1 = [[AsyncImageView alloc] init ];
             imageView1.frame= CGRectMake(0,0, 70,80) ;
             imageView1.userInteractionEnabled = YES;
             imageView1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
             imageView1.tag = 166;
             imageView1.contentMode =  UIViewContentModeScaleToFill;
             [photoScrolview addSubview:imageView1];
             
             self.automaticallyAdjustsScrollViewInsets = NO;
             photoScrolview.contentSize = CGSizeMake(x,photoScrolview.frame.size.height);
             // businessScrollview.contentSize = CGSizeMake(businessScrollview.contentSize.width,imageView1.frame.size.height);
             ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
             ImageBtn.frame = CGRectMake(imageView1.frame.origin.x,imageView1.frame.origin.y,imageView1.frame.size.width, imageView1.frame.size.height);
             [ImageBtn addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
             ImageBtn.tag = 167;
             [imageView1 addSubview:ImageBtn];
             
             userImage1Btn1 =[[UIButton alloc]init];
             userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
             userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
             userImage1Btn1.tag = 168;
             [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
             userImage1Btn1.backgroundColor = [UIColor clearColor];
             [PhotoView addSubview:userImage1Btn1];
             
             
             
             [cell.contentView  addSubview:PhotoView];
             }*/
        }
        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"])
        {
//            [cellView addSubview:likesLbl];
            [cellView addSubview:joinGoingLbl];
            [cellView addSubview:rallyBtn];
            [cellView addSubview:joinBtn];
            
            startRallyLbl.text = @"has started a Rally";
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
            {
                [addressBtn setTitle:@"" forState:UIControlStateNormal];
            }
            else
                
            {
                //  addressBtn.titleLabel.numberOfLines = 0;
                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                // addressBtn.backgroundColor = [UIColor redColor];
                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [addressBtn setTitle:@"MY BIRTHDAY BASH" forState:UIControlStateNormal];
//                [addressBtn1 setTitle:@"MY BIRTHDAY BASH" forState:UIControlStateNormal];
                [addressBtn setFrame:CGRectMake(114,77,200,20)];
                
                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
                
                
                
                //                 NSString * titleStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
                //
                //                 titleStr = [titleStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                //                 NSData *data = [titleStr dataUsingEncoding:NSUTF8StringEncoding];
                //                 NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                {
                    if (rallyNameStr.length>20)
                    {
                        //  NSString * range = [rallyNameStr substringWithRange:NSMakeRange(0,27)]  ;
                        NSRange rangeToSearch = NSMakeRange(0, 20); // get a range without the space character
                        
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 20;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location =20;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        
                        
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        [addressBtn setTitle:@"My Birthday Party" forState:UIControlStateNormal];
                        if (rallyNameStr.length>20)
                        {
                            
                            [cellView addSubview:addressBtn1];
                            
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            //                           addressBtn2.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
                                
                                //                                 CGRect currentFrame = addressBtn2.frame;
                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
                                //                                 CGSize expected = [range sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
                                //                                 currentFrame.size.height = expected.height;
                                //                                 addressBtn2.frame = currentFrame;
                                
                                
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                                //                                 CGRect currentFrame = addressBtn2.frame;
                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
                                //                                 CGSize expected = [rallyDataEncode sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
                                //                                 currentFrame.size.height = expected.height;
                                //                                 addressBtn2.frame = currentFrame;
                                
                            }
                            
                        }
                    }
                    else
                    {
                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                        [addressBtn setTitle:@"My Birthday Party" forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (rallyDataEncode.length>20)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 20); // get a range without the space character
                        
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 20;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 20;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
                        
                        if (rallyDataEncode.length>20)
                        {
                            [cellView addSubview:addressBtn1];
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
                            //                          addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
                                
                                
                                //                                 CGRect currentFrame = addressBtn2.frame;
                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
                                //                                 CGSize expected = [range sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
                                //                                currentFrame.size.height = expected.height;
                                //                                 addressBtn2.frame = currentFrame;
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 263, 10);
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                                //                                 CGRect currentFrame = addressBtn2.frame;
                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width,10);
                                //                                 CGSize expected = [rallyDataEncode sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
                                //                                 currentFrame.size.height = expected.height;
                                //                                 addressBtn2.frame = currentFrame;
                                //
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            
                        {
                            
                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                //                 if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                //                 {
                //                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                //
                //                     CGRect currentFrame = addressBtn.frame;
                //                     CGSize max = CGSizeMake(addressBtn.frame.size.width, 100);
                //                     CGSize expected = [rallyNameStr sizeWithFont:addressBtn.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn.titleLabel.lineBreakMode];
                //                     currentFrame.size.height = expected.height;
                //                     addressBtn.frame = currentFrame;
                //
                //                 }
                //                 else
                //                 {
                //
                //                     [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
                //
                //                     CGRect currentFrame = addressBtn.frame;
                //                     CGSize max = CGSizeMake(addressBtn.frame.size.width, 100);
                //                     CGSize expected = [rallyDataEncode sizeWithFont:addressBtn.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn.titleLabel.lineBreakMode];
                //                     currentFrame.size.height = expected.height;
                //                     addressBtn.frame = currentFrame;
                //
                //                 }
                //
                
                
            }
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
            {
                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
                
            {
                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
                {
                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                }
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
//                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
                [userNameBtn setFrame:CGRectMake(52,24,150,20)];
                
                
            }
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"] isEqual:[NSNull null]])
            {
                timeLbl.text = @"";
            }
            else
            {
                UILabel *timeLbel = (UILabel *) [cell viewWithTag:1000+indexPath.row];
                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"]];
                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
                NSDate *today1 = [NSDate date];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                // NSCalendar *c = [NSCalendar currentCalendar];
                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
                
                
                NSInteger years = [componentsDaysDiff year];
                NSInteger months = [componentsDaysDiff month];
                NSInteger days = [componentsDaysDiff day];
                NSInteger hours = [componentsDaysDiff hour];
                NSInteger minutes = [componentsDaysDiff minute];
                NSInteger seconds = [componentsDaysDiff second];
                
                
                
                
                if (days >0)
                {
                    if (days>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
                        
                        
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
                        
                        
                    }
                    if (days >30)
                    {
                        if (days>1)
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                            
                        }
                        else
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                            
                        }
                        
                    }
                }
                else if(months>12)
                {
                    if (months>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
                    }
                }
                else if (hours > 0)
                {
                    if (hours>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
                    }
                }
                else if(minutes >0)
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
                    }
                    
                    
                    
                }
                else
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
                    }
                }
            }
            
        }
        else if([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"])
        {
//            [cellView addSubview:likesLbl];
            [cellView addSubview:joinGoingLbl];
            [cellView addSubview:rallyBtn];
            [cellView addSubview:joinBtn];
            startRallyLbl.text = @"has joined a Rally at";
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
            {
                [addressBtn setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
                
            {
                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                // addressBtn.backgroundColor = [UIColor redColor];
                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
                [addressBtn setFrame:CGRectMake(stringsize.width+36,20,150,20)];
                
                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
                
                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                {
                    if (rallyNameStr.length>24)
                    {
                        
                        
                        NSRange rangeToSearch = NSMakeRange(0,24); // get a range without the space character
                        
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
                        
                        if (rallyNameStr.length>24)
                        {
                            
                            [cellView addSubview:addressBtn1];
                            
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55,40,265,10);
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            //                              addressBtn2.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:range forState:UIControlStateNormal];
                                
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (rallyDataEncode.length>24)
                    {
                        // NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        
                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
                        
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
                        
                        
                        if (rallyDataEncode.length>24)
                        {
                            [cellView addSubview:addressBtn1];
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
                            //                             addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            
                        {
                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
            {
                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
                
            {
                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
                {
                    
                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                }
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                
                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
                [userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
                
                
            }
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinDATE"] isEqual:[NSNull null]])
            {
                timeLbl.text = @"";
            }
            else
                
            {
                UILabel *timeLbel   = (UILabel *) [cell viewWithTag:1000+indexPath.row];
                
                
                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinDATE"]];
                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
                NSDate *today1 = [NSDate date];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
                
                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                // NSCalendar *c = [NSCalendar currentCalendar];
                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
                
                NSInteger years = [componentsDaysDiff year];
                NSInteger months = [componentsDaysDiff month];
                NSInteger days = [componentsDaysDiff day];
                NSInteger hours = [componentsDaysDiff hour];
                NSInteger minutes = [componentsDaysDiff minute];
                NSInteger seconds = [componentsDaysDiff second];
                
                
                
                
                if (days >0)
                {
                    if (days>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
                        
                        
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
                        
                        
                    }
                    if (days >30)
                    {
                        if (days>1)
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                            
                        }
                        else
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                            
                        }
                        
                    }
                }
                else if(months>12)
                {
                    if (months>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
                    }
                }
                else if (hours > 0)
                {
                    if (hours>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
                    }
                }
                else if(minutes >0)
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
                    }
                    
                    
                    
                }
                else
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
                    }
                }
                
            }
        }
        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"]  )
        {
//            [cellView addSubview:likesLbl];
            [cellView addSubview:joinGoingLbl];
            [cellView addSubview:rallyBtn];
            [cellView addSubview:joinBtn];
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"act_like"] isEqual:[NSNull null]])
            {
                likesLbl.text = @"";
                
            }
            else
                
            {
                NSString *  likesStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"act_like"]];
                
                likesLbl.text = likesStr;
                
            }
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"] isEqual:[NSNull null]])
            {
                
            }
            else
                
            {
                NSString *  likestatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"]];
                if ([likestatusStr isEqualToString:@"0"])
                {
                    [rallyBtn setImage:[UIImage imageNamed:@"grayRallyIcon.png"]  forState:UIControlStateNormal];
                }
                else
                {
                    
                    [rallyBtn setImage:[UIImage imageNamed:@"startRally.png"]  forState:UIControlStateNormal];
                }
                
            }
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"] isEqual:[NSNull null]])
            {
                
            }
            else
                
            {
                NSString *  joinstatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinstatus"]];
                if ([joinstatusStr isEqualToString:@"0"])
                {
                    [joinBtn setImage:[UIImage imageNamed:@"join_gray.png"]  forState:UIControlStateNormal];
                }
                else
                {
                    
                    [joinBtn setBackgroundImage:[UIImage imageNamed:@"join_button-real.png"]  forState:UIControlStateNormal];
                }
                [joinBtn setTitleColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0] forState:UIControlStateNormal];
                
            }
            
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
            {
                userImage.image =[UIImage imageNamed:@""];
            }
            else
            {
                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"]];
                NSURL *imageURL = [NSURL URLWithString:imgStr];
                imageView.imageURL = imageURL;
                
            }
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"totalrallyers"] isEqual:[NSNull null]])
            {
                joinGoingLbl.text = @"";
            }
            else
                
            {
                NSString *  totalrallyersStr = [NSString stringWithFormat:@"%@ going",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"totalrallyers"]];
                joinGoingLbl.text =totalrallyersStr;
            }
            
        }
        
        
        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
        {
            [cell addSubview:imageView1];
            [cell addSubview:ImageBtn];
            [cell addSubview:startRallyLbl];
            timeLbl.frame = CGRectMake(215, 106, 120, 30);
            
            startRallyLbl.text = @"uploaded a Photo from";
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"] isEqual:[NSNull null]])
            {
                timeLbl.text = @"";
            }
            else
                
            {
                UILabel *timeLbel   = (UILabel *) [cell viewWithTag:1000+indexPath.row];
                
                
                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"]];
                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
                NSDate *today1 = [NSDate date];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
                
                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                // NSCalendar *c = [NSCalendar currentCalendar];
                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
                
                
                NSInteger years = [componentsDaysDiff year];
                NSInteger months = [componentsDaysDiff month];
                NSInteger days = [componentsDaysDiff day];
                NSInteger hours = [componentsDaysDiff hour];
                NSInteger minutes = [componentsDaysDiff minute];
                NSInteger seconds = [componentsDaysDiff second];
                
                
                if (days >0)
                {
                    if (days>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
                        
                        
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
                        
                        
                    }
                    if (days >30)
                    {
                        if (days>1)
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                            
                        }
                        else
                        {
                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                            
                        }
                        
                    }
                }
                else if(months>12)
                {
                    if (months>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
                    }
                }
                else if (hours > 0)
                {
                    if (hours>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
                    }
                }
                else if(minutes >0)
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
                    }
                    
                    
                    
                }
                else
                {
                    if (minutes>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
                    }
                }
                
            }
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
            {
                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
                
            {
                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
                {
                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];            }
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                
                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
                
                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
                [userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
                
                
            }
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
            {
                [addressBtn setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
                
            {
                
                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                // addressBtn.backgroundColor = [UIColor redColor];
                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
                [addressBtn setFrame:CGRectMake(stringsize.width+31,20,150,20)];
                
                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
                
                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                {
                    if (rallyNameStr.length>24)
                    {
                        //  NSString * range = [rallyNameStr substringWithRange:NSMakeRange(0,24)]  ;
                        
                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
                        
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
                        
                        // [addressBtn setTitle:range forState:UIControlStateNormal];
                        if (rallyNameStr.length>24)
                        {
                            
                            [cellView addSubview:addressBtn1];
                            
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            //                        addressBtn2.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:range forState:UIControlStateNormal];
                                
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (rallyDataEncode.length>24)
                    {
                        // NSString * range1 = [rallyDataEncode substringWithRange:NSMakeRange(0, 21)]  ;
                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
                        
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 24;
                            }
                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn setTitle:range forState:UIControlStateNormal];
                        
                        
                        if (rallyDataEncode.length>24)
                        {
                            [cellView addSubview:addressBtn1];
                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
                            //                        addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {
                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
                                
                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                            
                        {
                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
                
                
            }
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
            {
                userImage.image =[UIImage imageNamed:@""];
            }
            else
            {
                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"]];
                NSURL *imageURL = [NSURL URLWithString:imgStr];
                // [AsyncImageLoader sharedLoader].cache ;
                
                
                // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
                imageView.imageURL = imageURL;
                
            }
            
            
            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"imageNAME"] isEqual:[NSNull null]])
            {
                // userImage1.image =[UIImage imageNamed:@""];
            }
            else
            {
                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-9];
                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"imageNAME"]];
                NSURL *imageURL = [NSURL URLWithString:imgStr];
                //  NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //  UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
                //  imageView1.image = imageLoad;
                // [AsyncImageLoader sharedLoader].cache = nil;
                
                //  [[AsyncImageCache sharedCache] removeImageForURL:imageURL];
                imageView.imageURL = imageURL;
            }
            
            
        }
        
        cell.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
        cellView = nil;
        userImage = nil;
        userNameBtn = nil;
        startRallyLbl = nil;
        timeLbl = nil;
        likesLbl = nil;
        joinGoingLbl= nil;
        addressBtn = nil;
        rallyBtn = nil;
        joinBtn = nil;
        PhotoView = nil;
        userImage1 = nil;
        
        startRallyLbl1  = nil;
        timeLbl1 = nil;
        addressBtn1 = nil;
        photoScrolview = nil;
        imageView1 = nil;
        ImageBtn = nil;
        

//    }
//    else
//    {
//        if (cell == nil)
//        {
//            if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
//                
//            {
//                cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
//                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
//                
//                cellView = [[UIView alloc]initWithFrame: CGRectMake(0,0,320,110)];
//                cellView.backgroundColor = [UIColor clearColor];
//                cellView.userInteractionEnabled = YES;
//                
//                
//                userImage = [[AsyncImageView alloc]init];
//                //userImage.image = [UIImage imageNamed:@""];
//                userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
//                userImage.backgroundColor = [UIColor clearColor];
//                userImage.tag = -1;
//                userImage.frame = CGRectMake(10, 8, 40, 40);
//                [cellView addSubview:userImage];
//                
//                userNameBtn =[[UIButton alloc]init];
//                userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                // userNameBtn.frame = CGRectMake(55,2,0,30);
//                userNameBtn.tag =-2;
//                [userNameBtn addTarget:self  action:@selector(userBtnAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
//                userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//                userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//                userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//                [userNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//                // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
//                // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
//                [cellView addSubview:userNameBtn];
//                
//                startRallyLbl = [[UILabel alloc]init];
//                startRallyLbl.frame =CGRectMake(55,15,130,30);
//                
//                startRallyLbl.textColor = [UIColor darkGrayColor];
//                startRallyLbl.tag = -3;
//                startRallyLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
//                [cellView addSubview:startRallyLbl];
//                
//                timeLbl = [[UILabel alloc]init];
//                timeLbl.frame = CGRectMake(215, 61, 120, 40);
//                timeLbl.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
//                timeLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
//                timeLbl.tag = 1000+indexPath.row;
//                //timeLbl.text = @"22 seconds ago";
//                [cellView addSubview:timeLbl];
//                
//                likesLbl = [[UILabel alloc]init];
//                likesLbl.frame = CGRectMake(55,60, 30, 40);
//                likesLbl.textColor = [UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1.0];
//                likesLbl.tag = indexPath.row+2000;
//                likesLbl.textAlignment = NSTextAlignmentLeft;
//                likesLbl.adjustsFontSizeToFitWidth = YES;
//                likesLbl.adjustsLetterSpacingToFitWidth = YES;
//                likesLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//                //likesLbl.text = @"Like";
//                //  [cellView addSubview:likesLbl];
//                
//                
//                
//                joinGoingLbl = [[UILabel alloc]init];
//                joinGoingLbl.frame = CGRectMake(155, 60, 60, 40);
//                joinGoingLbl.textColor = [UIColor darkGrayColor];
//                joinGoingLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//                joinGoingLbl.tag = -4;
//                joinGoingLbl.adjustsFontSizeToFitWidth = YES;
//                joinGoingLbl.adjustsLetterSpacingToFitWidth = YES;
//                //joinGoingLbl.text = @"45 going";
//                //    [cellView addSubview:joinGoingLbl];
//                
//                //addressBtn = [[UIButton alloc]init];
//                addressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                //    addressBtn.backgroundColor=[UIColor redColor];
//                //    addressBtn.frame = CGRectMake(55,20,265,30);
//                addressBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//                addressBtn.titleLabel.numberOfLines =2;
//                [addressBtn addTarget:self  action:@selector(addressAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                addressBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//                [addressBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                addressBtn.tag = -5;
//                addressBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//                [addressBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
//                addressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//                [cellView addSubview:addressBtn];
//                
//                addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//                // addressBtn.frame = CGRectMake(55,20,290, 20);
//                [addressBtn1 addTarget:self  action:@selector(addressAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                addressBtn1.tag = -15;
//                addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
//                addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//                //[cellView addSubview:addressBtn1];
//                
//                
//                
//                rallyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                [rallyBtn addTarget:self  action:@selector(rallyLikesAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                //[rallyBtn setTitle:@"X" forState:UIControlStateNormal];
//                // [rallyBtn setImage:[UIImage imageNamed:@"startRally.png"]  forState:UIControlStateNormal];
//                rallyBtn.frame = CGRectMake(10, 60, 40, 40);
//                rallyBtn.tag = indexPath.row+200;
//                // [cellView addSubview:rallyBtn];
//                
//                joinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                joinBtn.frame = CGRectMake(75,65,70,30);
//                
//                [joinBtn setImage:[UIImage imageNamed:@"join.png"]  forState:UIControlStateNormal];
//                [joinBtn setTitle:@"join" forState:UIControlStateNormal];
//                joinBtn.tag = -7;
//                [joinBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//                [joinBtn addTarget:self action:@selector(addressAction1:) forControlEvents:UIControlEventTouchUpInside];
//                // button.backgroundColor= [UIColor lightGrayColor];
//                // [cellView addSubview:joinBtn];
//                
//                
//                userImageBtn1 =[[UIButton alloc]init];
//                userImageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//                userImageBtn1.frame = CGRectMake(10, 10, 40, 40);
//                userImageBtn1.tag = -8;
//                [userImageBtn1 addTarget:self  action:@selector(userBtnAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                userImageBtn1.backgroundColor = [UIColor clearColor];
//                [cellView addSubview:userImageBtn1];
//                
//                imageView1 = [[AsyncImageView alloc] init ];
//                imageView1.frame= CGRectMake(10, 60, 70,80) ;
//                imageView1.userInteractionEnabled = YES;
//                imageView1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
//                imageView1.tag = -9;
//                imageView1.contentMode =  UIViewContentModeScaleToFill;
//                //  [cellView addSubview:imageView1];
//                ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                ImageBtn.frame = CGRectMake(imageView1.frame.origin.x,imageView1.frame.origin.y,imageView1.frame.size.width, imageView1.frame.size.height);
//                [ImageBtn addTarget:self  action:@selector(UploadPhotoAction1:)  forControlEvents:UIControlEventTouchUpInside];
//                ImageBtn.tag = -10;
//                // [imageView1 addSubview:ImageBtn];
//                
//                
//                [cell.contentView  addSubview:cellView];
//                
//                
//            }
//            
//            
//            /*else if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
//             {
//             cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier1];
//             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier1];
//             PhotoView = [[UIView alloc]initWithFrame: CGRectMake(0,0,320,110)];
//             PhotoView.backgroundColor = [UIColor clearColor];
//             PhotoView.userInteractionEnabled = YES;
//             PhotoView.tag = 159;
//             
//             userImage1 = [[AsyncImageView alloc]init];
//             userImage1.image = [UIImage imageNamed:@""];
//             userImage1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
//             userImage1.tag = 160;
//             userImage1.backgroundColor = [UIColor clearColor];
//             userImage1.frame = CGRectMake(10, 9, 40, 40);
//             [PhotoView addSubview:userImage1];
//             
//             
//             userNameBtn1 =[[UIButton alloc]init];
//             userNameBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//             //userNameBtn1.frame = CGRectMake(55,0,80,30);
//             userNameBtn1.tag = 161;
//             [userNameBtn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
//             // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
//             userNameBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//             userNameBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//             userNameBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
//             [userNameBtn1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//             //    userNameBtn1.titleLabel.adjustsLetterSpacingToFitWidth = YES;
//             //    userNameBtn1.titleLabel.adjustsFontSizeToFitWidth = YES;
//             [PhotoView addSubview:userNameBtn1];
//             
//             startRallyLbl1 = [[UILabel alloc]init];
//             //startRallyLbl1.frame = CGRectMake(130, 0, 110, 30);
//             startRallyLbl1.textColor = [UIColor darkGrayColor];
//             startRallyLbl1.tag = 162;
//             //    startRallyLbl1.adjustsLetterSpacingToFitWidth = YES;
//             //    startRallyLbl1.adjustsFontSizeToFitWidth = YES;
//             startRallyLbl1.textAlignment = NSTextAlignmentCenter;
//             startRallyLbl1.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
//             //startRallyLbl.text = @"started a Rally at";
//             [PhotoView addSubview:startRallyLbl1];
//             
//             timeLbl1 = [[UILabel alloc]init];
//             timeLbl1.frame = CGRectMake(230,105, 120, 30);
//             timeLbl1.tag = 1000+indexPath.row;
//             timeLbl1.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
//             timeLbl1.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
//             timeLbl1.text = @"22 seconds ago";
//             [PhotoView addSubview:timeLbl1];
//             
//             addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//             addressBtn1.frame = CGRectMake(55,20,290, 20);
//             [addressBtn1 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
//             addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:11.0f];
//             [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//             addressBtn1.tag = 164;
//             addressBtn1.titleLabel.adjustsLetterSpacingToFitWidth = YES;
//             addressBtn1.titleLabel.adjustsFontSizeToFitWidth = YES;
//             addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//             addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//             [PhotoView addSubview:addressBtn1];
//             
//             
//             
//             photoScrolview = [[UIScrollView alloc]init];
//             photoScrolview.frame = CGRectMake(10, 60,310, 80);
//             photoScrolview.scrollEnabled = YES;
//             photoScrolview.tag = 165;
//             photoScrolview.showsVerticalScrollIndicator = NO;
//             photoScrolview.showsHorizontalScrollIndicator=YES;
//             photoScrolview.delegate = self;
//             [PhotoView addSubview:photoScrolview];
//             
//             
//             imageView1 = [[AsyncImageView alloc] init ];
//             imageView1.frame= CGRectMake(0,0, 70,80) ;
//             imageView1.userInteractionEnabled = YES;
//             imageView1.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
//             imageView1.tag = 166;
//             imageView1.contentMode =  UIViewContentModeScaleToFill;
//             [photoScrolview addSubview:imageView1];
//             
//             self.automaticallyAdjustsScrollViewInsets = NO;
//             photoScrolview.contentSize = CGSizeMake(x,photoScrolview.frame.size.height);
//             // businessScrollview.contentSize = CGSizeMake(businessScrollview.contentSize.width,imageView1.frame.size.height);
//             ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//             ImageBtn.frame = CGRectMake(imageView1.frame.origin.x,imageView1.frame.origin.y,imageView1.frame.size.width, imageView1.frame.size.height);
//             [ImageBtn addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
//             ImageBtn.tag = 167;
//             [imageView1 addSubview:ImageBtn];
//             
//             userImage1Btn1 =[[UIButton alloc]init];
//             userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//             userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
//             userImage1Btn1.tag = 168;
//             [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
//             userImage1Btn1.backgroundColor = [UIColor clearColor];
//             [PhotoView addSubview:userImage1Btn1];
//             
//             
//             
//             [cell.contentView  addSubview:PhotoView];
//             }*/
//        }
//        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"])
//        {
//            [cellView addSubview:likesLbl];
//            [cellView addSubview:joinGoingLbl];
//            [cellView addSubview:rallyBtn];
//            [cellView addSubview:joinBtn];
//            
//            startRallyLbl.text = @"started a Rally at";
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
//            {
//                [addressBtn setTitle:@"" forState:UIControlStateNormal];
//            }
//            else
//                
//            {
//                //  addressBtn.titleLabel.numberOfLines = 0;
//                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
//                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
//                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                // addressBtn.backgroundColor = [UIColor redColor];
//                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
//                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [addressBtn setFrame:CGRectMake(stringsize.width+39,20,150,20)];
//                
//                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
//                
//                
//                
//                //                 NSString * titleStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
//                //
//                //                 titleStr = [titleStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                //                 NSData *data = [titleStr dataUsingEncoding:NSUTF8StringEncoding];
//                //                 NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                {
//                    if (rallyNameStr.length>20)
//                    {
//                        //  NSString * range = [rallyNameStr substringWithRange:NSMakeRange(0,27)]  ;
//                        NSRange rangeToSearch = NSMakeRange(0, 20); // get a range without the space character
//                        
//                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 20;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location =20;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        
//                        
//                        
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        if (rallyNameStr.length>20)
//                        {
//                            
//                            [cellView addSubview:addressBtn1];
//                            
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            //                           addressBtn2.backgroundColor = [UIColor blueColor];
//                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            
//                            
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
//                                
//                                //                                 CGRect currentFrame = addressBtn2.frame;
//                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
//                                //                                 CGSize expected = [range sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
//                                //                                 currentFrame.size.height = expected.height;
//                                //                                 addressBtn2.frame = currentFrame;
//                                
//                                
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                                
//                                //                                 CGRect currentFrame = addressBtn2.frame;
//                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
//                                //                                 CGSize expected = [rallyDataEncode sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
//                                //                                 currentFrame.size.height = expected.height;
//                                //                                 addressBtn2.frame = currentFrame;
//                                
//                            }
//                            
//                        }
//                    }
//                    else
//                    {
//                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                    }
//                    
//                }
//                else
//                {
//                    if (rallyDataEncode.length>20)
//                    {
//                        NSRange rangeToSearch = NSMakeRange(0, 20); // get a range without the space character
//                        
//                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 20;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 20;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        
//                        
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        
//                        if (rallyDataEncode.length>20)
//                        {
//                            [cellView addSubview:addressBtn1];
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
//                            //                          addressBtn2.backgroundColor = [UIColor blueColor];
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            
//                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
//                                
//                                
//                                //                                 CGRect currentFrame = addressBtn2.frame;
//                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width, 10);
//                                //                                 CGSize expected = [range sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
//                                //                                currentFrame.size.height = expected.height;
//                                //                                 addressBtn2.frame = currentFrame;
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 263, 10);
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                                
//                                //                                 CGRect currentFrame = addressBtn2.frame;
//                                //                                 CGSize max = CGSizeMake(addressBtn2.frame.size.width,10);
//                                //                                 CGSize expected = [rallyDataEncode sizeWithFont:addressBtn2.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn2.titleLabel.lineBreakMode];
//                                //                                 currentFrame.size.height = expected.height;
//                                //                                 addressBtn2.frame = currentFrame;
//                                //
//                                
//                            }
//                            
//                            
//                        }
//                    }
//                    else
//                    {
//                        if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
//                            
//                        {
//                            
//                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                        }
//                        else
//                        {
//                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
//                        }
//                        
//                    }
//                }
//                
//                //                 if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                //                 {
//                //                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                //
//                //                     CGRect currentFrame = addressBtn.frame;
//                //                     CGSize max = CGSizeMake(addressBtn.frame.size.width, 100);
//                //                     CGSize expected = [rallyNameStr sizeWithFont:addressBtn.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn.titleLabel.lineBreakMode];
//                //                     currentFrame.size.height = expected.height;
//                //                     addressBtn.frame = currentFrame;
//                //
//                //                 }
//                //                 else
//                //                 {
//                //
//                //                     [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
//                //
//                //                     CGRect currentFrame = addressBtn.frame;
//                //                     CGSize max = CGSizeMake(addressBtn.frame.size.width, 100);
//                //                     CGSize expected = [rallyDataEncode sizeWithFont:addressBtn.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn.titleLabel.lineBreakMode];
//                //                     currentFrame.size.height = expected.height;
//                //                     addressBtn.frame = currentFrame;
//                //
//                //                 }
//                //
//                
//                
//            }
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
//            {
//                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
//                
//            }
//            else
//                
//            {
//                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
//                {
//                    
//                    
//                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//                }
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
//                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
//                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
//                
//                
//            }
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"] isEqual:[NSNull null]])
//            {
//                timeLbl.text = @"";
//            }
//            else
//            {
//                UILabel *timeLbel = (UILabel *) [cell viewWithTag:1000+indexPath.row];
//                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"]];
//                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
//                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
//                NSDate *today1 = [NSDate date];
//                NSCalendar *calendar = [NSCalendar currentCalendar];
//                NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
//                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//                // NSCalendar *c = [NSCalendar currentCalendar];
//                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
//                
//                
//                NSInteger years = [componentsDaysDiff year];
//                NSInteger months = [componentsDaysDiff month];
//                NSInteger days = [componentsDaysDiff day];
//                NSInteger hours = [componentsDaysDiff hour];
//                NSInteger minutes = [componentsDaysDiff minute];
//                NSInteger seconds = [componentsDaysDiff second];
//                
//                
//                
//                
//                if (days >0)
//                {
//                    if (days>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
//                        
//                        
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
//                        
//                        
//                    }
//                    if (days >30)
//                    {
//                        if (days>1)
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
//                            
//                        }
//                        else
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
//                            
//                        }
//                        
//                    }
//                }
//                else if(months>12)
//                {
//                    if (months>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
//                    }
//                }
//                else if (hours > 0)
//                {
//                    if (hours>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
//                    }
//                }
//                else if(minutes >0)
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
//                    }
//                    
//                    
//                    
//                }
//                else
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
//                    }
//                }
//            }
//            
//        }
//        else if([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"])
//        {
//            [cellView addSubview:likesLbl];
//            [cellView addSubview:joinGoingLbl];
//            [cellView addSubview:rallyBtn];
//            [cellView addSubview:joinBtn];
//            startRallyLbl.text = @"has joined a Rally at";
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
//            {
//                [addressBtn setTitle:@"" forState:UIControlStateNormal];
//                
//            }
//            else
//                
//            {
//                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
//                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
//                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                // addressBtn.backgroundColor = [UIColor redColor];
//                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
//                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [addressBtn setFrame:CGRectMake(stringsize.width+36,20,150,20)];
//                
//                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
//                
//                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                {
//                    if (rallyNameStr.length>24)
//                    {
//                        
//                        
//                        NSRange rangeToSearch = NSMakeRange(0,24); // get a range without the space character
//                        
//                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        
//                        
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        
//                        if (rallyNameStr.length>24)
//                        {
//                            
//                            [cellView addSubview:addressBtn1];
//                            
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55,40,265,10);
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            //                              addressBtn2.backgroundColor = [UIColor blueColor];
//                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            
//                            
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:range forState:UIControlStateNormal];
//                                
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                                
//                            }
//                            
//                            
//                        }
//                    }
//                    else
//                    {
//                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                    }
//                    
//                }
//                else
//                {
//                    if (rallyDataEncode.length>24)
//                    {
//                        // NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        
//                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
//                        
//                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        
//                        
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        
//                        
//                        if (rallyDataEncode.length>24)
//                        {
//                            [cellView addSubview:addressBtn1];
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
//                            //                             addressBtn2.backgroundColor = [UIColor blueColor];
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            
//                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
//                                
//                                
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                            }
//                            
//                            
//                        }
//                    }
//                    else
//                    {
//                        if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            
//                        {
//                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                        }
//                        else
//                        {
//                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
//                        }
//                        
//                    }
//                }
//                
//                
//            }
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
//            {
//                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
//                
//            }
//            else
//                
//            {
//                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
//                {
//                    
//                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//                }
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
//                
//                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
//                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
//                
//                
//            }
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinDATE"] isEqual:[NSNull null]])
//            {
//                timeLbl.text = @"";
//            }
//            else
//                
//            {
//                UILabel *timeLbel   = (UILabel *) [cell viewWithTag:1000+indexPath.row];
//                
//                
//                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinDATE"]];
//                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
//                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
//                NSDate *today1 = [NSDate date];
//                NSCalendar *calendar = [NSCalendar currentCalendar];
//                NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
//                
//                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//                // NSCalendar *c = [NSCalendar currentCalendar];
//                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
//                
//                NSInteger years = [componentsDaysDiff year];
//                NSInteger months = [componentsDaysDiff month];
//                NSInteger days = [componentsDaysDiff day];
//                NSInteger hours = [componentsDaysDiff hour];
//                NSInteger minutes = [componentsDaysDiff minute];
//                NSInteger seconds = [componentsDaysDiff second];
//                
//                
//                
//                
//                if (days >0)
//                {
//                    if (days>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
//                        
//                        
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
//                        
//                        
//                    }
//                    if (days >30)
//                    {
//                        if (days>1)
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
//                            
//                        }
//                        else
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
//                            
//                        }
//                        
//                    }
//                }
//                else if(months>12)
//                {
//                    if (months>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
//                    }
//                }
//                else if (hours > 0)
//                {
//                    if (hours>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
//                    }
//                }
//                else if(minutes >0)
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
//                    }
//                    
//                    
//                    
//                }
//                else
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
//                    }
//                }
//                
//            }
//        }
//        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"1"] ||[[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"3"]  )
//        {
//            [cellView addSubview:likesLbl];
//            [cellView addSubview:joinGoingLbl];
//            [cellView addSubview:rallyBtn];
//            [cellView addSubview:joinBtn];
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"act_like"] isEqual:[NSNull null]])
//            {
//                likesLbl.text = @"";
//                
//            }
//            else
//                
//            {
//                NSString *  likesStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"act_like"]];
//                
//                likesLbl.text = likesStr;
//                
//            }
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"] isEqual:[NSNull null]])
//            {
//                
//            }
//            else
//                
//            {
//                NSString *  likestatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"]];
//                if ([likestatusStr isEqualToString:@"0"])
//                {
//                    [rallyBtn setImage:[UIImage imageNamed:@"grayRallyIcon.png"]  forState:UIControlStateNormal];
//                }
//                else
//                {
//                    
//                    [rallyBtn setImage:[UIImage imageNamed:@"startRally.png"]  forState:UIControlStateNormal];
//                }
//                
//            }
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"likestatus"] isEqual:[NSNull null]])
//            {
//                
//            }
//            else
//                
//            {
//                NSString *  joinstatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinstatus"]];
//                if ([joinstatusStr isEqualToString:@"0"])
//                {
//                    [joinBtn setImage:[UIImage imageNamed:@"join_gray.png"]  forState:UIControlStateNormal];
//                }
//                else
//                {
//                    
//                    [joinBtn setImage:[UIImage imageNamed:@"join.png"]  forState:UIControlStateNormal];
//                }
//                
//            }
//            
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
//            {
//                userImage.image =[UIImage imageNamed:@""];
//            }
//            else
//            {
//                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
//                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
//                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"]];
//                NSURL *imageURL = [NSURL URLWithString:imgStr];
//                imageView.imageURL = imageURL;
//                
//            }
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"totalrallyers"] isEqual:[NSNull null]])
//            {
//                joinGoingLbl.text = @"";
//            }
//            else
//                
//            {
//                NSString *  totalrallyersStr = [NSString stringWithFormat:@"%@ going",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"totalrallyers"]];
//                joinGoingLbl.text =totalrallyersStr;
//            }
//            
//        }
//        
//        
//        if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
//        {
//            [cell addSubview:imageView1];
//            [cell addSubview:ImageBtn];
//            [cell addSubview:startRallyLbl];
//            timeLbl.frame = CGRectMake(215, 106, 120, 30);
//            
//            startRallyLbl.text = @"uploaded a Photo from";
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"] isEqual:[NSNull null]])
//            {
//                timeLbl.text = @"";
//            }
//            else
//                
//            {
//                UILabel *timeLbel   = (UILabel *) [cell viewWithTag:1000+indexPath.row];
//                
//                
//                NSString *  dateStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"createDATE"]];
//                NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
//                [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
//                NSDate *today1 = [NSDate date];
//                NSCalendar *calendar = [NSCalendar currentCalendar];
//                NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
//                
//                //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//                // NSCalendar *c = [NSCalendar currentCalendar];
//                NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
//                
//                
//                NSInteger years = [componentsDaysDiff year];
//                NSInteger months = [componentsDaysDiff month];
//                NSInteger days = [componentsDaysDiff day];
//                NSInteger hours = [componentsDaysDiff hour];
//                NSInteger minutes = [componentsDaysDiff minute];
//                NSInteger seconds = [componentsDaysDiff second];
//                
//                
//                if (days >0)
//                {
//                    if (days>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];
//                        
//                        
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];
//                        
//                        
//                    }
//                    if (days >30)
//                    {
//                        if (days>1)
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
//                            
//                        }
//                        else
//                        {
//                            timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
//                            
//                        }
//                        
//                    }
//                }
//                else if(months>12)
//                {
//                    if (months>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
//                    }
//                }
//                else if (hours > 0)
//                {
//                    if (hours>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
//                    }
//                }
//                else if(minutes >0)
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
//                    }
//                    
//                    
//                    
//                }
//                else
//                {
//                    if (minutes>1)
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
//                    }
//                    else
//                    {
//                        timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
//                    }
//                }
//                
//            }
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"] isEqual:[NSNull null]])
//            {
//                [userNameBtn setTitle:@"" forState:UIControlStateNormal];
//                
//            }
//            else
//                
//            {
//                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userNAME"]];
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
//                {
//                    nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];            }
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
//                
//                [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
//                
//                CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
//                
//                
//            }
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
//            {
//                [addressBtn setTitle:@"" forState:UIControlStateNormal];
//                
//            }
//            else
//                
//            {
//                
//                NSString *  rallyNameStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"rallyNAME"]];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                rallyNameStr = [rallyNameStr  stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
//                NSData *data = [rallyNameStr dataUsingEncoding:NSUTF8StringEncoding];
//                NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                // addressBtn.backgroundColor = [UIColor redColor];
//                NSString * uploadPhotoStr = [NSString stringWithFormat:@"%@",startRallyLbl.text];
//                CGSize stringsize = [uploadPhotoStr sizeWithFont:[UIFont systemFontOfSize:14]];
//                [addressBtn setFrame:CGRectMake(stringsize.width+31,20,150,20)];
//                
//                NSLog(@"Adree Btn Frame x:%f,y:%f,Width:%f,Height:%f with %@",addressBtn.frame.origin.x,addressBtn.frame.origin.y,addressBtn.frame.size.width,addressBtn.frame.size.height,startRallyLbl.text);
//                
//                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                {
//                    if (rallyNameStr.length>24)
//                    {
//                        //  NSString * range = [rallyNameStr substringWithRange:NSMakeRange(0,24)]  ;
//                        
//                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
//                        
//                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        
//                        // [addressBtn setTitle:range forState:UIControlStateNormal];
//                        if (rallyNameStr.length>24)
//                        {
//                            
//                            [cellView addSubview:addressBtn1];
//                            
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            //                        addressBtn2.backgroundColor = [UIColor blueColor];
//                            NSString * rangeValue = [rallyNameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyNameStr.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:range forState:UIControlStateNormal];
//                                
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                                
//                            }
//                            
//                            
//                        }
//                    }
//                    else
//                    {
//                        [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                    }
//                    
//                }
//                else
//                {
//                    if (rallyDataEncode.length>24)
//                    {
//                        // NSString * range1 = [rallyDataEncode substringWithRange:NSMakeRange(0, 21)]  ;
//                        NSRange rangeToSearch = NSMakeRange(0, 24); // get a range without the space character
//                        
//                        
//                        NSRange rangeOfSecondToLastSpace ;
//                        NSString * range;
//                        NSRange rangeValue12 = [rallyNameStr rangeOfString:@"\\ud"];
//                        
//                        if (rangeValue12.length > 0)
//                            
//                        {
//                            rangeOfSecondToLastSpace = [rallyDataEncode rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyDataEncode substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                        } else
//                        {
//                            rangeOfSecondToLastSpace = [rallyNameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
//                            
//                            if (rangeOfSecondToLastSpace.location == NSNotFound)
//                            {
//                                rangeOfSecondToLastSpace.location = 24;
//                            }
//                            range = [rallyNameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
//                            
//                            
//                        }
//                        
//                        [addressBtn setTitle:range forState:UIControlStateNormal];
//                        
//                        
//                        if (rallyDataEncode.length>24)
//                        {
//                            [cellView addSubview:addressBtn1];
//                            UIButton * addressBtn2 = (UIButton *)[cell viewWithTag:-15];
//                            addressBtn2.frame = CGRectMake(55, 40, 263, 10);
//                            //                        addressBtn2.backgroundColor = [UIColor blueColor];
//                            //addressBtn2.titleLabel.numberOfLines = 0;
//                            
//                            NSString * rangeValue = [rallyDataEncode substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, rallyDataEncode.length-rangeOfSecondToLastSpace.location)]  ;
//                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
//                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                            
//                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
//                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//                            if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rangeValue forState:UIControlStateNormal];
//                                
//                                
//                            }
//                            else
//                            {
//                                addressBtn2.frame = CGRectMake(55, 40, 265, 10);
//                                
//                                [addressBtn2 setTitle:rallyDataEncode forState:UIControlStateNormal];
//                            }
//                            
//                            
//                        }
//                    }
//                    else
//                    {
//                        if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
//                            
//                        {
//                            [addressBtn setTitle:rallyNameStr forState:UIControlStateNormal];
//                        }
//                        else
//                        {
//                            [addressBtn setTitle:rallyDataEncode forState:UIControlStateNormal];
//                        }
//                        
//                    }
//                }
//                
//                
//                
//                
//            }
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
//            {
//                userImage.image =[UIImage imageNamed:@""];
//            }
//            else
//            {
//                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
//                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
//                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"userIMAGE"]];
//                NSURL *imageURL = [NSURL URLWithString:imgStr];
//                // [AsyncImageLoader sharedLoader].cache ;
//                
//                
//                // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//                //UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
//                imageView.imageURL = imageURL;
//                
//            }
//            
//            
//            if ([[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"imageNAME"] isEqual:[NSNull null]])
//            {
//                // userImage1.image =[UIImage imageNamed:@""];
//            }
//            else
//            {
//                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-9];
//                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
//                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"imageNAME"]];
//                NSURL *imageURL = [NSURL URLWithString:imgStr];
//                //  NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//                //  UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
//                //  imageView1.image = imageLoad;
//                // [AsyncImageLoader sharedLoader].cache = nil;
//                
//                //  [[AsyncImageCache sharedCache] removeImageForURL:imageURL];
//                imageView.imageURL = imageURL;
//            }
//            
//            
//        }
//        
//        cell.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
//        cellView = nil;
//        userImage = nil;
//        userNameBtn = nil;
//        startRallyLbl = nil;
//        timeLbl = nil;
//        likesLbl = nil;
//        joinGoingLbl= nil;
//        addressBtn = nil;
//        rallyBtn = nil;
//        joinBtn = nil;
//        PhotoView = nil;
//        userImage1 = nil;
//        
//        startRallyLbl1  = nil;
//        timeLbl1 = nil;
//        addressBtn1 = nil;
//        photoScrolview = nil;
//        imageView1 = nil;
//        ImageBtn = nil;
//    }
//    cellView = [[UIView alloc]initWithFrame: CGRectMake(8,0,304,70)];
    [cell setBackgroundColor:[UIColor clearColor]];
//    cell.clipsToBounds = YES;
    [cell.contentView addSubview:separatorLineView];
//    cell.layer.cornerRadius = 20;//half of the width
    return cell;
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"%i", (int)(self.scrollView.contentOffset.x / self.scrollView.frame.size.width));
    int val=self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
    if (val==1)
    {
        // showload=0;
         labelforNavigationTitle.text=@"Around You";
         rightBtn.hidden=NO;
         [self callWebService_aroundYou];
         [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"OnHubPage"];
         [[NSUserDefaults standardUserDefaults]synchronize];
        
       
    }
    else
    {
        // showload=0;
         labelforNavigationTitle.text=@"Your Pack";
         rightBtn.hidden=YES;
         [self callWebService];
        [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"OnHubPage"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    
    CGFloat currentOffSet_x = self.scrollView.contentOffset.x;
    
    if(lastOffset_x > currentOffSet_x)
    {
        
        NSInteger currentIndex = self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
        
        self.pageControllerObj.currentPage=currentIndex;
        
       NSLog(@"Current Index %d",currentIndex);
        
       
    }
    else
    {
        NSInteger currentIndex = self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
        
        self.pageControllerObj.currentPage=currentIndex;
        
        NSLog(@"Current Index %d",currentIndex);
        
       
    }
    
    lastOffset_x = self.scrollView.contentOffset.x;

//    cellView = nil;
//    userImage = nil;
//    userNameBtn = nil;
//    startRallyLbl = nil;
//    timeLbl = nil;
//    likesLbl = nil;
//    joinGoingLbl= nil;
//    addressBtn = nil;
//    rallyBtn = nil;
//    joinBtn = nil;
//    PhotoView = nil;
//    userImage1 = nil;
//    
//    startRallyLbl1  = nil;
//    timeLbl1 = nil;
//    addressBtn1 = nil;
//    photoScrolview = nil;
//    imageView1 = nil;
//    ImageBtn = nil;
//  // sorteddataArray = nil;
 
}




-(void)UploadPhotoAction1:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:ImageBtn.bounds toView:self.everyOneTableView];
    NSIndexPath *indexPath = [self.everyOneTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.row ; i++)
    {
        rowNumber += [self tableView:self.everyOneTableView numberOfRowsInSection:i];
    }
    rowNumber += indexPath.row;
    
    [ImageBtn setTag:rowNumber];
    
    RFullImageViewController  *ImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
    
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"]];
    ImageViewController.hubrallyImagesDic=[sorteddataArray objectAtIndex:rowNumber] ;
    ImageViewController.friendUserId_Image =userProfileIdStr ;
    [self.navigationController pushViewController:ImageViewController animated:NO];
}
- (void)UploadPhotoAction:(UIButton*)sender
{
   // UITableView *tableViewObj=(UITableView *)sender.superview;
    
    NSIndexPath *indexPath;
    NSInteger rowNumber;
    
    CGRect buttonFrameInTableView = [sender convertRect:ImageBtn.bounds toView:hubTableview];
    indexPath = [hubTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];
        
    rowNumber = 0;
        
    for(NSInteger i = 0; i < indexPath.row ; i++)
    {
            rowNumber += [self tableView:hubTableview numberOfRowsInSection:i];
    }
    rowNumber += indexPath.row;
        
    [ImageBtn setTag:rowNumber];
    
        
    RFullImageViewController  *ImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
        
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"]];
    ImageViewController.hubrallyImagesDic=[sorteddataArray objectAtIndex:rowNumber] ;
    ImageViewController.friendUserId_Image =userProfileIdStr ;
    [self.navigationController pushViewController:ImageViewController animated:NO];
    
    
    
    
   
    
//    RImageViewController *ImageViewController = [[RImageViewController alloc]initWithNibName:@"RImageViewController" bundle:nil];
//    
//    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"]];
//    ImageViewController.friendUserId_serch=userProfileIdStr;
//    [self.navigationController pushViewController:ImageViewController animated:NO];
    
    
  
    

}
- (void)userBtnAction:(UIButton*)sender
{
    NSInteger rowNumber;
    
    CGRect buttonFrameInTableView = [sender convertRect:addressBtn.bounds toView:hubTableview];
    
    NSIndexPath *indexPath = [hubTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];
        
    
    rowNumber = 0;
        
    
    for(NSInteger i = 0; i < indexPath.row ; i++)
        
    {
        
        rowNumber += [self tableView:hubTableview numberOfRowsInSection:i];
       
    }
        
    rowNumber += indexPath.row;
        
    [joinBtn setTag:rowNumber];
    
    
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"]];
   
    ProfileViewController.hubUserIDStr=userProfileIdStr;
    
    [self.navigationController pushViewController:ProfileViewController animated:NO];
    
    
}
- (void)addressAction:(UIButton*)sender
{
    NSInteger rowNumber;
    NSIndexPath *indexPath;
   
    CGRect buttonFrameInTableView = [sender convertRect:addressBtn.bounds toView:hubTableview];
    indexPath = [hubTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.row ; i++)
    {
        rowNumber += [self tableView:hubTableview numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [addressBtn setTag:rowNumber];
    
    buttonIndex1 = rowNumber;
    NSString *  checkRallyPrivate = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyPRIVATE"]];
    NSString *  joinstatusStr;
   joinstatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinstatus"]];
    if ([checkRallyPrivate isEqualToString:@"1"])
    {
         rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
        if (![[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"] isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            
            if ([joinstatusStr isEqualToString:@"0"])
            {
               [self displayAlertView];
//            if ([joinBtn.currentImage isEqual:[UIImage imageNamed:@"join.png"]]) {
//
           }
            else if ([joinstatusStr isEqualToString:@"1"])
                
            {
                                //  [self displayAlertView1];
                    
                RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
                
                // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
                //JoinRallyViewController.hubIDStr=rallyIdStr;
                JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
                [self.navigationController pushViewController:JoinRallyViewController animated:NO];
            }
            else if ([joinstatusStr isEqualToString:@"2"])
                {
                    
                UIAlertView *     alertRally =[[UIAlertView alloc ]initWithTitle:@"Sorry" message:@"Your request to join this Rally has not been accepted yet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    // alert1.tag =1;
                    [alertRally show];
                }

            
        }
        else
            
        {
//            if ([joinstatusStr isEqualToString:@"1"])
//                
//            {
//                [self displayAlertView1];
//            }
//            else
//            {
                RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
                
                // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
                //JoinRallyViewController.hubIDStr=rallyIdStr;
                JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
                [self.navigationController pushViewController:JoinRallyViewController animated:NO];

          //  }
                   }
       
         }
    else
    {
     RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
    
    // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
    //JoinRallyViewController.hubIDStr=rallyIdStr;
    JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
    [self.navigationController pushViewController:JoinRallyViewController animated:NO];
    }

}
- (void)rallyLikesAction:(UIButton*)sender
{
    NSIndexPath *indexPath;
    NSInteger rowNumber;
    
    CGRect buttonFrameInTableView = [sender convertRect:rallyBtn.bounds toView:hubTableview];
    indexPath = [hubTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];
        
    rowNumber = 0;
    rowNumber += indexPath.row+200;
        
        
    UITableViewCell *cell = [hubTableview cellForRowAtIndexPath:indexPath];
        //  [rallyBtn setTag:rowNumber];
    rallyBtn = (UIButton*)[cell viewWithTag:rowNumber];
    
//    rallyIdStrFor_likes=[[sorteddataArray objectAtIndex:(rowNumber-200)] valueForKey:@"act_id"];
    
    int val=rowNumber-200;
    
//    NSLog(@"Val %@",[NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:val] valueForKey:@"act_id"]]);
    
    rallyIdStrFor_likes = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:val] valueForKey:@"act_id"]];
    
    if( ![rallyIdStrFor_likes isEqualToString:@"<null>"] || ![rallyIdStrFor_likes isKindOfClass:[NSNull class]])
    {
     [self callWebService_addRally_Likes];
    }
    
    
}
- (void)userBtnAction1:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:addressBtn.bounds toView:self.everyOneTableView];
    NSIndexPath *indexPath = [self.everyOneTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.row ; i++)
    {
        rowNumber += [self tableView:self.everyOneTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [joinBtn setTag:rowNumber];
    
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"]];
    ProfileViewController.hubUserIDStr=userProfileIdStr;
    [self.navigationController pushViewController:ProfileViewController animated:NO];
    
    
}
- (void)addressAction1:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:addressBtn.bounds toView:self.everyOneTableView];
    NSIndexPath *indexPath = [self.everyOneTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.row ; i++)
    {
        rowNumber += [self tableView:self.everyOneTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [addressBtn setTag:rowNumber];
    
    buttonIndex1 = rowNumber;
    NSString *  checkRallyPrivate = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyPRIVATE"]];
    NSString *  joinstatusStr;
    joinstatusStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:indexPath.row ] objectForKey:@"joinstatus"]];
    if ([checkRallyPrivate isEqualToString:@"1"])
    {
        rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
        if (![[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"userID"] isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            
            if ([joinstatusStr isEqualToString:@"0"])
            {
                [self displayAlertView];
                //            if ([joinBtn.currentImage isEqual:[UIImage imageNamed:@"join.png"]]) {
                //
            }
            else if ([joinstatusStr isEqualToString:@"1"])
                
            {
                //  [self displayAlertView1];
                
                RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
                
                // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
                //JoinRallyViewController.hubIDStr=rallyIdStr;
                JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
                [self.navigationController pushViewController:JoinRallyViewController animated:NO];
            }
            else if ([joinstatusStr isEqualToString:@"2"])
            {
                
                UIAlertView *     alertRally =[[UIAlertView alloc ]initWithTitle:@"Sorry" message:@"Your request to join this Rally has not been accepted yet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                // alert1.tag =1;
                [alertRally show];
            }
            
            
        }
        else
            
        {
            //            if ([joinstatusStr isEqualToString:@"1"])
            //
            //            {
            //                [self displayAlertView1];
            //            }
            //            else
            //            {
            RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
            
            // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
            //JoinRallyViewController.hubIDStr=rallyIdStr;
            JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
            [self.navigationController pushViewController:JoinRallyViewController animated:NO];
            
            //  }
        }
        
    }
    else
    {
        RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
        
        // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
        //JoinRallyViewController.hubIDStr=rallyIdStr;
        JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:rowNumber];
        [self.navigationController pushViewController:JoinRallyViewController animated:NO];
    }
    
}
- (void)rallyLikesAction1:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:rallyBtn.bounds toView:self.everyOneTableView];
    NSIndexPath *indexPath = [self.everyOneTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    rowNumber += indexPath.row+200;
    
    
    
    UITableViewCell *cell = [self.everyOneTableView cellForRowAtIndexPath:indexPath];
    //  [rallyBtn setTag:rowNumber];
    rallyBtn = (UIButton*)[cell viewWithTag:rowNumber];
    
    int val=rowNumber-200;
    
    rallyIdStrFor_likes = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:val] objectForKey:@"act_id"]];
    
    if( ![rallyIdStrFor_likes isEqualToString:@"<null>"] || ![rallyIdStrFor_likes isKindOfClass:[NSNull class]])
    {
        [self callWebService_addRally_Likes];
    }
    
    
}

-(void)callWebService_addRally_Likes
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr = nil;
    if ([rallyBtn.currentImage isEqual:[UIImage imageNamed:@"grayRallyIcon.png"]])
    {
        
      // urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addlikes.php?userid=%@&rallyid=%@&likecheck=1",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],rallyIdStrFor_likes];
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addlikes.php?userid=%@&activityid=%@&likecheck=1",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],rallyIdStrFor_likes];
   }
    else if ([rallyBtn.currentImage isEqual:[UIImage imageNamed:@"startRally.png"]])
   {
   /// urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addlikes.php?userid=%@&rallyid=%@&likecheck=2",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],rallyIdStrFor_likes];
       urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addlikes.php?userid=%@&activityid=%@&likecheck=2",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],rallyIdStrFor_likes];
  }
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved_rallyLikes:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved_rallyLikes:(NSDictionary *)response
{
   
    NSDictionary *Dict = response;
  // NSLog(@"response12%@",response);
   
  

   switch ([Dict[kRAPIResult] integerValue])
    
    {
            case APIResponseStatus_RallyLikeViewFailed:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"like rally Failed!...please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }
        case APIResponseStatus_RallyLikeViewSuccessfully:
        {
                         //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Hub View Successfull!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            
            break;
        }
        case APIResponseStatus_RallyLikeViewSuccessfullWithLike:
        {
           
            if ([[NSUserDefaults standardUserDefaults]integerForKey:@"OnHubPage"]==0)
            {
            [self callWebService];
            }
            else
            {
              [self callWebService_aroundYou];
            }
            [hubTableview reloadData];
            [self.everyOneTableView reloadData];
            break;
        }
        case APIResponseStatus_RallyLikeViewFailedWithLike:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"like rally Failed!...please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }

        case APIResponseStatus_RallyLikeViewSuccessfullWithDislike:
        {
            if ([[NSUserDefaults standardUserDefaults]integerForKey:@"OnHubPage"]==0)
            {
            [self callWebService];
            }
            else
            {
            [self callWebService_aroundYou];
            }
            [hubTableview reloadData];
            [self.everyOneTableView reloadData];
            break;
        }

        case APIResponseStatus_RallyLikeViewFailedWithDislike:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"like rally Failed!...please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            
            break;
        }


        default:
            
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if ([[[sorteddataArray objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"2"])
    {
    return 150;
    }
    else
    
    return 151;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)displayAlertView1
{
    alert3 =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have already joined this Private Rally. Would you like to view it?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    // alert1.tag =1;
    [alert3 show];
    
}
-(void)displayAlertView
{
    alert1 =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This is a private Rally, would you like to send a request to join?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
   // alert1.tag =1;
    [alert1 show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    if (alertView == alert1)
    {
        if (buttonIndex == 0)
        {
            [self callWebSercvice_sendRallyRequest];
            
        }
    }
    if (alertView == alert2)
    {
        if (buttonIndex == 0)
        {
            RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
            
            // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
            //JoinRallyViewController.hubIDStr=rallyIdStr;
            JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:buttonIndex1];
            [self.navigationController pushViewController:JoinRallyViewController animated:NO];

        }
       
    }
    if (alertView == alert3)
    {
        if (buttonIndex == 0)
        {
        RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
        
        // NSString *  rallyIdStr = [NSString stringWithFormat:@"%@",[[sorteddataArray objectAtIndex:rowNumber ] objectForKey:@"rallyID"]];
        //JoinRallyViewController.hubIDStr=rallyIdStr;
        JoinRallyViewController.hubRallyDic=[sorteddataArray objectAtIndex:buttonIndex1];
        [self.navigationController pushViewController:JoinRallyViewController animated:NO];
        }
    }

}

-(void)callWebSercvice_sendRallyRequest
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    
    NSString * urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/sendrallyrequest.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],rallyIdStr];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_sendRallyRequest:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}

-(void)JSONRecieved_sendRallyRequest:(NSDictionary *)response
{
//    NSLog(@"%ld",(long)buttonIndex1);
    NSDictionary *dict = response;
    //NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
    {
        case  APIResponseStatus_ReuestSentSuccessfully:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"Request has been Sent Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case APIResponseStatus_ReuestFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Request Failed. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_RallyNotPrivate:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"This Rally is not a Private Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case   APIResponseStatus_RallyRequestAlreadySent:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have already requested to join this Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
            
        case   APIResponseStatus_UserAlreadyJoinedRally:
            
        {
             alert2 =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have already join this Rally. Would you like to see this Rally?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert2 show];
            break;
            
        }

            
        default:
            
            break;
            
    }
 }
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
//    int index = [navigationController.viewControllers indexOfObject:viewController];
//    self.pageControl.currentPage = index;
}
- (IBAction)myPackTableViewAction:(id)sender{
    if ( self.hubTableview.hidden == YES )
    {
        self.hubTableview.hidden = NO;
        self.everyOneTableView.hidden = YES;
        [self.myPackBtn setBackgroundImage:[UIImage imageNamed:@"green_button.png"] forState:UIControlStateNormal];
        [self.myPackImg setImage:[UIImage imageNamed:@"pack.png"]];
        [self.aroundYouBtn setBackgroundImage:[UIImage imageNamed:@"aruound_btn.png"] forState:UIControlStateNormal];
        [self.aroundyouImg setImage:[UIImage imageNamed:@"around-you.png"]];
        [self.myPackLbl setTextColor:[UIColor whiteColor]];
        [self.aroundyouLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
        
        self.hubTableview.alpha = 0.0f;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.hubTableview.alpha = 1.0f;
        [UIView commitAnimations];
    }
}
- (IBAction)aroundYouTableViewAction:(id)sender{
    if ( self.everyOneTableView.hidden == YES )
    {
        [self.myPackBtn setBackgroundImage:[UIImage imageNamed:@"aruound_btn.png"] forState:UIControlStateNormal];
        [self.myPackImg setImage:[UIImage imageNamed:@"pack-2.png"]];
        [self.aroundYouBtn setBackgroundImage:[UIImage imageNamed:@"green_button.png"] forState:UIControlStateNormal];
        [self.aroundyouImg setImage:[UIImage imageNamed:@"around-you-2.png"]];
        [self.aroundyouLbl setTextColor:[UIColor whiteColor]];
        [self.myPackLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
        self.hubTableview.hidden = YES;
        self.everyOneTableView.hidden = NO;
        self.everyOneTableView.alpha = 0.0f;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.everyOneTableView.alpha = 1.0f;
        [UIView commitAnimations];
    }
}

//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    int index = [navigationController.viewControllers indexOfObject:viewController];
//    self.pageControllerObj.currentPage = index;
//}

@end
