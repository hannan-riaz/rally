//
//  RLeaderBoardViewController.h
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLeaderBoardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *leaderTableview;

@end
