//
//  RImageViewController.m
//  Rally
//
//  Created by Ambika on 6/7/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RImageViewController.h"
#import "RTagCollectionViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "RFullImageViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
@interface RImageViewController ()
{
    NSMutableArray *fileList;
    RTagCollectionViewCell *sizingCell;
    MBProgressHUD *ProgressHUD;
    NSMutableArray * rallyImgArr;
    UIBarButtonItem *rightBarButton;
    UIBarButtonItem *rightTrashButton;
    UIBarButtonItem *cancelBarButton;
    BOOL showDeleteBtn;
    UIButton *checkBox;
    UIImageView *UntickImage;
    UICollectionViewCell *cell;
    NSMutableArray *selecteIndexArray;
    AsyncImageView * imageViw;
    
    UILabel *lblTitle;
}
@property(nonatomic, strong) NSArray *assets;
@end

@implementation RImageViewController
@synthesize imageCollectionView,friendUserId_serch,rallyid_str,albumId_str,albumname_str;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    selecteIndexArray = [[NSMutableArray alloc]init];
    rallyImgArr = [[NSMutableArray alloc]init];
    [imageCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
   // UINib *cellNib = [UINib nibWithNibName:@"RTagCollectionViewCell" bundle:nil];
    
	//[imageCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"TagCell"];
    // get a cell as template for sizing
	//sizingCell = [[cellNib instantiateWithOwner:nil options:nil] objectAtIndex:0];
   

   
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
   
}

-(void)callWebService_getalbumimages
{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getalbumimages.php?rallyid=%@&albumid=%@&userid=%@",rallyid_str,albumId_str,friendUserId_serch];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved_getalbumimages:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Error", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}


-(void)JSONRecieved_getalbumimages:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
   
    if ([dict objectForKey:@"albumimages"]== [NSNull null]  )
    {
        
    }
    else
    {
        rallyImgArr =[dict objectForKey:@"albumimages"];
        if ([rallyImgArr count ]== 0)
        {
            UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(120, 240, 80, 40)];
            lbl.text = @"No Photo";
            lbl.textColor = [UIColor whiteColor];
            [self.view addSubview:lbl];
            
        }
        else
        {
            [imageCollectionView reloadData];
        }
    }

    switch ([dict[kRAPIResult] integerValue])
    
    {
       case  APIResponseStatus_GetAlbumImagesFailed:
            
        {
              UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Somthing went wrong.Please Try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
               [alert show];
            
            
            break;
        }
        case  APIResponseStatus_GetAlbumImagesSuccessfull:
        {
            
                        break;
        }
            
            
        default:
            break;
            
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    showDeleteBtn = NO;
     [self callWebService_getalbumimages];
    
    self.navigationItem.title = nil;
    lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
   // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    lblTitle.font = [UIFont boldSystemFontOfSize:17.0];
    //[lblTitle sizeToFit];
    self.navigationItem.titleView = lblTitle;

    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    NSString * phptoname  = [NSString stringWithFormat:@"%@",albumname_str];
    phptoname = [phptoname  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSData *data = [phptoname dataUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedata = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    
    if (encodedata == nil || [encodedata isEqualToString:@""])
    {
        lblTitle.text = phptoname;
    }
    else
    {
        
         lblTitle.text = encodedata;
    }

   
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:friendUserId_serch])
    {
    rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(deleteAlbumBtn:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
//    self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        
        
        self.navigationController.navigationBar.hidden = NO;
        //    self.navigationItem.title = @"Sign Up";
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        
        
        
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        
        UILabel *lblTitle1 = [[UILabel alloc] init];
        lblTitle1.frame = CGRectMake(0,0, 220, 40) ;
        // lblTitle.text =nameStr;
        lblTitle1.textAlignment = NSTextAlignmentCenter;
        lblTitle1.numberOfLines = 0;
        //lblTitle.backgroundColor = [UIColor redColor];
        lblTitle1.textColor = [UIColor whiteColor];
        [lblTitle1 setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
        //[lblTitle sizeToFit];
//        lblTitle1.text = @"Photo Albums";
        self.navigationItem.titleView = lblTitle1;
  
    }
    
}
-(void)deleteAlbumBtn:(id)sender
{
    UIImage *trashImage = [UIImage imageNamed:@"trashGreen.png"];
    UIButton *trashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    trashBtn.bounds = CGRectMake( 20, 0, 30, 28 );
    [trashBtn addTarget:self action:@selector(trashBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [trashBtn setImage:trashImage forState:UIControlStateNormal];
    rightTrashButton = [[UIBarButtonItem alloc] initWithCustomView:trashBtn];
    self.navigationItem.rightBarButtonItem = rightTrashButton;
    
    cancelBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelLeftBtn:)];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
    self.navigationItem.leftBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];

    showDeleteBtn = YES;
   [imageCollectionView reloadData];
    
}
-(void)trashBtn1:(id)sender
{
    [self  callWebSercvice_DeleteAalbum];
    //showDeleteBtn = NO;
    //[albunTableView reloadData];
}

-(void)cancelLeftBtn:(id)sender
{
    [selecteIndexArray removeAllObjects];
     showDeleteBtn = NO;
    for (UIView *sub in imageCollectionView.subviews)
    {
        if ([sub isKindOfClass:[UICollectionViewCell class]])
        {
            
            UICollectionViewCell *cell1 = (UICollectionViewCell*)sub;
            for (UIView *view in cell1.contentView.subviews) {
                if ([view isKindOfClass:[UIButton class]] ||[view isKindOfClass:[UIImageView class]] ) {
                    [view removeFromSuperview];
                }
            }
            
        }
        
    }
   
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    UIBarButtonItem *  rightBarButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(deleteAlbumBtn:)];
    self.navigationItem.rightBarButtonItem = rightBarButton1;
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [imageCollectionView reloadData];
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
   
    return [rallyImgArr count];
}
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    imageViw =nil;
    checkBox = nil;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    imageViw = [[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,cell.frame.size.width,cell.frame.size.height)];
     imageViw.userInteractionEnabled = YES;
    imageViw.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[rallyImgArr objectAtIndex:indexPath.row] objectForKey:@"imagename"]];
    NSURL *imageURL = [NSURL URLWithString:imgStr];

    [cell.contentView addSubview:imageViw];
    
    imageViw.imageURL = imageURL;
    if (showDeleteBtn == YES)
    {
        cell.tag = [[[rallyImgArr objectAtIndex:indexPath.row] objectForKey:@"imageid"]integerValue];
        UntickImage = [[UIImageView alloc]init];
        UntickImage.frame = CGRectMake(76,94, 22,22);
        UntickImage.userInteractionEnabled = YES;
        UntickImage.tag = -8;
        UntickImage.image = [UIImage imageNamed:@"untick.png"] ;
        [cell.contentView addSubview:UntickImage];
        
        checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
        checkBox.frame = CGRectMake(73,91, 25,25);
        checkBox.userInteractionEnabled = YES;

        checkBox.tag = -9;
        //[checkBox setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [checkBox addTarget:self action:@selector(CheckBoxPressed:) forControlEvents:UIControlEventTouchUpInside ];
        // btn.tag = indexPath.row;
        //[checkBox addTarget:self action:@selector(myClickEvent:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:checkBox];
        
    }
    
    // cell.backgroundColor = [UIColor redColor];
    
    return cell;
    
}

-(void)CheckBoxPressed:(UIButton *) sender
{
 
    
    CGRect buttonFrameInTableView = [sender convertRect:checkBox.bounds toView:imageCollectionView];
    NSIndexPath *indexPath = [imageCollectionView indexPathForItemAtPoint:buttonFrameInTableView.origin];
    UICollectionViewCell *cell1 = [imageCollectionView cellForItemAtIndexPath:indexPath];
    NSInteger rowNumber = 0;
    rowNumber += indexPath.row;
    // UIButton * bt = (UIButton*)[cell1 viewWithTag:-9];
    
    //[bt setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    if (![sender isSelected])
    {
        UIImageView * checkImage = (UIImageView*)[cell1 viewWithTag:-8];
       checkImage.image  = [UIImage imageNamed:@"tick.png"];
      //  checkImage.backgroundColor  = [UIColor redColor];
        //[bt setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        NSString * selectedRow = [NSString stringWithFormat:@"%ld",(long)[cell1 tag]];
        [selecteIndexArray addObject:selectedRow];
        
        [sender setSelected:YES];
    }
    else
    {
        UIImageView * checkImage = (UIImageView*)[cell1 viewWithTag:-8];
        checkImage.image  = [UIImage imageNamed:@"untick.png"];
        // checkImage.backgroundColor  = [UIColor whiteColor];
        //[bt setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        NSString * selectedRow = [NSString stringWithFormat:@"%ld",(long)[cell1 tag]];
        [selecteIndexArray removeObject:selectedRow];
                // UntickImage.image = [UIImage imageNamed:@"untick.png"] ;
        [sender setSelected:NO];
    }
    
}
-(void)callWebSercvice_DeleteAalbum
{
    
    NSString * urlStr;
    NSString* ImageIdArray = [selecteIndexArray componentsJoinedByString:@","];

    
      urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterallyimage.php?imageid=%@&albumid=%@",ImageIdArray,albumId_str];
   
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_DeleteAalbum:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_DeleteAalbum:(NSDictionary *)response
{
    NSDictionary *dict = response;
    // NSLog(@"response12%@",response);
    
   
    switch ([dict[kRAPIResult] integerValue])
    {
            
        case APIResponseStatus_RallyImageDeletedSuccessfully:
        {
            
            [self callWebService_getalbumimages];
            [imageCollectionView reloadData];
            break;
        }
        case APIResponseStatus_DeleteRallyImageFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_FriendNotInPack:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This User is not in your pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        default:
            break;
            
    }
}

/*- (void)_configureCell:(RTagCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    //cell.profileImgView.activityIndicatorStyle = UIActivityIndicatorViewStyleWhite;
    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/rallythumb2/%@",[[rallyImgArr objectAtIndex:indexPath.row] objectForKey:@"imagename"]];
    NSURL *imageURL = [NSURL URLWithString:imgStr];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
    
    cell.profileImgView.image = imageLoad;
    UIButton *checkBox = nil;
    //UIButton *btn = nil;
    //[cell.checkBox setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    

    if (showDeleteBtn == YES)
    {
                checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
        checkBox.frame = CGRectMake(0,cell.profileImgView.frame.origin.y+5, cell.profileImgView.frame.size.width,cell.profileImgView.frame.size.height);
        [cell.checkBox setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [checkBox addTarget:self action:@selector(CheckBoxPressed:) forControlEvents:UIControlEventTouchUpInside];
        // btn.tag = indexPath.row;
        [cell.checkBox addSubview:checkBox];

            }
    else
    {
       // [checkBox removeFromSuperview];
        checkBox.hidden = YES;
        btn = [[UIButton alloc]initWithFrame:CGRectMake(cell.profileImgView.frame.origin.x,cell.profileImgView.frame.origin.y, cell.profileImgView.frame.size.width,cell.profileImgView.frame.size.height)];
        [btn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.profileImgView addSubview:btn];
    }
    
    
		
}*/

/*- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  //  RTagCollectionViewCell *cell = [imageCollectionView dequeueReusableCellWithReuseIdentifier:@"TagCell" forIndexPath:indexPath];
    
   
   // [self _configureCell:cell forIndexPath:indexPath];
    
    static NSString *cellIdentifier = @"Cell";
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc]init];
    }
 
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}*/


/*- (IBAction)myClickEvent:(id)sender event:(id)event {
    
    NSSet *touches = [event allTouches];
    
    UITouch *touch = [touches anyObject];
    
    CGPoint currentTouchPosition = [touch locationInView:imageCollectionView];
    
    NSIndexPath *indexPath = [imageCollectionView indexPathForItemAtPoint: currentTouchPosition];
     NSLog(@"indexPath%ld",(long)indexPath.item);
    UIImageView * iam =  (UIImageView*)[cell viewWithTag:indexPath.item];
    UntickImage.image = [UIImage imageNamed:@"tick.png"] ;
}*/

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (showDeleteBtn == NO)
    {
        RFullImageViewController * FullImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
        FullImageViewController.rallyImagesDic = [rallyImgArr objectAtIndex:indexPath.row];
        
        FullImageViewController.index = indexPath.row;
        FullImageViewController.friendUserId_Image = friendUserId_serch;
        FullImageViewController.albumIdString = albumId_str;
        FullImageViewController.albumDataArray = rallyImgArr;
        FullImageViewController.rallyId_string = rallyid_str;
        FullImageViewController.userid_str = friendUserId_serch;
        [self.navigationController pushViewController:FullImageViewController animated:YES];
    }
    else
        
    {
//    UICollectionViewCell *cell1 = [collectionView cellForItemAtIndexPath:indexPath];
//    UIButton * bt = (UIButton*)[cell1 viewWithTag:indexPath.row];
//    [bt setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    //selectedPhotoIndex = indexPath.row;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
