//
//  RTagCollectionViewCell.h
//  Rally
//
//  Created by Ambika on 6/7/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface RTagCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet AsyncImageView *profileImgView;
@property (strong, nonatomic) IBOutlet UIButton *checkBox;


@end
