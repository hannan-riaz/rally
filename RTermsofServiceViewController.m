//
//  RTermsofServiceViewController.m
//  Rally
//
//  Created by Ambika on 9/11/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RTermsofServiceViewController.h"

@interface RTermsofServiceViewController ()

@end

@implementation RTermsofServiceViewController
@synthesize termsWebViw;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [termsWebViw loadHTMLString:[NSString stringWithFormat:@"<html><body p style='color:clear' text=\"#FFFFFF\" face=\"Chaparral-Display\" size=\"15\">%@</body></html>",DiscriptionStrg] baseURL:nil];
    
    [termsWebViw setBackgroundColor:[UIColor clearColor]];
    [termsWebViw setOpaque:NO];
    // [self.web_itinery loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Itinerary" ofType:@"html"]isDirectory:NO]]];
    NSString *documentsDr =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileString;
    
    
    fileString = [documentsDr stringByAppendingPathComponent:@"terms.html"];
    [termsWebViw loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fileString]]];
    //[termsWebViw setHidesWhenStopped:YES];

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationItem.title = @"Terms of Service";
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];

}


-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
