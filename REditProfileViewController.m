//
//  REditProfileViewController.m
//  Rally
//
//  Created by Ambika on 4/8/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "REditProfileViewController.h"
#import  "REditPackViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RHomeViewController.h"
#import "RMoreViewController.h"
#import "RAppDelegate.h"

@interface REditProfileViewController ()

{
    MBProgressHUD *ProgressHUD;
    UIView * pickerView;
    UIButton *closeBtn;
    UIButton  *doneButton;
    UIDatePicker *datePicker;
    NSString * pickerdate_str;
    NSDictionary * userInfoDic;
    int checkimagePicker;
    BOOL keyboardApear;
}
@end

@implementation REditProfileViewController
@synthesize profilePicView,emailView,BioView,schoolTextView,HomeTextView,BioTextField,emailTextField,passwordTextField,cnfrmTextField,photoButton,saveBtn,profileScrollView,dobTxt,userInfoDictionary,profileImageView,editUserNameTXt;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    RAppDelegate *delegate1=(RAppDelegate *)[[UIApplication sharedApplication]delegate];
    checkimagePicker = 0;
     profileImageView.layer.cornerRadius =profileImageView.bounds.size.width / 2.0;
     profileImageView.layer.masksToBounds = YES;
    
    self.BioTextField.delegate=self;
    
    [[delegate1 window]addSubview:self.toolbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
   
     //    NSLog(@"%@",userInfoDictionary);
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    [self becomeFirstResponder];
    if([BioTextField.text length]>0 && ![BioTextField.text isEqualToString:@"Bio"])
    {
        [BioTextField setTextColor:[UIColor blackColor]];
    }
    else
    {
        [BioTextField setTextColor:[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0]];
    }
    keyboardApear = NO;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        emailTextField.userInteractionEnabled    = NO;
        passwordTextField.userInteractionEnabled = NO;
        cnfrmTextField.userInteractionEnabled    = NO;
        emailView.hidden = YES;
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        emailTextField.userInteractionEnabled    = NO;
        passwordTextField.userInteractionEnabled = NO;
        cnfrmTextField.userInteractionEnabled    = NO;
        emailView.hidden = YES;
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
    {
        
        emailTextField.userInteractionEnabled    = NO;
        passwordTextField.userInteractionEnabled = NO;
        cnfrmTextField.userInteractionEnabled    = NO;
        emailView.hidden = YES;
    }
    else
    {
        emailView.hidden = NO;
        emailTextField.userInteractionEnabled    = NO;
        passwordTextField.userInteractionEnabled = YES;
        cnfrmTextField.userInteractionEnabled    = YES;

    }

   // userInfoDictionary = [[NSDictionary alloc]init];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Edit Profile";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
   //self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"arrow.png"];
   // self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"arrow.png"];
//   // [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"arrow.png"]
//                                                      forState:UIControlStateNormal
//                                                    barMetrics:UIBarMetricsDefault];
    
    
//    UIImage *faceImage = [UIImage imageNamed:@"edit.png"];
//    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
//    face.bounds = CGRectMake( 10, 0, 60, 25 );
//    [face addTarget:self action:@selector(editPackaction:) forControlEvents:UIControlEventTouchUpInside];
//    [face setImage:faceImage forState:UIControlStateNormal];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
//    self.navigationItem.rightBarButtonItem = backButton;
    profileImageView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    profilePicView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0];
    BioView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0];
    emailView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0];
    profilePicView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    profilePicView.layer.borderWidth= 1.5f;
    BioView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    BioView.layer.borderWidth= 1.5f;
    emailView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    emailView.layer.borderWidth= 1.5f;
    
   // BioTextField.layer.borderColor = [[UIColor lightGrayColor]CGColor];
   // BioTextField.layer.borderWidth= 0.5f;

    
    UIView *UserNameline = [[UIView alloc] initWithFrame:CGRectMake(0,BioView.frame.size.height-256,BioView.frame.size.width, 1.2)];
    [UserNameline setBackgroundColor:[UIColor lightGrayColor]];
    [BioView addSubview:UserNameline];
    
      UIView *bioline = [[UIView alloc] initWithFrame:CGRectMake(0,BioView.frame.size.height-115,BioView.frame.size.width, 1.2)];
    [bioline setBackgroundColor:[UIColor lightGrayColor]];
    [BioView addSubview:bioline];
    UIView *dobline = [[UIView alloc] initWithFrame:CGRectMake(0,BioView.frame.size.height-208,BioView.frame.size.width, 1.2)];
    [dobline setBackgroundColor:[UIColor lightGrayColor]];
    [BioView addSubview:dobline];


    UIView *homeline = [[UIView alloc] initWithFrame:CGRectMake(0,BioView.frame.size.height-160,BioView.frame.size.width, 1.2)];
    [homeline setBackgroundColor:[UIColor lightGrayColor]];
    [BioView addSubview:homeline];
    
    UIView *cPassline = [[UIView alloc] initWithFrame:CGRectMake(0,emailView.frame.size.height-46,emailView.frame.size.width, 1.2)];
    [cPassline setBackgroundColor:[UIColor lightGrayColor]];
    [emailView addSubview:cPassline];
    UIView *Passline = [[UIView alloc] initWithFrame:CGRectMake(0,emailView.frame.size.height-92,emailView.frame.size.width, 1.2)];
    [Passline setBackgroundColor:[UIColor lightGrayColor]];
   // [emailView addSubview:Passline];
    photoButton.titleLabel.textColor = [UIColor grayColor];
    saveBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];

//    BioTextField.
    
    //    [self.navigationItem setHidesBackButton:YES animated:YES];
//    [self.navigationItem setLeftBarButtonItem:nil animated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    
    backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
   
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"PushFromSignUP"])
    {
        [backButton1 setHidden:TRUE];
    }
    else
    {
        [backButton1 setHidden:FALSE];
    }
    
    
    profileScrollView.scrollEnabled = YES;
    
    if (keyboardApear == NO)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
        {
            profileScrollView.contentSize = CGSizeMake(320, 500);
        }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            profileScrollView.contentSize = CGSizeMake(320, 500);
        }
        else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            profileScrollView.contentSize = CGSizeMake(320, 500);
        }
        else
        {
            profileScrollView.contentSize = CGSizeMake(320, 580);
        }
    
    }
    profileScrollView.showsVerticalScrollIndicator = NO;
    profileScrollView.showsHorizontalScrollIndicator=NO;
    profileScrollView.delegate = self;
    if ( checkimagePicker == 0)
    {
    if (userInfoDictionary != nil)
    {
        
    NSString * dobDateStr = [NSString stringWithFormat:@"%@",[userInfoDictionary objectForKey:@"userDOB"]];
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"yyyy-MM-dd"];
    NSDate * date = [formate dateFromString:dobDateStr];
    NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
    [formate1 setDateFormat:@"MM/dd/yyyy"];
    NSString * dobDateStr1 = [NSString stringWithFormat:@"%@",[formate1 stringFromDate:date]];
        if (dobDateStr1 == (id)[NSNull null]||[dobDateStr1 isEqualToString:@"(null)"])
        {
            dobTxt.placeholder=@"Birthday";
            // dobTxt.text = @"Birthday"; //Changed by swati 3Nov
        }
        else
        {
             dobTxt.text = dobDateStr1;
        }
    schoolTextView.text = [userInfoDictionary objectForKey:@"userSCHOOL"];
    HomeTextView.text = [userInfoDictionary objectForKey:@"userTOWN"];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            NSString * user_name = [NSString stringWithFormat:@"%@",[userInfoDictionary objectForKey:@"userNAME"]];
            user_name = [user_name   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
              user_name = [user_name  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            editUserNameTXt.text = user_name;
        }
        else
        { NSString * user_name = [NSString stringWithFormat:@"%@",[userInfoDictionary objectForKey:@"userNAME"]];
            user_name = [user_name  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
              user_name = [user_name  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            editUserNameTXt.text = user_name;
        }

    NSString *  bio_Str = [NSString stringWithFormat:@"%@",[userInfoDictionary objectForKey:@"userBIO"]];
    bio_Str = [bio_Str stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSData *data = [bio_Str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *bioDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

        if (bioDataEncode == nil||[bioDataEncode isEqualToString:@""])
        {
             BioTextField.text = @"Bio"; //changed
            [BioTextField setTextColor:[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0]];
        }
        else
        {
             BioTextField.text = bioDataEncode;
            [BioTextField setTextColor:[UIColor blackColor]];
            
        }
   
    emailTextField.text = [userInfoDictionary objectForKey:@"userEMAIL"];
    passwordTextField.text = [userInfoDictionary objectForKey:@"userPASSWORD"];
    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userInfoDictionary objectForKey:@"userIMAGE"]];
    NSURL *imageURL = [NSURL URLWithString:imgStr];
   // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
   // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
    profileImageView.imageURL = imageURL;
    }
}
    //profileCircleImage.image = imageLoad;

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"updateImage"])
    {
        NSData *imageData = [NSData dataWithData:[[NSUserDefaults standardUserDefaults]objectForKey:@"updateImage"]];
        UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        profileImageView.image = imageLoad;
        
    }
}
//- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
//{
//    
//    [growingtextView resignFirstResponder];
//    
//}

-(void)popToView
{
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"PushFromSignUP"])
    {
        [self.navigationController popViewControllerAnimated:YES];
//        RMoreViewController *MoreViewController = [[RMoreViewController alloc]initWithNibName:@"RMoreViewController" bundle:nil];
//        
//       [self.navigationController pushViewController:MoreViewController animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = BioTextField.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}*/
- (IBAction)SaveAction:(id)sender
{
    if ([self.editUserNameTXt.text length]>0 && [self.schoolTextView.text length]>0 && [self.HomeTextView.text length]>0 && ![self.BioTextField.text isEqualToString:@"Bio"] && [BioTextField.text length]>0)
    {
        if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
        
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        
        NSString *HomeTextViewStr = [NSString stringWithFormat:@"%@",HomeTextView.text];
        HomeTextViewStr = [HomeTextViewStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        
        
        //  NSString * goodMsg1 = [goodMsg stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
        
        
        NSString *schoolTextViewStr = [NSString stringWithFormat:@"%@",schoolTextView.text];
        schoolTextViewStr = [schoolTextViewStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *BioTextFieldStr = [NSString stringWithFormat:@"%@",BioTextField.text];
        BioTextFieldStr = [BioTextFieldStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *editUserNameStr = [NSString stringWithFormat:@"%@",editUserNameTXt.text];
        editUserNameStr = [editUserNameStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *uniText = [NSString stringWithUTF8String:[BioTextFieldStr UTF8String]];
        NSData *goodMsg = [uniText dataUsingEncoding:NSUTF8StringEncoding];
        
        
        // NSString *goodMsg1 = [[NSString alloc] initWithData:goodMsg encoding:NSUTF8StringEncoding] ;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"id"];
        
        NSString * dobDateStr = [NSString stringWithFormat:@"%@",dobTxt.text];
        NSDateFormatter *formate=[[NSDateFormatter alloc]init];
        [formate setDateFormat:@"MM/dd/yyyy"];
        NSDate * date = [formate dateFromString:dobDateStr];
        NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
        [formate1 setDateFormat:@"yyyy-MM-dd"];
        NSString * dobDateStr1 = [NSString stringWithFormat:@"%@",[formate1 stringFromDate:date]];
        
        NSString *idStr = [NSString stringWithFormat:@"%@", [defaults objectForKey:@"id"]];
        NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/updateprofile.php?userid=%@&email=%@&password=%@&town=%@&school=%@&updatedate=%@&image=1&dob=%@&username=%@",idStr,emailTextField.text,passwordTextField.text,HomeTextViewStr,schoolTextViewStr,dateStr,dobDateStr1,editUserNameStr];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        UIImage *image=profileImageView.image;
        NSData *imageData =UIImageJPEGRepresentation(image, 0.1);
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // boi text
        //NSData *req=[NSData dataWithBytes:[goodMsg1 UTF8String] length:[goodMsg1 length]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"bio\";"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:goodMsg]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self JSONRecieved_UpdateProfie:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                             ProgressHUD = nil;
                                             kRAlert(@"Whoops",error.localizedDescription);                                     }];
        
        [[NSOperationQueue mainQueue] addOperation:operation];

    }
    else
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"All fields need to be completed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
   
}

-(void)JSONRecieved_UpdateProfie:(NSDictionary *)response
{
    
    
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
   // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
    
    {        case APIResponseStatus_EmailRequired:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Please enter email Id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            emailTextField.userInteractionEnabled = YES;
            break;
        }
        case  APIResponseStatus_UpdateSuccessfull:
        {
           [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImageView.image) forKey:@"updateImage"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if ([[NSUserDefaults standardUserDefaults]boolForKey:@"PushFromSignUP"])
            {
                [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"PushFromSignUP"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                
                [self.navigationController pushViewController:HomeViewController animated:NO];
                
                
            }
            else
            {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"Changes successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.tag=5000;
            [alert show];
           
            //[self.navigationController popViewControllerAnimated:YES];
            }
            break;
        }
        case APIResponseStatus_PasswordRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Password Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_UpdateFailed:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }

        case  APIResponseStatus_UpdateDateRequired:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
        case  APIResponseStatusEmailInUse:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }

       
            
    

        default:
            
            break;
            
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==5000)
    {
        if (buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

- (IBAction)dobAction:(id)sender
{
    self.toolbar.hidden=YES;
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [cnfrmTextField resignFirstResponder];
    [schoolTextView resignFirstResponder];
    [dobTxt resignFirstResponder];
    [HomeTextView resignFirstResponder];
    [BioTextField resignFirstResponder];
    pickerView = [[UIView alloc]initWithFrame: CGRectMake(35,80,250,250)];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.layer.cornerRadius = 5.0f;
    pickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    pickerView.layer.borderWidth = 1.0f;

    pickerView.userInteractionEnabled = YES;
    datePicker = [[UIDatePicker alloc] init];
    datePicker.frame =CGRectMake(0,25,pickerView.frame.size.width,pickerView.frame.size.height-50);
    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [pickerView addSubview:datePicker];
    [self.view addSubview:pickerView];
    
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    closeBtn.frame = CGRectMake(5, 5, 24, 24);
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    [pickerView addSubview:closeBtn];
       
    doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [doneButton addTarget:self  action:@selector(doneButtonAction:)  forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
    doneButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 5.0f;

    doneButton.frame = CGRectMake(85, 207, 80, 30);
    [pickerView addSubview:doneButton];
    
}

- (IBAction)editPhotoAction:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Camera", @"Photo Album", nil];
    [actionSheet showInView:self.view];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}
- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        controller = nil;
    }
}
-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        controller = nil;
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    // NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    checkimagePicker = 1;
    profileImageView.image = image;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    checkimagePicker = 1;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)CloseAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    
}
- (void)doneButtonAction:(id)sender
{
    dobTxt.text = pickerdate_str;
    [pickerView removeFromSuperview];
    
}
- (void)datepickerChanged:(id)sender
{
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM/dd/yyyy"];
//    NSLog(@"value: %@",[formate stringFromDate:datePicker.date]);
    pickerdate_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 500);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 500);
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 500);
    }
    else
    {
        profileScrollView.contentSize = CGSizeMake(320, 580);
    }
    
    if (textField==self.schoolTextView)
    {
//         [self.BioTextField becomeFirstResponder];
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.5];
//        [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,-50,self.BioView.frame.size.width,self.BioView.frame.size.height)];
//        [UIView commitAnimations];

        
        [self.schoolTextView resignFirstResponder];
    }
    else if (textField==self.HomeTextView)
    {
        [self.HomeTextView resignFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,94,self.BioView.frame.size.width,self.BioView.frame.size.height)];
        [UIView commitAnimations];

       // [self.BioTextField becomeFirstResponder];
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.5];
//        [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,-10,self.BioView.frame.size.width,self.BioView.frame.size.height)];
//        [UIView commitAnimations];
       
    }
    else
    {
       [textField resignFirstResponder];
    }
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    //[self.view endEditing:YES];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
//    self.toolbar.hidden=NO;
    NSLog(@"Toolbar %d",self.toolbar.hidden);
    NSDictionary* info = [aNotification userInfo];
    CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (kbRect.size.height==253 || kbRect.size.height==258 )
    {
        
        if (IS_IPHONE_5)
        {
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,272,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
        }
        else
        {
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,178,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
        }
        
    }
    else if (kbRect.size.height==224 || kbRect.size.height==225)
    {
        if (IS_IPHONE_5)
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,300,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,210,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
    }
    else
    {
        
        
        if (IS_IPHONE_5)
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,310,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,220,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
    }
    
}
-(IBAction)closeBtnAction:(id)sender
{
    //self.stratRallyBtn.userInteractionEnabled=TRUE;
    if ([BioTextField.text length]==0)
    {
        BioTextField.text=@"Bio";
        BioTextField.textColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0];
    }
    self.toolbar.hidden=YES;
    [BioTextField resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,94,self.BioView.frame.size.width,self.BioView.frame.size.height)];
    [UIView commitAnimations];

//    [addressTxt resignFirstResponder];
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
//    self.toolbar.hidden=YES;
    
    //    NSDictionary* info = [aNotification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //    [UIView animateWithDuration:0.2f animations:^{
    //        CGRect frame = self.toolbar.frame;
    //        frame.origin.y += kbSize.height;
    //        self.toolbar.frame = frame;
    //       
    //    }];
}

/*- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([BioTextField isFirstResponder] && [touch view] != BioTextField) {
        [BioTextField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(int)up
{
    const int movementDistance = -20; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
      return  YES;
}*/
#pragma mark - ---------- TextFieldDelegate methods ------------

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    //[profileScrollView setContentOffset:CGPointMake(0,-50)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 660);

    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 660);

    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 660);

    }
    else
    {
        profileScrollView.contentSize = CGSizeMake(320, 800);
    }
    
//    if (textField==self.schoolTextView)
//    {
//        profileScrollView.contentOffset=CGPointMake(0,-30);
//    }
//    else if (textField==self.HomeTextView)
//    {
//        profileScrollView.contentOffset=CGPointMake(0,-50);
//    }
}
- (void)textViewDidChange:(UITextView *)textView
{
        NSLog(@"text %@",textView.text);
    
        NSString *textViewStr = [textView.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
        if([textViewStr isEqualToString:@""] || [textViewStr length]==0 || [textViewStr isEqualToString:@" "])
        {
            BioTextField.text=@"Bio";
            BioTextField.textColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0];
            [BioTextField resignFirstResponder];
            self.toolbar.hidden=YES;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,94,self.BioView.frame.size.width,self.BioView.frame.size.height)];
            [UIView commitAnimations];
        }
        else if([textViewStr isEqualToString:@"Bio"])
        {
            BioTextField.text=@"";
            BioTextField.textColor=[UIColor blackColor];
        }
        else if ([textViewStr length]>0)
        {
           // [BioTextField becomeFirstResponder];
        }
//        else
//        {
//            BioTextField.text=@"";
//            BioTextField.textColor=[UIColor blackColor];
//        }
    

//    if([BioTextField.text length]==0)
//    {
//        BioTextField.text=@"Bio";
//        BioTextField.textColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0];
//    }
    
}
//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
//{
//    if(textView.bounces == NO)
//    {
//        [textView setText:@"Bio"];
//        //[textView setTextColor:[UIColor blackColor]];
//        [textView setBounces:YES]; //YES for non-placeholder text
//    }
//    return YES;
//}




//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if ([text isEqualToString:@"\n"])
//    {
//        
//       // [textView resignFirstResponder];
//        // Return FALSE so that the final '\n' character doesn't get added
//        return NO;
//    }
//    return YES;
////
//}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([BioTextField.text isEqualToString:@""])
    {
        BioTextField.text = @"Bio"; //changed
        [BioTextField setTextColor:[UIColor colorWithRed:189/255.0f green:188/255.0f blue:194/255.0f alpha:1.0]];
    }

}
- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    self.toolbar.hidden=NO;
    if([BioTextField.text isEqualToString:@"Bio"])
    {
       BioTextField.text=@"";
    }
    BioTextField.textColor=[UIColor blackColor];
   // [profileScrollView setContentOffset:CGPointMake(0,-50)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 660);
        
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        profileScrollView.contentSize = CGSizeMake(320, 660);
        
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
    {
        profileScrollView.contentSize = CGSizeMake(320,  660);
        
    }
    else
    {
        profileScrollView.contentSize = CGSizeMake(320, 800);
    }
    
   if (textView==self.BioTextField)
   {
       [UIView beginAnimations:nil context:NULL];
       [UIView setAnimationDuration:0.5];
       [self.BioView setFrame:CGRectMake(self.BioView.frame.origin.x,-70,self.BioView.frame.size.width,self.BioView.frame.size.height)];
       [UIView commitAnimations];
   }
    //[BioTextField becomeFirstResponder];
    
}

/*- (void)textFieldDidEndEditing:(UITextField *)textField
{
   // [self animateTextField:textField up:10];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [self animateTextField:textField up:0];
    [textField resignFirstResponder];
    return YES;
}
-(void)animateTextField:(UITextField*)textField up:(int)up
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -=up;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];

}
*/
- (IBAction)endEditingPressed:(id)sender
{
    
   // [self.view endEditing:YES];
    
}
@end
