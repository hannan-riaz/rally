//
//  REditProfileViewController.h
//  Rally
//
//  Created by Ambika on 4/8/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface REditProfileViewController : UIViewController<UIScrollViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    UIButton *backButton1;
}
@property(nonatomic,strong)IBOutlet UIToolbar* toolbar;
@property (weak, nonatomic) IBOutlet UIView *profilePicView;
@property (weak, nonatomic) IBOutlet UIView *BioView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextView;
@property (weak, nonatomic) IBOutlet UITextField *HomeTextView;
@property (weak, nonatomic) IBOutlet UITextView *BioTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *cnfrmTextField;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextField *dobTxt;
@property(nonatomic,strong)IBOutlet UIBarButtonItem *crossBtn;
@property ( strong , nonatomic ) IBOutlet UITextField *locationField;


@property (strong, nonatomic) NSDictionary *userInfoDictionary;
- (IBAction)SaveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;
- (IBAction)dobAction:(id)sender;
- (IBAction)editPhotoAction:(id)sender;
@property (weak, nonatomic) IBOutlet AsyncImageView *profileImageView;
- (IBAction)endEditingPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *editUserNameTXt;

-(IBAction)closeBtnAction:(id)sender;

@end
