//
//  RStartRallyViewController.m
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RStartRallyViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "RHomeViewController.h"
#import "MBProgressHUD.h"
#import "RAppDelegate.h"

@interface RStartRallyViewController ()

{
    UIImageView *imageView1;
    UIImageView *imageView ;
    NSArray * rallyarr;
    NSString * privateStr;
    NSString *currentdate_Str;
    NSString *adressStr;
    NSString *userIdStr;
    int offsetView;
    UILabel *lblTitle;
}

@property(strong,nonatomic)MBProgressHUD *progressHUD;

@end

@implementation RStartRallyViewController
@synthesize addressLbl;
@synthesize businessScrollview,pastRScrollView,latitudeStr,longitudeStr,nameStr,addressStr,tileStr,dateStr,timeStr,addressTxt,discriptionTxt,addressView,privateValue,progressHUD,inviteUserArr,dateLbl,timeLbl,resignBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
//    self.stratRallyBtn.exclusiveTouch=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
            //pastRScrollView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height, pastRScrollView.frame.size.width, pastRScrollView.frame.size.height);
            addressView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-70);
            addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width,addressTxt.frame.size.height);
            dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
            timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
            
            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
        
        }
        else
        {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
            addressView.frame = CGRectMake(2,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-155);
            businessScrollview.frame = CGRectMake(0,0, businessScrollview.frame.size.width, businessScrollview.frame.size.height);
            
            addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width-5,addressTxt.frame.size.height);
            dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
            timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width-5,discriptionTxt.frame.size.height);

            

            

        }
    }
     else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
            //pastRScrollView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height, pastRScrollView.frame.size.width, pastRScrollView.frame.size.height);
            addressView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-70);
            addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width,addressTxt.frame.size.height);
            dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
            timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
            
            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
        }
        else
        {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
            addressView.frame = CGRectMake(2,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-155);
            businessScrollview.frame = CGRectMake(0,0, businessScrollview.frame.size.width, businessScrollview.frame.size.height);
            
            addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width-5,addressTxt.frame.size.height);
            dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
            timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width-5,discriptionTxt.frame.size.height);
            
        }
     
    }
     else
     {
         CGFloat height = [UIScreen mainScreen].bounds.size.height;
         if (height== 568)
         {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
             //pastRScrollView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height, pastRScrollView.frame.size.width, pastRScrollView.frame.size.height);
           addressView.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-70);
           addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width,addressTxt.frame.size.height);
           dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
           timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
             
           discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
         }
         else
         {
            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, _mapview.frame.size.width, _mapview.frame.size.height);
            addressView.frame = CGRectMake(2,_mapview.frame.origin.y+_mapview.frame.size.height,addressView.frame.size.width,addressView.frame.size.height-155);
            businessScrollview.frame = CGRectMake(0,0, businessScrollview.frame.size.width, businessScrollview.frame.size.height);

//             addressTxt.frame = CGRectMake(5,5,addressTxt.frame.size.width-5,addressTxt.frame.size.height);
//             dateLbl.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,dateLbl.frame.size.width,dateLbl.frame.size.height);
//             timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
//             discriptionTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,discriptionTxt.frame.size.width-5,discriptionTxt.frame.size.height);
             
             addressTxt.frame = CGRectMake(5,timeLbl.frame.origin.y+timeLbl.frame.size.height+5,addressTxt.frame.size.width-5,addressTxt.frame.size.height);
             dateLbl.frame = CGRectMake(5,5,dateLbl.frame.size.width,dateLbl.frame.size.height);
             timeLbl.frame = CGRectMake(5,dateLbl.frame.origin.y+dateLbl.frame.size.height+5,timeLbl.frame.size.width,timeLbl.frame.size.height);
             discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width-5,discriptionTxt.frame.size.height);

         }
         
     }
    

    
    _mapview.delegate = self;
    _mapview.showsUserLocation = NO;
    _mapview.userInteractionEnabled = YES;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake([latitudeStr doubleValue],
                                                       [longitudeStr doubleValue]);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([annotation coordinate], 40, 40);
    [self.mapview setRegion:region animated:YES];
    //[self.mapview selectAnnotation:annotation animated:YES];
//    if ( ([latitudeStr doubleValue] >= -90)     && ([latitudeStr doubleValue] <= 90)     && ([longitudeStr doubleValue] >= -180)     && ([longitudeStr doubleValue] <= 180))
//    {

    [self.mapview addAnnotation:annotation];
//    }
//    imageArray = [[NSArray alloc]init];
//    imageArray = @[@"image_one.png"];
//    imageArray1 = [[NSArray alloc]init];
//    imageArray1 = @[@"image_two.png",@"image_three.png",@"image_four.png"];
  
  // [self businessImg];
  
  // [self PastRallyImg];
    resignBtn.hidden = YES;
    UIImage *faceImage = [UIImage imageNamed:@"setting.png"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake( 10, 0, 30, 27 );
    //[face addTarget:self action:@selector(ChatAction1:) forControlEvents:UIControlEventTouchUpInside];
    [face setImage:faceImage forState:UIControlStateNormal];
    
    //UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
   // self.navigationItem.rightBarButtonItem = backButton;

   /* self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0,0,22.0f,18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];*/ //Added by Swati
    
   
    
    
    addressTxt.textColor = [UIColor lightGrayColor];
    timeLbl.textColor = [UIColor blackColor];
    dateLbl.textColor = [UIColor blackColor];
    discriptionTxt.textColor = [UIColor grayColor];
    addressTxt.textContainerInset = UIEdgeInsetsZero;
    addressTxt.textContainer.lineFragmentPadding = 2;
    discriptionTxt.textContainerInset = UIEdgeInsetsZero;
    discriptionTxt.textContainer.lineFragmentPadding = 6;
    discriptionTxt.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    addressTxt.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    //discriptionTxt.userInteractionEnabled =YES;
   // discriptionTxt.editable = YES;
   
    [super viewDidLoad];
}

-(void)doneBtnAction:(id)sender  //Added By Swati_7Oct
{
    [addressTxt resignFirstResponder];
    [discriptionTxt resignFirstResponder];
    
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*-(void)businessImg
{
    x = 0;
    // here give you image name is proper so we can access it by for loop.
    businessScrollview.scrollEnabled = YES;
    businessScrollview.showsVerticalScrollIndicator = NO;
    businessScrollview.showsHorizontalScrollIndicator=YES;
    businessScrollview.delegate = self;

    for (int i = 0; i <[imageArray count]; i++)
    {
        
        imageView1 = [[UIImageView alloc] initWithFrame: CGRectMake(x,0, 320,177)] ;
        [imageView1 setImage:[UIImage imageNamed:[imageArray objectAtIndex:i]]];
        imageView1.contentMode =  UIViewContentModeScaleToFill;
        [businessScrollview addSubview:imageView1];
        x = x + imageView1.frame.size.width+10;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    businessScrollview.contentSize = CGSizeMake(x,businessScrollview.frame.size.height);
   // businessScrollview.contentSize = CGSizeMake(businessScrollview.contentSize.width,imageView1.frame.size.height);
   
}*/
/*-(void)PastRallyImg
{
     y = 0;
   
    pastRScrollView.scrollEnabled = YES;
    pastRScrollView.showsVerticalScrollIndicator = YES;
    pastRScrollView.showsHorizontalScrollIndicator=NO;
    pastRScrollView.delegate = self;
  
    for (int j = 0; j <[imageArray1 count]; j++)
    {
         imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0,y, 102, 100)] ;
        [imageView setImage:[UIImage imageNamed:[imageArray1 objectAtIndex:j]]];
        //imageView.contentMode = UIViewContentModeScaleAspectFit;
        [pastRScrollView addSubview:imageView];
        y = y + imageView.frame.size.height;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
 pastRScrollView.contentSize = CGSizeMake(102, y);
}*/

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    
    self.navigationItem.hidesBackButton=TRUE;
    
    customView=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,44)];
    
    customView.backgroundColor=[UIColor clearColor];
    
    
    
//    UILabel *labelforNavigationTitle = [[UILabel alloc] initWithFrame:CGRectMake(36,5,248,40)];
    UILabel *labelforNavigationTitle = [[UILabel alloc] initWithFrame:CGRectMake(40,0,237,40)];

    labelforNavigationTitle.backgroundColor = [UIColor clearColor];
    
    labelforNavigationTitle.text =nameStr;
   
    labelforNavigationTitle.adjustsFontSizeToFitWidth=TRUE;
    
    labelforNavigationTitle.numberOfLines = 0;
   
    labelforNavigationTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
   
    labelforNavigationTitle.font = [UIFont boldSystemFontOfSize:17.0];


    labelforNavigationTitle.textAlignment = NSTextAlignmentCenter;
    
    [customView addSubview:labelforNavigationTitle];
    
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn addTarget:self action:@selector(popToView)
     
      forControlEvents:UIControlEventTouchUpInside];
    
    leftBtn.frame = CGRectMake(14,15,22,18);
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    
    [customView addSubview:leftBtn];
    
     [self.navigationController.navigationBar addSubview:customView];
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Start Rally";
    self.navigationItem.titleView = lblTitle;
   
    /*self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title =nil;
    lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0,250,40) ;
     lblTitle.text =nameStr;
    lblTitle.adjustsFontSizeToFitWidth=TRUE;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    lblTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    lblTitle.font = [UIFont boldSystemFontOfSize:17.0];
    self.navigationItem.titleView = lblTitle;*/ //added by swati
    
    
    //self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//      self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Heavy" size:10.0],NSFontAttributeName , [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] ,NSForegroundColorAttributeName,nil];
    
    
    addressTxt.text = addressStr;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    //  NSString *date = [NSString stringWithFormat:@"%@",[dateFormat dateFromString:dateStr]];
    [dateFormat setDateFormat:@"M/d/yyyy"];
    NSString *  dateStr1 = [dateFormat stringFromDate:date];
    
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDate *time = [timeFormat dateFromString:timeStr];
    // NSString *time = [NSString stringWithFormat:@"%@",[timeFormat dateFromString:timeStr]];
    [timeFormat setDateFormat:@"h:mm a"];
       NSString * timeStr1 = [timeFormat stringFromDate:time];
    dateLbl.text =[NSString stringWithFormat:@"Date: %@",dateStr1];
    timeLbl.text = [NSString stringWithFormat:@"Time: %@",timeStr1];

    UITapGestureRecognizer *lpgr = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self action:@selector(handleLongPress:)];
    // lpgr.numberOfTapsRequired = 1;
//    [addressView addGestureRecognizer:lpgr];
    
    businessScrollview.scrollEnabled = YES;
    businessScrollview.showsVerticalScrollIndicator = NO;
    businessScrollview.showsHorizontalScrollIndicator=YES;
    businessScrollview.delegate = self;
    businessScrollview.contentSize = CGSizeMake(320,900);
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    [discriptionTxt resignFirstResponder];
    [addressTxt resignFirstResponder];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [customView removeFromSuperview];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
  
}
/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier1 = @"Cell1";
    UILabel * rallyLbl;
    
    UITableViewCell *cell = [startTableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:CellIdentifier1];
        }
    rallyLbl = [[UILabel alloc]init];
    
    if(indexPath.row == 0)
    {
          cell.textLabel.text =nameStr;
    }
    if(indexPath.row == 1)
    {
        cell.textLabel.text =addressStr;
    }
    if(indexPath.row == 2)
    {
        cell.textLabel.text =dateStr;
    }
    if(indexPath.row == 3)
    {
        cell.textLabel.text =timeStr;
    }
   
   cell.textLabel.font  = [UIFont systemFontOfSize:15.0f];
    return cell;
}*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    
    annotationView.image = [UIImage imageNamed:@"r.png"];
    annotationView.annotation = annotation;
    annotationView.canShowCallout = NO;
    
    
    return annotationView;
}


- (IBAction)startRallyAction:(id)sender
{
//    [discriptionTxt resignFirstResponder];
    if (progressHUD == nil)
    {
        progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Starting Rally...";
        progressHUD.dimBackground = YES;
    }
   // NSString * arrstr = [NSString stringWithFormat:@"%@",inviteUserArr];
    
//    arrstr = [arrstr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
//    arrstr = [arrstr stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
//    arrstr = [arrstr stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
//    arrstr = [arrstr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
   NSString* arrstr = [inviteUserArr componentsJoinedByString:@","];
    
    
    privateStr=[NSString stringWithFormat:@"%@",privateValue];
    
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
     currentdate_Str = [currentdate_Str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *spaceRmoveadressStr =[ NSString stringWithFormat:@"%@",addressTxt.text];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"!" withString:@"%21"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"#" withString:@"%23"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"$" withString:@"%24"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"'" withString:@"%27"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"*" withString:@"%2A"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
    
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"[" withString:@"%5B"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"]" withString:@"%5D"];
    
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"-" withString:@"%2D"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"." withString:@"%2E"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"<" withString:@"%3C"];
    
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@">" withString:@"%3E"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"\\" withString:@"%5C"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"_" withString:@"%5F"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"`" withString:@"%60"];
    
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"{" withString:@"%7B"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"|" withString:@"%7C"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"}" withString:@"%7D"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"~" withString:@"%7E"];
    

    
    NSString *uniStr1 = [NSString stringWithUTF8String:[addressTxt.text UTF8String]];
    NSData *addressData = [uniStr1 dataUsingEncoding:NSUTF8StringEncoding];
   // nameStr = [nameStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
  //  nameStr = [nameStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    NSString *uniStr = [NSString stringWithUTF8String:[nameStr UTF8String]];
    NSData *nameData = [uniStr dataUsingEncoding:NSUTF8StringEncoding];
    
 
    
    NSString*discriptionStr = [NSString stringWithFormat:@"%@",discriptionTxt.text];
    discriptionStr = [discriptionStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
  //  discriptionStr = [discriptionStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    NSString *uniText = [NSString stringWithUTF8String:[discriptionStr UTF8String]];
    NSData *goodMsg = [uniText dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    //  NSString *date = [NSString stringWithFormat:@"%@",[dateFormat dateFromString:dateStr]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString * dateStr3 = [dateFormat stringFromDate:date];
    
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDate *time = [timeFormat dateFromString:timeStr];
    // NSString *time = [NSString stringWithFormat:@"%@",[timeFormat dateFromString:timeStr]];
    [dateFormat setDateFormat:@"HH:mm:ss"];
     NSString * timeStr3 = [dateFormat stringFromDate:time];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    userIdStr = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"id"]];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/startrally.php?userid=%@&rallyprivatevalue=%@&rallylongitude=%@&rallylatitude=%@&rallydate=%@&rallytime=%@&date=%@&friendids=%@",userIdStr,privateStr,longitudeStr,latitudeStr,dateStr3,timeStr3,currentdate_Str,arrstr];
    
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rallydesc\";"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:goodMsg]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rallyname\";"] dataUsingEncoding:NSUTF8StringEncoding]];
   
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:nameData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rallyaddress\";"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:addressData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
             [self JSONRecieved:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            progressHUD = nil;

        
    }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        
         kRAlert(@"Whoops", error.localizedDescription);
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         progressHUD = nil;
         
    }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
  }

-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);

    switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case APIResponseStatus_RallyStartSuccessfull:
        {
//            self.viewController = [RCalenderViewController new];
//            self.viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            [self presentViewController:self.viewController animated:YES completion:nil];
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats!" message:@"You have just started a Rally" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Go Back",@"Share",nil];
            alert.tag = 100;
            [alert show];
            

            break;
        }
        case  APIResponseStatus_RallyStartFailed:
        {
           
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please try  again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
            
        case  APIResponseStatus_RallyNameRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"There is a name required to create a new Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }

        case  APIResponseStatus_RallyAddressRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"There should be an address for the Rally." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }

        default:
            
            break;
            
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    resignBtn.hidden = YES;
}
/*- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
       return YES;
}*/  //Added By Swati_7Oct
-(IBAction)closeBtnAction:(id)sender
{
    //self.stratRallyBtn.userInteractionEnabled=TRUE;
    [discriptionTxt resignFirstResponder];
    [addressTxt resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
//    [self.view bringSubviewToFront:self.toolbar];
   // self.stratRallyBtn.userInteractionEnabled=FALSE;
    if ([textView.text isEqualToString:@"Description "])
    {
        textView.text = @"";
    }
    [textView becomeFirstResponder];
   /* if (textView == addressTxt)
    {
       // [self animateTextView: YES];
    }
    else
    {
        [self animateTextView: YES];
    }*/
    resignBtn.hidden = NO;
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
//    self.toolbar.hidden=YES;
   //  self.stratRallyBtn.userInteractionEnabled=TRUE;
   /* if (textView == addressTxt)
    {
      //  [self animateTextView:NO];
    
    }
    else
    {
        [self animateTextView:NO];
    }
}*/
    [self animateTextView:NO];

}

- (void) animateTextView:(BOOL) up
{
     int movementDistance ;
    float systemOS=[[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemOS>=8.0)
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance = 200; // tweak as needed //Added by swati_7Oct
        }
        else
        {
            movementDistance = 200;
        }

    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance = 200; // tweak as needed //Added by swati_7Oct
        }
        else
        {
            movementDistance = 170;
        }

    }
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    
    if (alertView.tag == 100)
    {
        if (buttonIndex==0)
        {
            RHomeViewController *homeVC = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:homeVC animated:YES];
        }
        else
        {
            [self addShareView];
            
        }
       
    }
    
   
}
-(void)addShareView
{
    RAppDelegate *appDelegate=(RAppDelegate *)[[UIApplication sharedApplication]delegate];
    settingView=[[UIView alloc]init];
    tempView=[[UIView alloc]init];
    if (IS_IPHONE_5)
    {
        settingView.frame=CGRectMake(0,0,320,568);
        tempView.frame=CGRectMake(0,0,320,568);
    }
    else
    {
        settingView.frame=CGRectMake(0,0,320,480);
        tempView.frame=CGRectMake(0,0,320,480);
    }
    tempView.backgroundColor=[UIColor clearColor];
    
    settingView.backgroundColor=[UIColor lightGrayColor];
    settingView.alpha=0.8;
    
    [[appDelegate window] addSubview:settingView];
    
    UIView *popUp = [[UIView alloc]init];
    popUp.layer.cornerRadius = 5.0f;
    if (IS_IPHONE_5)
    {
        popUp.frame=CGRectMake(60,220,200,100);
    }
    else
    {
        popUp.frame=CGRectMake(60,220,200,100);
    }
   
    popUp.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    popUp.layer.borderWidth = 1.0f;
    
    popUp.backgroundColor = [UIColor whiteColor];
    
    
    UIButton *facebookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookBtn.frame=CGRectMake(30,18,58,59);
    [facebookBtn setImage:[UIImage imageNamed:@"facebook_gray.png"]  forState:UIControlStateNormal];
    [facebookBtn setImage:[UIImage imageNamed:@"f-green.png"] forState:UIControlStateHighlighted];
    [facebookBtn addTarget:self action:@selector(facebookBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [popUp addSubview:facebookBtn];
    
    UIButton *twitterBtn =[UIButton buttonWithType:UIButtonTypeCustom];
     twitterBtn.frame=CGRectMake(115, 18, 58, 59);
    [twitterBtn setImage:[UIImage imageNamed:@"twitter_gray.png"]  forState:UIControlStateNormal];
    [twitterBtn setImage:[UIImage imageNamed:@"t-green.png"] forState:UIControlStateHighlighted];
    [twitterBtn addTarget:self action:@selector(twitterBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [popUp addSubview:twitterBtn];

    
    [tempView addSubview:popUp];
    
    [[appDelegate window]addSubview:tempView];
    
    
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [tempView addGestureRecognizer:tapGesture];
    
    
    
}
-(void)twitterBtnAction:(id)sender
{
    
}
-(void)facebookBtnAction:(id)sender
{
    
}
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.view==tempView || recognizer.view==settingView)
    {
        
        [tempView removeFromSuperview];
        [settingView removeFromSuperview];
    }
    
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    self.toolbar.hidden=NO;
    NSDictionary* info = [aNotification userInfo];
    CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (kbRect.size.height==253)
    {
        
        if (IS_IPHONE_5)
        {
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,410,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
         }
        else
        {
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
             self.toolbar.frame =CGRectMake(0,320,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
        }
        
    }
    else if (kbRect.size.height==224)
    {
        if (IS_IPHONE_5)
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,435,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,350,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
    }
    else
    {
        
        
        if (IS_IPHONE_5)
        {
           
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,445,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
           
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            if (iOSVersion>=8.0)
            {
            
            self.toolbar.frame =CGRectMake(0,360,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            }
            else
            {
            self.toolbar.frame =CGRectMake(0,328,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            }
            
            [UIView commitAnimations];
            
            
        }
    }
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.toolbar.hidden=YES;

//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    [UIView animateWithDuration:0.2f animations:^{
//        CGRect frame = self.toolbar.frame;
//        frame.origin.y += kbSize.height;
//        self.toolbar.frame = frame;
//       
//    }];
}

- (IBAction)resignAction:(id)sender
{
    if (![sender isSelected])
    {
        [addressTxt resignFirstResponder];
        [discriptionTxt resignFirstResponder];
        [sender setSelected:YES];
        resignBtn.hidden = YES;
    }
    else
    {
        resignBtn.hidden = NO;
        [sender setSelected:NO];
    }
    

}
@end
