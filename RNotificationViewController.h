//
//  RNotificationViewController.h
//  Rally
//
//  Created by Ambika on 6/13/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNotificationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *noitificationTableView;

@end
