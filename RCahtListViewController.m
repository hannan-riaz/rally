//
//  RCahtListViewController.m
//  Rally
//
//  Created by Ambika on 6/14/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RCahtListViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RHomeViewController.h"
#import "RChatViewController.h"

@interface RCahtListViewController ()
{
    NSIndexPath* selectedIndex;
    MBProgressHUD *ProgressHUD;
    NSMutableArray *joinRalliesEventArr;
    NSTimer * timer;
    NSString *hoursRemainingStr;
    NSString *minsRemainingStr;
    NSString *secsRemainingStr;
    NSString *rallyIDStr;
    int y;
    int showProgess ;
    UIView *  cellView;
    UILabel * titleLbl;
    UIImageView * Dotimg;
    NSMutableArray *arrData ;
    NSString * rallytime;
    NSDate *dateFromString;
    UIButton * chatBtn;
     UIButton * chatBtn1;
    UIButton *rallyNAMELbl;
    UIView *  sepratorView;
    UIAlertView * alert1 ;
    UIAlertView * alert2;
    NSTimer * timmerRepeatForCahtList;
    int labelHide;
   }
@end

@implementation RCahtListViewController
@synthesize chatScrollView,dateLastView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self callWebsercice_rally];
    showProgess = 0;
    labelHide = 0;
    [self callWebsercice_rally];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    joinRalliesEventArr = [[NSMutableArray alloc]init];
    arrData = [[NSMutableArray alloc]init];
   
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    Dotimg.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Rally Chats";
    self.navigationItem.titleView = lblTitle;
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16.0],NSFontAttributeName , [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] ,NSForegroundColorAttributeName,nil];

    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    timmerRepeatForCahtList =  [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timerCallForChatList) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timmerRepeatForCahtList forMode:NSRunLoopCommonModes];
    [self timerFired:[NSTimer scheduledTimerWithTimeInterval:0.1 invocation:nil repeats:YES]];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    [self timerFired:[NSTimer scheduledTimerWithTimeInterval:0.1 invocation:nil repeats:YES]];

}
-(void)timerCallForChatList
{
   
    [self callWebsercice_rally];
    
}
//-(void)dealloc
//{
//    joinRalliesEventArr = nil;
//    arrData = nil;
//    hoursRemainingStr = nil;
//    minsRemainingStr = nil;
//    secsRemainingStr = nil;
//    rallyIDStr = nil;
//    rallytime = nil;
//}
-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer = nil;
   [timmerRepeatForCahtList invalidate];
    timmerRepeatForCahtList = nil;
   
}

/*- (void)refreshView:(NSTimer *)timer {
    // only refresh visible cells
    for (UITableViewCell *cell in [chatTableView visibleCells])
    {
        NSIndexPath *indexPath = [chatTableView indexPathForCell:cell];
        //[self configureCell:cell forRowAtIndexPath:indexPath];
        [chatTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]  withRowAnimation:UITableViewRowAnimationNone];
    }
}*/

-(void)PastRallyImg
{
    NSArray* subviews = [[NSArray alloc] initWithArray: chatScrollView.subviews];
    for (UIView* view in subviews)
    {
        if ([view isKindOfClass:[cellView class]])
        {
            [view removeFromSuperview];
            
        }
    }

       y = 2;
    // joinRalliesEventArr = nil;
        chatScrollView.scrollEnabled = YES;
    chatScrollView.showsVerticalScrollIndicator = YES;
    chatScrollView.showsHorizontalScrollIndicator=NO;
    chatScrollView.delegate = self;
      for (int j = 0; j <[joinRalliesEventArr count]; j++)
    {
        //
       // [[cellView viewWithTag:-2 ]removeFromSuperview];
       


        cellView = [[UIView alloc]initWithFrame:CGRectMake(0,y,320,60)];
        cellView.backgroundColor = [UIColor clearColor];
        cellView.tag = -2;
        cellView.userInteractionEnabled = YES;
        sepratorView = [[UIView alloc]initWithFrame:CGRectMake(0,49,320,0.5)];
        // sepratorView.tag = -6;
        sepratorView.backgroundColor = [UIColor lightGrayColor];
        sepratorView.userInteractionEnabled = YES;
        [cellView addSubview:sepratorView];

        
        titleLbl = [[UILabel alloc]init];
        titleLbl.frame = CGRectMake(215, 3, 100, 30);
        titleLbl.textColor = [UIColor darkGrayColor];
        titleLbl.tag = j;
        titleLbl.userInteractionEnabled = YES;
        [cellView addSubview:titleLbl];
        
        
        Dotimg = [[UIImageView alloc]init];
        Dotimg.frame = CGRectMake(10, 15, 5, 5);
        Dotimg.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        Dotimg.tag = j;
        Dotimg.layer.cornerRadius = Dotimg.bounds.size.width/2;
        Dotimg.clipsToBounds = YES;
        
        rallyNAMELbl =  [[UIButton alloc]init];
       // rallyNAMELbl = [UIButton buttonWithType:UIButtonTypeCustom];
        rallyNAMELbl.frame = CGRectMake(16,10,185,10);
           // rallyNAMELbl.adjustsFontSizeToFitWidth = YES;
       // rallyNAMELbl.adjustsFontSizeToFitWidth = YES;
        [rallyNAMELbl setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        rallyNAMELbl.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0f];
        rallyNAMELbl.enabled = NO;
        rallyNAMELbl.tag = -7;
        
        
//        rallyNAMELbl.backgroundColor=[UIColor redColor];
        
        [rallyNAMELbl setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
        rallyNAMELbl.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        rallyNAMELbl.userInteractionEnabled = YES;
        

        [cellView addSubview:rallyNAMELbl];

        
        chatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        chatBtn.userInteractionEnabled = YES;
        chatBtn.frame = CGRectMake(190,0,cellView.frame.size.width,cellView.frame.size.height-10);
        chatBtn.tag = j;
        [chatBtn addTarget:self  action:@selector(gotoChatAction:)  forControlEvents:UIControlEventTouchUpInside];
        chatBtn.backgroundColor = [UIColor clearColor];
        chatBtn.userInteractionEnabled = YES;
         [cellView addSubview:chatBtn];
        [chatScrollView addSubview:cellView];
        
        chatBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        chatBtn1.userInteractionEnabled = YES;
        chatBtn1.frame = CGRectMake(0,0,cellView.frame.size.width,cellView.frame.size.height-10);
        chatBtn1.tag = j;
        [chatBtn1 addTarget:self  action:@selector(gotoChatAction1:)  forControlEvents:UIControlEventTouchUpInside];
        chatBtn1.backgroundColor = [UIColor clearColor];
        chatBtn1.userInteractionEnabled = YES;
       // [cellView addSubview:chatBtn1];
       
        [chatScrollView addSubview:cellView];

        y = y + cellView.frame.size.height;
        
        NSString * rallyName_str = [NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyNAME"]];
         rallyName_str = [rallyName_str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [rallyName_str dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
       rallyNAMELbl.frame = CGRectMake(16, 10, 185, 10);
        
        if (rallynameStr == nil||[rallynameStr isEqualToString:@""])
        {
            
             //rallyNAMELbl.text = rallyName_str;
            [rallyNAMELbl setTitle:rallyName_str forState:UIControlStateNormal];
            rallyNAMELbl.titleLabel.numberOfLines =0;
            CGRect currentFrame = rallyNAMELbl.frame;
            CGSize max = CGSizeMake(rallyNAMELbl.frame.size.width, 50);
            CGSize expected = [rallyName_str sizeWithFont:rallyNAMELbl.titleLabel.font constrainedToSize:max lineBreakMode:rallyNAMELbl.titleLabel.lineBreakMode];
            currentFrame.size.height = expected.height;
           rallyNAMELbl.frame = currentFrame;
            
        }
        else
        {
            [rallyNAMELbl setTitle:rallynameStr forState:UIControlStateNormal];
            rallyNAMELbl.titleLabel.numberOfLines =0;
            CGRect currentFrame = rallyNAMELbl.frame;
            CGSize max = CGSizeMake(rallyNAMELbl.frame.size.width, 50);
            CGSize expected = [rallynameStr sizeWithFont:rallyNAMELbl.titleLabel.font constrainedToSize:max lineBreakMode:rallyNAMELbl.titleLabel.lineBreakMode];
            currentFrame.size.height = expected.height;
            rallyNAMELbl.frame = currentFrame;
          
            
        }
        if (rallyNAMELbl.titleLabel.text.length>26)
        {
            //UILabel * titleLbl_2 = (UILabel *)[cell viewWithTag:-252];
            rallyNAMELbl.frame = CGRectMake(16, 15,  185, 10);
                      
        }
        
        
        

        NSString * rallyDateStr =[NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyDATE"]];
        NSString * rallyTimeStr =[NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyTIME"]];
        NSString * appendRallyDateStr = [rallyDateStr stringByAppendingString:rallyTimeStr];
        NSString *dateString = appendRallyDateStr;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:dateString];
        [arrData addObject:dateString];
        NSInteger timeUntilEnd = (NSInteger)[dateFromString timeIntervalSinceDate:[NSDate date]];
        //NSLog(@"%ld",(long)timeUntilEnd);
        
        [cellView addSubview:chatBtn1];
        
        NSString * chatdateStr =[NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"chatdate"]];
        if ([chatdateStr isEqual:(id)[NSNull null]] ||[chatdateStr isEqualToString:@"<null>" ]) {
            chatdateStr = @"0";
        }
        
        
        
        NSComparisonResult result;
        if ([[NSUserDefaults standardUserDefaults]objectForKey:[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyID"]] != nil) {
            result = [chatdateStr compare:[[NSUserDefaults standardUserDefaults]objectForKey:[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyID"]]]; // comparing two dates
        }
        else
        {
            if ([chatdateStr isEqual:(id)[NSNull null]] ||[chatdateStr isEqualToString:@"<null>" ])
            {
                chatdateStr = @"0";
            }
            else
            {
                
                [[NSUserDefaults standardUserDefaults]setValue:[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"chatdate"] forKey:[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyID"]];
            }
            
            result = [chatdateStr compare:[[NSUserDefaults standardUserDefaults]objectForKey:[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"rallyID"]]]; // comparing two dates
        }
        
        
        if(result==NSOrderedSame)
        {
            
            
            
        }
        
        else if(result == NSOrderedDescending)
        {
            //                if (chatdateStr>[[NSUserDefaults standardUserDefaults]valueForKey:@"seeLastTimeView"])
            //                {
            [cellView addSubview:Dotimg];
            //                }
            
        }
        else if(result ==  NSOrderedAscending)
        {
            
            for (UIView *view in self.view.subviews)
                
            {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    for (UIView *lblview in view.subviews) {
                        
                        for (UIView *lblview1 in view.subviews) {
                            
                            if (lblview1.tag == -2) {
                                for (UIView *dotView in lblview.subviews) {
                                    
                                    if ([dotView isKindOfClass:[UIImageView class]]) {
                                        if (dotView.tag == j)
                                        {
                                            [dotView removeFromSuperview];
                                        }
                                        
                                    }
                                }
                                
                                
                            }
                        }
                    }
                    
                }
            }
            
        }

               if (timeUntilEnd <= 0)
        {
            //titleLbl.text = @"Attend";
      
            
           
//            if ([[[joinRalliesEventArr objectAtIndex:j] objectForKey:@"checkin"] isEqualToString:@"0"])
//            {
//                titleLbl.textColor = [UIColor whiteColor];
//                titleLbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//                titleLbl.text = @"Attend";
//                titleLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
//                titleLbl.textAlignment = NSTextAlignmentCenter;
//            }
//            else
//            {
//                titleLbl.textColor = [UIColor whiteColor];
//                titleLbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//                titleLbl.text = @"End";
//                titleLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
//                titleLbl.textAlignment = NSTextAlignmentCenter;
//            }
             ///[arrData addObject:dateFromString];
        }
        else
        {
            NSDate *now = [NSDate date];
           /* NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *componentsHours = [calendar components:NSHourCalendarUnit fromDate:now];
            NSDateComponents *componentMint = [calendar components:NSMinuteCalendarUnit fromDate:now];
            NSDateComponents *componentSec = [calendar components:NSSecondCalendarUnit fromDate:now];
            
            
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *componentsDaysDiff = [gregorianCalendar components:NSDayCalendarUnit
                                                                        fromDate:now
                                                                          toDate:dateFromString
                                                                         options:0];
            
           rallytime = [NSString stringWithFormat:@"%02d %02d:%02d:%02d",componentsDaysDiff.day, (24-componentsHours.hour), (60-componentMint.minute), (60-componentSec.second)];*/
          // [arrData addObject:dateFromString];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
            
            //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            // NSCalendar *c = [NSCalendar currentCalendar];
            NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:now    toDate:dateFromString    options:0];
            
            
             //NSInteger years = [componentsDaysDiff year];
            NSInteger months = [componentsDaysDiff month];
            NSInteger days = [componentsDaysDiff day];
            NSInteger hours = [componentsDaysDiff hour];
            NSInteger minutes = [componentsDaysDiff minute];
            NSInteger seconds = [componentsDaysDiff second];
        
          
                if (months)
                {
                    
                       if (months>1)
                    {
                        rallytime = [NSString stringWithFormat:@"%2d months left",months];
                        //timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                        
                    }
                    else
                    {
                        rallytime = [NSString stringWithFormat:@"%2d month left",months];
                       // timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                        
                    }
                    
                }
          
//            else if(years)
//            {
//                if (years>1)
//                {
//                     rallytime = [NSString stringWithFormat:@"%2d years left",years];
//                    //timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
//                }
//                else
//                {
//                    rallytime = [NSString stringWithFormat:@"%2d year left",years];
//                    //timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
//                }
//            }
else
{
    if (days>1)
    {

        rallytime = [NSString stringWithFormat:@"%02d days %02d:%02d:%02d",days, hours, minutes, seconds];
    }
    else
    {
        rallytime = [NSString stringWithFormat:@"%02d day %02d:%02d:%02d",days, hours, minutes, seconds];

    }
}
             titleLbl.text =[NSString stringWithFormat:@"%@",rallytime];
            //lbl.minimumScaleFactor =8.0f;
            titleLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
            titleLbl.adjustsFontSizeToFitWidth = YES;
        }
        
        
        
    } 
    self.automaticallyAdjustsScrollViewInsets = NO;
    chatScrollView.contentSize = CGSizeMake(320, y+200);
}

- (void)gotoChatAction1:(UIButton*)sender
{
    
    NSInteger tag1 = sender.tag;
    NSDictionary *dict = [joinRalliesEventArr objectAtIndex:tag1];
   
    RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
    chatViewController.rallyIDStr = [dict objectForKey:@"rallyID"];
    chatViewController.rallyTitleName = [dict objectForKey:@"rallyNAME"];
    [self.navigationController pushViewController:chatViewController animated:YES];
    
}

- (void)gotoChatAction:(UIButton*)sender
{
    
   // NSInteger tag = sender.tag;
    //UILabel *label1 = (UILabel *)[chatScrollView viewWithTag:tag];
  
//    if ([label1.text isEqualToString:@"Attend"])
//                {
//                    NSDictionary *dict = [joinRalliesEventArr objectAtIndex:tag];
//                    rallyIDStr = [dict objectForKey:@"rallyID"];
//                    // [[[UIAlertView alloc]initWithTitle:@"Attend Rally" message:@"Do you want to attend this rally ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil]show ];
//                    alert2 =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"Do you want to attend this rally ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//                    alert2.tag = tag;
//                    [alert2 show];
//                    
//                }
//                if([label1.text isEqualToString:@"End"])
//                {
//                    alert1 =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"Do you want to End this rally ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//                    alert1.tag = tag;
//                    [alert1 show];
//                  }
//                else
//                {
//                    NSDictionary *dict = [joinRalliesEventArr objectAtIndex:tag];
//                    
//                    RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
//                     chatViewController.rallyIDStr = [dict objectForKey:@"rallyID"];
//                    [self.navigationController pushViewController:chatViewController animated:YES];
//                }
//       
   
    
    
   
}
-(void)viewDidAppear:(BOOL)animated
{
   // [self callWebsercice_rally];
}

-(void)callWebsercice_EndRally
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/~jointhg8/Rally_API/endrally.php?userid=%@&rallyid=%@",[defaults objectForKey:@"id"],rallyIDStr];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_EndRally:responseObject];
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         kRAlert(@"Whoops", error.localizedDescription);               }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];

}

-(void)JSONRecieved_EndRally:(NSDictionary *)response
{
    NSDictionary *Dict = response;
    //NSLog(@"response12%@",response);
    [self callWebsercice_rally];
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_EndRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_RallyEndedSuccessfully:
        {
            
            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            HomeViewController.cameraImg = NO;
            
            [[NSUserDefaults standardUserDefaults ]  removeObjectForKey:@"rallyIDStr"];
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
            
        default:
            
            break;
            
            
    }
}

-(void)callWebsercice_rally
{
    if (showProgess == 0)
    {
     
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/latestrallylist.php?userid=%@",[defaults objectForKey:@"id"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
        if (showProgess == 0)
        {

        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
            showProgess  = 1;
        }
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         [timmerRepeatForCahtList invalidate];
         timmerRepeatForCahtList = nil;
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);               }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *Dict = response;
//    NSLog(@"response12%@",response);
    
  

       if ([Dict objectForKey:@"joinedrallies"] == [NSNull null])
    {
        
       // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else
    {
        joinRalliesEventArr = [[Dict objectForKey:@"joinedrallies"]mutableCopy];
        
        [self PastRallyImg];
     
    }
    
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallyListViewFailed:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }
        case APIResponseStatus_RallendarViewSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];

           
            break;
        }
            
        default:
            
            break;
            
            
    }
}

- (void)timerFired:(NSTimer *)theTimer
{
    
    for (UIView *view in chatScrollView.subviews)
        
    {
        
        for (UIView *lblview in view.subviews)
        {
            if ([lblview isKindOfClass:[UILabel class]])
            {
                
                
                UILabel *lbl = (UILabel*)lblview;
                
                NSInteger tag1 = lbl.tag;
                
                NSDate *now = [NSDate date];
                NSString * dateString =[NSString stringWithFormat:@"%@",[arrData objectAtIndex:tag1]];
                // NSString * rallyTimeStr =[NSString stringWithFormat:@"%@",[[arrData objectAtIndex:tag1] objectForKey:@"rallyTIME"]];
                // NSString * appendRallyDateStr = [rallyDateStr stringByAppendingString:rallyTimeStr];
                //NSString *dateString = appendRallyDateStr;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                [dateFormatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                NSDate * dateFromString1 = [[NSDate alloc] init];
                // voila!
                dateFromString1 = [dateFormatter dateFromString:dateString];
                
                NSInteger timeUntilEnd = (NSInteger)[dateFromString1 timeIntervalSinceDate:now];
                // NSLog(@"%ld",(long)timeUntilEnd);
                if (timeUntilEnd == -1)
                {
                    titleLbl.text = @"";
                }
                
                if (timeUntilEnd <= 0)
                {
                    
                    //                        if ([[[joinRalliesEventArr objectAtIndex:tag1] objectForKey:@"checkin"] isEqualToString:@"0"])
                    //                        {
                    //                            lbl.textColor = [UIColor whiteColor];
                    //                            lbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
                    //                            lbl.text = @"Attend";
                    //                            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
                    //                            lbl.textAlignment = NSTextAlignmentCenter;
                    //                        }
                    ////                        else
                    ////                        {
                    //                            lbl.textColor = [UIColor whiteColor];
                    //                            lbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
                    //                            lbl.text = @"End";
                    //                            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
                    //                            lbl.textAlignment = NSTextAlignmentCenter;
                    ////                        }
                    ///[arrData addObject:dateFromString];
                }
                else
                    
                {
                    /*  NSCalendar *calendar = [NSCalendar currentCalendar];
                     NSDateComponents *componentsHours = [calendar components:NSHourCalendarUnit fromDate:now];
                     NSDateComponents *componentMint = [calendar components:NSMinuteCalendarUnit fromDate:now];
                     NSDateComponents *componentSec = [calendar components:NSSecondCalendarUnit fromDate:now];
                     NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                     NSDateComponents *componentsDaysDiff = [gregorianCalendar components:NSDayCalendarUnit
                     fromDate:dateFromString1
                     toDate:now
                     options:0];
                     rallytime = [NSString stringWithFormat:@"%02d days %02d:%02d:%02d",componentsDaysDiff.day, (24-componentsHours.hour), (60-componentMint.minute), (60-componentSec.second)];
                     lbl.text = rallytime;*/
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
                    
                    //NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    // NSCalendar *c = [NSCalendar currentCalendar];
                    NSDateComponents *componentsDaysDiff = [calendar components:unitFlags  fromDate:now    toDate:dateFromString1    options:0];
                    
                    
                    // NSInteger years = [componentsDaysDiff year];
                    NSInteger months = [componentsDaysDiff month];
                    NSInteger days = [componentsDaysDiff day];
                    NSInteger hours = [componentsDaysDiff hour];
                    NSInteger minutes = [componentsDaysDiff minute];
                    NSInteger seconds = [componentsDaysDiff second];
                    //rallytime = nil;
                    if (months)
                    {
                        
                        
                        
                        if (months>1)
                        {
                            rallytime = [NSString stringWithFormat:@"%2d months left",months];
                            //timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                            
                        }
                        else
                        {
                            rallytime = [NSString stringWithFormat:@"%2d month left",months];
                            // timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                            
                        }
                        
                    }
                    
                    //    else if(years)
                    //    {
                    //        if (years>1)
                    //        {
                    //            rallytime = [NSString stringWithFormat:@"%2d years left",years];
                    //            //timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
                    //        }
                    //        else
                    //        {
                    //            rallytime = [NSString stringWithFormat:@"%2d year left",years];
                    //            //timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
                    //        }
                    //    }
                    else
                    {
                        if (days>1)
                        {
                            
                            
                            rallytime = [NSString stringWithFormat:@"%02d days %02d:%02d:%02d",days, hours, minutes, seconds];
                        }
                        else
                        {
                            rallytime = [NSString stringWithFormat:@"%02d day %02d:%02d:%02d",days, hours, minutes, seconds];
                        }
                    }
                    
                    //rallytime = [NSString stringWithFormat:@"%02d days %02d:%02d:%02d",days, hours, minutes, seconds];
                    lbl.font = [UIFont fontWithName:@"Helvetica" size:16];
                    lbl.text = rallytime;
                }
            }
        }
    }
}



-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)timerFired:(NSTimer *)timer
{
  //  [chatTableView reloadData];
   }


- (void)scrollViewWillBeginDragging:(UIScrollView *)myScrollView
{
    [timer invalidate];
    timer = nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)myScrollView
{
    assert(timer == nil);
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.5f target:self selector:@selector(timerFireMethod:) userInfo:nil repeats: YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
   // _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timerFireMethod:) userInfo:nil  repeats:NO];
}*/


/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [joinRalliesEventArr count];
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier1 = @"Cell1";
    UILabel *notificationLbl = nil;
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
       //cell.indentationLevel = 5;
        
        notificationLbl = [[UILabel alloc]init];
        notificationLbl.frame = CGRectMake(200.0,5,cell.frame.size.width-220,cell.frame.size.height-10 );
        //notificationLbl.text=@"";
        
        notificationLbl.tag = indexPath.row+100;

        [cell.contentView addSubview:notificationLbl];
    }
    
    

        NSString * rallyDateStr =[NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:indexPath.row] objectForKey:@"rallyDATE"]];
        NSString * rallyTimeStr =[NSString stringWithFormat:@"%@",[[joinRalliesEventArr objectAtIndex:indexPath.row] objectForKey:@"rallyTIME"]];
        NSString * appendRallyDateStr = [rallyDateStr stringByAppendingString:rallyTimeStr];
        NSString *dateString = appendRallyDateStr;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
        [dateFormatter setDateFormat:@"yyyy-MM-ddhh:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        dateFromString = [dateFormatter dateFromString:dateString];
        NSInteger timeUntilEnd = (NSInteger)[dateFromString timeIntervalSinceDate:[NSDate date]];
    
        UILabel *lbl = (UILabel*)[cell viewWithTag:indexPath.row+100];
       if (timeUntilEnd <= 0)
        {
            if ([[[joinRalliesEventArr objectAtIndex:indexPath.row] objectForKey:@"checkin"] isEqualToString:@"0"])
            {
                lbl.textColor = [UIColor whiteColor];
                lbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
                lbl.text = @"Attend";
                lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
                lbl.textAlignment = NSTextAlignmentCenter;
            }
            else
            {
                lbl.textColor = [UIColor whiteColor];
                lbl.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
                lbl.text = @"Attending";
                lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
                lbl.textAlignment = NSTextAlignmentCenter;
            }
         }
        else
        {
            NSDate *now = [NSDate date];
            NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *componentsHours = [calendar components:NSHourCalendarUnit fromDate:now];
        NSDateComponents *componentMint = [calendar components:NSMinuteCalendarUnit fromDate:now];
        NSDateComponents *componentSec = [calendar components:NSSecondCalendarUnit fromDate:now];
       
        
       NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *componentsDaysDiff = [gregorianCalendar components:NSDayCalendarUnit
                                                                    fromDate:now
                                                                      toDate:dateFromString
                                                                    options:0];
        
        
       
      
        
        
       
       
        
        //NSLog(@"%@",componentsDaysDiff.date);
        
       // lblDaysSetting.text=[NSString stringWithFormat:@"%02d",componentsDaysDiff.day];
       // lblHouresSetting.text=[NSString stringWithFormat:@"%02d",(24-componentsHours.hour)];
       // lblMinitSetting.text=[NSString stringWithFormat:@"%02d",(60-componentMint.minute)];
       // lblSecSetting.text=[NSString stringWithFormat:@"%02d",(60-componentSec.second)];
        
       // NSInteger seconds = timeUntilEnd % 60;
       // NSInteger minutes = (timeUntilEnd / 60) % 60;
       // NSInteger hours = (timeUntilEnd / 3600);
        
        //lbl.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
        
            
        lbl.text = [NSString stringWithFormat:@"%02d days %02d:%02d:%02d",componentsDaysDiff.day, (24-componentsHours.hour), (60-componentMint.minute), (60-componentSec.second)];
            
          //  lbl.text =[NSString stringWithFormat:@"%02d",   timeUntilEnd];
         //lbl.minimumScaleFactor =8.0f;
        lbl.font = [UIFont fontWithName:@"TrebuchetMS" size:13];
        lbl.adjustsFontSizeToFitWidth = YES;
    }
    
   
   // NSString *str = [NSString stringWithFormat:@"%@:%@:%@",hoursRemainingStr,minsRemainingStr,secsRemainingStr];
 //   UILabel *lbl = (UILabel*)[cell viewWithTag:indexPath.row+100];
    
   // lbl.text = [self updateCountdown:appendRallyDateStr];
    cell.textLabel.text = [[joinRalliesEventArr objectAtIndex:indexPath.row] objectForKey:@"rallyNAME"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //selectedIndex = indexPath;
   
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
     UILabel *lbl = (UILabel*)[cell viewWithTag:indexPath.row+100];
    if ([lbl.text isEqualToString:@"Attend"])
    {
        NSDictionary *dict = [joinRalliesEventArr objectAtIndex:indexPath.row];
        rallyIDStr = [dict objectForKey:@"rallyID"];
       // [[[UIAlertView alloc]initWithTitle:@"Attend Rally" message:@"Do you want to attend this rally ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil]show ];
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Do you want to attend this rally ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = indexPath.row;
        [alert show];
        
    }
    else
    {
            NSDictionary *dict = [joinRalliesEventArr objectAtIndex:indexPath.row];
      
    RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
    chatViewController.rallyIDStr = [dict objectForKey:@"rallyID"];
    [self.navigationController pushViewController:chatViewController animated:YES];
    }
}*/


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
     if (alertView == alert2)
     {
      if (buttonIndex == 1)
    {
        NSDictionary *dict = [joinRalliesEventArr objectAtIndex:alertView.tag];
        rallyIDStr = [dict objectForKey:@"rallyID"];

        NSString * rallyTimeStr = [dict objectForKey:@"rallyTIME"];
        NSString * rallyDateStr = [dict objectForKey:@"rallyDATE"];
        [[NSUserDefaults standardUserDefaults ]setObject:rallyIDStr forKey:@"rallyIDStr"];
        [[NSUserDefaults standardUserDefaults ]setObject:rallyTimeStr forKey:@"rallyTIME"];

        [[NSUserDefaults standardUserDefaults ]setObject:rallyDateStr forKey:@"rallyDATE"];
        [[NSUserDefaults standardUserDefaults ]synchronize];
         NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/checkinrally.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],rallyIDStr];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            [self JSONRecieved_forCheckIn:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);               }];
        
        [[NSOperationQueue mainQueue] addOperation:operation];
        
    }
    else
    {
        
        NSDictionary *dict = [joinRalliesEventArr objectAtIndex:alertView.tag];
        rallyIDStr = [dict objectForKey:@"rallyID"];

        RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
        chatViewController.RallyIDStr = rallyIDStr;
        
        [self.navigationController pushViewController:chatViewController animated:YES];
    }
         
}
    if (alertView == alert1)
    {   if (buttonIndex == 1)
       {
           
           NSDictionary *dict = [joinRalliesEventArr objectAtIndex:alert1.tag];
           rallyIDStr = [dict objectForKey:@"rallyID"];
           [self callWebsercice_EndRally ];
        }
        else
        {
            NSDictionary *dict = [joinRalliesEventArr objectAtIndex:alert1.tag];
            
            RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
            chatViewController.rallyIDStr = [dict objectForKey:@"rallyID"];
            [self.navigationController pushViewController:chatViewController animated:YES];
        }
    }
}
-(void)JSONRecieved_forCheckIn:(NSDictionary *)response
{
    NSDictionary *Dict = response;
//    NSLog(@"response12%@",response);
   
  
    if ([Dict objectForKey:@"joinedrallies"] == [NSNull null]) {
        
       // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else{
         joinRalliesEventArr = [Dict objectForKey:@"joinedrallies"];
    }
    
    switch ([Dict[kRAPIResult] integerValue])
    
    {
            
            
        case APIResponseStatus_CheckInRallySuccessfull:
        {
           // UITableViewCell *cell = [chatTableView cellForRowAtIndexPath:selectedIndex];
           // UILabel *lbl = (UILabel*)[cell viewWithTag:indexPath.row+100];
           // lbl.text isEqualToString:@"Attend"
            RChatViewController *chatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
            chatViewController.RallyIDStr = rallyIDStr;
            [self.navigationController pushViewController:chatViewController animated:YES];
            break;
        }
        case APIResponseStatus_CheckInRallyFailed:
        {
            
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Sorry" message:@"Check In Rally Failed. Please Try Again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
            
            break;
        }
            
        default:
            
            break;
            
            
    }
}



@end
