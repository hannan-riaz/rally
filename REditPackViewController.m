//
//  REditPackViewController.m
//  Rally
//
//  Created by Ambika on 4/4/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "REditPackViewController.h"

@interface REditPackViewController (){
    NSMutableArray *dataArr;
    UIButton *btn;
       NSInteger * sectionr;
   
}

@end

@implementation REditPackViewController
@synthesize editpackTableview,deletePackTableview,searchResult,searchResult1,searchbar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // editpackTableview = [[UITableView alloc]init];
 //   deletePackTableview = [[UITableView alloc]init];

    dataArray = [[NSMutableArray alloc] initWithObjects:@"Adam", @"Smith", @"Adam Smiuth",@"Mac",@"Deniel", nil];

    dataArr = [[NSMutableArray alloc]init];
  
       self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
  
       // searchResult = [NSString arrayWithCapacity:[searchResult1 count]];
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//    NSLog(@"Failed %ld",(long)[error code]);
    
}
-(void)viewWillAppear:(BOOL)animated
{

    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Edit Pack";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Edit Pack";
    self.navigationItem.titleView = lblTitle;
    
    [editpackTableview setContentInset:UIEdgeInsetsZero];
    [self setSearchIconToFavicon];
  }
- (void)setSearchIconToFavicon {
    // Really a UISearchBarTextField, but the header is private.
    UITextField *searchField = nil;
    for (UIView *subview in searchbar.subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchField = (UITextField *)subview;
            break;
        }
    }
    
    if (searchField) {
        UIImage *image = [UIImage imageNamed: @"arrow.png"];
        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
        searchField.leftView = iView;
        
    }  
}
#pragma mark - ------------ Table View Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView ==  editpackTableview)
    {
        return 1;
    }
    else if(tableView ==  deletePackTableview)
    {
        return 1;
    }
 return 1;
}
-(void)viewDidAppear:(BOOL)animated
{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Number of rows it should expect should be based on the section
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.searchResult count];
    }
        if (tableView ==  editpackTableview)
    {
       
        return [dataArray count];
        }
   
    else if(tableView ==  deletePackTableview)
    {
        return [dataArr count];
    }

    
       return 1;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier1 = @"Cell";
    UITableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
    }
  
    
    if(tableView == editpackTableview)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(cell.frame.origin.x + 200, cell.frame.origin.y + 5, 100, 30);
            button.titleLabel.textColor =[UIColor greenColor];
            //[button setTitle:@"+" forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@""]  forState:UIControlStateNormal];
            [button addTarget:self action:@selector(customActionPressed:) forControlEvents:UIControlEventTouchUpInside];
            button.backgroundColor= [UIColor lightGrayColor];
            [cell.contentView addSubview:button];
            
         }
       
        cell.textLabel.text = [dataArray  objectAtIndex:indexPath.row];
           return cell;

   }
    
    if(tableView == deletePackTableview)
    {
       // static NSString* CellIdentifier2 = @"Cell2";
      //  UITableViewCell * cell;
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(cell.frame.origin.x + 200, cell.frame.origin.y + 5, 100, 30);
            //[btn setTitle:@"-" forState:UIControlStateNormal];
            btn.titleLabel.textColor = [UIColor blackColor];
            [btn setImage:[UIImage imageNamed:@""]  forState:UIControlStateNormal];

            [btn addTarget:self action:@selector(customActionPressed1:) forControlEvents:UIControlEventTouchUpInside];
            // btn.tag = indexPath.row;
            btn.backgroundColor= [UIColor lightGrayColor];
            
            
            [cell.contentView addSubview:btn];
        }
               cell.textLabel.text = [dataArr objectAtIndex:indexPath.row];
           return cell;
        
    }
   
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        cell.textLabel.text = [searchResult objectAtIndex:indexPath.row];
    }
    
   return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0;
    if (tableView == editpackTableview)
    {
         height = 1;
    } else if (tableView==deletePackTableview)
    {
        height =  40;
    }

    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView1;
         if (tableView == editpackTableview)
    {
        return sectionHeaderView1;
    }
   else if (tableView == deletePackTableview)
    {
        sectionHeaderView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, deletePackTableview.frame.size.width, 50.0)];
        sectionHeaderView1.backgroundColor = [UIColor lightGrayColor];
        UILabel *titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 2, 200, 40)];
        titleLbl.text = @"Your Pack";
        titleLbl.textColor = [UIColor whiteColor];
        [sectionHeaderView1 addSubview:titleLbl];
        return sectionHeaderView1;
    }

    return sectionHeaderView1 ;
    
}
- (void)customActionPressed:(UIControl*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:button.bounds toView:editpackTableview];
    NSIndexPath *indexPath = [editpackTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];

    NSInteger rowNumber = 0;
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:self.editpackTableview numberOfRowsInSection:i];
       
    }
       rowNumber += indexPath.row;
    [button setTag:rowNumber];
    [editpackTableview beginUpdates];
    [dataArr addObject:[dataArray objectAtIndex:rowNumber]];
    [dataArray removeObjectAtIndex:rowNumber];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[dataArr count]-1  inSection:0]];
     [deletePackTableview insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
    if ([dataArray count]>0)
    {
        
        NSArray *paths1 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowNumber inSection:0]];
//        NSLog(@"%@",paths1);
        [editpackTableview deleteRowsAtIndexPaths:paths1 withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        
        NSArray *paths1 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
//        NSLog(@"%@",paths1);
        [editpackTableview deleteRowsAtIndexPaths:paths1 withRowAnimation:UITableViewRowAnimationTop];
    }
   
           [editpackTableview endUpdates];
  }
- (void)customActionPressed1:(UIButton*)sender {
    
    CGRect buttonFrameInTableView = [sender convertRect:btn.bounds toView:deletePackTableview];
    NSIndexPath *indexPath = [deletePackTableview indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
   {
        rowNumber += [self tableView:self.deletePackTableview numberOfRowsInSection:i];
   }
    
    rowNumber += indexPath.row;
    
    [btn setTag:rowNumber];
 
    [deletePackTableview beginUpdates];
    [dataArray addObject:[dataArr objectAtIndex:rowNumber]];
    [dataArr removeObjectAtIndex:rowNumber];
    

    NSArray *path = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[dataArray count]-1 inSection:0]];
    
    [[self editpackTableview] insertRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationTop];
   if ([dataArr count]>0) {
    NSArray *paths1 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowNumber inSection:0]];
    [deletePackTableview deleteRowsAtIndexPaths:paths1 withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        
        NSArray *paths1 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
//        NSLog(@"%@",paths1);
        [deletePackTableview deleteRowsAtIndexPaths:paths1 withRowAnimation:UITableViewRowAnimationTop];
    }

  //  [deletePackTableview reloadData];
   [deletePackTableview endUpdates];
  
    
  }


#pragma mark - ------------ Searchbar Delegate Methods ------------

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
   // [self.searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
          self.searchResult =  [dataArr filteredArrayUsingPredicate:resultPredicate];
    
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}
- (void)didReceiveMemoryWarninge
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
