//
//  RMapViewController.h
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h> 
#import <CoreLocation/CoreLocation.h>
#import "RJoinRallyViewController.h"
@interface RMapViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

{
   
//    CLLocationManager *locationManager;
    
}
@property (nonatomic) BOOL calenderViewBool;
@property (strong, nonatomic) IBOutlet MKMapView *rallymapView;
@property (strong, nonatomic)UITableView * inviteTableview;
@property (strong, nonatomic) IBOutlet CLGeocoder *Geocoder;
@property (strong, nonatomic) NSString * calenerDateStr;
@property (strong, nonatomic) MKPointAnnotation *annotation1;
@property (strong, nonatomic) IBOutlet UISearchBar *addressSearchBar;
@property (strong, nonatomic) RJoinRallyViewController *JoinRallyViewController;
@end
