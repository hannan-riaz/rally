//
//  RSignUpViewController.m
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RSignUpViewController.h"
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import <QuartzCore/QuartzCore.h>
#import "RHomeViewController.h"
#import "STTwitter.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

#import <QuartzCore/QuartzCore.h>
@interface RSignUpViewController ()<GPPSignInDelegate>

{
    MBProgressHUD *ProgressHUD;
    BOOL FirstLogin;
    BOOL twitterLogin;
    NSString *oauthAccessTokenSecret;
    NSString *oauthAccessToken;
    UIView * pickerView;
    UIDatePicker * datePicker;
    UIButton * closeBtn;
    UIButton * doneButton;
    NSString *date_str;
    int imageset;

}
@property (nonatomic, strong) STTwitterAPI *twitter;
@property (nonatomic, strong)GPPSignIn *signIn;
@end

@implementation RSignUpViewController
@synthesize nameText,dobText,emailText,passwordText,verticalPadding,horizontalPadding,profileImg,mailBtn,signIn,emailBtn,twittetBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    imageset = 0;
    self.overlayView.hidden = YES;
    self.datePopupView.hidden = YES;
    
    [self.addPhotosLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    [self.fullDateLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    [self.dayLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    [self.monthLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    [self.yearLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    
    [self.addPhotosLbl setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
    [self.emailText setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
    [self.passwordText setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
    [self.nameText setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
    [self.dobText setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
    [self.saveBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    [self.doneBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Medium" size:14.0]];
    [self.cancelBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Medium" size:14.0]];

    [self.addPhotosLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    [self.dayLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    [self.monthLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    [self.yearLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    
//    twittetBtn.tintColor = [UIColor clearColor];
//    [twittetBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateSelected];
//    [twittetBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateHighlighted];
//    
//    
//    emailBtn.tintColor = [UIColor clearColor];
//    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateSelected];
//    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateHighlighted];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"id"]){
        
        RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
        [self.navigationController pushViewController:HomeViewController animated:NO];
        return;
    }
   
    
    FirstLogin = NO;
    twitterLogin=NO;
        if ([dobText.text isEqual:[NSNull null]])
    {
        dobText.text= @"";
    }
    FBLoginView *loginview = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email",@"user_about_me"]];
    for (id loginObject in loginview.subviews)
    {
        if ([loginObject isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  loginObject;
           
            UIImage *loginImage = [UIImage imageNamed:@"f-green.png"];
            loginButton.opaque = YES;
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_gray.png"] forState:UIControlStateSelected];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_gray.png"] forState:UIControlStateHighlighted];
        }
        if ([loginObject isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  loginObject;
            loginLabel.text = @"";
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
   
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    
    if (height== 568) {
        
        loginview.frame = CGRectMake(68,386, 50, 47);
    }
    
    else {
        
        loginview.frame = CGRectMake(68,310, 50, 47);
    }
    //   loginview.frame = CGRectMake(68,376, 50, 90);

    
    loginview.delegate = self;
//    [self.view addSubview:loginview];
    
    
    
    
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    // signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;

    [self setDateAndTimeToThePopup];
    [self navbutton];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void) setDateAndTimeToThePopup{
    
    
    NSDate *date = [NSDate date];
  
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger units = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *components = [calendar components:units fromDate:date];
    NSInteger year = [components year];
    updatingYear = year;
    NSInteger month=[components month];       // if necessary
    monthUpdate = month;
    
    NSInteger day = [components day];
    dayUpdate = day;
    
    NSInteger weekday = [components weekday]; // if necessary
    updatingWeekDay = weekday;
    
    NSDateFormatter *weekDay = [[NSDateFormatter alloc] init];
    [weekDay setDateFormat:@"EEE"];
    
    NSDateFormatter *calMonth = [[NSDateFormatter alloc] init];
    [calMonth setDateFormat:@"MMMM"];
    
    NSRange rng = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate: date];
    monthRange = rng.length;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
//    [df setDateFormat:@"EEEE"];
//    NSString *myDayString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"dd"];
    NSString *dayDate = [df stringFromDate:[NSDate date]];
    self.dayLbl.text = dayDate;
    [df setDateFormat:@"MMMM"];
    NSString *myMonthString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"MMM"];
    NSString *shortMonthName = [df stringFromDate:[NSDate date]];
    self.monthLbl.text = shortMonthName;
    
    [df setDateFormat:@"YYYY"];
    NSString *myYearString = [df stringFromDate:[NSDate date]];
    [self.fullDateLbl setText:[NSString stringWithFormat:@"%@ %@, %@",myMonthString,dayDate,myYearString]];
    self.yearLbl.text = myYearString;
}
- (void)updateWholeDateAndLbls{
    NSDate *date = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger units = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *components = [calendar components:units fromDate:date];
    components.year = updatingYear;
    components.month = monthUpdate;       // if necessary
    components.day = dayUpdate;

    NSRange rng = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate: [calendar dateFromComponents:components]];
    monthRange = rng.length;
    
    NSInteger weekday = [components weekday]; // if necessary
    updatingWeekDay = weekday;
    
    NSDateFormatter *weekDay = [[NSDateFormatter alloc] init];
    [weekDay setDateFormat:@"EEE"];
    
    NSDateFormatter *calMonth = [[NSDateFormatter alloc] init];
    [calMonth setDateFormat:@"MMMM"];
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    //    [df setDateFormat:@"EEEE"];
    //    NSString *myDayString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"dd"];
    NSString *dayDate = [df stringFromDate:[calendar dateFromComponents:components]];
    self.dayLbl.text = dayDate;
    [df setDateFormat:@"MMMM"];
    NSString *myMonthString = [df stringFromDate:[calendar dateFromComponents:components]];
    
    [df setDateFormat:@"MMM"];
    NSString *shortMonthName = [df stringFromDate:[calendar dateFromComponents:components]];
    self.monthLbl.text = shortMonthName;
    
    [df setDateFormat:@"YYYY"];
    NSString *myYearString = [df stringFromDate:[calendar dateFromComponents:components]];
    [self.fullDateLbl setText:[NSString stringWithFormat:@"%@ %@, %@",myMonthString,dayDate,myYearString]];
    self.yearLbl.text = myYearString;
}
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    

       CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -=100;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];

   
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
   
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Sign Up";
    self.navigationItem.titleView = lblTitle;
    

    //dobText.text =[NSString stringWithFormat:@"%@",datePicker.date];
    [[GPPSignIn sharedInstance] trySilentAuthentication];

}
-(void)navbutton
{
//    self.navigationItem.title = @"Sign Up";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
//    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.hidden = NO;
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.f green:245.0/255.f blue:245.0/255.f alpha:1.0];
//    self.navigationController.navigationBar.translucent =NO;
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 25.0f)];
//    [backButton setImage:[UIImage imageNamed:@"back.png"]  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    UIBarButtonItem *submitBtn = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitBtn:)];
//    submitBtn.tintColor = [UIColor lightGrayColor];
//    [submitBtn setTitleTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0]} forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = submitBtn;
 
}
#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    // first get the buttons set for login mode
    
//    FirstLogin = NO;
//    NSLog(@"already logged in ");
 
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
     NSString * facebook_userId = [NSString stringWithFormat:@"%@",user.id];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FBlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"facebookLogin"])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?facebookid=%@&deviceid=%@&devicetype=iPhone",facebook_userId,deviceToken];
        
        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
     
    }
    else{
   
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
        NSString *username = [nameStr
                              stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        
        NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",user.birthday];
        
        NSDateFormatter *formate=[[NSDateFormatter alloc]init];
        [formate setDateFormat:@"dd/MM/yyyy"];
        // NSLog(@"value: %@",[formate dateFromString:birthdayDate_str]);
        NSDate * date=[formate dateFromString:birthdayDate_str];
        
        NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
        [formate1 setDateFormat:@"yyyy-MM-dd"];
        NSString * dobStr = [formate1 stringFromDate:date];
        NSString * imgStr  =[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",user.id];;
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        NSData * FacebookImageData = [NSData dataWithContentsOfURL:imageURL];
        
        NSString *urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=%@&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&facebookid=%@",username,dobStr,[user objectForKey:@"email"],user.id,deviceToken, dateStr,                          facebook_userId];
        
    //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
     
    
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:FacebookImageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];

        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]){
                 [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"facebookLogin"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }

             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
         
         
         
         }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
}
}
-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    switch ([dict[kRAPIResult] integerValue])
    {
            
        case APIResponseStatusEmailInUse:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This email already exists. Please sign up with a different email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [alert show];
            
            
            break;
        }
        case  APIResponseStatus_RegistrationSuccessfullWithImage:
        {
           
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];

            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self callWebServiceForUser_id];
            
           
           
           // RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            //[self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
        }
            
        case APIResponseStatus_RegistrationSuccessfullWithNoImage:
            
        {
            
          
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self callWebServiceForUser_id];
            
//             REditProfileViewController *editprofileController=[[REditProfileViewController alloc]initWithNibName:@"REditProfileViewController" bundle:nil];
           
//             editprofileController.userInfoDictionary=userInfoDict;
            
//            [self.navigationController pushViewController:editprofileController animated:NO];
            //RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
           // [self.navigationController pushViewController:HomeViewController animated:NO];
            break;
            
        }
            
        case APIResponseStatus_RegistrationFailed:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Registration Faileld! Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
        case APIResponseStatusFacebookIDInUse:
            
        {
           
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            // [self callWebServiceForUser_id];

           RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
           [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusFacebookIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"FacebookID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
            
        case APIResponseStatusTwitterIDInUse:
            
        {
           
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
             //[self callWebServiceForUser_id];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusTwitterIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"TwitterID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
        case APIResponseStatus_UserAlreadyExist:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            // [self callWebServiceForUser_id];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
        case APIResponseStatusGoogleIDInUse:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(profileImg.image) forKey:          @"register_Img"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            //[self callWebServiceForUser_id];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusGoogleIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"GoogleID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }

        
        default:
            
            break;
            
    }
    
}


/*- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    // Called after logout
//    NSLog(@"Logged out");
    
    //self.profilePic.profileID = nil;
    [FBSession.activeSession closeAndClearTokenInformation];
    
    
}*/
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSString *alertMessage, *alertTitle;
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook Error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    }
    else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
    {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
       // NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
//        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage)
    {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
   
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += 100;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
   
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return  YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    if (alertView.tag == -121)
    {
        if (buttonIndex == 0)
        {
           [self loginToscreen];

        }
        else
        {
            
        }
    }
}
-(void)callWebServiceForUser_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@",[defaults objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Userid:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_Userid:(NSDictionary *)response
{
    NSDictionary * responseDict = response;
    
    if ([responseDict objectForKey:@"userinfo"] != [NSNull null])
   {
       
       [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"PushFromSignUP"];
      
       [[NSUserDefaults standardUserDefaults]synchronize];
       
        userInfoDict = [responseDict objectForKey:@"userinfo"];
       
        REditProfileViewController *editprofileController=[[REditProfileViewController alloc]initWithNibName:@"REditProfileViewController" bundle:nil];
       
        editprofileController.userInfoDictionary=userInfoDict;
       
        [self.navigationController pushViewController:editprofileController animated:NO];
       
    }
}
-(void)loginToscreen
{

if(nameText.text.length ==0 || dobText.text.length == 0 || emailText.text.length == 0 || passwordText .text.length == 0 )
{
    UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"All Fields Mandatory!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
    
else if (passwordText .text.length <6)
{
    UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Password Length Should be 6 Charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    
}
else
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
    [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
    NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"deviceToken"];
    NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    
    NSString * dobDateStr = [NSString stringWithFormat:@"%@",dobText.text];
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM/dd/yyyy"];
    NSDate * date = [formate dateFromString:dobDateStr];
    NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
    [formate1 setDateFormat:@"yyyy-MM-dd"];
    NSString * dobDateStr1 = [NSString stringWithFormat:@"%@",[formate1 stringFromDate:date]];
    
    NSString * namestrText = [NSString stringWithFormat:@"%@",nameText.text];
    namestrText = [namestrText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *urlStr = [NSString stringWithFormat:registerURL,namestrText,dobDateStr1,emailText.text,passwordText.text,deviceToken,dateStr];
    // NSData *imgData=UIImagePNGRepresentation(profileImg.image);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    if (imageset == 1)
    {
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        //UIImage *image=profileImg.image;
        
        
        NSData *imageData =UIImageJPEGRepresentation(new_image, 0.1);
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
    }
    else
    {
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        profileImg.image = [UIImage imageNamed:@"profileImg.png"];
        UIImage *image=profileImg.image;
        
        NSData *imageData =UIImageJPEGRepresentation(image, 0.1);
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         
         
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}

}
-(IBAction)submitBtn:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];
    if (imageset == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Are you sure you want to sign up without a photo?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = -121;
        [alert show];
           }
    
    else if (imageset ==  1)
    {
        [self loginToscreen];
        }
}


-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)imageTapped:(id)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Camera", @"Photo Album", nil];
    [actionSheet showInView:self.view];
}

- (IBAction)twitterAction:(id)sender
{
  
    twittetBtn.tintColor = [UIColor clearColor];
    [twittetBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateSelected];
    [twittetBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateHighlighted];

    self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"JdCjH0kAvlB7WWMFmP96d6eU3"    consumerSecret:@"aqiGPCsxIT4Do65Y0hpSP2RtJj4l16SdVTAedsEvmm8d0QeFHT"];

    
    [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
//        NSLog(@"-- url: %@", url);
//        NSLog(@"-- oauthToken: %@", oauthToken);
        
        [[UIApplication sharedApplication] openURL:url];
        
    }
                    forceLogin:@(YES)
                    screenName:nil
                 oauthCallback:@"rallyapp://twitter_access_tokens/"
                    errorBlock:^( NSError *error) {
                       // NSLog(@"-- Error: %@", error);
                        //_loginStatusLabel.text = [error localizedDescription];
                        
                    }];
    
}

- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier
{
    
    
    [_twitter postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
        
        
//        NSLog(@"-- screenName: %@", screenName);
//        NSLog(@"-- screenName: %@", userID);
        oauthAccessTokenSecret =[NSString stringWithFormat:@"%@",_twitter.oauthAccessTokenSecret ];
        oauthAccessToken =[NSString stringWithFormat:@"%@",_twitter.oauthAccessToken];
          NSString * twitter_userId = [NSString stringWithFormat:@"%@",userID];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TWitterlogged_in"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];
        
       /* if ([[NSUserDefaults standardUserDefaults]objectForKey:@"twitterLogin"])
        {
           
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults objectForKey:@"deviceToken"];
            NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
            
            NSString * facebookUrlStr= [NSString stringWithFormat:@"http://rally.am198.1.114.240bikasoftwaretechnologies.com/Rally_API/login.php?twitterid=%@&deviceid=%@&devicetype=iPhone",twitter_userId,deviceToken];

          
            
            NSURL *url = [NSURL URLWithString:facebookUrlStr];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [self JSONRecieved:responseObject];
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 
                 
                 
             }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 kRAlert(@"Whoops", error.localizedDescription);
             }];
            [[NSOperationQueue mainQueue] addOperation:operation];
            
            

        }
        else{
            */
            [_twitter getUsersShowForUserID:nil orScreenName:screenName includeEntities:nil successBlock:^(NSDictionary *user){
                
                
//                NSLog(@"user--%@", user );
                
                NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
                [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
                NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
                NSString *username2 = [nameStr
                                       stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults objectForKey:@"deviceToken"];
                NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
                
                
                
                NSString * checkurl =[NSString stringWithFormat:@"http://abs.twimg.com/sticky/default_profile_images/default_profile_6.png"];
                NSString *code = [checkurl  substringWithRange:NSMakeRange(0, 50)];
                NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url"]];
                NSString *code1 = [imageURLString substringWithRange:NSMakeRange(0, 50)];
                
                NSData *avatarData;
                if ([code1 isEqualToString:code])
                {
                    UIImage *image=[UIImage imageNamed:@"profileImg.png"];
                    // profileImg.image = [UIImage imageNamed:@"profileImg.png"];
                    
                    avatarData =UIImageJPEGRepresentation(image, 0.1);
                    
                }
                else
                {
                    
                    NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url_https"]];
                    NSString *imageUrlNew=[imageURLString stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
                    NSURL *imageURL = [NSURL URLWithString:imageUrlNew];
                    avatarData = [NSData dataWithContentsOfURL:imageURL];
                    
                   // NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url"]];
                   // NSURL *imageURL = [NSURL URLWithString:imageURLString];
                    //avatarData = [NSData dataWithContentsOfURL:imageURL];
                }
//        NSString * urlStr2= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/~jointhg8/Rally_API/register.php?username=%@&dob=123&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@",username2,[user objectForKey:@"screen_name"],userID,deviceToken, dateStr];
                  NSString * urlStr2= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=123&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&twitterid=%@",username2,[user objectForKey:@"screen_name"],userID,deviceToken, dateStr,twitter_userId];
                //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:urlStr2]];
                [request setHTTPMethod:@"POST"];
                
                NSMutableData *body = [NSMutableData data];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                // UIImage *image=avtarimage;
                //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:avatarData]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [request setHTTPBody:body];
                

                
//                NSURL *url = [NSURL URLWithString:urlStr2];
//                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                
                operation.responseSerializer = [AFJSONResponseSerializer serializer];
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     [self JSONRecieved:responseObject];
                     if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]){
                         [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"twitterLogin"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     ProgressHUD = nil;
                     
                     
                     
                 }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
                 {
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     ProgressHUD = nil;
                     kRAlert(@"Whoops", error.localizedDescription);                 }];
                [[NSOperationQueue mainQueue] addOperation:operation];
                // NSLog(@"%@",[user valueForKey:@"profile_image_url"]);
            }errorBlock:^(NSError *error) {
//                NSLog(@"%@",[error localizedDescription]);
            }];
        }
    //}
                                 errorBlock:^(NSError *error)
     {
         
        // NSLog(@"-- %@", [error localizedDescription]);
         
         
     }];
    
}



- (IBAction)messageAction:(id)sender
{
    emailBtn.tintColor = [UIColor clearColor];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateSelected];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateHighlighted];

    [signIn authenticate];
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    //signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [[GPPSignIn sharedInstance] trySilentAuthentication];
    [self refreshUserInfo];
}


- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth   error: (NSError *) error
{
    if (error)
    {
//        NSLog(@"Received error %@ and auth object %@",error, auth);
        return;
        
    }
    else
    {
    
    [self refreshUserInfo];
    }
}


- (void)didDisconnectWithError:(NSError *)error {
    if (error)
    {
     // NSLog( @"Status: Failed to disconnect");
    } else
    {
//        NSLog( @"Status: Disconnected");
        
    }
    [self refreshUserInfo];
    
}

- (void)reportAuthStatus {
       [self refreshUserInfo];
}
- (void)refreshUserInfo

{
    if ([GPPSignIn sharedInstance].authentication == nil)
    {
        return;
    }
    
    else
    {
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
   if (person == nil) {
        return;
    }
    
    
//    NSLog(@"%@",person);
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"googleLogin"];

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"googleLogin"])
    {
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?googleid=%@&deviceid=%@&devicetype=iPhone",person.identifier,deviceToken];
        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops",error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
        
        
    }
    else
    {
        
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * nameStr = [NSString stringWithFormat:@"%@",person.displayName];
        NSString *username = [nameStr
                              stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        
       // NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",person.birthday];
        
        NSString * checkurl =[NSString stringWithFormat:@"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50"];
        NSString *imageURLString = person.image.url;
        NSData *avatarData;
        if ([imageURLString isEqualToString:checkurl]) {
            UIImage *image=[UIImage imageNamed:@"profileImg.png"];
            // profileImg.image = [UIImage imageNamed:@"profileImg.png"];
            
            avatarData =UIImageJPEGRepresentation(image, 0.1);
            
        }
        else
        {
            NSString *imageURLString = person.image.url;
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }

        // UIImage * avtarimage1= [UIImage imageWithData:avatarData];
        
        
       NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=1&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&googleid=%@",username,[GPPSignIn sharedInstance].userEmail,person.identifier,deviceToken, dateStr,person.identifier];
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // UIImage *image=avtarimage;
        //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:avatarData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             
             if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]   )
             {
                 [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"googleLogin"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
    }
}
}




- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}
- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion: nil];
    }
}
-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    new_image = [info valueForKey:UIImagePickerControllerEditedImage];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(new_image,self,nil,nil);
    }
   // NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    imageset = 1;
    profileImg.image = new_image;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)dobAction:(id)sender
{
    [nameText resignFirstResponder];
    [passwordText resignFirstResponder];
    [emailText resignFirstResponder];
    [dobText resignFirstResponder];
    
    self.overlayView.hidden = NO;
    self.datePopupView.hidden = NO;
    self.datePopupView.alpha = 0.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.datePopupView.alpha = 1.0f;
    [UIView commitAnimations];
    
    
//    pickerView = [[UIView alloc]init];
//    pickerView.backgroundColor = [UIColor whiteColor];
//    pickerView.userInteractionEnabled = YES;
//    pickerView.layer.cornerRadius = 5.0f;
//    pickerView.layer.borderColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
//    pickerView.layer.borderWidth = 1.0f;
//    
//
//    pickerView.backgroundColor = [UIColor whiteColor] ;
//    datePicker = [[UIDatePicker alloc] init];
//
//    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    //datePicker.tintColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//    
//    
//    
//    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
//    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
//    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
//    closeBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [doneButton addTarget:self  action:@selector(doneButtonAction:)  forControlEvents:UIControlEventTouchUpInside];
//    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
//    doneButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    doneButton.layer.cornerRadius = 5.0f;
//    
//    CGFloat height = [UIScreen mainScreen].bounds.size.height;
//    if (height== 568)
//    {
//        pickerView.frame =CGRectMake(35,105,250,250);
//        closeBtn.frame = CGRectMake(5, 5, 24, 24);
//        doneButton.frame = CGRectMake(85, 207, 80, 30);
//        datePicker.frame =CGRectMake(0,25,pickerView.frame.size.width,pickerView.frame.size.height-50);
//    }
//    else
//    {
//        pickerView.frame =CGRectMake(35,87,250,220);
//        closeBtn.frame = CGRectMake(5, 5, 24, 24);
//        doneButton.frame = CGRectMake(85, 180, 80, 30);
//        datePicker.frame =CGRectMake(0,25,pickerView.frame.size.width,pickerView.frame.size.height-50);
//
//    }
//    
//    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
//    closeBtn.clipsToBounds =YES;
//    
//    [pickerView addSubview:datePicker];
//    [self.view addSubview:pickerView];
//    [pickerView addSubview:closeBtn];
//    [pickerView addSubview:doneButton];
    self.overlayView.hidden = NO;
    self.datePopupView.hidden = NO;

}
-(void)CloseAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    
}
- (void)doneButtonAction:(id)sender
{
    dobText.text = date_str;
    [pickerView removeFromSuperview];

}


- (void)datepickerChanged:(id)sender
{
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM/dd/yyyy"];
   
   date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
   
}
- (IBAction)monthUpBtnAction:(id)sender{
    monthUpdate = monthUpdate + 1;
    if ( monthUpdate == 13 )
    {
        monthUpdate = 1;
    }
    [self updateWholeDateAndLbls];
}
- (IBAction)monthDownBtnAction:(id)sender{
    monthUpdate = monthUpdate - 1;
    if ( monthUpdate == 0 )
    {
        monthUpdate = 12;
    }
    [self updateWholeDateAndLbls];
}
- (IBAction)dayUpBtnAction:(id)sender{
    if ( dayUpdate < monthRange )
    {
        dayUpdate = dayUpdate + 1;
        [self updateWholeDateAndLbls];
    }
}
- (IBAction)dayDownBtnAction:(id)sender{
    if ( dayUpdate > 0 )
    {
        dayUpdate = dayUpdate - 1;
        [self updateWholeDateAndLbls];
    }
}
- (IBAction)yearUpBtnAction:(id)sender{
    updatingYear = updatingYear + 1;
    [self updateWholeDateAndLbls];
}
- (IBAction)yearDownBtnAction:(id)sender{
    updatingYear = updatingYear - 1;
    [self updateWholeDateAndLbls];
}
- (IBAction)dobPopupViewDoneBtnAction:(id)sender{
    
}
- (IBAction)dobPopupViewCancelBtnAction:(id)sender{
    [self.overlayView setHidden:YES];
    [self.datePopupView setHidden:YES];
}
@end
