//
//  RLeaderBoardViewController.m
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RLeaderBoardViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RTableCellViewConrollerTableViewCell.h"
@interface RLeaderBoardViewController ()

@end

@implementation RLeaderBoardViewController
@synthesize leaderTableview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Leaderboard";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    RTableCellViewConrollerTableViewCell* cell = [leaderTableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:@"RTableCellViewConrollerTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
   // cell.phoneNoLbl.hidden = YES;
    leaderTableview.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor whiteColor];
    //cell.TitleLbl.text = @"test";
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(44, 0, 2, 44)];
    separatorLineView.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.30f];
    [cell.contentView addSubview:separatorLineView];
    
    UIView* separatorLineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    separatorLineView1.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.30f];
    [cell.contentView addSubview:separatorLineView1];
    
    return cell;
}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
//                                 CGRectMake(0, 0, tableView.frame.size.width, 50.0)];
//    sectionHeaderView.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//    
//    return sectionHeaderView;
//}

    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
