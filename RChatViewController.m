
//
//  RChatViewController.m
//  Rally
//
//  Created by Ambika on 4/9/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RChatViewController.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RProfileViewController.h"
#import "RCahtListViewController.h"
@interface RChatViewController ()
{
    NSString *currentdate_Str;
    NSString * userIdStr;
    NSMutableArray * chatArray;
     NSMutableArray * userChatArray;
     NSMutableArray * mineArray;
    NSString * chattext;
    NSBubbleData *heyBubble ;
    NSBubbleData *userBubble ;
    NSString * imageStr;
    NSString * nameStr;
    NSString * ChatDateStr;
    NSString * chatUserId;
    int txtLength ;
    int notif;
    int stopProgress;
    NSMutableArray * pushAppsArray;
    NSTimer * timmerRepeat;
    int var;
    NSString * rally_id;
    
    UILabel * lblTitle;
}
@property(strong,nonatomic)MBProgressHUD *ProgressHUD;
@end

@implementation RChatViewController
@synthesize bubbleTable,textInputView,chatTextField,sendBtn,ProgressHUD,RallyIDStr,rallyTitleName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Bubble Table view x:%f,y:%f,Width:%f,Height:%f",bubbleTable.frame.origin.x,bubbleTable.frame.origin.y,bubbleTable.frame.size.width,bubbleTable.frame.size.height);
    
    if(iOSVersion>=7.0)
    {
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
//    bubbleTable.frame=CGRectMake(0,64,bubbleTable.frame.size.width,bubbleTable.frame.size.height-64);
    
//   bubbleTable.backgroundColor=[UIColor redColor];
    
//    self.chatTextField.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.chatTextField.keyboardType=UIKeyboardTypeASCIICapable;
    
   /* self.navigationController.navigationBar.hidden = NO;
    
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0,0,22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView1) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    self.navigationController.title = nil;*/ //added by swati 11Oct
    
    /*lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(15,0,250,40) ;
    lblTitle.adjustsFontSizeToFitWidth=TRUE;
    lblTitle.backgroundColor=[UIColor redColor];
    //lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    lblTitle.font = [UIFont boldSystemFontOfSize:17.0];*/ //added by swati 11Oct
    
    //[lblTitle sizeToFit];
//    self.navigationItem.titleView = lblTitle;
    
//    self.navigationItem.title=lblTitle.text;

    txtLength = 40;
    stopProgress = 0;
    pushAppsArray = [[NSMutableArray alloc]init];
    rally_id = RallyIDStr;
    
    
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"el", nil]
//                                              forKey:@"AppleLanguages"];
//    
//    [[NSUserDefaults standardUserDefaults] synchronize];
    growingtextView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(11, 8, 242, 30)];
    growingtextView.isScrollable = NO;
    growingtextView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
   
	growingtextView.minNumberOfLines = 1;
	growingtextView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
//    if(IS_IPHONE_5)
//    {
    growingtextView.keyboardType = UIKeyboardTypeDefault;
//    }
//    else
//    {
//    growingtextView.keyboardType = UIKeyboardTypeASCIICapable;
//    }
    growingtextView.autocorrectionType = UITextAutocorrectionTypeDefault
    ;
	growingtextView.returnKeyType = UIReturnKeyNext; //just as an example
	growingtextView.font = [UIFont systemFontOfSize:15.0f];
	growingtextView.delegate = self;
    growingtextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    growingtextView.backgroundColor = [UIColor whiteColor];
    //growingtextView.placeholder = @"Type to see the textView grow!";
    growingtextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    growingtextView.autocorrectionType = UITextAutocapitalizationTypeWords;
    growingtextView.layer.cornerRadius = 4.2f;
    growingtextView.layer.borderWidth = 0.2f;
    [textInputView addSubview:growingtextView];

      sendBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    sendBtn.layer.cornerRadius = 4.2f;
    sendBtn.layer.borderWidth = 0.2f;
   
    textInputView.layer.cornerRadius = 4.2f;
    textInputView.layer.borderWidth = 0.2f;
    //chatTextField.scrollEnabled = NO;
        //heyBubble.avatar = [UIImage imageNamed:@"avatar1.png"];
    
    //NSBubbleData *photoBubble = [NSBubbleData dataWithImage:[UIImage imageNamed:@"halloween.jpg"] date:[NSDate dateWithTimeIntervalSinceNow:-290] type:BubbleTypeSomeoneElse];
  //  photoBubble.avatar = [UIImage imageNamed:@"avatar1.png"];
    
    //NSBubbleData *replyBubble = [NSBubbleData dataWithText:@"Wow.. Really cool picture out there. iPhone 5 has really nice camera, yeah?" date:[NSDate dateWithTimeIntervalSinceNow:-5] type:BubbleTypeMine];
   // replyBubble.avatar = nil;
    chatArray = [[NSMutableArray alloc] init];
    bubbleData = [[NSMutableArray alloc] init];
   // bubbleData = [[NSMutableArray alloc] initWithObjects:chatArray,userChatArray, nil];
    bubbleTable.bubbleDataSource = self;

    // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
    // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
    // Groups are delimited with header which contains date and time for the first message in the group.
    
    bubbleTable.snapInterval = 120;
    
    // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
    // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
    
    bubbleTable.showAvatars = YES;
    
    // Uncomment the line below to add "Now typing" bubble
    // Possible values are
    //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
    //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
    //    - NSBubbleTypingTypeNone - no "now typing" bubble
    
    bubbleTable.typingBubble = NSBubbleTypingTypeSomebody;
    
  //  [bubbleTable reloadData];
    
    // Keyboard events
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

      // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defaultsChanged:) name:NSUserDefaultsDidChangeNotification object:nil];

    UITapGestureRecognizer *lpgr = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
   // lpgr.numberOfTapsRequired = 1;
    [bubbleTable addGestureRecognizer:lpgr];
    timmerRepeat =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES];
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:timmerRepeat forMode: NSDefaultRunLoopMode];

    }
-(void)defaultsChanged:(NSNotification *)notification
{
    NSUserDefaults *defaults = (NSUserDefaults *)[notification object];
    
    
    
    if([defaults  boolForKey:@"profile"])
    {
         [[ NSUserDefaults standardUserDefaults] setBool:NO forKey:@"profile"];
        RProfileViewController * ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
        ProfileViewController.frienduserId = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"userid_key"];
       [self.navigationController pushViewController:ProfileViewController animated:NO];
   }

}

-(void)timerCallback
{
    
    notif = 1;
    [self callWebService_viewChat];
   
      //  [bubbleTable scrollBubbleViewToBottomAnimated:YES];
    [bubbleTable reloadData];
    
}
-(void)dealloc
{
    currentdate_Str  = nil;
    userIdStr  = nil;
    chatArray  = nil;
    userChatArray = nil;
    mineArray = nil;
    chattext = nil;
    heyBubble = nil;
    userBubble = nil;
    imageStr = nil;
    nameStr = nil;
    ChatDateStr = nil;
    chatUserId = nil;
    pushAppsArray = nil;
   // rally_id = nil;

}
-(void)viewWillDisappear:(BOOL)animated
{
    [customView removeFromSuperview];
    
    [timmerRepeat invalidate];
    
    timmerRepeat = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
     notif = 0;
    stopProgress = 0;
    
    
    NSString * rallyName_str = [NSString stringWithFormat:@"%@",rallyTitleName];
    rallyName_str = [rallyName_str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSData *data = [rallyName_str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    
   

     // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable:) name:@"reloadTheTable" object:nil];
    
    
    self.navigationItem.hidesBackButton=TRUE;
    
    customView=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,44)];
    
    customView.backgroundColor=[UIColor clearColor];
    
    
    
    UILabel *labelforNavigationTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,5,320,40)];
    
    labelforNavigationTitle.backgroundColor = [UIColor clearColor];
    
    if (rallynameStr == nil||[rallynameStr isEqualToString:@""])
    {
        labelforNavigationTitle.text = rallyName_str;
    }
    else
    {
        labelforNavigationTitle.text =rallynameStr;
    }
    
    labelforNavigationTitle.adjustsFontSizeToFitWidth=TRUE;
    
    labelforNavigationTitle.numberOfLines = 0;
    
    labelforNavigationTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    labelforNavigationTitle.font = [UIFont boldSystemFontOfSize:17.0];
    
    
    labelforNavigationTitle.textAlignment = NSTextAlignmentCenter;
    
    [customView addSubview:labelforNavigationTitle];
    
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn addTarget:self action:@selector(popToView1)
     
      forControlEvents:UIControlEventTouchUpInside];
    
    leftBtn.frame = CGRectMake(14,15,22,18);
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    
    [customView addSubview:leftBtn];
    
    [self.navigationController.navigationBar addSubview:customView];
    
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = rallyTitleName;
    self.navigationItem.titleView = lblTitle;
    
    

    [self callWebService_viewChat];
}
-(void)viewDidAppear:(BOOL)animated
{
   // [bubbleTable reloadData];

}
/*- (void)reloadTable:(NSNotification *)notification
{
    NSDictionary * pushMessage =  notification.object;
    //pushAppsArray =[pushMessage objectForKey:@"aps"];
    NSString * alert = [NSString stringWithFormat:@"%@",[[pushMessage objectForKey:@"aps"] objectForKey:@"alert"]
];
//    heyBubble = [NSBubbleData dataWithText:alert date:nil type:BubbleTypeSomeoneElse];
//    [bubbleData addObject:heyBubble];
//    notif = 1;
//    [bubbleTable reloadData];
    
   
}*/
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
//    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
//        return;
    
    [growingtextView resignFirstResponder];
    
}
/*- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = chatTextField.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    if (chatTextField.text.length > txtLength )
    {
       textInputView.frame = CGRectMake(0, textInputView.frame.origin.y-textView.frame.origin.y-10, 320, textView.frame.size.height+40);
        txtLength += 40;
    
    }
    
    
 }*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [growingtextView resignFirstResponder];
    return NO;
}
/*- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 140){
        if (location != NSNotFound){
            [chatTextField resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [chatTextField resignFirstResponder];
        return NO;
    }
    return YES;
}*/


/*- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    CGSize maximumSize = CGSizeMake(250,999); //specify width of textView  and maximum height for text to fit in width of textView
    CGSize txtSize = [textView.text sizeWithFont:[UIFont fontWithName:@"Arial" size:16] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping]; //calulate size of text by specifying font here
    //Add UIViewAnimation here if needed
    [textView setFrame:CGRectMake(textView.frame.origin.x,textView.frame.origin.y,270,txtSize.height+10)]; // change accordingly
   
       return YES;
}
 */
-(void)callWebService_viewChat
{
    if (stopProgress == 0)
    {
        
        if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
        
    }
    
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/chat.php?rallyid=%@&page_no=0",RallyIDStr];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self JSONRecieved_GetChat:responseObject];
        if (stopProgress == 0)
        {
            
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                ProgressHUD = nil;
            stopProgress = 1;
        }
   
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         [timmerRepeat invalidate];
                                         timmerRepeat = nil;

                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                                                              }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}

-(void)JSONRecieved_GetChat:(NSDictionary *)response
{
    NSDictionary *Dict = response;
    [bubbleData removeAllObjects];
  //  NSLog(@"response12%@",response);
    if ([Dict objectForKey:@"chat"]!= [NSNull null])
    {
        chatArray = [[Dict objectForKey:@"chat"]mutableCopy];
        var = chatArray.count;
    }
    
    NSString * starteridStr= [NSString stringWithFormat:@"%@",[Dict objectForKey:@"starterid"]];
   
   // NSString * userNameStr=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userNAME"]];
    
    for (int i = 0; i <[chatArray count]; i++) {
        NSString *  chat_Str = [NSString stringWithFormat:@"%@",[[chatArray objectAtIndex:i]objectForKey:@"chatTEXT"]];
        NSData *data = [chat_Str dataUsingEncoding:NSUTF8StringEncoding];
        NSString *chatDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        if (chatDataEncode == nil||[chatDataEncode isEqualToString:@""])
        {
            chattext = chat_Str;
        }
        else
        {
           chattext = chatDataEncode;
        }
        
        imageStr = [[chatArray objectAtIndex:i]objectForKey:@"userIMAGE"];
        NSString * namestr1 = [NSString stringWithFormat:@"%@",[[chatArray objectAtIndex:i]objectForKey:@"userNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
        namestr1 = [namestr1   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        nameStr = namestr1 ;
        ChatDateStr = [[chatArray objectAtIndex:i]objectForKey:@"chatDATE"];
        chatUserId =[[chatArray objectAtIndex:i]objectForKey:@"userID"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateChat = [dateFormat dateFromString:ChatDateStr];
        
        //if ([chatUserId isEqualToString:starteridStr])
        //{
          //  [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"starterId"];
           // [[NSUserDefaults standardUserDefaults]synchronize];
       // }
        if (![nameStr isEqual:[NSNull null]])
        {
         if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"id"] isEqualToString:chatUserId])
            {
                chattext = [chattext  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                heyBubble = [NSBubbleData dataWithText:chattext date:dateChat type:BubbleTypeMine];
            }
            else
            {
                if ([chatUserId isEqualToString:starteridStr])
                {
                    chattext = [chattext  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    heyBubble = [NSBubbleData dataWithText:chattext date:dateChat type:BubbleTypeRallyOwner];
                    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",imageStr];
                    NSURL *imageURL         = [NSURL URLWithString:imgStr];
                    heyBubble.imageURL      = imageURL;
                    heyBubble.profileName   = nameStr;
                    heyBubble.userIdStr     = chatUserId;
                    
                }
                else
                {
                    chattext = [chattext  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    heyBubble = [NSBubbleData dataWithText:chattext date:dateChat type:BubbleTypeSomeoneElse];
                    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",imageStr];
                    NSURL *imageURL         = [NSURL URLWithString:imgStr];
                    heyBubble.imageURL      = imageURL;
                    heyBubble.profileName   = nameStr;
                    heyBubble.userIdStr     = chatUserId;
                }
            }
           
            
        }
        
     [bubbleData addObject:heyBubble];
        
    }
  

    [bubbleTable reloadData];
    if (notif == 0)
    {
        [bubbleTable scrollBubbleViewToBottomAnimated:NO];
    }
    
    switch ([Dict[kRAPIResult] integerValue])
    {
            
        case  APIResponseStatus_GetChatFailed:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
        case  APIResponseStatus_GetChatSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
            
            
        default:
            
            break;
            
    }
}
#pragma mark - HPGrowingTextViewDataSource implementation

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = textInputView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	textInputView.frame = r;
   // [bubbleTable scrollBubbleViewToBottomAnimated:YES];
    bubbleTable.frame  = CGRectMake(bubbleTable.frame.origin.x, textInputView.frame.origin.y-bubbleTable.frame.size.height,  bubbleTable.frame.size.width, bubbleTable.frame.size.height);
   
}
-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
    
}
-(void)growingTextViewDidEndEditing:(HPGrowingTextView *)growingTextView
{
    
}
-(void)popToView1
{
        NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
        [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
        rally_id = RallyIDStr;
        [[NSUserDefaults standardUserDefaults]setValue:currentdate_Str forKey:rally_id];
      //  NSLog(@"%@",currentdate_Str);
    
        
        [self.navigationController popViewControllerAnimated:NO];

  

   
}

#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    
    
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
   
    
    return [bubbleData objectAtIndex:row];
}

#pragma mark - Keyboard events

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    NSLog(@"Start");
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSLog(@"End");
}

//Added by Swati_8-Oct
- (void)keyboardWasShown:(NSNotification*)aNotification
{
//    if (IS_OS_8_OR_LATER)
//    {
        NSDictionary* info = [aNotification userInfo];
        CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        NSLog(@"KbRect height %f",kbRect.size.height);
        
        if (kbRect.size.height==253)
        {
            
            if (IS_IPHONE_5)
            {
                
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame=CGRectMake(0,200,textInputView.frame.size.width,textInputView.frame.size.height);
                
//                NSLog(@")
                
                bubbleTable.frame = CGRectMake(0,0,320,195);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
                
            }
            else
            {
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame=CGRectMake(0,115,textInputView.frame.size.width,textInputView.frame.size.height);
                
                bubbleTable.frame = CGRectMake(0,0,320,110);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
            }
            
        }
        else if(kbRect.size.height==224)
        {
            if (IS_IPHONE_5)
            {
                
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame = CGRectMake(0,230,textInputView.frame.size.width,textInputView.frame.size.height);;
                
                bubbleTable.frame = CGRectMake(0,0,320,224);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
            }
            else
            {
                
                
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame=CGRectMake(0,140,textInputView.frame.size.width,textInputView.frame.size.height);
                
                bubbleTable.frame = CGRectMake(0,0,320,139);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
            }
        }
        else
        {
            if (IS_IPHONE_5)
            {
                
                
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame = CGRectMake(0,235,textInputView.frame.size.width,textInputView.frame.size.height);;
                
                bubbleTable.frame = CGRectMake(0,0,320,234);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
            }
            else
            {
                
                
                [UIView beginAnimations:@"Start" context:nil];
                
                [UIView setAnimationDuration:0.2];
                
                textInputView.frame=CGRectMake(0,151,textInputView.frame.size.width,textInputView.frame.size.height);
                
                bubbleTable.frame = CGRectMake(0,0,320,147);
                
                [UIView commitAnimations];
                
                [bubbleTable scrollBubbleViewToBottomAnimated:YES];
            }
            
        }

//    }
    
    
}

//Added by Swati_8-Oct

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
//    if (IS_OS_8_OR_LATER)
//    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        [UIView animateWithDuration:0.2f animations:^{
            CGRect frame = textInputView.frame;
            frame.origin.y += kbSize.height;
            textInputView.frame = frame;
            frame = bubbleTable.frame;
            frame.size.height += kbSize.height;
            bubbleTable.frame = frame;
        }];
//    }
  
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Actions
- (IBAction)sayPressed:(id)sender
{
 // [bubbleData removeAllObjects];
//    NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
//    [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//     NSDate *now = [[NSDate alloc] init];
//    
//    NSString *dateString = [dateTimeFormat stringFromDate:now];
//    
//    NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
//    [inFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
//    
//  //  NSDate *textDate = [inFormat dateFromString:dateString];
  
    if ([growingtextView.text isEqualToString:@""])
    {
        
    }
    else
    {
      //  [dict setObject:[NSNumber numberWithBool:YES] forKey:@"KeyboardEmojiEverywhere"];
      
    bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
      NSBubbleData *sayBubble = [NSBubbleData dataWithText:growingtextView.text date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine];
    [bubbleData addObject:sayBubble];
    [bubbleTable reloadData];
       

   // [chatTextField resignFirstResponder];
    // chatTextField.text = @"";
    
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
    currentdate_Str = [currentdate_Str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // NSLog(@"%@", [defaults objectForKey:@"id"]);
    userIdStr = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"id"]];
   
    NSString * chatStr = [NSString stringWithFormat:@"%@",growingtextView.text];
    chatStr = [chatStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *uniText = [NSString stringWithUTF8String:[chatStr UTF8String]];
    NSData *goodMsg = [uniText dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/chat.php?rallyid=%@&userid=%@&chatdate=%@",RallyIDStr,userIdStr,currentdate_Str];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"chattext\";"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:goodMsg]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
        growingtextView.text = @"";
        [bubbleTable scrollBubbleViewToBottomAnimated:YES];
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    }

}


-(void)JSONRecieved:(NSDictionary *)response
{
   NSDictionary *Dict = response;
  //  NSLog(@"response12%@",response);
    // NSLog(@"response12%@",[dict objectForKey:@"packcount"]);
    if ([Dict objectForKey:@"chat"]!=[NSNull null])
    {
        userChatArray = [Dict objectForKey:@"chat"];
      //  [bubbleTable reloadData];
       // growingtextView.text = @"";

    }
    
   // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // NSString * userNameStr=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userNAME"]];
    
   /* bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
    
    for (int i = 0; i <[userChatArray count]; i++)
    {
        text = [[userChatArray objectAtIndex:i]objectForKey:@"chatTEXT"];
        imageStr = [[userChatArray objectAtIndex:i]objectForKey:@"userIMAGE"];
        nameStr = [[userChatArray objectAtIndex:i]objectForKey:@"userNAME"];
        ChatDateStr = [[userChatArray objectAtIndex:i]objectForKey:@"chatDATE"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        

        NSDate *dateChat = [dateFormat dateFromString:ChatDateStr];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        NSString * dateStr = [NSString stringWithFormat:@"%@",[formatter stringFromDate:dateChat]];
                NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
       [dateFormat1 setDateFormat:@"dd-MM-yyyy HH:mm"];
        NSDate *dateChat1 = [dateFormat1 dateFromString:dateStr];
        userBubble = [NSBubbleData dataWithText:text date:dateChat1 type:BubbleTypeMine];
       // userBubble.avatar = nil;
        
        [bubbleData addObject:userBubble];
    }*/
    
      //  [bubbleTable reloadData];
    //[chatTextField resignFirstResponder];
    
    
        switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallyIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_UserIDRequired:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
    case  APIResponseStatus_ChatTextRequired:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }

    case  APIResponseStatus_ChatDateRequired:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }

    case  APIResponseStatus_NewChatSuccessfull:
        {
           
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }

    case  APIResponseStatus_NewChatFailed:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
        default:
            
            break;
    
            
    }
}


@end
