//
//  RMoreViewController.m
//  Rally
//
//  Created by Ambika on 6/6/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RMoreViewController.h"
#import "REditProfileViewController.h"
#import "RLogInViewController.h"
#import "RAppDelegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RSliderViewController.h"
#import "RLogInViewController.h"
#import "RPrivacyPolicyViewController.h"
#import "RTermsofServiceViewController.h"
#import "RAppDelegate.h"
@interface RMoreViewController ()

{
    MBProgressHUD *ProgressHUD;
    NSDictionary * userInfoDic;
}
@end

@implementation RMoreViewController
@synthesize logOutBtn,profileImg,profilenameLbl,policyBtn,termsBtn,helpedTip,policyView,editView,editProfile,profileScrollview,getuserInfoDictionary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userInfoDic = [[NSDictionary alloc]init];
  //  getuserInfoDictionary = [[NSDictionary alloc]init];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    [self callWebServiceForUser_id];
    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.title = @"More";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    
    
       editView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baground.png"]];
    policyView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baground.png"]];
    editView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    editView.layer.borderWidth= 1.5f;
    policyView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    policyView.layer.borderWidth= 1.5f;
    
    UIView *bioline = [[UIView alloc] initWithFrame:CGRectMake(0,editView.frame.size.height-46,editView.frame.size.width, 1.2)];
    [bioline setBackgroundColor:[UIColor lightGrayColor]];
    [editView addSubview:bioline];
    UIView *dobline = [[UIView alloc] initWithFrame:CGRectMake(0,editView.frame.size.height-138,editView.frame.size.width, 1.2)];
    [dobline setBackgroundColor:[UIColor lightGrayColor]];
    [editView addSubview:dobline];
    
    
    UIView *homeline = [[UIView alloc] initWithFrame:CGRectMake(0,policyView.frame.size.height-138,policyView.frame.size.width, 1.2)];
    [homeline setBackgroundColor:[UIColor lightGrayColor]];
    [policyView addSubview:homeline];
    
    UIView *cPassline = [[UIView alloc] initWithFrame:CGRectMake(0,policyView.frame.size.height-46,policyView.frame.size.width, 1.2)];
    [cPassline setBackgroundColor:[UIColor lightGrayColor]];
    [policyView addSubview:cPassline];
    UIView *Passline = [[UIView alloc] initWithFrame:CGRectMake(0,policyView.frame.size.height-92,policyView.frame.size.width, 1.2)];
    [Passline setBackgroundColor:[UIColor lightGrayColor]];
    [policyView addSubview:Passline];
   // photoButton.titleLabel.textColor = [UIColor grayColor];
    //saveBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    //    BioTextField.
    
    //    [self.navigationItem setHidesBackButton:YES animated:YES];
    //    [self.navigationItem setLeftBarButtonItem:nil animated:NO];
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"More";
    self.navigationItem.titleView = lblTitle;
    
    
    profileScrollview.scrollEnabled = YES;
    profileScrollview.contentSize = CGSizeMake(320, 900);
    profileScrollview.showsVerticalScrollIndicator = NO;
    profileScrollview.showsHorizontalScrollIndicator=NO;
    profileScrollview.delegate = self;
    profileImg.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    profileImg.layer.cornerRadius = profileImg.bounds.size.width/2;
    profileImg.clipsToBounds =YES;
    if ([[getuserInfoDictionary objectForKey:@"userNAME"] isEqual:[NSNull null]])
    {
        profilenameLbl.text =@"";

    }
    else
    {
        NSString *  namestr = [NSString stringWithFormat:@"%@",[getuserInfoDictionary objectForKey:@"userNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            
       namestr = [namestr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
           namestr = [namestr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        namestr = [namestr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];

        profilenameLbl.text = namestr;
    }
          //profileImageView
    
    if ([[getuserInfoDictionary objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
    {
        
        profileImg.image = [UIImage  imageNamed:@""];
        

           }
    else
    {
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[getuserInfoDictionary objectForKey:@"userIMAGE"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
       // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
       // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        profileImg.imageURL = imageURL;
        

    }
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"updateImage"])
    {
        NSData *imageData = [NSData dataWithData:[[NSUserDefaults standardUserDefaults]objectForKey:@"updateImage"]];
        UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        
        profileImg.image = imageLoad;

    }
    

}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editProfileBtn:(id)sender
{
    REditProfileViewController *EditProfileViewController = [[REditProfileViewController alloc]initWithNibName:@"REditProfileViewController" bundle:nil];
    if (![getuserInfoDictionary isEqual:[NSNull null]])
    {
        EditProfileViewController.userInfoDictionary= userInfoDic;
    }
    [self.navigationController pushViewController:EditProfileViewController animated:NO];
   

}
-(void)callWebServiceForUser_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@",[defaults objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Userid:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_Userid:(NSDictionary *)response
{
    NSDictionary * responseDict = response;
//    NSLog(@"response12%@",response);
    // NSLog(@"response12%@",[dict objectForKey:@"packcount"]);
    
    
    if ([responseDict objectForKey:@"userinfo"] != [NSNull null])
    {
        userInfoDic = [responseDict objectForKey:@"userinfo"];
        NSString *  namestr = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            
            namestr = [namestr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
           namestr = [namestr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        namestr = [namestr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];

        profilenameLbl.text = namestr;
    }
    
   
    switch ([responseDict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ProfileviewSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
        }
        default:
            
            break;
            
    }
}



- (IBAction)logoutAction:(id)sender
{
    
    [self callWebService_logout];
}
-(void)callWebService_logout
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/logout.php?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Error", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
        switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_LogoutSuccessfully:
            
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"id"];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"slideView"];
            
            
            
            [[NSUserDefaults standardUserDefaults]synchronize ];
          // [FBSession.activeSession closeAndClearTokenInformation];
//            FBSession* session = [FBSession activeSession];
//            [session closeAndClearTokenInformation];
//            [session close];
//            [FBSession setActiveSession:nil];
            
                    [[GPPSignIn sharedInstance] signOut];
            
            
            
           // if (appDelegate.session.isOpen) {
                
                // if a user logs out explicitly, we delete any cached token information, and next
                // time they run the applicaiton they will be presented with log in UX again; most
                // users will simply close the app or switch away, without logging out; this will
                // cause the implicit cached-token login to occur on next launch of the application
                
                //[appDelegate.session closeAndClearTokenInformation];
                
                FBSession *session=[FBSession activeSession];
                [session closeAndClearTokenInformation];
                [session close];
                [[FBSession activeSession] closeAndClearTokenInformation];
                [[FBSession activeSession] close];
                [FBSession setActiveSession:nil];
                
                // [ASAppDelegate facebook].accessToken=nil;
                //[ASAppDelegate facebook].expirationDate=nil;
                
                NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                NSArray* facebookCookies = [cookies cookiesForURL:
                                            [NSURL URLWithString:@"http://login.facebook.com"]];
                for (NSHTTPCookie* cookie in facebookCookies)
                {
                    [cookies deleteCookie:cookie];
                }
                
                for (NSHTTPCookie *_cookie in cookies.cookies)
                {
                    NSRange domainRange = [[_cookie domain] rangeOfString:@"facebook"];
                    if(domainRange.length > 0){
                        [cookies deleteCookie:_cookie];
                    }
                    
                    
                }
            //[[NSUserDefaults standardUserDefaults] setObject:facebook_userId  forKey:@"facebook_userId"];

             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fB_succesfully_lg"];
            RLogInViewController *LogInViewController = [[RLogInViewController alloc]initWithNibName:@"RLogInViewController" bundle:nil];
            LogInViewController.FirstTimelogout = YES;
          
            [self.navigationController pushViewController:LogInViewController animated:YES];

            //[self.navigationController popToRootViewControllerAnimated:YES];
            break;
        }
        case  APIResponseStatus_FailedLogout:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally is not Edit....please try  again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
            
            
        default:
            
            break;
            
    }
}

- (IBAction)helpfulTipAction:(id)sender

{
    RSliderViewController *SliderViewController = [[RSliderViewController alloc]initWithNibName:@"RSliderViewController" bundle:nil];
    SliderViewController.helpFullTipBtn = YES;
    [self.navigationController pushViewController:SliderViewController animated:YES];
}

- (IBAction)privacyBtn:(id)sender {
    RPrivacyPolicyViewController *PrivacyPolicyViewController = [[RPrivacyPolicyViewController alloc]initWithNibName:@"RPrivacyPolicyViewController" bundle:nil];
    //SliderViewController.helpFullTipBtn = YES;
    [self.navigationController pushViewController:PrivacyPolicyViewController animated:YES];
}

- (IBAction)termsBtn:(id)sender {
    RTermsofServiceViewController *TermsofServiceViewController = [[RTermsofServiceViewController alloc]initWithNibName:@"RTermsofServiceViewController" bundle:nil];
    //SliderViewController.helpFullTipBtn = YES;
    [self.navigationController pushViewController:TermsofServiceViewController animated:YES];
}
@end
