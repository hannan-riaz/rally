//
//  RAlbumViewController.m
//  Rally
//
//  Created by Ambika on 7/11/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RAlbumViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "RImageViewController.h"
@interface RAlbumViewController ()
{
    MBProgressHUD *ProgressHUD;
    NSMutableArray * useralbumArray;
    AsyncImageView * userImage;
    NSMutableArray * packArr;
    UILabel * timeLbl1;
    UILabel * albumNameLbl;
    UILabel * photosCountLbl;
    UIView *cellView;
    UITableViewCell *cell;
    UIBarButtonItem *rightBarButton ;
    UIButton * redDeleteBtn;
    BOOL showDeleteBtn;
    NSString *albumid_StrForDelete;
    int rowNumber;
}
@end

@implementation RAlbumViewController
@synthesize  profileUSerID,albunTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    showDeleteBtn = NO;
    useralbumArray = [[NSMutableArray alloc]init];
    packArr = [[NSMutableArray alloc]init];
    albunTableView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    redDeleteBtn.hidden = YES;
    [self callWebSercvice_getrallyalbum];
    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.title = @"Photo Albums";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
   if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:profileUSerID])
   {
 
   rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(deleteAlbumBtn:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
   }
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Photo Albums";
    self.navigationItem.titleView = lblTitle;
}

-(void)deleteAlbumBtn:(id)sender
{
    rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(removeDleteBtn1:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];

    showDeleteBtn = YES;
    [albunTableView reloadData];
    
}
-(void)removeDleteBtn1:(id)sender
{
    rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(deleteAlbumBtn:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
     showDeleteBtn = NO;
    [albunTableView reloadData];
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)callWebSercvice_getrallyalbum
{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
      NSString * urlStr;
  //  urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@",profileUSerID];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:profileUSerID])
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@&friendid=",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    }
    else
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyalbum.php?userid=%@&friendid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],profileUSerID];
    }

    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_getrallyalbum:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_getrallyalbum:(NSDictionary *)response
{
    NSDictionary *dict = response;
   // NSLog(@"response12%@",response);
    
    if (![[dict objectForKey:@"useralbum"] isEqual:[NSNull null]] )
    {
        useralbumArray= [dict objectForKey:@"useralbum"];
    // [self getUserData];
    [albunTableView reloadData];
    }
    switch ([dict[kRAPIResult] integerValue])
    {
        case APIResponseStatus_GetUserAlbumSuccessfull:
        {
            break;
        }
        case APIResponseStatus_GetUserAlbumFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_FriendNotInPack:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This User is not in your pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 512;
            [alert show];
            
            break;
        }
        default:
        break;
            
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    return [useralbumArray count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
     cell = nil;
    static NSString *TableViewCellIdentifier = @"Cell";
    
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
        cellView = [[UIView alloc]initWithFrame: CGRectMake(0,0,320,60)];
        cellView.backgroundColor = [UIColor clearColor];
        cellView.userInteractionEnabled = YES;

        userImage = [[AsyncImageView alloc]init];
        //userImage.image = [UIImage imageNamed:@""];
        userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
        userImage.backgroundColor = [UIColor clearColor];
        userImage.tag = -1;
        userImage.frame = CGRectMake(8, 10, 50, 50);
        [cellView addSubview:userImage];

        
        timeLbl1 = [[UILabel alloc]init];
        timeLbl1.frame = CGRectMake(64,27, 120, 20);
        timeLbl1.tag = -2;
        timeLbl1.textColor = [UIColor grayColor];
        timeLbl1.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
        [cellView addSubview:timeLbl1];
        
        albumNameLbl = [[UILabel alloc]init];
        albumNameLbl.frame = CGRectMake(64,10, 250, 20);
        albumNameLbl.tag = -3;
        albumNameLbl.textColor = [UIColor blackColor];
        albumNameLbl.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
        [cellView addSubview:albumNameLbl];
        
        photosCountLbl = [[UILabel alloc]init];
        photosCountLbl.frame = CGRectMake(64,42, 120, 20);
        photosCountLbl.tag = -4;
        photosCountLbl.textColor = [UIColor lightGrayColor];
        photosCountLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
       [cellView addSubview:photosCountLbl];
        if (showDeleteBtn == YES)
        {
      
        //cell = [[albunTableView visibleCells] objectAtIndex:i];
        redDeleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        redDeleteBtn.frame = CGRectMake(260,21,30,30);
        
        [redDeleteBtn setImage:[UIImage imageNamed:@"redMinus.png"]  forState:UIControlStateNormal];
        //[redDeleteBtn setTitle:@"join" forState:UIControlStateNormal];
         redDeleteBtn.tag = -5;
        [redDeleteBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [redDeleteBtn addTarget:self action:@selector(DeleteRowAndAlbumaction:) forControlEvents:UIControlEventTouchUpInside];
        
        [cellView addSubview:redDeleteBtn];
        }
         [cell addSubview:cellView];
    }
    cell.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
    if ([[[useralbumArray objectAtIndex:indexPath.row ] objectForKey:@"thumb"] isEqual:[NSNull null]])
    {
        userImage.image =[UIImage imageNamed:@""];
    }
    else
    {
        AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[useralbumArray objectAtIndex:indexPath.row ] objectForKey:@"thumb"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        imageView.imageURL = imageURL;
    }
    if (![[[useralbumArray objectAtIndex:indexPath.row ] objectForKey:@"rallydate"] isEqual:[NSNull null]])
    {
        UILabel * dateLbl = (UILabel *)[cell viewWithTag:-2];
        NSString * dateString =[NSString stringWithFormat:@"%@",[[useralbumArray objectAtIndex:indexPath.row]objectForKey:@"rallydate"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * dateFromString1 = [[NSDate alloc] init];
        
        dateFromString1 = [dateFormatter dateFromString:dateString];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        
        [dateFormatter1 setDateFormat:@"M/d/yyyy"];
        // NSDate * dateFromString2 = [[NSDate alloc] init];
        
        NSString * dateFromString2 = [dateFormatter1 stringFromDate:dateFromString1];
        
        
        dateLbl.text = dateFromString2;
    }
    if (![[[useralbumArray objectAtIndex:indexPath.row ] objectForKey:@"albumname"] isEqual:[NSNull null]])
    {
        UILabel * albumnameLbl = (UILabel *)[cell viewWithTag:-3];
        
        NSString * albumNameStr = [NSString stringWithFormat:@"%@",[[useralbumArray objectAtIndex:indexPath.row]objectForKey:@"albumname"]];
        albumNameStr = [albumNameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [albumNameStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *albumname_Str = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        
        if (albumname_Str == nil||[albumname_Str isEqualToString:@""])
        {
             albumnameLbl.text = albumNameStr;
        }
        else
        {
            
            albumnameLbl.text = albumname_Str;
        }
        

       
       
    }
    if (![[[useralbumArray objectAtIndex:indexPath.row ] objectForKey:@"imagecount"] isEqual:[NSNull null]])
    {
        UILabel * countLbl = (UILabel *)[cell viewWithTag:-4];
        countLbl.text =[NSString stringWithFormat:@"%@ Photos",[[useralbumArray objectAtIndex:indexPath.row]objectForKey:@"imagecount"]];
        
        
        
    }
    //    CGSize stringsize = [albumNameStr sizeWithFont:[UIFont systemFontOfSize:14]];
    //    //or whatever font you're using
    //    //[userNameBtn setFrame:CGRectMake(55,5,stringsize.width,stringsize.height)];
    //    timeLbl1.frame = CGRectMake(stringsize.width+54,4,100, stringsize.height);
    
    
       return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (showDeleteBtn == NO)
    {
    
    if(useralbumArray.count > 0)
    {
        
        NSString *albumidstr = [NSString stringWithFormat:@"%ld",(long)[[[useralbumArray objectAtIndex:indexPath.row]objectForKey:@"albumid"]integerValue]] ;
        NSString *rallyidstr = [NSString stringWithFormat:@"%ld",(long)[[[useralbumArray objectAtIndex:indexPath.row]  objectForKey:@"rallyid"]integerValue]] ;
        NSString *albumnameStr = [NSString stringWithFormat:@"%@",[[useralbumArray objectAtIndex:indexPath.row]  objectForKey:@"albumname"]];

        
        RImageViewController *ImageViewController = [[RImageViewController alloc]initWithNibName:@"RImageViewController" bundle:nil];
        ImageViewController.rallyid_str =rallyidstr;
        ImageViewController.albumId_str=albumidstr;
        ImageViewController.albumname_str=albumnameStr;
        ImageViewController.friendUserId_serch = profileUSerID;
        [self.navigationController pushViewController:ImageViewController animated:NO];
    }
    }
}
-(void)DeleteRowAndAlbumaction:(UIButton *)sender
{   CGRect buttonFrameInTableView = [sender convertRect:redDeleteBtn.bounds toView:albunTableView];
    NSIndexPath *indexPath = [albunTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    rowNumber = 0;
    
    rowNumber += indexPath.row;
    
    [redDeleteBtn setTag:rowNumber];
    albumid_StrForDelete = [NSString stringWithFormat:@"%ld",(long)[[[useralbumArray objectAtIndex:rowNumber]objectForKey:@"albumid"]integerValue]] ;
     NSString *albumname_Str = [NSString stringWithFormat:@"Are you want to delete the album %@ ?",[[useralbumArray objectAtIndex:indexPath.row]  objectForKey:@"albumname"]];
    UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Delete" message:albumname_Str delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    alert.tag = -7;
    [alert show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == -7)
    {
        if (buttonIndex == 0)
        {
            [self callWebSercvice_DeleteAalbum];
        }
        else
        {
            
        }
    }
    else if (alertView.tag == 512){
         [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)callWebSercvice_DeleteAalbum
{
    
    NSString * urlStr;
   
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] isEqualToString:profileUSerID])
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deletealbum.php?userid=%@&albumid=%@",profileUSerID,albumid_StrForDelete];
    }
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_DeleteAalbum:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_DeleteAalbum:(NSDictionary *)response
{
    NSDictionary *dict = response;
    // NSLog(@"response12%@",response);
    
    
    [self callWebSercvice_getrallyalbum];
    [albunTableView reloadData];
    switch ([dict[kRAPIResult] integerValue])
    {
            
        case APIResponseStatus_RallyImageDeletedSuccessfully:
        {
            break;
        }
        case APIResponseStatus_DeleteRallyImageFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_FriendNotInPack:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This User is not in your pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        default:
            break;
            
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
