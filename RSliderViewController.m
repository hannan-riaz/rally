//
//  RSliderViewController.m
//  Rally
//
//  Created by Ambika on 9/5/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RSliderViewController.h"
#import "RLogInViewController.h"
@interface RSliderViewController ()

{
    NSArray * albumDataArray;
    int x;
   BOOL  pageControlBeingUsed;
    NSTimer * timer;
}
@end

@implementation RSliderViewController
@synthesize imageView,pageCntrl,imageScrollView,helpFullTipBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//   if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
//    {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.automaticallyAdjustsScrollViewInsets = NO;
//    }
    if (helpFullTipBtn == NO) {
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
         imageScrollView .frame = CGRectMake(0, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height);
    }else
    {
         imageScrollView .frame = CGRectMake(0, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height-88);
    }
    }else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            imageScrollView .frame = CGRectMake(0, 64, imageScrollView.frame.size.width, imageScrollView.frame.size.height- 64);
        }else
        {
            imageScrollView .frame = CGRectMake(0, 44, imageScrollView.frame.size.width, imageScrollView.frame.size.height-132);
        }
    }
    albumDataArray = [[NSMutableArray alloc]init];
    //pageCntrl.currentPageIndicatorTintColor =[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    pageCntrl.currentPageIndicatorTintColor =[UIColor colorWithRed:103.0f/255.0f green:215.0f/255.0f blue:103.0f/255.0f alpha:1.0];
        [self imageScroll ];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    if (helpFullTipBtn == NO) {
        self.navigationController.navigationBar.hidden = YES;

    }
    else
    {
        self.navigationController.navigationBar.hidden = NO;

//        self.navigationItem.title = @"Helpful Tips";
//        UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//        [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//        [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
        
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        UILabel *lblTitle = [[UILabel alloc] init];
        lblTitle.frame = CGRectMake(0,0, 220, 40) ;
        // lblTitle.text =nameStr;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.numberOfLines = 0;
        //lblTitle.backgroundColor = [UIColor redColor];
        lblTitle.textColor = [UIColor whiteColor];
        [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
        //[lblTitle sizeToFit];
        lblTitle.text = @"Helpful Tips";
        self.navigationItem.titleView = lblTitle;
    }
    
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)imageScroll
{
x = 0;
imageScrollView.scrollEnabled = YES;
    imageScrollView.pagingEnabled = YES;

imageScrollView.showsVerticalScrollIndicator = YES;
imageScrollView.showsHorizontalScrollIndicator=NO;
imageScrollView.delegate = self;
     if (helpFullTipBtn == NO) {
   albumDataArray = @[@"ios-1",@"ios-2",@"ios-3",@"ios-4",@"ios-5",@"ios-6",@"ios-7",@""];
     }
    else
    {
        albumDataArray = @[@"ios-1",@"ios-2",@"ios-3",@"ios-4",@"ios-5",@"ios-6",@"ios-7"];

    }

for (int i = 0; i <[albumDataArray count]; i++)
{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        imageView = [[ UIImageView alloc]initWithFrame:CGRectMake(x, 0, 320, imageScrollView.frame.size.height)];
    }else
    {
        imageView = [[ UIImageView alloc]initWithFrame:CGRectMake(x, 0, 320, imageScrollView.frame.size.height)];
    }
   //imageView = [[ UIImageView alloc]initWithFrame:CGRectMake(x, 0, 320, 480)];
 //   NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[albumDataArray objectAtIndex:i]objectForKey:@"imagename"] ];
   // NSURL *imageURL = [NSURL URLWithString:imgStr];
   imageView.image=[UIImage imageNamed:[albumDataArray objectAtIndex:i]];
   // imageView.tag = i;
    imageView.backgroundColor = [UIColor clearColor];
  [imageScrollView addSubview:imageView];
    
    x = x + imageView.frame.size.width;
}
self.automaticallyAdjustsScrollViewInsets = NO;
imageScrollView.contentSize = CGSizeMake(x,imageScrollView.frame.size.height);
    //[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

//- (void)scrollingTimer {
//    // access the scroll view with the tag
//    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
//    // same way, access pagecontroll access
//    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
//    // get the current offset ( which page is being displayed )
//    CGFloat contentOffset = scrMain.contentOffset.x;
//    // calculate next page to display
//    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
//    // if page is not 10, display it
//    if( nextPage!=10 )  {
//        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
//        pgCtr.currentPage=nextPage;
//        // else start sliding form 1 :)
//    } else {
//        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
//        pgCtr.currentPage=0;
//    }
//}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = imageScrollView.frame.size.width;
    int page = floor((imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageCntrl.currentPage = page;
    if (helpFullTipBtn == NO) {
  
    if (imageScrollView.contentOffset.x > 1920) {

        RLogInViewController *LogInViewController = [[RLogInViewController alloc]initWithNibName:@"RLogInViewController" bundle:nil];
        [self.navigationController pushViewController:LogInViewController animated:YES];
          }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)PagecontrolAction:(id)sender
{
    CGRect frame;
    frame.origin.x =imageScrollView.frame.size.width * pageCntrl.currentPage;
    frame.origin.y = 0;
    frame.size = imageScrollView.frame.size;
    [imageScrollView scrollRectToVisible:frame animated:YES];
    

}

- (IBAction)skipBtn:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"slideView"];

     RLogInViewController *LogInViewController = [[RLogInViewController alloc]initWithNibName:@"RLogInViewController" bundle:nil];
    //[self.view addSubview:LogInViewController];
   [self.navigationController pushViewController:LogInViewController animated:YES];
 
}
@end
