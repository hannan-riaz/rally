//
//  RForgetPasswordViewController.m
//  Rally
//
//  Created by Ambika on 7/24/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RForgetPasswordViewController.h"
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"

@interface RForgetPasswordViewController ()

{
  MBProgressHUD *  ProgressHUD;
}
@end

@implementation RForgetPasswordViewController
@synthesize emilIdTxt;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    emilIdTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    emilIdTxt.layer.borderWidth= 1.3f;

    // Do any additional setup after loading the view from its nib.
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    emilIdTxt.leftView = paddingView;
    emilIdTxt.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)viewWillAppear:(BOOL)animated
{
//    self.navigationItem.title = @"Forgot password";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
//    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.hidden = NO;
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.f green:245.0/255.f blue:245.0/255.f alpha:1.0];
//    self.navigationController.navigationBar.translucent =NO;
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    
    
    
    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Forgot password";
    self.navigationItem.titleView = lblTitle;
    
    
    
    
//    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 25.0f)];
//    [backButton setImage:[UIImage imageNamed:@"back.png"]  forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    UIBarButtonItem *submitBtn = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(forgetAction:)];
//    submitBtn.tintColor = [UIColor lightGrayColor];
//    [submitBtn setTitleTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0]} forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = submitBtn;

}
-(void)callWebSercvice_forgotpassword
{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Requesting...";
        ProgressHUD.dimBackground = YES;
    }
    
    NSString * urlStr;
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/forgotpassword.php?email=%@",emilIdTxt.text];
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_forgotpassword:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_forgotpassword:(NSDictionary *)response
{
    NSDictionary *dict = response;
    // NSLog(@"response12%@",response);
    
    
    switch ([dict[kRAPIResult] integerValue])
    {
     
        case APIResponseStatus_ForgotPasswordSuccessfull:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"Further instructions has been sent to your email account. Please check Your email account to change password. " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

            break;
        }
        case APIResponseStatus_ForgotPasswordFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_UserNotRegistered:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This email address is not registered. Please check you email address and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_EmailRequired:
        {
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Photo" message:@"This User is not in your pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }

        case APIResponseStatus_InvalidEmail:
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This email is Invalid." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }

        default:
            break;
            
    }
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)forgetAction:(id)sender
{
    [emilIdTxt resignFirstResponder];
    [self callWebSercvice_forgotpassword];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
