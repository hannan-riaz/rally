//
//  RJoinEditViewController.h
//  Rally
//
//  Created by Ambika on 6/13/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJoinEditViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextViewDelegate>
@property(nonatomic,strong)IBOutlet UIBarButtonItem *crossBtn;
@property(nonatomic,strong)IBOutlet UIBarButtonItem *nextBtn;
@property(nonatomic,strong)IBOutlet UIToolbar* toolbar;
@property (strong, nonatomic) IBOutlet UITextView *adressTxtView;
@property (strong, nonatomic) IBOutlet UITextView *discriptionTxtView;
@property (strong, nonatomic) IBOutlet UIView *addressView;
@property (strong, nonatomic) IBOutlet UIView *dicriptionView;
@property (strong, nonatomic) NSString *dicriptionString;
@property (strong, nonatomic) NSString *userIdStr;
@property (strong, nonatomic) NSString *rallyIdStr;
@property (strong, nonatomic) NSString *nameString;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) NSString *JoinRallyId;
@property (strong, nonatomic) IBOutlet UIButton *resignBtn;
@property (strong, nonatomic) IBOutlet UITableView *joinRallyTableView;
- (IBAction)saveAction:(id)sender;
- (IBAction)resignBtnAction:(id)sender;
-(IBAction)closeBtnAction:(id)sender;
@end
