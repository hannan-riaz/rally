//
//  RMoreViewController.h
//  Rally
//
//  Created by Ambika on 6/6/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface RMoreViewController : UIViewController<UIScrollViewDelegate>
- (IBAction)editProfileBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *logOutBtn;
- (IBAction)logoutAction:(id)sender;
@property (strong, nonatomic) IBOutlet AsyncImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *profilenameLbl;
@property (strong, nonatomic) IBOutlet UIButton *policyBtn;
@property (strong, nonatomic) IBOutlet UIButton *termsBtn;
@property (strong, nonatomic) IBOutlet UIButton *helpedTip;
@property (strong, nonatomic) IBOutlet UIView *policyView;
@property (strong, nonatomic) IBOutlet UIScrollView *profileScrollview;
@property (strong, nonatomic) NSDictionary *getuserInfoDictionary;
- (IBAction)helpfulTipAction:(id)sender;
- (IBAction)privacyBtn:(id)sender;

- (IBAction)termsBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *editView;
@property (strong, nonatomic) IBOutlet UIButton *editProfile;
@end
