//
//  CKViewController.m
//   MBCalendarKit
//
//  Created by Moshe Berman on 4/10/13.
//  Copyright (c) 2013 Moshe Berman. All rights reserved.
//

#import "CKCalendarViewControllerInternal.h"

#import "CKCalendarView.h"

#import "CKCalendarEvent.h"

#import "NSCalendarCategories.h"

#import "RMapViewController.h"
@interface CKCalendarViewControllerInternal () <CKCalendarViewDataSource, CKCalendarViewDelegate>

@property (nonatomic, strong) CKCalendarView *calendarView;

@property (nonatomic, strong) UISegmentedControl *modePicker;

@property (nonatomic, strong) NSMutableArray *events;

@end

@implementation CKCalendarViewControllerInternal 

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    /* iOS 7 hack*/
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    [self setTitle:NSLocalizedString(@"Rallendar", @"A title for the calendar view.")];
    
    UINavigationBar* navBar = self.navigationController.navigationBar;
    int borderSize = 3;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navBar.frame.size.height-borderSize,navBar.frame.size.width, borderSize)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0]];
    [self.navigationController.navigationBar addSubview:navBorder];

    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"New" style:UIBarButtonItemStyleBordered target:self action:@selector(pushToMapView)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.rightBarButtonItem.tintColor=[UIColor blackColor];    /* Prepare the events array */
    
    [self setEvents:[NSMutableArray new]];
    
    /* Calendar View */

    [self setCalendarView:[CKCalendarView new]];
    [[self calendarView] setDataSource:self];
    [[self calendarView] setDelegate:self];
    [[self view] addSubview:[self calendarView]];

    [[self calendarView] setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] animated:NO];
    [[self calendarView] setDisplayMode:CKCalendarViewModeMonth animated:NO];
    
    /* Mode Picker */
    
    NSArray *items = @[NSLocalizedString(@"Day", @"A title for the Day view button."), NSLocalizedString(@"Week", @"A title for the Week view button."), NSLocalizedString(@"Month",@"A title for the Month view button.")];
    
    [self setModePicker:[[UISegmentedControl alloc] initWithItems:items]];
    [[self modePicker] setFrame:CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 30, 25)];
    [[self modePicker] setSegmentedControlStyle:UISegmentedControlStyleBar];
    [[self modePicker] addTarget:self action:@selector(modeChangedUsingControl:) forControlEvents:UIControlEventValueChanged];
    [[self modePicker] setSelectedSegmentIndex:2];
    [[self modePicker] setTintColor:[UIColor colorWithRed:132.0f/255.0f green:132.0f/255.0f blue:130.0f/255.0f alpha:1.0]];

    /* Toolbar setup */
    
   /*2 NSString *todayTitle = NSLocalizedString(@"Today", @"A button which sets the calendar to today.");
    UIBarButtonItem *todayButton = [[UIBarButtonItem alloc] initWithTitle:todayTitle style:UIBarButtonItemStyleBordered target:self action:@selector(todayButtonTapped:)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:[self modePicker]];
    
    [self setToolbarItems:@[todayButton, item] animated:NO];*/
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:[self modePicker]];
    
    [self setToolbarItems:@[item] animated:NO];
    [[self navigationController] setToolbarHidden:NO animated:NO];

}
-(void)pushToMapView
{
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"dateevent"] == YES)
    {
        
    }
    else
    {
        
        NSComparisonResult result;
        
        result = [[[self calendarView] date] compare:[NSDate date]]; // comparing two dates
        
               if(result==NSOrderedSame)
        {
           
            [self showMapView];

        }
        
        else if(result == NSOrderedDescending)
        {
            [self showMapView];
        
        }
        else if(result ==  NSOrderedAscending)
        {
            NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
            [dateFormat1  setDateFormat:@"dd LLLL yyyy"];
            NSString *dateStr1 = [dateFormat1 stringFromDate:[[self calendarView] date]];
            
            NSDateFormatter *dateFormat2  = [[NSDateFormatter alloc]init];
            [dateFormat2  setDateFormat:@"dd LLLL yyyy"];
            NSString *dateStr2 = [dateFormat1 stringFromDate:[NSDate date]];
            if ([dateStr1 isEqualToString:dateStr2]) {
                [self showMapView];
            }
            else
            {
            [[[UIAlertView alloc]initWithTitle:@"Whoops" message:@"Whoops You cannot create a Rally on a date that has passed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
            }
        }
        
       
       
    }
   
}
-(void)showMapView
{
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"dd LLLL yyyy"];
    NSString *dateStr1 = [dateFormat1 stringFromDate:[[self calendarView] date]];
    
    RMapViewController *MapViewController  = [[RMapViewController alloc] init];
    UINavigationController *navcont = [[UINavigationController alloc] initWithRootViewController:MapViewController];
    //                 //MapViewController.rallyInfoDict = event.info;
    MapViewController.calenderViewBool = YES;
    MapViewController.calenerDateStr = dateStr1;
    [self presentViewController:navcont animated:YES completion:nil];
    

}
-(void)popToView
{
     [self dismissViewControllerAnimated:YES completion:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Toolbar Items

- (void)modeChangedUsingControl:(id)sender
{
    
 [[self calendarView] setDisplayMode:(CKCalendarDisplayMode)[[self modePicker] selectedSegmentIndex]];
   
}

- (void)todayButtonTapped:(id)sender
{
    [[self calendarView] setDate:[NSDate date] animated:NO];
}

#pragma mark - CKCalendarViewDataSource

- (NSArray *)calendarView:(CKCalendarView *)CalendarView eventsForDate:(NSDate *)date
{
    if ([[self dataSource] respondsToSelector:@selector(calendarView:eventsForDate:)]) {
        return [[self dataSource] calendarView:CalendarView eventsForDate:date];
    }
    return nil;
}

#pragma mark - CKCalendarViewDelegate

// Called before/after the selected date changes
- (void)calendarView:(CKCalendarView *)calendarView willSelectDate:(NSDate *)date
{
    if ([self isEqual:[self delegate]]) {
        return;
    }
    
    if ([[self delegate] respondsToSelector:@selector(calendarView:willSelectDate:)]) {
        [[self delegate] calendarView:calendarView willSelectDate:date];
    }
}

- (void)calendarView:(CKCalendarView *)calendarView didSelectDate:(NSDate *)date
{
    if ([self isEqual:[self delegate]]) {
        return;
    }
    
    if ([[self delegate] respondsToSelector:@selector(calendarView:didSelectDate:)]) {
        [[self delegate] calendarView:calendarView didSelectDate:date];
    }
}

//  A row is selected in the events table. (Use to push a detail view or whatever.)
- (void)calendarView:(CKCalendarView *)calendarView didSelectEvent:(CKCalendarEvent *)event
{
    if ([self isEqual:[self delegate]]) {
        return;
    }
    
    if ([[self delegate] respondsToSelector:@selector(calendarView:didSelectEvent:)]) {
        [[self delegate] calendarView:calendarView didSelectEvent:event];
    }
}


#pragma mark - Calendar View

- (CKCalendarView *)calendarView
{
    return _calendarView;
}
@end
