//
//  CKCalendarHeaderColors.h
//   MBCalendarKit
//
//  Created by Moshe Berman on 4/14/13.
//  Copyright (c) 2013 Moshe Berman. All rights reserved.
//

#ifndef MBCalendarKit_CKCalendarHeaderColors_h
#define MBCalendarKit_CKCalendarHeaderColors_h

#import "NSString+Color.h"

#define kCalendarColorHeaderWeekdayTitle    [@"#545454" toColor]
#define kCalendarColorHeaderWeekdayShadow   [@"#f3f3f4" toColor]

#define kCalendarColorHeaderMonth           [@"#808080" toColor]// 2  [@"#959492" toColor]
#define kCalendarColorHeaderMonthShadow     [@"#f6f6f7" toColor]
#define kCalendarColorDisclousreBtn         [@"#C0C0C0" toColor]


#define kCalendarColorHeaderGradientLight   [@"#f4f4f5" toColor]
#define kCalendarColorHeaderGradientDark    [@"#E5E4E2" toColor] //2 [@"#ccccd1" toColor]

#define kCalendarColorHeaderTitleHighlightedBlue [@"#1980e5" toColor]

#endif
