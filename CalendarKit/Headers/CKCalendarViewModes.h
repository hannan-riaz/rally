//
//  CKCalendarViewModes.h
//   MBCalendarKit
//
//  Created by Moshe Berman on 4/10/13.
//  Copyright (c) 2013 Moshe Berman. All rights reserved.
//

#ifndef MBCalendarKit_CKCalendarViewModes_h
#define MBCalendarKit_CKCalendarViewModes_h

typedef enum {
    CKCalendarViewModeMonth = 2,
    CKCalendarViewModeWeek = 1,
    CKCalendarViewModeDay = 0
    } CKCalendarDisplayMode;

#endif
