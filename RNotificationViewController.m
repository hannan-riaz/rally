
//
//  RNotificationViewController.m
//  Rally
//
//  Created by Ambika on 6/13/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RNotificationViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "RProfileViewController.h"
#import "RJoinRallyViewController.h"
#import "RFullImageViewController.h"
#import "AsyncImageView.h"
@interface RNotificationViewController ()
{
    MBProgressHUD *ProgressHUD;
    NSMutableArray * notificationArr;
    NSMutableArray * otheruserArr;
    UIView  * cellView;
    UIButton *acceptBtn1;
    UIButton *rejectBtn1;
    UIButton * acceptRallyBtn2;
    UIButton * rejectRallyBtn2;
    UIButton *acceptBtn;
    UILabel *titleLbl;
    UIButton *commentLbl1;
    UIButton *rejectBtn;
    NSString *  sentfromStr;
    NSString *  senttoStr;
    NSString *notificationidStr;
    AsyncImageView *userImage;
    AsyncImageView *likeImage;
    UIButton * ImageBtn;
    UIButton * userImage1Btn1;
    UIButton * userNameBtn;
    UILabel *timeLbl;
    UIButton * addressBtn1;
    UIButton * titleBtn1;
    UITableView * userListTableview;
    AsyncImageView *userImagView;
    UIView  * userListView;
    UIButton * closeBtn;
    UIView * bgView;
    NSString *  rallyIdStr;
    UIButton *albumBtn1;
    UILabel * titleLbl2;
    NSTimer * timmerRepeatFor_Noti;
    int  showload;

    UIButton * addressBtn2;
}
@end

@implementation RNotificationViewController
@synthesize noitificationTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    notificationArr = [[NSMutableArray alloc]init];
   otheruserArr = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
     showload =0;
    [self callWebService];
    
    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.title = @"Notifications";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
//    noitificationTableView.backgroundColor  = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Notifications";
    self.navigationItem.titleView = lblTitle;
    
    
    timmerRepeatFor_Noti =  [NSTimer scheduledTimerWithTimeInterval:8  target:self selector:@selector(timerCallFor_hub:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timmerRepeatFor_Noti forMode:NSRunLoopCommonModes];

}

-(void)timerCallFor_hub:(NSTimer *)timer
{
    [self callWebService];

}
-(void)dealloc
{
    notificationArr = nil;
    otheruserArr = nil;
    rallyIdStr = nil;
    timmerRepeatFor_Noti = nil;
}
-(void)viewDidDisappear:(BOOL)animated
{
        [timmerRepeatFor_Noti invalidate];
    timmerRepeatFor_Noti = nil;
   
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)callWebService
{
    
    if (showload == 0) {
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    }
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/notifications.php?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] ];
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         if (showload == 0)
         {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         showload = 1;
         }
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         [timmerRepeatFor_Noti invalidate];
         timmerRepeatFor_Noti = nil;
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    if ([dict objectForKey:@"notifications"] == [NSNull null])
    {
//        [timmerRepeatFor_Noti invalidate];
//        timmerRepeatFor_Noti = nil;
        notificationArr = 0;
        [noitificationTableView reloadData];
//        [[[UIAlertView alloc]initWithTitle:@"Rally" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
        else{
            notificationArr = [dict objectForKey:@"notifications"];
            [noitificationTableView reloadData];
        }
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
    
        case  APIResponseStatus_NotificationsSuccessfull:
            
        {
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally Description Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
            
            
            break;
        }
        case  APIResponseStatus_NotificationsFailed:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally is not Edit....please try  again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
            
            
            break;
        }
            
           
        default:
            
            break;
            
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView ==  userListTableview)
   return [otheruserArr count];
    if (tableView ==  noitificationTableView)
    return [notificationArr count];
    return NO;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier1 = @"Cell1";
        static NSString *TableViewCellIdentifier = @"Cell2";
    static NSString *TableViewCellIdentifier1 = @"Cell3";
    
    static NSString *TableViewCellIdentifier2 = @"Cell4";
    static NSString* CellIdentifier2 = @"Cell5";
    
    //UILabel *nameLabel;

        //UILabel * notificationLbl;
    UITableViewCell *cell;
    if (tableView ==  userListTableview)
    {
      
         cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
                
                if (iOSVersion>=8.0)
                {
                    cell.indentationLevel = 4.0; //Added by swati 27Oct
                }
                else
                {
                    cell.indentationLevel = 5.0;
                }
                cell.indentationWidth = 10;
               // cell.indentationLevel = 6;
                userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(12,4,35 ,35)];
                userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
                userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
                userImagView.clipsToBounds = YES;
                userImagView.tag = -34;
                userImagView.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:userImagView];
                
               /* nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(53,6,220,30)];
                nameLabel.backgroundColor=[UIColor clearColor];
//                nameLabel.tag=100;
                nameLabel.textColor=[UIColor grayColor];
                nameLabel.font =[UIFont fontWithName:@"HelveticaNeue" size:16];
                nameLabel.adjustsFontSizeToFitWidth=TRUE;
                [cell.contentView addSubview:nameLabel];*/

            }
        userImagView = (AsyncImageView *)[cell viewWithTag:-34];
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[otheruserArr objectAtIndex:indexPath.row]objectForKey:@"userimage"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        userImagView.imageURL = imageURL;
        

//        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
//        cell.textLabel.textColor = [UIColor grayColor];
        if ([[[otheruserArr objectAtIndex:indexPath.row ] objectForKey:@"username"] isEqual:[NSNull null]])
        {
           // nameLabel.text=@"";
         cell.textLabel.text = @" ";
        }
        else
        {
        NSString *  namestr1 = [NSString stringWithFormat:@"%@",[[otheruserArr objectAtIndex:indexPath.row]objectForKey:@"username"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
                namestr1 = [namestr1   stringByReplacingOccurrencesOfString:@"_" withString:@" "];        }
               namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
           // nameLabel.text=namestr1;
        cell.textLabel.text = namestr1;
        }
                   return cell;
            
        }
        

    
if (tableView == noitificationTableView)
{
    if (cell == nil)
    {
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequest"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"] )
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
            cellView = [[UIView alloc]initWithFrame: CGRectMake(cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,cell.frame.size.height+54)];
            cellView.backgroundColor = [UIColor clearColor];
            cellView.tag = 50;
            cellView.userInteractionEnabled = YES;
            
            userImage1Btn1 =[[UIButton alloc]init];
            userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
            userImage1Btn1.tag = 168;
            [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            userImage1Btn1.backgroundColor = [UIColor clearColor];
            [cellView addSubview:userImage1Btn1];

            titleBtn1 =[[UIButton alloc]init];
            titleBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            titleBtn1.frame = CGRectMake(cellView.frame.origin.x+132,28, 60, 13);
            titleBtn1.tag = indexPath.row+1000;
            [titleBtn1 addTarget:self  action:@selector(viewOtherUserAction:)  forControlEvents:UIControlEventTouchUpInside];
            titleBtn1.backgroundColor = [UIColor clearColor];
           // [cellView addSubview:titleBtn1];
            

            
            titleLbl = [[UILabel alloc]init];
            titleLbl.frame = CGRectMake(55, userNameBtn.frame.origin.y+userNameBtn.frame.size.height, 120, 150);
            titleLbl.textColor = [UIColor darkGrayColor];
            titleLbl.tag = 51;
        
             //titleLbl.adjustsFontSizeToFitWidth = YES;
            //titleLbl.textAlignment = NSTextAlignmentLeft;
            titleLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
            [cellView addSubview:titleLbl];
            
            //acceptBtn =[[UIButton alloc]init];
            acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            acceptBtn.userInteractionEnabled = YES;
            acceptBtn.frame = CGRectMake(cellView.frame.origin.x+135,52,80,30);
            acceptBtn.tag = 52;
            [acceptBtn addTarget:self  action:@selector(acceptBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            acceptBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            
            [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
            acceptBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [acceptBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            acceptBtn.layer.cornerRadius = 10.0f;
            acceptBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            acceptBtn.layer.borderWidth = 0.5;
          //  [cellView addSubview:acceptBtn];
            
            rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rejectBtn.frame = CGRectMake(cellView.frame.origin.x+225,52,80,30);
            [rejectBtn setTitle:@"Decline" forState:UIControlStateNormal];
            rejectBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            rejectBtn.tag = 53;
            rejectBtn.layer.cornerRadius = 10.0f;
            rejectBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            rejectBtn.layer.borderWidth = 0.5;
            
            rejectBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [rejectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [rejectBtn addTarget:self action:@selector(rejectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            
           // [cellView addSubview:rejectBtn];
            
            acceptBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            acceptBtn1.userInteractionEnabled = YES;
            acceptBtn1.frame = CGRectMake(cellView.frame.origin.x+135,42+25,80,30);
            acceptBtn1.tag = 178;
           [acceptBtn1 addTarget:self  action:@selector(acceptBtnAction1:)  forControlEvents:UIControlEventTouchUpInside];
            acceptBtn1.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            
            [acceptBtn1 setTitle:@"Accept" forState:UIControlStateNormal];
            acceptBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [acceptBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            acceptBtn1.layer.cornerRadius = 10.0f;
            acceptBtn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            acceptBtn1.layer.borderWidth = 0.5;
            //[cellView addSubview:acceptBtn1];
            
            rejectBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            rejectBtn1.frame = CGRectMake(cellView.frame.origin.x+225,42+25,80,30);
            [rejectBtn1 setTitle:@"Decline" forState:UIControlStateNormal];
            rejectBtn1.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            rejectBtn1.tag = 53;
            rejectBtn1.layer.cornerRadius = 10.0f;
            rejectBtn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            rejectBtn1.layer.borderWidth = 0.5;
            
            rejectBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [rejectBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [rejectBtn1 addTarget:self action:@selector(deleteInvitationAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //[cellView addSubview:rejectBtn1];
            
            acceptRallyBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            acceptRallyBtn2.userInteractionEnabled = YES;
            acceptRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+135,42+25,80,30);
            acceptRallyBtn2.tag = 178;
            [acceptRallyBtn2 addTarget:self  action:@selector(aacceptRallyAction1:)  forControlEvents:UIControlEventTouchUpInside];
            acceptRallyBtn2.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            
            [acceptRallyBtn2 setTitle:@"Accept" forState:UIControlStateNormal];
            acceptRallyBtn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [acceptRallyBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            acceptRallyBtn2.layer.cornerRadius = 10.0f;
            acceptRallyBtn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
            acceptRallyBtn2.layer.borderWidth = 0.5;
            //[cellView addSubview:acceptRallyBtn2];
            
            rejectRallyBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            rejectRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+225,42+25,80,30);
            [rejectRallyBtn2 setTitle:@"Decline" forState:UIControlStateNormal];
            rejectRallyBtn2.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            rejectRallyBtn2.tag = 53;
            rejectRallyBtn2.layer.cornerRadius = 10.0f;
            rejectRallyBtn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
            rejectRallyBtn2.layer.borderWidth = 0.5;
            
            rejectRallyBtn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [rejectRallyBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [rejectRallyBtn2 addTarget:self action:@selector(deleteRallyRequestAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //[cellView addSubview:rejectRallyBtn2];


            
            userImage = [[AsyncImageView alloc]init];
            //userImage.image = [UIImage imageNamed:@""];
            userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImage.backgroundColor = [UIColor clearColor];
            userImage.tag = 54;
            userImage.frame = CGRectMake(10, 9, 40, 40);
            [cellView addSubview:userImage];
            
            addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            addressBtn1.frame = CGRectMake(125,27,220,20);
            [addressBtn1 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn1.tag = 164;
            addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cellView addSubview:addressBtn1];

            
            addressBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            // addressBtn.frame = CGRectMake(55,20,290, 20);
            [addressBtn2 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn2.tag = -15;
            //addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [addressBtn2 setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
            addressBtn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //[cellView addSubview:addressBtn2];
            

            userNameBtn =[[UIButton alloc]init];
            userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //userNameBtn.frame = CGRectMake(55,15,0,30);
            userNameBtn.tag = 55;
            [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
            userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [userNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
            // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [cellView addSubview:userNameBtn];
            
            timeLbl = [[UILabel alloc]init];
            timeLbl.frame = CGRectMake(54, 35, 120,30);
            timeLbl.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
            timeLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            timeLbl.tag = 1000+indexPath.row;
            timeLbl.textAlignment = NSTextAlignmentLeft;
            //timeLbl.text = @"22 seconds ago";
            [cellView addSubview:timeLbl];
            
           
            
            [cell.contentView  addSubview:cellView];
        }
        else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequestaccepted"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"actlike"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinviteaccepted"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallydecline"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packdecline"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"declinerallyrequest"])
       
        {
            cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier1];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier1];

            cellView = [[UIView alloc]initWithFrame: CGRectMake(cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,cell.frame.size.height+54)];
            cellView.backgroundColor = [UIColor clearColor];
            //cellView.tag = 50;
            cellView.userInteractionEnabled = YES;
            
            userImage1Btn1 =[[UIButton alloc]init];
            userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
            userImage1Btn1.tag = 168;
            [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            userImage1Btn1.backgroundColor = [UIColor clearColor];
            [cellView addSubview:userImage1Btn1];
            

            titleLbl = [[UILabel alloc]init];
            titleLbl.frame = CGRectMake(55, userNameBtn.frame.origin.y+userNameBtn.frame.size.height, 120, 150);
            titleLbl.textColor = [UIColor darkGrayColor];
            titleLbl.tag = 51;
           // titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
            //titleLbl.numberOfLines = 0;
           // [titleLbl sizeToFit];
            titleLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
            [cellView addSubview:titleLbl];
            
            userImage = [[AsyncImageView alloc]init];
            //userImage.image = [UIImage imageNamed:@""];
            userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImage.backgroundColor = [UIColor clearColor];
            userImage.tag = 54;
            userImage.frame = CGRectMake(10, 9, 40, 40);
            [cellView addSubview:userImage];
            
            
            userNameBtn =[[UIButton alloc]init];
            userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //userNameBtn.frame = CGRectMake(55,15,0,30);
            userNameBtn.tag = 55;
            [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
            userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [userNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
            // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [cellView addSubview:userNameBtn];

            addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            addressBtn1.frame = CGRectMake(titleLbl.frame.origin.x+titleLbl.frame.size.width+2,userNameBtn.frame.origin.y+userNameBtn.frame.size.height+20,220,20);
            [addressBtn1 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn1.tag = 164;
            addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cellView addSubview:addressBtn1];
            
            
            addressBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            // addressBtn.frame = CGRectMake(55,20,290, 20);
            [addressBtn2 addTarget:self  action:@selector(addressAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn2.tag = -15;
            //addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [addressBtn2 setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
            addressBtn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //[cellView addSubview:addressBtn2];


            timeLbl = [[UILabel alloc]init];
            timeLbl.frame = CGRectMake(54, 35, 120, 30);
            timeLbl.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
            timeLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            timeLbl.tag = 1000+indexPath.row;
            timeLbl.textAlignment = NSTextAlignmentLeft;
            //timeLbl.text = @"22 seconds ago";
            [cellView addSubview:timeLbl];

            [cell.contentView  addSubview:cellView];

        }
        else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
            
        {
            cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier2];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier2];
            
            cellView = [[UIView alloc]initWithFrame: CGRectMake(cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,cell.frame.size.height+54)];
            cellView.backgroundColor = [UIColor clearColor];
            //cellView.tag = 50;
            cellView.userInteractionEnabled = YES;
            
           
            titleLbl = [[UILabel alloc]init];
            titleLbl.frame = CGRectMake(55, userNameBtn.frame.origin.y+userNameBtn.frame.size.height, 120, 150);
            titleLbl.textColor = [UIColor darkGrayColor];
            titleLbl.tag = 51;
            // titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
            //titleLbl.numberOfLines = 0;
            // [titleLbl sizeToFit];
            titleLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
            [cellView addSubview:titleLbl];
            
            titleLbl2 = [[UILabel alloc]init];
            titleLbl2.frame = CGRectMake(155, titleLbl.frame.origin.y+titleLbl.frame.size.height, 120, 150);
            titleLbl2.textColor = [UIColor darkGrayColor];
            titleLbl2.tag = -252;
            // titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
            //titleLbl.numberOfLines = 0;
            // [titleLbl sizeToFit];
            titleLbl2.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
            [cellView addSubview:titleLbl2];

            addressBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            addressBtn1.frame = CGRectMake(titleLbl.frame.origin.x+titleLbl.frame.size.width+2,userNameBtn.frame.origin.y+userNameBtn.frame.size.height+20,220,20);
            [addressBtn1 addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn1.tag = 164;
            addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            addressBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cellView addSubview:addressBtn1];
            
            
            addressBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
            // addressBtn.frame = CGRectMake(55,20,290, 20);
            [addressBtn2 addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
            addressBtn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [addressBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addressBtn2.tag = -15;
            //addressBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [addressBtn2 setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
            addressBtn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //[cellView addSubview:addressBtn2];

           // commentLbl1 = [[UIButton alloc]init];
            commentLbl1 = [UIButton buttonWithType:UIButtonTypeCustom];
            commentLbl1.frame = CGRectMake(55, likeImage.frame.origin.y+likeImage.frame.size.height+10, 150, 10);
             [commentLbl1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
           // commentLbl1.titleLabel.textColor = [UIColor darkGrayColor];
            commentLbl1.tag = -22;
           // commentLbl1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            commentLbl1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            // titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
            //titleLbl.numberOfLines = 0;
            // [titleLbl sizeToFit];
           commentLbl1.titleLabel.font  = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
           

            
            userImage = [[AsyncImageView alloc]init];
            //userImage.image = [UIImage imageNamed:@""];
            userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImage.backgroundColor = [UIColor clearColor];
            userImage.tag = 54;
            userImage.userInteractionEnabled = YES;
            userImage.frame = CGRectMake(10, 9, 40, 40);
            [cellView addSubview:userImage];
            
            likeImage = [[AsyncImageView alloc]init];
            //userImage.image = [UIImage imageNamed:@""];
            likeImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            likeImage.backgroundColor = [UIColor clearColor];
            likeImage.tag = -1;
            likeImage.userInteractionEnabled = YES;
            likeImage.frame = CGRectMake(55, 62, 70, 70);
            [cellView addSubview:likeImage];

            ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            ImageBtn.frame = CGRectMake(likeImage.frame.origin.x,likeImage.frame.origin.y,likeImage.frame.size.width, likeImage.frame.size.height);
           [ImageBtn addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
            ImageBtn.tag = 167;
            [cellView addSubview:ImageBtn];

            userImage1Btn1 =[[UIButton alloc]init];
            userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
            userImage1Btn1.tag = 168;
            [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            userImage1Btn1.backgroundColor = [UIColor clearColor];
            [cellView addSubview:userImage1Btn1];

            
            userNameBtn =[[UIButton alloc]init];
            userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //userNameBtn.frame = CGRectMake(55,15,0,30);
            userNameBtn.tag = 55;
            [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
            userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [userNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
            // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [cellView addSubview:userNameBtn];
            
            albumBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            albumBtn1.frame = CGRectMake(190,26,290,20);
           // [albumBtn1 addTarget:self  action:@selector(UploadPhotoAction:)  forControlEvents:UIControlEventTouchUpInside];
            albumBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:11.0f];
            [albumBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            albumBtn1.tag = 194;
            albumBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            albumBtn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //[cellView addSubview:albumBtn1];
            
            
            timeLbl = [[UILabel alloc]init];
            timeLbl.frame = CGRectMake(54, 35, 120, 30);
            timeLbl.textColor = [UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0];
            timeLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            timeLbl.tag = 1000+indexPath.row;
            timeLbl.textAlignment = NSTextAlignmentLeft;
            //timeLbl.text = @"22 seconds ago";
            [cellView addSubview:timeLbl];
            
            [cell.contentView  addSubview:cellView];
            
        }

        else
        {
            
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        }
    }
    if ([notificationArr count]>0)
    {
        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequest"])
        {
            acceptBtn1.hidden = YES;
            rejectBtn1.hidden = YES;
            titleBtn1.hidden = YES;
           
            [cellView addSubview:acceptBtn];
            [cellView addSubview:rejectBtn];
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"] )
        {
            acceptBtn.hidden = YES;
            rejectBtn.hidden = YES;
            [cellView addSubview:titleBtn1];

            [cellView addSubview:acceptBtn1];
            [cellView addSubview:rejectBtn1];
            
            
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"] )
        {
            acceptBtn.hidden = YES;
            rejectBtn.hidden = YES;
            acceptBtn1.hidden = YES;
            rejectBtn1.hidden = YES;
            [cellView addSubview:titleBtn1];
            
            [cellView addSubview:acceptRallyBtn2];
            [cellView addSubview:rejectRallyBtn2];
            
            
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"imagename"] isEqual:[NSNull null]])
            {
                likeImage.image =[UIImage imageNamed:@""];
            }
            else
            {
                AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
                NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages/%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"imagename"]];
                NSURL *imageURL = [NSURL URLWithString:imgStr];
                imageView.imageURL = imageURL;
            }
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"albumname"] isEqual:[NSNull null]])
            {
                albumBtn1.titleLabel.text =@"";
                
            }
            else
            {
               [cellView addSubview:albumBtn1];
                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"albumname"]];
                
                [albumBtn1 setTitle:nameStr forState:UIControlStateNormal];
           }
            
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
        {
            [cellView addSubview:commentLbl1];
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"imagecomment"] isEqual:[NSNull null]])
            {
                //commentLbl1.titleLabel.text = @"";
                 [commentLbl1 setTitle:@"" forState:UIControlStateNormal];
            }
            else
            {
                commentLbl1.userInteractionEnabled = NO;
               commentLbl1.frame = CGRectMake(55,149, 250, 10);
                 commentLbl1.titleLabel.numberOfLines  = 0;
                //commentLbl1.backgroundColor = [UIColor redColor];
                [commentLbl1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];

                NSString *  title_Str = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"imagecomment"]];
                title_Str = [title_Str stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                title_Str = [title_Str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                
//                NSData *data = [titleStr dataUsingEncoding:NSUTF8StringEncoding];
//                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
               // NSString *titleStr = [NSString stringWithFormat:@"\"%@\"",title_Str];
            //  titleStr = [titleStr  stringByReplacingOccurrencesOfString:@" \"" withString:@""];
                
                
                NSRange rangeToSearch = NSMakeRange(0, title_Str.length); // get a range without the space character
                NSRange rangeOfSecondToLastSpace ;
                NSString * titleStr;
                NSRange rangeValue12 = [title_Str rangeOfString:@" "];
                
                if (rangeValue12.length > 0)
                    
                {
                    rangeOfSecondToLastSpace = [title_Str rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                    
                    if (rangeOfSecondToLastSpace.location == NSNotFound)
                    {
                        rangeOfSecondToLastSpace.location = 19;
                    }
                    NSString * str =[title_Str substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                    titleStr = [NSString stringWithFormat:@"\"%@\"",str];
                    
                }
                else
                {
                    titleStr = [NSString stringWithFormat:@"\"%@\"",title_Str];
                }
                NSData *data = [titleStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                     if (goodValue == nil|| [goodValue isEqualToString:@""])
                {
                    //commentLbl1.titleLabel.text = titleStr;
                    [commentLbl1 setTitle:titleStr forState:UIControlStateNormal];
                    CGRect currentFrame = commentLbl1.frame;
                    CGSize max = CGSizeMake(commentLbl1.frame.size.width, 100);
                    CGSize expected = [titleStr sizeWithFont:commentLbl1.titleLabel.font constrainedToSize:max lineBreakMode:commentLbl1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                   commentLbl1.frame = currentFrame;

                    
                }
                else
                {
                   // commentLbl1.titleLabel.text = goodValue;
                     [commentLbl1 setTitle:goodValue forState:UIControlStateNormal];
                    CGRect currentFrame = commentLbl1.frame;
                    CGSize max = CGSizeMake(commentLbl1.frame.size.width, 100);
                    CGSize expected = [goodValue sizeWithFont:commentLbl1.titleLabel.font constrainedToSize:max lineBreakMode:commentLbl1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                   commentLbl1.frame = currentFrame;

                }
                
                
//               commentLbl1.lineBreakMode = NSLineBreakByWordWrapping;
//               commentLbl1.numberOfLines = 0;
                
               
            }
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"notification"] isEqual:[NSNull null]])
            {
                titleLbl.text = @"";            }
            else
            {
                
                NSString *  titleStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
                titleLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
                titleLbl.text = titleStr;
                [cellView addSubview:titleBtn1];
                CGSize stringsize = [titleStr sizeWithFont:[UIFont systemFontOfSize:14]];
                
                if(titleStr.length >24)
                {
                    [addressBtn1 setFrame:CGRectMake(stringsize.width+17,29,70,10)];
                }
                else
                {
                    titleBtn1.userInteractionEnabled =NO;
                    [addressBtn1 setFrame:CGRectMake(stringsize.width+32,29,140,10)];
                }
            }
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"declinerallyrequest"])
        {
            titleLbl.text = @"has declined your Rally request";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+25,29,90,10)];

        }
        if([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packdecline"])
        {
             titleLbl.text = @"has declined your pack request";
            addressBtn1.hidden = YES;
           
            addressBtn1.userInteractionEnabled = NO;

        }
        if  ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallydecline"])
        {
            titleLbl.text = @"has declined your Rally invite";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
           [addressBtn1 setFrame:CGRectMake(stringsize.width+25,29,103,10)];
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"actlike"])
        {
            titleLbl.text = @"liked your activity";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
           

            [addressBtn1 setFrame:CGRectMake(stringsize.width+37,29,185,10)];
           
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"])
        {
            titleLbl.text = @"has accepted the request for";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+25,29,110,10)];
        }
        
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"])
        {
            titleLbl.text = @"sent request to join the Rally";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+25,29,110,10)];
        }
        
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinviteaccepted"])
        {
             titleLbl.text = @"has accepted your invitation to the Rally";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+11,29,55,10)]; //changed by swati 2 dec
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"])
        {
            titleLbl.text = @"liked your picture from the album";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+19,29,49,10)];

        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
        {
            titleLbl.text = @"commented your picture from the album";
            CGSize stringsize = [titleLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
            [addressBtn1 setFrame:CGRectMake(stringsize.width+11,29,49,10)];

        }
     /*  if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"])
        {
          
            NSString * titleLbl_str = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
            
            titleLbl_str = [titleLbl_str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            NSData *data = [titleLbl_str dataUsingEncoding:NSUTF8StringEncoding];
            NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            if (goodValue == nil || [goodValue isEqualToString:@""])
            {
                if (titleLbl_str.length>48)
                {
                    NSString * range_Value = [titleLbl_str substringWithRange:NSMakeRange(0, 48)];
                titleLbl.text = range_Value;
                    if (titleLbl_str.length>48)
                    {
                        UILabel * titleLbl_2 = (UILabel *)[cell viewWithTag:-252];
                        titleLbl_2.frame = CGRectMake(155, 35, 120, 30);
                        NSString * rangeValue = [titleLbl_str substringWithRange:NSMakeRange(48, titleLbl_str.length-48)]  ;
                        
                        rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                        NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                        if (goodValue == nil||[goodValue isEqualToString:@""])
                        {
                            titleLbl2.text = rangeValue;
                            
                        }
                        else
                        {
                            titleLbl2.text = goodValue;
                            
                        }
                        
                        
                    }

                    
                }
                else
                {
                      titleLbl.text = titleLbl_str;
                }
            }
            else
            {
                if (goodValue.length>48)
                {
                     NSString * range_Value = [goodValue substringWithRange:NSMakeRange(0, 48)];                titleLbl.text = range_Value;
                    
                    if (goodValue.length>48)
                    {
                        UILabel * titleLbl_2 = (UILabel *)[cell viewWithTag:-252];
                        titleLbl_2.frame = CGRectMake(155, 35, 120, 30);
                        NSString * rangeValue = [titleLbl_str substringWithRange:NSMakeRange(48, titleLbl_str.length-48)]  ;
                        
                        rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                        NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                        if (goodValue == nil||[goodValue isEqualToString:@""])
                        {
                            titleLbl2.text = rangeValue;
                            
                        }
                        else
                        {
                            titleLbl2.text = goodValue;
                            
                        }
                        
                        
                    }

                }
                else
                {
                     titleLbl.text = goodValue;
                }
            }

            
          
        }
        if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
        {
            NSString * titleStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
          
            titleStr = [titleStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            NSData *data = [titleStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            if (goodValue == nil||[goodValue isEqualToString:@""])
            {
                if (titleStr.length>48)
                {
                    NSString * range = [titleStr substringWithRange:NSMakeRange(0, 48)]  ;
                    

                titleLbl.text = range;
                    
                    if (titleStr.length>48)
                    {
                        UILabel * titleLbl_2 = (UILabel *)[cell viewWithTag:-252];
                        titleLbl_2.frame = CGRectMake(155, 35, 120, 30);
                        NSString * rangeValue = [titleStr substringWithRange:NSMakeRange(48, titleStr.length-48)]  ;
                        
                        rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                        NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                        if (goodValue == nil||[goodValue isEqualToString:@""])
                        {
                            titleLbl2.text = rangeValue;
                            
                        }
                        else
                        {
                            titleLbl2.text = goodValue;
                            
                        }
                        
                        
                    }
                }
                else
                {
                     titleLbl.text = titleStr;
                }
                
            }
            else
            {
                if (goodValue.length>48)
                {
                    NSString * range = [goodValue substringWithRange:NSMakeRange(0, 48)]  ;
                    
                    
                    titleLbl.text = range;
                    if (goodValue.length>48)
                    {
                        UILabel * titleLbl_2 = (UILabel *)[cell viewWithTag:-252];
                        titleLbl_2.frame = CGRectMake(155, 35, 120, 30);
                        NSString * rangeValue = [titleStr substringWithRange:NSMakeRange(48, titleStr.length-48)]  ;
                        
                        rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                        NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                        if (goodValue == nil||[goodValue isEqualToString:@""])
                        {
                            titleLbl2.text = rangeValue;
                            
                        }
                        else
                        {
                            titleLbl2.text = goodValue;
                            
                        }
                        
                        
                    }
                }
                else
                {
                     titleLbl.text = goodValue;
                }
            }
            
           
              }
        */
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>19)
                    {
                        //NSString * range = [nameStr substringWithRange:NSMakeRange(0,19)]  ;
                        NSRange rangeToSearch = NSMakeRange(0, 19); // get a range without the space character
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        
                        

                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        if (nameStr.length>19)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            acceptBtn.hidden = YES;
                            rejectBtn.hidden = YES;
                            acceptBtn1.hidden = YES;
                            rejectBtn1.hidden = YES;
                            [cellView addSubview:titleBtn1];
                            
                            [cellView addSubview:acceptRallyBtn2];
                            [cellView addSubview:rejectRallyBtn2];
                            acceptRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+135,42+35,80,30);
                            rejectRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+225,42+35,80,30);
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, nameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                 addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>19)
                    {
                       // NSString * range = [goodValue substringWithRange:NSMakeRange(0, 19)]  ;
                        NSRange rangeToSearch = NSMakeRange(0, 19); // get a range without the space character
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        

                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>19)
                        {
                            [cellView addSubview:addressBtn2];
                                 acceptBtn.hidden = YES;
                                rejectBtn.hidden = YES;
                                acceptBtn1.hidden = YES;
                                rejectBtn1.hidden = YES;
                                [cellView addSubview:titleBtn1];
                                
                                [cellView addSubview:acceptRallyBtn2];
                                [cellView addSubview:rejectRallyBtn2];
                                acceptRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+135,42+35,80,30);
                                rejectRallyBtn2.frame = CGRectMake(cellView.frame.origin.x+225,42+35,80,30);
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
            }
    }
        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                //addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>29)
                    {
                        //NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        NSRange rangeToSearch = NSMakeRange(0, 29); // get a range without the space character
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 29;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 29;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }

                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        if (nameStr.length>29)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                           
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, nameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>29 )
                    {
                        NSLog(@"Value length %lu",(unsigned long)goodValue.length);
                        //NSString * range = [goodValue substringWithRange:NSMakeRange(0, 28)]  ;
                        
                        NSRange rangeToSearch = NSMakeRange(0, 29); // get a range without the space character
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 29;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 29;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }

                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>29)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }

        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"actlike"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
               // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>28)
                    {
                        NSLog(@"Value length %lu",(unsigned long)goodValue.length);
                        NSRange rangeToSearch = NSMakeRange(0, 28); // get a range without the space character
                                              //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 28;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 28;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];

                    //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                                           if (nameStr.length>28)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>28)
                    {
                        NSLog(@"Value length %lu",(unsigned long)goodValue.length);
                        
                        NSRange rangeToSearch = NSMakeRange(0, 28); // get a range without the space character
                   
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 28;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 28;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                      [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>28)
                        {
                            NSLog(@"Value length %lu",(unsigned long)goodValue.length);
                            
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>18)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 18); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 18;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 18;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>18)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>18)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 18); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 18;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 18;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>18)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }
        

        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
               // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
               // [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil|| [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>5 &&  nameStr.length>22)
                    {
                        NSRange rangeToSearch;
                        NSString * noti_Str = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"notification"]];
                        if (noti_Str.length>24)
                        {
                            rangeToSearch = NSMakeRange(0, 5);
                                                  }
                        else
                        {
                            rangeToSearch = NSMakeRange(0, 22);
                                                   }

                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                if (nameStr.length>5 )
                                {
                                rangeOfSecondToLastSpace.location = 5;
                                }
                                else if(  nameStr.length>22)
                                {
                                    rangeOfSecondToLastSpace.location = 22;

                                }
                                
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                if (nameStr.length>5 )
                                {
                                    rangeOfSecondToLastSpace.location = 5;
                                }
                                else if(  nameStr.length>22)
                                {
                                    rangeOfSecondToLastSpace.location = 22;
                                    
                                }
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        

                        

                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        if (nameStr.length>5 &&   nameStr.length>22)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            acceptBtn.hidden = YES;
                            rejectBtn.hidden = YES;
                            [cellView addSubview:titleBtn1];
                            
                            [cellView addSubview:acceptBtn1];
                            [cellView addSubview:rejectBtn1];
                            
                            acceptBtn1.frame = CGRectMake(cellView.frame.origin.x+135,42+35,80,30);
                            rejectBtn1.frame = CGRectMake(cellView.frame.origin.x+225,42+35,80,30);

                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                           
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, nameStr.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                 addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>5 &&  goodValue.length>17)
                    {
                        NSRange rangeToSearch;
                        NSString * noti_Str = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"notification"]];
                        if (noti_Str.length>24)
                        {
                            rangeToSearch = NSMakeRange(0, 5);
                            //rangeToSearch = NSMakeRange(0, 25);
                        }
                        else
                        {
                           rangeToSearch = NSMakeRange(0, 17);
                            //rangeToSearch = NSMakeRange(0, 8);
                        }
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                if (goodValue.length>5 )
                                {
                                    rangeOfSecondToLastSpace.location = 5;
                                }
                                else if(  goodValue.length>17)
                                {
                                    rangeOfSecondToLastSpace.location = 17;
                                    
                                }
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            
                                 rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                if (nameStr.length>5 )
                                {
                                    rangeOfSecondToLastSpace.location = 5;
                                }
                                else if(  nameStr.length>17)
                                {
                                    rangeOfSecondToLastSpace.location = 17;
                                    
                                }
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        

                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                         if (goodValue.length>5 &&  goodValue.length>17)
                        {
                            [cellView addSubview:addressBtn2];
                                acceptBtn.hidden = YES;
                                rejectBtn.hidden = YES;
                                [cellView addSubview:titleBtn1];
                                
                                [cellView addSubview:acceptBtn1];
                                [cellView addSubview:rejectBtn1];
                                
                                acceptBtn1.frame = CGRectMake(cellView.frame.origin.x+135,42+35,80,30);
                                rejectBtn1.frame = CGRectMake(cellView.frame.origin.x+225,42+35,80,30);
                           
                        
                            timeLbl.frame = CGRectMake(54, 49, 120,30);
                           
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil|| [rallyDataEncode isEqualToString:@""])
                            {
                                 addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
                
                /*addressBtn1.titleLabel.numberOfLines = 0;
                addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                if (goodValue == nil|| [goodValue isEqualToString:@""])
                {
                    [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    CGRect currentFrame = addressBtn1.frame;
                    CGSize max = CGSizeMake(addressBtn1.frame.size.width, 100);
                    CGSize expected = [nameStr sizeWithFont:addressBtn1.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                    addressBtn1.frame = currentFrame;
                    
                    
                }
                else
                {
                    [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                    CGRect currentFrame = addressBtn1.frame;
                    CGSize max = CGSizeMake(addressBtn1.frame.size.width, 100);
                    CGSize expected = [goodValue sizeWithFont:addressBtn1.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                    addressBtn1.frame = currentFrame;
                    
                }
                
                
            }*/

        }
        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinviteaccepted"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>5)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 5); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>5)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil ||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>5)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 5); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>5)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil ||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }
        
        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallydecline"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>19)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 19); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>19)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>19)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 19); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 19;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>19)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }

        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"declinerallyrequest"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>16)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 16); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 16;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 16;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>16)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>16)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 16); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 16;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location =16;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>16)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }
        
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>5)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 5); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>5)
                        {
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                            imageView.frame = CGRectMake(55, 72, 70, 70);

                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>5)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 5); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 5;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>5)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                            imageView.frame = CGRectMake(55, 72, 70, 70);

                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
        {
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                // addressBtn1.backgroundColor = [UIColor redColor];
                addressBtn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
                [addressBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //addressBtn1.userInteractionEnabled = NO;
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                if (goodValue == nil || [goodValue isEqualToString:@""])
                {
                    if (nameStr.length>6)
                    {
                        NSRange rangeToSearch = NSMakeRange(0, 6); // get a range without the space character
                        //    NSString * range = [rallyDataEncode substringWithRange:NSMakeRange(0, 23)]  ;
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 6;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 6;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        //   NSString * range = [nameStr substringWithRange:NSMakeRange(0,32)]  ;
                        
                        if (nameStr.length>6)
                        {
                            //timeLbl.frame = CGRectMake(54, 48, 120,30);
                            [cellView addSubview:addressBtn2];
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                            AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                            imageView.frame = CGRectMake(55, 72, 70, 70);
                          // commentLbl1.frame = CGRectMake(55,149, 250, 10);
                         //    ImageBtn.backgroundColor = [UIColor redColor];
                            ImageBtn.frame = CGRectMake(imageView.frame.origin.x,imageView.frame.origin.y,imageView.frame.size.width, imageView.frame.size.height);
                            // albumBtn1

                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            
                            addressBtn3.frame = CGRectMake(55, 40, 265, 10);
                            //addressBtn3.titleLabel.numberOfLines = 0;
                            // addressBtn3.backgroundColor = [UIColor blueColor];
                            NSString * rangeValue = [nameStr substringWithRange:NSMakeRange( rangeOfSecondToLastSpace.location , nameStr.length- rangeOfSecondToLastSpace.location )]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue forState:UIControlStateNormal];
                                
                            }
                            else
                            { addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                                
                            }
                            
                            
                        }
                    }
                    else
                    {
                        [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    }
                    
                }
                else
                {
                    if (goodValue.length>6)
                    {
                        
                        NSRange rangeToSearch = NSMakeRange(0, 6); // get a range without the space character
                        
                        NSRange rangeOfSecondToLastSpace ;
                        NSString * range;
                        NSRange rangeValue12 = [nameStr rangeOfString:@"\\ud"];
                        
                        if (rangeValue12.length > 0)
                            
                        {
                            rangeOfSecondToLastSpace = [goodValue rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 6;
                            }
                            range = [goodValue substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                        } else
                        {
                            rangeOfSecondToLastSpace = [nameStr rangeOfString:@" " options:NSBackwardsSearch range:rangeToSearch];
                            
                            if (rangeOfSecondToLastSpace.location == NSNotFound)
                            {
                                rangeOfSecondToLastSpace.location = 6;
                            }
                            range = [nameStr substringWithRange:NSMakeRange(0, rangeOfSecondToLastSpace.location)];
                            
                            
                        }
                        
                        [addressBtn1 setTitle:range forState:UIControlStateNormal];
                        
                        if (goodValue.length>6)
                        {
                            [cellView addSubview:addressBtn2];
                            
                            timeLbl.frame = CGRectMake(54, 48, 120,30);
                             AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:-1];
                            imageView.frame = CGRectMake(55, 72, 70, 70);
                          //commentLbl1.frame = CGRectMake(55,149, 250, 10);
                          
                           ImageBtn.frame = CGRectMake(imageView.frame.origin.x,imageView.frame.origin.y,imageView.frame.size.width, imageView.frame.size.height);
                           // albumBtn1
                            UIButton * addressBtn3 = (UIButton *)[cell viewWithTag:-15];
                            addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                            //addressBtn2.backgroundColor = [UIColor blueColor];
                            //addressBtn2.titleLabel.numberOfLines = 0;
                            
                            NSString * rangeValue = [goodValue substringWithRange:NSMakeRange(rangeOfSecondToLastSpace.location, goodValue.length-rangeOfSecondToLastSpace.location)]  ;
                            rangeValue = [rangeValue stringByTrimmingCharactersInSet:
                                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            rangeValue = [rangeValue  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                            NSData *data = [rangeValue dataUsingEncoding:NSUTF8StringEncoding];
                            NSString *rallyDataEncode = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                            if (rallyDataEncode == nil||[rallyDataEncode isEqualToString:@""])
                            {
                                addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rangeValue  forState:UIControlStateNormal];
                                
                                
                            }
                            else
                            {addressBtn3.frame = CGRectMake(55, 43, 265, 10);
                                [addressBtn3 setTitle:rallyDataEncode forState:UIControlStateNormal];
                            }
                            
                            
                        }
                    }
                    else
                    {
                        if (goodValue == nil|| [goodValue isEqualToString:@""])
                            
                        {
                            [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                        }
                        else
                        {
                            [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                        }
                        
                    }
                }
                
                
            }
            
            
        }


        /*if ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
                {
            
            if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"] isEqual:[NSNull null]])
            {
                addressBtn1.titleLabel.text =@"";
            }
            else
            {
                addressBtn1.titleLabel.numberOfLines = 0;
               
                [addressBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0, 0.0, 0.0)];
                NSString *  nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"rallyname"]];
                nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data = [nameStr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
               
                if (goodValue == nil|| [goodValue isEqualToString:@""])
                {
                    [addressBtn1 setTitle:nameStr forState:UIControlStateNormal];
                    CGRect currentFrame = addressBtn1.frame;
                    CGSize max = CGSizeMake(addressBtn1.frame.size.width, 100);
                    CGSize expected = [nameStr sizeWithFont:addressBtn1.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                    addressBtn1.frame = currentFrame;


                }
                else
                {
                    [addressBtn1 setTitle:goodValue forState:UIControlStateNormal];
                    CGRect currentFrame = addressBtn1.frame;
                    CGSize max = CGSizeMake(addressBtn1.frame.size.width, 100);
                    CGSize expected = [goodValue sizeWithFont:addressBtn1.titleLabel.font constrainedToSize:max lineBreakMode:addressBtn1.titleLabel.lineBreakMode];
                    currentFrame.size.height = expected.height;
                    addressBtn1.frame = currentFrame;

                }
                
               
           }

        }*/
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequest"] )
            
        {
            if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"] isEqual:[NSNull null]])
            {
                titleLbl.text = @"";
            }
            else
            {
                titleLbl.numberOfLines = 0;

                NSString *  titleStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
                CGRect currentFrame = titleLbl.frame;
                CGSize max = CGSizeMake(10, 500);
                CGSize expected = [titleStr sizeWithFont:titleLbl.font constrainedToSize:max lineBreakMode:titleLbl.lineBreakMode];
                currentFrame.size.height = expected.height;
                titleLbl.frame = currentFrame;
                titleLbl.text = titleStr;
            }
        }
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequestaccepted"])
        {
            titleLbl.text = @"was added to your pack";
            addressBtn1.hidden = YES;
            addressBtn1.userInteractionEnabled = NO;
       
        }
       
   
    if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequest"] || [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequestaccepted"] || [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"actlike"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinviteaccepted"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallydecline"]|| [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packdecline"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"declinerallyrequest"])
            
    {
        
        if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"userimage"] isEqual:[NSNull null]])
        {
            userImage.image =[UIImage imageNamed:@""];
        }
        else
        {
            AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:54];
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
            NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"userimage"]];
            NSURL *imageURL = [NSURL URLWithString:imgStr];
            imageView.imageURL = imageURL;
            
        }
        if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"username"] isEqual:[NSNull null]])
        {
            userNameBtn.titleLabel.text =@"";
        }
        else
        {
            NSString *  nameStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"username"]];
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
            {
            nameStr = [nameStr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            }
             nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
            CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
            //or whatever font you're using
            [userNameBtn setFrame:CGRectMake(55,10,stringsize.width,stringsize.height)];
            titleLbl.frame = CGRectMake(55,stringsize.height+8,cell.frame.size.width, stringsize.height);
        
        }

        if ([[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"notificationdate"] isEqual:[NSNull null]])
        {
            timeLbl.text = @"";
        }
        else
        {
            UILabel *timeLbel = (UILabel *) [cell viewWithTag:1000+indexPath.row];
            NSString *  dateStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row ] objectForKey:@"notificationdate"]];
            
            NSLog(@"Date before %@",dateStr);
            
            NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
            [dateF1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"Europe/Budapest"];
//           [dateF1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//           [dateF1 setTimeZone:tz];
            NSDate *todayDate1 = [dateF1 dateFromString:dateStr];
            
            
           // NSDate *today1 = [NSDate date];
            
            
            NSDateFormatter *dateF2= [[NSDateFormatter alloc] init];
            [dateF2  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
             NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"America/New_York"];
             //NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"en_US"];
            [dateF2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [dateF2 setTimeZone:tz];

            NSString * str = [dateF2 stringFromDate:[NSDate date]];
            
            NSLog(@"Current Time For checking %@",str);
            
            NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
            [dateF3  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
             NSDate *today1 = [dateF3 dateFromString:str];
            
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
            
            
            NSDateComponents *componentsDaysDiff=[[NSDateComponents alloc] init];             //specify

            componentsDaysDiff = [calendar components:unitFlags  fromDate:todayDate1    toDate:today1   options:0];
            
            NSInteger years = [componentsDaysDiff year];
            NSInteger months = [componentsDaysDiff month];
            NSInteger days = [componentsDaysDiff day];
            NSInteger hours = [componentsDaysDiff hour];
            NSInteger minutes = [componentsDaysDiff minute];
            NSInteger seconds = [componentsDaysDiff second];
            
             NSLog(@"Date after %@",componentsDaysDiff);
            
            
            if (days >0)
            {
                if (days>1)
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld days ago",(long)days];

                    
                }
                else
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld day ago",(long)days];

                    
                }
                if (days >30)
                {
                    if (days>1)
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld months ago",(long)months];
                        
                    }
                    else
                    {
                        timeLbel.text = [NSString stringWithFormat:@"%2ld month ago",(long)months];
                        
                    }
                    
                }
            }
            else if(months>12)
            {
                if (months>1)
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld years ago",(long)years];
                }
                else
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld year ago",(long)years];
                }
            }
            else if (hours > 0)
            {
                if (hours>1)
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld hours ago",(long)hours];
                }
                else
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld hour ago",(long)hours];
                }
            }
            else if(minutes >0)
            {
                if (minutes>1)
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld mins ago",(long)minutes];
                }
                else
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld min ago",(long)minutes];
                }
                
                
                
            }
            else
            {
                if (minutes>1)
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld secs ago",(long)seconds];
                }
                else
                {
                    timeLbel.text = [NSString stringWithFormat:@"%2ld sec ago",(long)seconds];
                }
            }
        }

    }

   /* else
    {
        if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"] isEqual:[NSNull null]])
        {
            cell.textLabel.text = @"";
        }
        else
        {
            cell.textLabel.numberOfLines = 0;
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.textLabel.text = [[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"];
        }
    }*/
}
}
    else
    {
        cell.textLabel.text = @"";
    }
    cell.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
    return cell;
}


- (void)addressAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:addressBtn1.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [addressBtn1 setTag:rowNumber];
    
    RJoinRallyViewController *JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
        
    rallyIdStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"rallyid"]];
    JoinRallyViewController.hubIDStr=rallyIdStr;
    //JoinRallyViewController.hubRallyDic=[notificationArr objectAtIndex:rowNumber];
    [self.navigationController pushViewController:JoinRallyViewController animated:NO];
    
}
- (void)UploadPhotoAction:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:ImageBtn.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [ImageBtn setTag:rowNumber];
     NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    RFullImageViewController *ImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
    ImageViewController.hubrallyImagesDic=[notificationArr objectAtIndex:rowNumber] ;
    ImageViewController.friendUserId_Image =userProfileIdStr ;
    [self.navigationController pushViewController:ImageViewController animated:NO];
    
}

- (void)aacceptRallyAction1:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:acceptRallyBtn2.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
     rowNumber += indexPath.row;
    
    [acceptRallyBtn2 setTag:rowNumber];
    rallyIdStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"rallyid"]];
    sentfromStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    senttoStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentto"]];
    [self callWebSercvice_JoinRally];

}
- (void)deleteRallyRequestAction:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:rejectRallyBtn2.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    rowNumber += indexPath.row;
    
    [rejectRallyBtn2 setTag:rowNumber];
    
    notificationidStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"notificationid"]];
    [self callWebSercvice_deleterallyrequest];

}
-(void)callWebSercvice_JoinRally
{
    NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
    [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
    NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
    NSString * urlStr;
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/joinrally.php?userids=%@&rallyid=%@&addedby=%@&joindate=%@",sentfromStr,rallyIdStr,senttoStr,dateStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_joinRally:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_joinRally:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //        NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
    
    
    {          case  APIResponseStatus_UserAlreadyJoinedRally:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You Have Already Joined this Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case APIResponseStatus_JoinRallySuccessfull:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"You have accepted Rally request successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self callWebService];
            [noitificationTableView reloadData];
            
            break;
        }
        case  APIResponseStatus_JoinRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
            
        default:
            
            break;
            
    }

}

-(void)callWebSercvice_deleterallyrequest
{
    
    NSString * urlStr;
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterallyrequest.php?notificationid=%@",notificationidStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_deleterallyrequest:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_deleterallyrequest:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //    NSLog(@"response12%@",response);
    
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
    {
            
        case  APIResponseStatus_RallyRequestDeletedSuccessfully:
        {
            
            [self callWebService];
            [noitificationTableView reloadData];
            break;
        }
        case APIResponseStatus_DeleteRallyRequestFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Delete Rally Request Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        default:
            
            break;
            
    }
    
    
    
}

- (void)acceptBtnAction1:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:acceptBtn1.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [acceptBtn1 setTag:rowNumber];
    rallyIdStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"rallyid"]];
    sentfromStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    senttoStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentto"]];
    [self callWebService_AcceptInvitation];

}
 - (void)acceptBtnAction:(UIButton*)sender
{
    CGRect buttonFrameInTableView = [sender convertRect:acceptBtn.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [acceptBtn setTag:rowNumber];
    //rallyIdStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber ] objectForKey:@"rallyid"]];
    sentfromStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    senttoStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentto"]];
    [self callWebService_adduserpacks];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == noitificationTableView)
    {
      if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"actlike"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallydecline"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packdecline"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"declinerallyrequest"] |[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinviteaccepted"])
    {
        return 100;
    }
       else if ([[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequestaccepted"] )
        {
            return 100;
        }
      else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequest"])
      {
          return 100;
      }
 
      else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyinvite"]||[[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"rallyrequest"])
      {
          return 120;
      }

    else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"packrequestaccepted"])
    {
        return 80;
    }
   else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"comment"])
    { //NSString *  titleStr = [NSString stringWithFormat:@"\"%@\"",[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"imagecomment"]];
        NSString *  titleStr = [NSString stringWithFormat:@"\"%@\"",[notificationArr objectAtIndex:indexPath.row]];
        CGSize size = [titleStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:8] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
         return size.height+5;
    }
   else if  ( [[[notificationArr objectAtIndex:indexPath.row]objectForKey:@"tag"] isEqualToString:@"like"])
   {
       return 170;
   }
    else
    {
      NSString *cellTextStr = [NSString stringWithFormat:@"%@", [[notificationArr objectAtIndex:indexPath.row]objectForKey:@"notification"]];
       CGSize size = [cellTextStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
//    NSLog(@"%f",size.height);
    return size.height + 40;
    }
    }
    else if(tableView == userListTableview)
    {
        return 44;
    }
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == userListTableview)
    {
        RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
        ProfileViewController.hubUserIDStr = [[otheruserArr objectAtIndex:indexPath.row ] objectForKey:@"userid"];
        [self.navigationController pushViewController:ProfileViewController animated:NO];
    }

}

- (void)viewOtherUserAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:titleBtn1.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    if ([[[notificationArr objectAtIndex:rowNumber]objectForKey:@"tag"] isEqualToString:@"rallyinvite"] )
    {
        otheruserArr = [[notificationArr  objectAtIndex:rowNumber ]objectForKey:@"otherinfo"];
        
    }
    
    
    
    bgView = [[UIView alloc]init];
    bgView.frame = CGRectMake(noitificationTableView.frame.origin.x,noitificationTableView.frame.origin.y,noitificationTableView.frame.size.width,noitificationTableView.frame.size.height);
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.95;
    [self.view addSubview:bgView];
    
    userListView = [[UIView alloc]init];
    userListView.frame = CGRectMake(20,bgView.frame.origin.y+120,280,320);
    userListView.layer.cornerRadius = 5.0f;
    userListView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    userListView.layer.borderWidth = 1.0f;
    userListView.backgroundColor = [UIColor whiteColor];
    userListView.userInteractionEnabled = YES;
    
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    closeBtn.frame = CGRectMake(5, 5, 24, 24);

    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    [closeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
       closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    [userListView addSubview:closeBtn];

    userListTableview = [[UITableView alloc]init];
    userListTableview.frame = CGRectMake(0,32,userListView.frame.size.width,userListView.frame.size.height-42);
   
    [userListTableview setSeparatorInset:UIEdgeInsetsZero];
    userListTableview.dataSource = self;
    userListTableview.delegate = self;
    [userListView addSubview:userListTableview];
    
    [bgView addSubview:userListView];
    [bgView bringSubviewToFront:userListTableview];
  
}
-(void)CloseAction:(UIButton *)sender
{
    
    [bgView removeFromSuperview];

    [userListView removeFromSuperview];
}
- (void)userBtnAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:userNameBtn.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
   //[joinBtn setTag:rowNumber];
    
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    ProfileViewController.hubUserIDStr=userProfileIdStr;
    [self.navigationController pushViewController:ProfileViewController animated:NO];
    
    
}

- (void)deleteInvitationAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:rejectBtn.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [rejectBtn setTag:rowNumber];
    
    notificationidStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"notificationid"]];
    [self callWebSercvice_deleteInvitation];
    
}

- (void)rejectBtnAction:(UIButton*)sender
{
             
    CGRect buttonFrameInTableView = [sender convertRect:rejectBtn.bounds toView:noitificationTableView];
    NSIndexPath *indexPath = [noitificationTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:noitificationTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [rejectBtn setTag:rowNumber];
    
    notificationidStr = [NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:rowNumber ] objectForKey:@"notificationid"]];
    [self callWebSercvice_deletePackrequest];
    
}
-(void)callWebService_AcceptInvitation
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
    
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
    currentdate_Str = [currentdate_Str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    //change to add join date
    
    urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/acceptinvitaion.php?userid=%@&friendid=%@&rallyid=%@&joindate=%@",senttoStr,sentfromStr,rallyIdStr,currentdate_Str];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Accept_Invitation:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops",error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved_Accept_Invitation:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    [noitificationTableView reloadData];
    
    
    //packArr = [dict objectForKey:@"userfriends"];
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case   APIResponseStatus_UserAlreadyJoinedRally:
            
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"You have already joined this rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
        case   APIResponseStatus_JoinRallySuccessfull:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"You have successfully joined this Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self callWebService];
            [noitificationTableView reloadData];
            break;
        }
            
        case   APIResponseStatus_JoinRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
        default:
            
            break;
            
    }
    
}


-(void)callWebService_adduserpacks
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
    
    urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/adduserpacks.php?userid=%@&friendid=%@",sentfromStr,senttoStr];
    // urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addpackrequest.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],searchuserid];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Add_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops",error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


 -(void)JSONRecieved_Add_User_Pack:(NSDictionary *)response
 {
     NSDictionary *dict = response;
//     NSLog(@"response12%@",response);
     
     [noitificationTableView reloadData];
     
     
     //packArr = [dict objectForKey:@"userfriends"];
     
     switch ([dict[kRAPIResult] integerValue])
     
     {
             
         case APIResponseStatus_FriendIDRequired:
         {
             
             UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Friend ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             break;
         }
         case   APIResponseStatus_AlreadyHaveFiveFriends
             :
         {
             
             UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"You have Already Five Friends Add into your pack" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             break;
         }
         case   APIResponseStatus_FriendAlreadyExist:
         {
             
             UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This friend is already in your pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [self callWebService];
             [noitificationTableView reloadData];

             break;
         }
             
             
         case   APIResponseStatus_UserPackSuccessfull:
         {
             
             UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"You’ve added a new pack member, now start a Rally together!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
            [self callWebService];
            [noitificationTableView reloadData];
             break;
         }
             
         case   APIResponseStatus_UserPackFailed:
         {
             
             UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             break;
         }
             
         default:
             
             break;
             
     }
     
 }

-(void)callWebSercvice_deletePackrequest
{
    
    NSString * urlStr;
     urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deletepackrequest.php?notificationid=%@",notificationidStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self JSONRecieved_deletePackrequest:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_deletePackrequest:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
         case  APIResponseStatus_PackRequestDeletedSuccessfully:
        {
            
            [self callWebService];
            [noitificationTableView reloadData];

            break;
        }
        case APIResponseStatus_DeletePackRequestFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something went wrong. Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        default:
            
            break;
            
    }

}
-(void)callWebSercvice_deleteInvitation
{
    
    NSString * urlStr;
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleteinvitation.php?notificationid=%@",notificationidStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved_deleteInvitation:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_deleteInvitation:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_RallyInvitationDeletedSuccessfully:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally Request Deleted Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            [self callWebService];
            [noitificationTableView reloadData];
            break;
        }
        case APIResponseStatus_DeleteRallyInvitationFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something went wrong. Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        default:
            
            break;
            
    }
    
    
    
}

@end
