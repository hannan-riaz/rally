//
//  RHubViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
@interface RHubViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,CLLocationManagerDelegate>
{
    CGFloat lastOffset_x;
    UIView *customView;
    UILabel *labelforNavigationTitle;
    UIButton *rightBtn;
    UILabel *showRadiusLbl;
    NSString *strRadiusVal;
    UIView *settingView;
    UITableView *refernceTableView;
    int pageVal;
    CLLocationManager *locationManager;
    float Currlat;
    float Currlon;
    UIView *tempView;
}
@property (strong, nonatomic) IBOutlet UITableView *hubTableview;
@property(strong,nonatomic)UIPageControl *pageControllerObj;
@property(strong,nonatomic)IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic)IBOutlet UITableView *everyOneTableView;
@property ( strong , nonatomic ) IBOutlet UILabel *myPackLbl;
@property ( strong , nonatomic ) IBOutlet UILabel *aroundyouLbl;
@property ( strong , nonatomic ) IBOutlet UIButton *myPackBtn;
@property ( strong , nonatomic ) IBOutlet UIButton *aroundYouBtn;
@property ( strong , nonatomic ) IBOutlet UIImageView *myPackImg;
@property ( strong , nonatomic ) IBOutlet UIImageView *aroundyouImg;

- (IBAction)myPackTableViewAction:(id)sender;
- (IBAction)aroundYouTableViewAction:(id)sender;
@end
