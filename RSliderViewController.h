//
//  RSliderViewController.h
//  Rally
//
//  Created by Ambika on 9/5/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSliderViewController : UIViewController<UIScrollViewDelegate,UIPageViewControllerDataSource,UIPageViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageCntrl;
@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (nonatomic) BOOL helpFullTipBtn;
- (IBAction)PagecontrolAction:(id)sender;
- (IBAction)skipBtn:(id)sender;

@end
