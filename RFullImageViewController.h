//
//  RFullImageViewController.h
//  Rally
//
//  Created by Ambika on 6/12/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "HPGrowingTextView.h"


@interface RFullImageViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,HPGrowingTextViewDelegate,UIScrollViewDelegate>
{
    HPGrowingTextView *growingtextView;
}
@property (strong, nonatomic) IBOutlet AsyncImageView *fullImageview;
@property (strong, nonatomic) IBOutlet UILabel *LikesLbl;
@property (strong, nonatomic) IBOutlet UILabel *commentsLbl;
@property (strong, nonatomic) NSDictionary * rallyImagesDic;
@property (strong, nonatomic) NSMutableArray * albumDataArray;
@property (strong, nonatomic) NSDictionary * PushDataDic;
@property (strong, nonatomic) NSDictionary * hubrallyImagesDic;
@property (strong, nonatomic) NSString * friendUserId_Image;
@property (strong, nonatomic) NSString * imageNames;
@property (strong, nonatomic) NSString * albumIdString;
@property (strong, nonatomic) IBOutlet UIImageView *trashGrayImge;
@property (strong, nonatomic) NSString * rallyId_string;
@property (strong, nonatomic) NSString *userid_str;
- (IBAction)likesBtn:(id)sender;
@property(nonatomic)NSInteger index;
- (IBAction)commentAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *likesCheckLbl;
@property (strong, nonatomic) IBOutlet UITextView *commentTxtView;
- (IBAction)sendAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *postBtn;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UITableView *commentTableView;

@property (strong, nonatomic) IBOutlet UIView *commentview;
@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
@property (strong, nonatomic) IBOutlet UIView *fullImgView;
@property (strong, nonatomic) IBOutlet UIButton *likeImageBtn;
@property (strong, nonatomic) IBOutlet UIButton *commentBtn;
@property (strong, nonatomic) IBOutlet UIView *lineView;
@property (strong, nonatomic) IBOutlet UIImageView *commnetImageView;

@property (strong, nonatomic) IBOutlet UIView *grayBoxView;
@property (strong, nonatomic) IBOutlet UIImageView *rImageView;
- (IBAction)deleteImageAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *deleteImageBtn;

@end
