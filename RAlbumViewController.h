//
//  RAlbumViewController.h
//  Rally
//
//  Created by Ambika on 7/11/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAlbumViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *albunTableView;
@property (strong, nonatomic) NSString * profileUSerID;
@end
