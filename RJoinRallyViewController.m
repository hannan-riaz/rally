//
//  RJoinRallyViewController.m
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RJoinRallyViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RJoinEditViewController.h"
#import "RProfileViewController.h"
#import "AsyncImageView.h"
#import "RHomeViewController.h"
//static int x = 0;


@interface RJoinRallyViewController ()
{
    UIImageView *imageView1;
    UIImageView *imageView ;
    NSArray * rallyarr;
    NSString * hubRallyId;
    NSDictionary *rallyInfoDic;
    NSMutableArray * rallyersArray;
    MBProgressHUD *ProgressHUD;
    NSString *  addressStr1;
    NSString * DESCStr;
    NSString *  namestr;
    NSString * starterUserId;
    NSString * userId;
    NSString * hubRallyPrivateStr;
    NSMutableDictionary * rallyersinfoDic;
    UILabel *lblTitle;
   }
@end

@implementation RJoinRallyViewController
@synthesize businessScrollView,pastRallyScrollView,joinlatitudeStr,joinlongitudeStr,jointitleStr,joinnameStr,name,rallyid,addressTxt,discriptionTxt,addressview,rallyersTableview,hubIDStr,hubRallyDic,calenderViewBoolForJoin,rallyInfoDictFromCalender,Joinmapview,rallyPrivateFromSearch,joinRallyBtn,leaveRallyBtn,dateLbl,timeLbl,rallyNameLbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    leaveRallyBtn.hidden = YES;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

  /*  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
//            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, _mapview.frame.size.width, _mapview.frame.size.height);
//          //  pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
//            rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
//
//            addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-80);
//            addressTxt.frame = CGRectMake(5,3,addressTxt.frame.size.width,addressTxt.frame.size.height);
//            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
            
        }
        else
        {
//            _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, _mapview.frame.size.width, _mapview.frame.size.height);
//           // pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
//            rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
//
//            addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-200);
//            addressTxt.frame = CGRectMake(5,3,addressTxt.frame.size.width,addressTxt.frame.size.height);
//            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
            
        }
    }
       else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;

            if (height== 568)
            {
//                _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, _mapview.frame.size.width, _mapview.frame.size.height);
//             //   pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
//                rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
//                addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-80);
//                addressTxt.frame = CGRectMake(5,3,addressTxt.frame.size.width,addressTxt.frame.size.height);
//                discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
                
            }
            else
            {
//                _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, _mapview.frame.size.width, _mapview.frame.size.height);
//               // pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
//                 rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
//                addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-200);
//                addressTxt.frame = CGRectMake(5,3,addressTxt.frame.size.width,addressTxt.frame.size.height);
//                discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
                
            }
        }
        
        else
        {*/
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            
            if (height== 568)
            {
//                _mapview.frame = CGRectMake(0,0, _mapview.frame.size.width, _mapview.frame.size.height);
//                //   pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
//                rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
//                addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-80);
//            addressTxt.frame = CGRectMake(5,addressview.frame.origin.y-171,addressTxt.frame.size.width,addressTxt.frame.size.height);
//            discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height);
                
            }
            else
            {
               // _mapview.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, _mapview.frame.size.width, _mapview.frame.size.height);
                // pastRallyScrollView.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, pastRallyScrollView.frame.size.width, pastRallyScrollView.frame.size.height-67);
               // rallyersTableview.frame = CGRectMake(addressview.frame.origin.x+addressview.frame.size.width,_mapview.frame.origin.y+_mapview.frame.size.height, rallyersTableview.frame.size.width, rallyersTableview.frame.size.height-67);
               // addressview.frame = CGRectMake(0,_mapview.frame.origin.y+_mapview.frame.size.height,addressview.frame.size.width,addressview.frame.size.height-200);
//                addressTxt.frame = CGRectMake(5,addressview.frame.origin.y-171,addressTxt.frame.size.width,addressTxt.frame.size.height);
//               discriptionTxt.frame = CGRectMake(5,addressTxt.frame.origin.y+addressTxt.frame.size.height+5,discriptionTxt.frame.size.width,discriptionTxt.frame.size.height-60);
               
            }

            
    
    rallyersTableview.clipsToBounds = YES;
    
    CALayer *rightBorder = [CALayer layer];
    rightBorder.borderColor = [UIColor lightGrayColor].CGColor;
    rightBorder.borderWidth = 0.5;
    rightBorder.frame = CGRectMake(0, 1, CGRectGetWidth(rallyersTableview.frame), CGRectGetHeight(rallyersTableview.frame)+42);
    
    [rallyersTableview.layer addSublayer:rightBorder];
    Joinmapview.delegate = self;
    Joinmapview.showsUserLocation = NO;
    Joinmapview.userInteractionEnabled = YES;
    rallyInfoDic = [[NSDictionary alloc]init];
     addressTxt.editable  = NO;
     discriptionTxt.editable = NO;
    
    addressTxt.scrollEnabled = YES;
    discriptionTxt.scrollEnabled = YES;
    discriptionTxt.textColor = [UIColor grayColor];
    addressTxt.textColor = [UIColor lightGrayColor];
    discriptionTxt.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    addressTxt.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];

//    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
//    annotation.coordinate = CLLocationCoordinate2DMake([joinlatitudeStr doubleValue],
//                                                       [joinlongitudeStr doubleValue]);
//    
//     [self.mapview addAnnotation:annotation];
//    imageArray = [[NSArray alloc]init];
//    imageArray = @[@"image_one.png"];
//    imageArray1 = [[NSArray alloc]init];
//    imageArray1 = @[@"image_two.png",@"image_three.png",@"image_four.png"];
    
    rallyersArray = [[NSMutableArray alloc]init];
    rallyersinfoDic = [[NSMutableDictionary alloc]init];

   // [self businessImg];
    
   // [self PastRallyImg];
    //67d767
    
    if(hubRallyDic !=nil)
    {
        hubIDStr =[NSString stringWithFormat:@"%@", [hubRallyDic objectForKey:@"rallyID"]];
        hubRallyPrivateStr = [NSString stringWithFormat:@"%@",[hubRallyDic objectForKey:@"rallyPRIVATE"]];
    }
    
    else if(rallyInfoDictFromCalender != nil)
    {
        hubIDStr =[NSString stringWithFormat:@"%@", [rallyInfoDictFromCalender objectForKey:@"rallyID"]];
        hubRallyPrivateStr = [NSString stringWithFormat:@"%@",[rallyInfoDictFromCalender objectForKey:@"rallyPRIVATE"]];

     /*   Joinmapview.delegate = self;
        Joinmapview.showsUserLocation = NO;
        Joinmapview.userInteractionEnabled = YES;
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake([[rallyInfoDictFromCalender objectForKey:@"rallyLATITUDE"]doubleValue], [[rallyInfoDictFromCalender objectForKey:@"rallyLONGITUDE"]doubleValue]);
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([annotation coordinate], 1500, 1500);
        [Joinmapview setRegion:region animated:YES];
        
        namestr = [NSString stringWithFormat:@"%@",[rallyInfoDictFromCalender objectForKey:@"rallyNAME"]];
        self.title  =namestr;
       // addressStr1 = [NSString stringWithFormat:@"%@",[rallyInfoDictFromCalender objectForKey:@"rallyADDRESS"]];
        addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"`" withString:@""];
       // addressTxt.text = addressStr1;
       // DESCStr = [NSString stringWithFormat:@"%@",[rallyInfoDictFromCalender objectForKey:@"rallyDESC"]];
       // DESCStr = [DESCStr stringByReplacingOccurrencesOfString:@"`" withString:@"\n"];
       // discriptionTxt.text  =DESCStr;*/

        
    }
    else
    {
        hubRallyPrivateStr = rallyPrivateFromSearch;
    }

    
    
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    addressTxt.textContainerInset = UIEdgeInsetsZero;
    addressTxt.textContainer.lineFragmentPadding = 2;
    discriptionTxt.textContainerInset = UIEdgeInsetsZero;
    discriptionTxt.textContainer.lineFragmentPadding = 2;
    [super viewDidLoad];
    
}

-(void)settingAction
{
    RJoinEditViewController *JoinEditViewController = [[RJoinEditViewController alloc]initWithNibName:@"RJoinEditViewController" bundle:nil];
    JoinEditViewController.dicriptionString = DESCStr;
    JoinEditViewController.nameString = namestr;
    //JoinEditViewController.JoinRallyId = rallyid;
    JoinEditViewController.userIdStr = starterUserId;
    if (rallyid)
    {
        JoinEditViewController.rallyIdStr = rallyid;
        
    }
    else
    {
       JoinEditViewController.rallyIdStr = hubRallyId;
    }

    [self.navigationController pushViewController:JoinEditViewController animated:NO];
}

-(void)popToView
{
    if (calenderViewBoolForJoin == YES)
    {
        [ self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

/*-(void)businessImg
{  x = 0;
    // here give you image name is proper so we can access it by for loop.
    businessScrollView.scrollEnabled = YES;
    businessScrollView.showsVerticalScrollIndicator = NO;
    businessScrollView.showsHorizontalScrollIndicator=YES;
    businessScrollView.delegate = self;
    
    for (int i = 0; i <[imageArray count]; i++)
    {
        
        imageView1 = [[UIImageView alloc] initWithFrame: CGRectMake(x,0, 320,177)] ;
        [imageView1 setImage:[UIImage imageNamed:[imageArray objectAtIndex:i]]];
        imageView1.contentMode =  UIViewContentModeScaleToFill;
        [businessScrollView addSubview:imageView1];
        x = x + imageView1.frame.size.width+10;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    businessScrollView.contentSize = CGSizeMake(x,businessScrollView.frame.size.height);
    // businessScrollview.contentSize = CGSizeMake(businessScrollview.contentSize.width,imageView1.frame.size.height);
    
    
}
-(void)PastRallyImg
{
    y = 0;
    
    pastRallyScrollView.scrollEnabled = YES;
    
    pastRallyScrollView.showsVerticalScrollIndicator = YES;
    pastRallyScrollView.showsHorizontalScrollIndicator=NO;
    pastRallyScrollView.delegate = self;
    
    for (int j = 0; j <[imageArray1 count]; j++)
    {
        imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0,y, 102, 100)] ;
        [imageView setImage:[UIImage imageNamed:[imageArray1 objectAtIndex:j]]];
        //imageView.contentMode = UIViewContentModeScaleAspectFit;
        [pastRallyScrollView addSubview:imageView];
        y = y + imageView.frame.size.height;
    }
     self.automaticallyAdjustsScrollViewInsets = NO;
    pastRallyScrollView.contentSize = CGSizeMake(102, y);
}*/

-(void)viewWillAppear:(BOOL)animated
{
   // name.text = joinnameStr;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = nil;
//   self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
  lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0,230,40) ;
    lblTitle.adjustsFontSizeToFitWidth=TRUE;
  //  lblTitle.text =jointitleStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
//    lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        lblTitle.font = [UIFont boldSystemFontOfSize:17.0];
    //[lblTitle sizeToFit];
   self.navigationItem.titleView = lblTitle;
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle1 = [[UILabel alloc] init];
    lblTitle1.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle1.textAlignment = NSTextAlignmentCenter;
    lblTitle1.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle1.textColor = [UIColor whiteColor];
    [lblTitle1 setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle1.text = @"Join Rally";
    self.navigationItem.titleView = lblTitle1;
    
    //self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Heavy" size:10.0],NSFontAttributeName , [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] ,NSForegroundColorAttributeName,nil];

    addressTxt.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    //addressTxt.layer.borderWidth= 0.5f;
    discriptionTxt.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    //discriptionTxt.layer.borderWidth= 0.5f;
    pastRallyScrollView.scrollEnabled = YES;
    pastRallyScrollView.showsVerticalScrollIndicator = NO;
    pastRallyScrollView.showsHorizontalScrollIndicator=YES;
    pastRallyScrollView.delegate = self;
    pastRallyScrollView.contentSize = CGSizeMake(183,348);
    self.automaticallyAdjustsScrollViewInsets = NO;
    timeLbl.textColor = [UIColor blackColor];
    dateLbl.textColor = [UIColor blackColor];
      [self callWebServiceForRally_Id];
}

-(void)viewDidAppear:(BOOL)animated

{
  

}
-(void)callWebServiceForRally_Id
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStr;
    hubRallyId =[NSString stringWithFormat:@"%@",hubIDStr] ;
    if (rallyid)
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/get_rally_info_byid.php?rallyid=%@",rallyid];
        
    }
    else
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/get_rally_info_byid.php?rallyid=%@",hubRallyId];
    }
    //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self JSONRecieved_RallyId:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_RallyId:(NSDictionary *)response
{
    
    NSDictionary *dict = response;
    //    NSLog(@"response12%@",response);
    rallyInfoDic = [dict objectForKey:@"rallyinfo"];
    rallyersArray = [[rallyInfoDic  objectForKey:@"rallyersinfo"]mutableCopy];
    starterUserId =[NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"userID"]];
    if ([starterUserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        UIImage *settingImage = [UIImage imageNamed:@"setting.png"];
        UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        settingBtn.bounds = CGRectMake( 10, 0, 30, 27 );
        [settingBtn addTarget:self action:@selector(settingAction) forControlEvents:UIControlEventTouchUpInside];
        [settingBtn setImage:settingImage forState:UIControlStateNormal];
        UIBarButtonItem *rightsettingButton = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
        self.navigationItem.rightBarButtonItem = rightsettingButton;
        
        joinRallyBtn.hidden = YES;
        leaveRallyBtn.hidden = YES;
        addressview.frame = CGRectMake(addressview.frame.origin.x,addressview.frame.origin.y,addressview.frame.size.width,addressview.frame.size.height+50);
        rallyersTableview.frame = CGRectMake(rallyersTableview.frame.origin.x,rallyersTableview.frame.origin.y,rallyersTableview.frame.size.width,rallyersTableview.frame.size.height+50);
    }
    else
    {
        
        for (int i= 0;i<[rallyersArray count];i++)
        {
            if ([[[rallyersArray objectAtIndex:i]objectForKey:@"userID"] isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
            {
                
                leaveRallyBtn.hidden = NO;
                joinRallyBtn.hidden = YES;
                leaveRallyBtn.backgroundColor = [UIColor lightGrayColor];
                [leaveRallyBtn setTitle:@"Leave Rally" forState:UIControlStateNormal];
                leaveRallyBtn.userInteractionEnabled = YES;
            }
        }
        
        
    }
    [self getRallyData];
    [rallyersTableview reloadData];
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallyIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_GetRallyInfoWithRallyersSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            break;
            
        }
            
            
        default:
            
            break;
            
    }
    
}

-(void)getRallyData
{
    if ([[rallyInfoDic objectForKey:@"rallyNAME"] isEqual:[NSNull null]])
    {
        
        lblTitle.text  =@"";
    }
    else
    {
        namestr = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyNAME"]];
        namestr = [namestr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        
               NSData *data1 = [namestr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
        if (goodValue == nil || [goodValue isEqualToString:@""])
        {
             lblTitle.text = namestr;

        }
        else
        {
            lblTitle.text  =goodValue;
        }

        rallyNameLbl.text = namestr;
        rallyNameLbl.textColor = [UIColor colorWithRed:103.0/255.0 green:215.0/255.0 blue:103.0/255.0 alpha:1.0];
    }
    
    if ([[rallyInfoDic objectForKey:@"rallyADDRESS"] isEqual:[NSNull null]])
    {
        addressTxt.text = @"";
    }
    else
      
    {
        addressStr1 = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyADDRESS"]];
         addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"`" withString:@""];
        addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"%2C" withString:@","];
        //addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"%60" withString:@""];
        addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"  " withString:@" "];
       // addressStr1 = [addressStr1 stringByReplacingOccurrencesOfString:@"\\" withString:@"\"];
        NSData *data = [addressStr1 dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        if (goodValue == nil || [goodValue isEqualToString:@""])
        {
           addressTxt.text = addressStr1;
        }
        else
        {
            addressTxt.text  =goodValue;
        }

        
    }
    if ([[rallyInfoDic objectForKey:@"rallyDESC"] isEqual:[NSNull null]])
    {
        discriptionTxt.text = @"";
    }
    else
        
    {
        DESCStr = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyDESC"]];
        DESCStr = [DESCStr stringByReplacingOccurrencesOfString:@"`" withString:@"\n"];
        DESCStr = [DESCStr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
       // DESCStr = [DESCStr stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
        NSData *data = [DESCStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        if (goodValue == nil || [goodValue isEqualToString:@""])
        {
             discriptionTxt.text  =DESCStr;
        }
        else
        {
             discriptionTxt.text  =goodValue;
        }
       
    }
    NSString * ralluLatitude;
     NSString * ralluLongitude;
    if ([[rallyInfoDic objectForKey:@"rallyLATITUDE"] isEqual:[NSNull null]] || [[rallyInfoDic objectForKey:@"rallyLONGITUDE"] isEqual:[NSNull null]] )
    {
        ralluLatitude = @"";
        ralluLongitude = @"";

    }
    else
    {
        ralluLatitude =[NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyLATITUDE"] ];
        ralluLongitude = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyLONGITUDE"] ];
        double latRally = [ralluLatitude doubleValue];
        double longitideRally = [ralluLongitude doubleValue];
        MKPointAnnotation *annotation1 = [[MKPointAnnotation alloc] init];
        annotation1.coordinate = CLLocationCoordinate2DMake(latRally,longitideRally);
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([annotation1 coordinate], 40, 40);
//        if ( ([ralluLatitude doubleValue] >= -90)     && ([ralluLatitude doubleValue] <= 90)     && ([ralluLongitude doubleValue] >= -180)     && ([ralluLongitude doubleValue] <= 180))
//        {
        [Joinmapview setRegion:region animated:YES];
        [Joinmapview selectAnnotation:annotation1 animated:YES];
        [Joinmapview addAnnotation:annotation1];
//        }
    
    }
    if (![[rallyInfoDic objectForKey:@"rallyDATE"] isEqual:[NSNull null]])
    {
        NSString * rallyDatestr = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyDATE"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:rallyDatestr];
        //  NSString *date = [NSString stringWithFormat:@"%@",[dateFormat dateFromString:dateStr]];
        [dateFormat setDateFormat:@"M/d/yyyy"];
        NSString *  dateStr1 = [dateFormat stringFromDate:date];
        dateLbl.text = [NSString stringWithFormat:@"Date: %@",dateStr1];
        
    }
    if (![[rallyInfoDic objectForKey:@"rallyTIME"] isEqual:[NSNull null]])
    {
        NSString * rallyTimestr = [NSString stringWithFormat:@"%@",[rallyInfoDic objectForKey:@"rallyTIME"]];
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HH:mm:ss"];
        NSDate *time = [timeFormat dateFromString:rallyTimestr];
        // NSString *time = [NSString stringWithFormat:@"%@",[timeFormat dateFromString:timeStr]];
        [timeFormat setDateFormat:@"h:mm a"];
        NSString * timeStr1 = [timeFormat stringFromDate:time];
        
        timeLbl.text = [NSString stringWithFormat:@"Time: %@",timeStr1];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    
    annotationView.image = [UIImage imageNamed:@"r.png"];
   // annotationView.annotation = annotation;
    annotationView.canShowCallout = NO;
        return annotationView;
}
-(void)callWebSercvice_JoinRally
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
    [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
    NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString *idStr = [NSString stringWithFormat:@"%@", [defaults objectForKey:@"id"]];
    NSString * urlStr;
    hubRallyId =[NSString stringWithFormat:@"%@", hubIDStr] ;
    if (rallyid)
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/joinrally.php?userids=%@&rallyid=%@&addedby=0&joindate=%@",idStr,rallyid,dateStr];
    
    }
    else
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/joinrally.php?userids=%@&rallyid=%@&addedby=0&joindate=%@",idStr,hubRallyId,dateStr];

    }

    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_joinRally:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
- (IBAction)leaveRallyaction:(id)sender
{
    [self callwebService_LeaveRally];
}
-(void)callwebService_LeaveRally
{
    NSString * urlStr;
    if (rallyid)
   
    {
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/leaverally.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],rallyid];
    }
    else if (hubIDStr)
    {
         urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/leaverally.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],hubIDStr];
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_LeaveRally:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}
-(void)JSONRecieved_LeaveRally:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //    NSLog(@"response12%@",response);
    switch ([dict[kRAPIResult] integerValue])
    {
        case  APIResponseStatus_RallyLeavedSuccessfully:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have left this Rally"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
           [self.navigationController pushViewController:HomeViewController animated:NO];
            break;
        }
        case  APIResponseStatus_LeaveRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong.Please Try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
            
        default:
            
            break;
            
    }
    
    
}


- (IBAction)joinRallyAction:(id)sender
{
    
        if (![starterUserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
        [self callWebSercvice_JoinRally];
        }
    
}

-(void)JSONRecieved_joinRally:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //NSLog(@"response12%@",response);
    
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
    
    
    {
        case  APIResponseStatus_UserAlreadyJoinedRally:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have already joined this Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [self callWebServiceForRally_Id];
            [alert show];
            break;
        }
        case APIResponseStatus_JoinRallySuccessfull:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats!" message:@"You have just joined a Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [self callWebServiceForRally_Id];
            [alert show];
           
            break;
        }
        case  APIResponseStatus_JoinRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Failed to join the Rally. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
            
        default:
            
            break;
            
    }



}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
    
}
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
     
     return [rallyersArray count];
 }
 - (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
 {
        static NSString* CellIdentifier1 = @"Cell1";
        AsyncImageView * userImagView;
        UILabel * UserNameLbl;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
     if (cell == nil)
      {
          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
          cell.indentationLevel = 5;
          cell.indentationWidth = 10;
          userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(4,7,30 ,30 )];
          userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
          userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
          userImagView.clipsToBounds = YES;
          userImagView.image = [UIImage imageNamed:@""];
          userImagView.backgroundColor = [UIColor clearColor];
          userImagView.tag = -11;
          [cell.contentView addSubview:userImagView];
       
          UserNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(38,0,100 ,cell.frame.size.height )];
          UserNameLbl.numberOfLines = 0;
         // [UserNameLbl sizeToFit];
          UserNameLbl.tag = -12;
          [cell.contentView addSubview:UserNameLbl];
      }
     
    // rallyersinfoDic =[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"rallyersinfo"] ;
     if ([[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"] isEqual:[NSNull null]])
     {
         UserNameLbl.text = @"";
     }
     else
     {
        NSString *  namestr1 = [NSString stringWithFormat:@"%@",[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"]];
         if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
         {
              namestr1 = [namestr1   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
         }
          namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
         namestr1 = [namestr1  stringByReplacingOccurrencesOfString:@"  " withString:@" "];

         UserNameLbl = (UILabel *)[cell viewWithTag:-12];
         UserNameLbl.text = [NSString stringWithFormat:@"%@",namestr1];
         UserNameLbl.textColor = [UIColor grayColor];
         UserNameLbl.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
     }
     
     if ([[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
     {
         userImagView.image =[UIImage imageNamed:@""];
     }
     else
     {
         NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userIMAGE"]];
         NSURL *imageURL = [NSURL URLWithString:imgStr];
        // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
         userImagView= (AsyncImageView *)[cell viewWithTag:-11];
         userImagView.imageURL = imageURL;
         
     }

     
        return cell;
 }

- (void)tableView:(UITableView*)tableView  didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    ProfileViewController.JoinRallyersuserId =[[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userID"];
    [self.navigationController pushViewController:ProfileViewController animated:NO];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        UIView *sectionHeaderView1;
        sectionHeaderView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rallyersTableview.frame.size.width, 60.0)];
        sectionHeaderView1.backgroundColor =[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        UILabel *titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, sectionHeaderView1.frame.origin.y, sectionHeaderView1.frame.size.width, 40)];
        titleLbl.text = @"Rallyers";
        titleLbl.textColor = [UIColor whiteColor];
        [sectionHeaderView1 addSubview:titleLbl];
        return sectionHeaderView1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
           NSString *cellTextStr = [NSString stringWithFormat:@"%@", [[rallyersArray objectAtIndex:indexPath.row] objectForKey:@"userNAME"]];
        CGSize size = [cellTextStr sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByCharWrapping];
//        NSLog(@"%f",size.height);
        return size.height + 30;
    
}


@end
