//
//  RQrCodeViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface RQrCodeViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

- (IBAction)startReading:(id)sender;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *startBtn;
@property (weak, nonatomic) IBOutlet UILabel *tapLbl;

@end
