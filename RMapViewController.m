//
//  RMapViewController.m
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import "RJoinRallyViewController.h"
#import "RStartRallyViewController.h"
#import "AsyncImageView.h"

@interface RMapViewController ()

{
    NSString * adressGetFromServer;
    NSString *searchString;
    NSDictionary *dict;
    NSMutableArray *businessArr;
    NSDictionary * businesdic;
    NSMutableArray * currentCategory;
    NSString * nameSrr;
    NSString * latitudeSrr;
    NSString * longitudeSrr;
    NSString * titleSrr;
    NSString * map_latitudeSrr;
    NSString *map_longitudeSrr;
    float latitude;
    BOOL searchAddress;
    float longitude;
    NSString *searchBarName;
    UIView * infoview;
    UIView * pickerView;
    UIDatePicker *datePicker;
    UIButton *nextButton;
    UIView * timepickerView;
    UIDatePicker *timePicker;
    UIButton *nextaddressBtn;
    UIView *addressView;
    UITextField *nameTxt;
    UITextView *addressTxt;
    UIButton *publicRadioBtn;
    UILabel * publicLbl;
    UILabel * privateLbl;
    UIButton *privateRadioBtn;
    UIButton *okBtn;
    NSString *name_str;
    NSString *date_str;
    NSString *address_str;
    int checkedIndex;
    NSString *title_str;
    NSString *time_str;
    NSString * JoinRallyIDStr;
    NSDictionary * userFriendDic;
    BOOL public;
    int private;
    NSString * idStr;
    NSArray *idArray;
    UIButton * closeBtn ;
    NSString *  annTitle;
    MKPointAnnotation *annotation1 ;
    NSString *loactionAddressStr;
    UIView * inviteView;
    int zoomSet;
    MBProgressHUD *ProgressHUD;
    NSMutableArray * packArr;
    AsyncImageView * userImagView;
    NSMutableArray *selectedRows;
    NSMutableArray * arr ;
    UIButton * selectAllBtn;
    UITableViewCell *cell;
    NSInteger indexrows;
    NSString * map_latitudeSearchAddress;
    NSString * map_longitudeSearchAddress;
    NSString * searchAddressStr;
    NSString *loactionSearchAddressStr;
}
@end

@implementation RMapViewController
@synthesize JoinRallyViewController,Geocoder,calenderViewBool,rallymapView,calenerDateStr,annotation1,inviteTableview,addressSearchBar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    searchAddress = NO;
     zoomSet = 0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    selectedRows = [[NSMutableArray alloc]init];

    businessArr = [[NSMutableArray alloc]init];
    packArr = [[NSMutableArray alloc]init];
    dict = [[NSDictionary alloc]init];
   arr = [[NSMutableArray alloc]init];
    userFriendDic = [[NSDictionary alloc]init];
    businesdic = [[NSDictionary alloc]init];
    currentCategory = [[NSMutableArray alloc]init];
    rallymapView.delegate = self;
    rallymapView.userInteractionEnabled = YES;
    Geocoder = [[CLGeocoder alloc] init];
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5;
    [rallymapView addGestureRecognizer:lpgr];
  
}
- (void)DisplayDatePickerView
{
    [addressSearchBar resignFirstResponder];
    [arr removeAllObjects];
    [selectedRows removeAllObjects];
    
    pickerView = [[UIView alloc]init];
    pickerView.layer.cornerRadius = 5.0f;
    pickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    pickerView.layer.borderWidth = 1.0f;
    
    pickerView.backgroundColor = [UIColor whiteColor];
    rallymapView.userInteractionEnabled = NO;
    datePicker = [[UIDatePicker alloc] init];
    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    if (calenderViewBool == YES)
    {
        if (calenerDateStr != nil)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd LLLL yyyy"];
            NSDate *date1 = [dateFormat dateFromString:calenerDateStr];
            [datePicker setDate:date1 animated:NO];
        }
    }
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextButton addTarget:self  action:@selector(nextTimePickerAction:)forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    nextButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.layer.cornerRadius = 5.0f;
    UILabel * DateLabel = [[UILabel alloc]init];
    DateLabel.text = @"Pick a Date";
    DateLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    
    [rallymapView setExclusiveTouch:NO];
    [pickerView addSubview:datePicker];
    [self.view addSubview:pickerView];
    [self.view bringSubviewToFront:pickerView];
    pickerView.hidden = NO;
    [datePicker becomeFirstResponder];
    pickerView.userInteractionEnabled = YES;
    [pickerView setExclusiveTouch:YES];
    [datePicker setExclusiveTouch:YES];
    [pickerView addSubview:closeBtn];
    
    [pickerView addSubview:DateLabel];
    
    [pickerView addSubview:nextButton];
    
}

   
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    
    [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];

    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    [addressSearchBar resignFirstResponder];
    [arr removeAllObjects];
    [selectedRows removeAllObjects];
    searchAddress = NO;
    CGPoint touchPoint = [gestureRecognizer locationInView:rallymapView];
     
    CLLocationCoordinate2D touchMapCoordinate = [rallymapView convertPoint:touchPoint toCoordinateFromView:rallymapView];
    
    map_latitudeSrr = [NSString stringWithFormat:@"%f",touchMapCoordinate.latitude];
    map_longitudeSrr = [NSString stringWithFormat:@"%f",touchMapCoordinate.longitude];
   
    
    CLLocation *currentLocation = [[CLLocation alloc]
                                   initWithLatitude:touchMapCoordinate.latitude
                                   longitude:touchMapCoordinate.longitude];
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([currentLocation coordinate], 1500, 1500);
    //[self.mapView setRegion:region animated:YES];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getaddress.php?lat=%@&lng=%@",map_latitudeSrr,map_longitudeSrr];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self JSONRecieved_GetGoogleAddress:responseObject];
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
         if (!Geocoder)
        Geocoder = [[CLGeocoder alloc] init];
    
    [Geocoder reverseGeocodeLocation:currentLocation completionHandler:
     ^(NSArray* placemarks, NSError* error)
     {
         if ([placemarks count] > 0)
         {
             CLPlacemark *topResult = [placemarks objectAtIndex:0];
             annTitle = [NSString stringWithFormat:@"%@",[ topResult.addressDictionary objectForKey:@"FormattedAddressLines" ]];
             NSCharacterSet *Character = [NSCharacterSet characterSetWithCharactersInString:@"(\"\")"];
             annTitle = [[annTitle componentsSeparatedByCharactersInSet:Character] componentsJoinedByString:@""];
             
             NSString *squashed = [annTitle stringByReplacingOccurrencesOfString:@"[ ]+\\  "
                                                                      withString:@" "
                                                                         options:NSRegularExpressionSearch
                                                                           range:NSMakeRange(0, annTitle.length)];
             
             loactionAddressStr = [squashed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         }
     }];
    
    
    pickerView = [[UIView alloc]init];
    pickerView.layer.cornerRadius = 5.0f;
    pickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    pickerView.layer.borderWidth = 1.0f;
    
    pickerView.backgroundColor = [UIColor whiteColor];
    rallymapView.userInteractionEnabled = NO;
    datePicker = [[UIDatePicker alloc] init];
    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    if (calenderViewBool == YES)
    {
        if (calenerDateStr != nil)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd LLLL yyyy"];
            NSDate *date1 = [dateFormat dateFromString:calenerDateStr];
            [datePicker setDate:date1 animated:NO];
        }
     }
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];

    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextButton addTarget:self  action:@selector(nextTimePickerAction:)forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    nextButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.layer.cornerRadius = 5.0f;
    
    UILabel * DateLabel = [[UILabel alloc]init];
    DateLabel.text = @"Pick a Date";
    DateLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);

    }
    else
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
   
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    
    [rallymapView setExclusiveTouch:NO];
    [pickerView addSubview:datePicker];
    [self.view addSubview:pickerView];
    [self.view bringSubviewToFront:pickerView];
    pickerView.hidden = NO;
    [datePicker becomeFirstResponder];
    pickerView.userInteractionEnabled = YES;
    [pickerView setExclusiveTouch:YES];
    [datePicker setExclusiveTouch:YES];
    [pickerView addSubview:closeBtn];
     [pickerView addSubview:DateLabel];
    [pickerView addSubview:nextButton];
    
}

-(void)JSONRecieved_GetGoogleAddress:(NSDictionary *)response
{
    NSDictionary *Dict = response;
    //NSLog(@"response12%@",response);
    if (![[Dict objectForKey:@"address"] isEqual:(id)[NSNull null]]) {
        

    adressGetFromServer = [NSString stringWithFormat:@"%@",[Dict objectForKey:@"address"]];
    }
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case  APIResponseStatus_address:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
        case  APIResponseStatus_AddressNotFound:
        {
            adressGetFromServer = @"";
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
 
        default:
            
            break;
            
            
    }
}

-(void)CloseAction:(UIButton *)sender
{
    [rallymapView becomeFirstResponder];
    rallymapView.userInteractionEnabled = YES;
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
    [addressView removeFromSuperview];
    [inviteView removeFromSuperview];
}

-(void)nextTimePickerAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
   
    timepickerView = [[UIView alloc]init];
    timepickerView.layer.cornerRadius = 5.0f;
    timepickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    timepickerView.layer.borderWidth = 1.0f;
    timepickerView.backgroundColor = [UIColor whiteColor];
    timepickerView.userInteractionEnabled = NO;
    [rallymapView setExclusiveTouch:NO];
    
    timePicker = [[UIDatePicker alloc] init];
    
    [timePicker addTarget:self action:@selector(timepickerChanged:) forControlEvents:UIControlEventValueChanged];
    timePicker.datePickerMode = UIDatePickerModeTime;
    timepickerView.userInteractionEnabled = YES;
    [timepickerView setExclusiveTouch:YES];
    [timePicker setExclusiveTouch:YES];
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"HH:mm:ss"];
    time_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:timePicker.date]];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextaddressBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextaddressBtn addTarget:self  action:@selector(nextAddressAction:)  forControlEvents:UIControlEventTouchUpInside];
    [nextaddressBtn setTitle:@"NEXT" forState:UIControlStateNormal];
    nextaddressBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextaddressBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextaddressBtn.layer.cornerRadius = 5.0f;
    
    UILabel * TimeLabel = [[UILabel alloc]init];
    TimeLabel.text = @"Pick a Time";
    TimeLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
   

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);
        
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);

    }
    else
    {
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);

        
    }

    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    [timepickerView addSubview:timePicker];
    [self.view addSubview:timepickerView];
    [self.view bringSubviewToFront:timepickerView];
    [timepickerView addSubview:closeBtn];
    [timepickerView addSubview:nextaddressBtn];
    [timepickerView addSubview:TimeLabel];

 
}
-(void)nextAddressAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
  
    addressView = [[UIView alloc]init];
    addressView.layer.cornerRadius = 5.0f;
    addressView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    addressView.layer.borderWidth = 1.0f;
    addressView.backgroundColor = [UIColor whiteColor];
    addressView.userInteractionEnabled = NO;
    [rallymapView setExclusiveTouch:NO];
    
    nameTxt = [[UITextField alloc] init];
    nameTxt.borderStyle = UITextBorderStyleNone;
    nameTxt.font = [UIFont systemFontOfSize:15];
    nameTxt.placeholder = @"  Name";
    nameTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    nameTxt.keyboardType = UIKeyboardTypeDefault;
    nameTxt.returnKeyType = UIReturnKeyDefault;
    nameTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    nameTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    nameTxt.delegate = self;
    nameTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    nameTxt.layer.borderWidth= 1.3f;
    
    addressTxt = [[UITextView alloc] init];
    addressTxt.font = [UIFont systemFontOfSize:14];
    adressGetFromServer = [adressGetFromServer stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    adressGetFromServer = [adressGetFromServer stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    adressGetFromServer = [adressGetFromServer stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    
    NSMutableString *Mutastring1 = [NSMutableString string];
    NSArray *elements = [adressGetFromServer componentsSeparatedByString:@","];
    
    for (int i= 0;i<elements.count; i++) {
        if (i == elements.count-3)
        {
            [Mutastring1 appendString:[elements objectAtIndex:i]];
            [Mutastring1 appendString:@","];
            
            
        }
        else
        {
            [Mutastring1 appendString:[elements objectAtIndex:i]];
        }
    }

//    NSData *data = [loactionAddressStr dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//    
//    if (goodValue == nil)
//    {
    if (searchAddress == YES)
    {
        
        
        loactionAddressStr = [loactionAddressStr stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        
        
        NSMutableString *Mutastring1 = [NSMutableString string];
        NSArray *elements = [loactionSearchAddressStr componentsSeparatedByString:@","];
        
        for (int i= 0;i<elements.count; i++) {
            if (i == elements.count-3)
            {
                [Mutastring1 appendString:[elements objectAtIndex:i]];
                [Mutastring1 appendString:@","];
                
                
            }
            else
            {
                [Mutastring1 appendString:[elements objectAtIndex:i]];
            }
        }
        
        NSData *data = [Mutastring1 dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        
        if (goodValue == nil || [goodValue isEqualToString:@""])
        {
            addressTxt.text =Mutastring1;
        }
        else
        {
            addressTxt.text =goodValue;
            //addressTxt.text =@"";
        }

    }
    else
    {
        if (adressGetFromServer)
        {
            NSData *data = [Mutastring1 dataUsingEncoding:NSUTF8StringEncoding];
            NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            
            if (goodValue == nil || [goodValue isEqualToString:@""])
            {
                addressTxt.text = Mutastring1;
            }
            else
            {
              addressTxt.text =goodValue;
              //  addressTxt.text =@"";
            }
            

        }
    }

    
//    }
//    else
//    {
//        addressTxt.text =goodValue;
//    }

   
    addressTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    addressTxt.keyboardType = UIKeyboardTypeASCIICapable;
    addressTxt.returnKeyType = UIReturnKeyDefault;
    addressTxt.textContainerInset = UIEdgeInsetsZero;
    addressTxt.textContainer.lineFragmentPadding =10;
    addressTxt.delegate = self;
    addressTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    addressTxt.layer.borderWidth= 1.3f;
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    [closeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [addressView addSubview:closeBtn];
    addressView.userInteractionEnabled = YES;
    [addressView setExclusiveTouch:YES];
    
    publicRadioBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [publicRadioBtn addTarget:self  action:@selector(publicRadioBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [publicRadioBtn setImage:[UIImage imageNamed:@"radio-button_off1.png"]  forState:UIControlStateNormal];
    //[publicRadioBtn setTitle:@"Ok" forState:UIControlStateNormal];
    publicRadioBtn.tag=0;
    publicLbl = [[UILabel alloc]init ];
    publicLbl.text = @"Public";
    
    
    publicLbl.userInteractionEnabled = YES;
    publicLbl.font = [UIFont fontWithName:@"AvenirNext" size:16.0f];
    privateLbl = [[UILabel alloc]init];
    privateLbl.text =@"Private";
    
    privateLbl.userInteractionEnabled = YES;
    privateLbl.font = [UIFont fontWithName:@"AvenirNext" size:16.0f];
    privateRadioBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [privateRadioBtn addTarget:self  action:@selector(publicRadioBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    
    privateRadioBtn.tag=1;
    [privateRadioBtn setImage:[UIImage imageNamed:@"radio-button_off1.png"]  forState:UIControlStateNormal];
    
    okBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [okBtn addTarget:self  action:@selector( okAction:)  forControlEvents:UIControlEventTouchUpInside];
    [okBtn setTitle:@"Next" forState:UIControlStateNormal];
    okBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    okBtn.layer.cornerRadius = 5.0f;
    
    [publicRadioBtn  setImage:[UIImage imageNamed:@"radio-button_on1.png"]  forState:UIControlStateSelected];
    [privateRadioBtn  setImage:[UIImage imageNamed:@"radio-button_on1.png"]  forState:UIControlStateSelected];
    [publicRadioBtn setSelected:YES];

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);

    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);

    }
    else
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);
        
    }
    
        closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
        closeBtn.clipsToBounds =YES;

       [addressView addSubview:nameTxt];
       [addressView addSubview:addressTxt];
       [self.view addSubview:addressView];
       [self.view bringSubviewToFront:addressView];
       [addressView addSubview:publicRadioBtn];
       [addressView addSubview:publicLbl];
       [addressView addSubview:privateLbl];
       [addressView addSubview:privateRadioBtn];
       [addressView addSubview:okBtn];
       [addressView addSubview:closeBtn];
    
}

-(void)publicRadioBtnAction:(id)sender
{
       if ([sender tag] == 0)
       {
        
        [publicRadioBtn setSelected:YES];
        [privateRadioBtn setSelected:NO];
        
    }
    else
    {
        
        [publicRadioBtn setSelected:NO];
        [privateRadioBtn setSelected:YES];
        
    }

    
}
-(void)okAction:(UIButton *)sender
{
    if (nameTxt.text.length == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Please enter a name for the Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([t1 length]>25)
    {
          UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
   }
    else
    {
        [pickerView removeFromSuperview];
        [timepickerView removeFromSuperview];
        [addressView removeFromSuperview];
        
        
        [nameTxt resignFirstResponder];
        [addressTxt resignFirstResponder];
        [self callWebService_viewuserpacks];
        
        inviteTableview = [[UITableView alloc]init ];
        [inviteTableview setSeparatorInset:UIEdgeInsetsZero];
        inviteTableview.allowsMultipleSelection = YES;
        
        inviteView = [[UIView alloc]init];
        inviteView.layer.cornerRadius = 5.0f;
        inviteView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
        inviteView.layer.borderWidth = 1.0f;
        inviteView.backgroundColor = [UIColor whiteColor];
        inviteView.userInteractionEnabled = YES;
        [rallymapView setExclusiveTouch:NO];
        
        UILabel * InviteUserLabel = [[UILabel alloc]init];
        InviteUserLabel.text = @"Invite Users";
        InviteUserLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setTitle:@"X" forState:UIControlStateNormal];
        closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
        [closeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
        closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [okBtn addTarget:self  action:@selector(goToNextViewAction:)  forControlEvents:UIControlEventTouchUpInside];
        [okBtn setTitle:@"OK" forState:UIControlStateNormal];
        okBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okBtn.layer.cornerRadius = 5.0f;
        
        selectAllBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [selectAllBtn addTarget:self  action:@selector(selectAllUserAction:)  forControlEvents:UIControlEventTouchUpInside];
        [selectAllBtn setTitle:@"Select All" forState:UIControlStateNormal];
        selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectAllBtn.layer.cornerRadius = 5.0f;
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-80);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
            
        }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-80);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
        }
        else
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-80);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
            
        }
        
        
        closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
        closeBtn.clipsToBounds =YES;
        
        inviteTableview.dataSource = self;
        inviteTableview.delegate = self;
        [inviteView addSubview:inviteTableview];
        [inviteView addSubview:closeBtn];
        [inviteView addSubview:okBtn];
        [inviteView addSubview:selectAllBtn];
        [inviteView addSubview:InviteUserLabel];
        
        
        [self.view addSubview:inviteView];
        [self.view bringSubviewToFront:inviteView];
        if (privateRadioBtn.selected== YES)
        {
            private= 1;
            [privateRadioBtn setSelected:NO];
            
        }
        else
            
        {
            [privateRadioBtn setSelected:YES];
            private = 0;
        }
        
        
        /* NSDateFormatter *formate=[[NSDateFormatter alloc]init];
         [formate setDateFormat:@"MM-dd-yyyy"];
         //  NSLog(@"value: %@",[formate stringFromDate:datePicker.date]);
         NSString *datepickerDate=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
         NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
         [formate1 setDateFormat:@"HH:mm:ss"];
         // NSLog(@"value: %@",[formate stringFromDate:timePicker.date]);
         NSString *timepickerDate=[NSString stringWithFormat:@"%@",[formate1 stringFromDate:timePicker.date]];*/
        
    }
}
-(void)goToNextViewAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
    [addressView removeFromSuperview];
    NSString * private_Value = [NSString stringWithFormat:@"%d",private];
    RStartRallyViewController *StartRallyViewController = [[RStartRallyViewController alloc]initWithNibName:@"RStartRallyViewController" bundle:nil];
    name_str=[NSString stringWithFormat:@"%@",nameTxt.text];
    address_str =[NSString stringWithFormat:@"%@",addressTxt.text];
    if (searchAddress == YES)
    {
        StartRallyViewController.latitudeStr=map_latitudeSearchAddress;
        StartRallyViewController.longitudeStr=map_longitudeSearchAddress;
    }
    else
    {
        StartRallyViewController.latitudeStr=map_latitudeSrr;
        StartRallyViewController.longitudeStr=map_longitudeSrr;
    }
    

    

    StartRallyViewController.dateStr= date_str;
    StartRallyViewController.timeStr= time_str;
    StartRallyViewController.tileStr= title_str;
    StartRallyViewController.addressStr= address_str;
    StartRallyViewController.nameStr= name_str;
    StartRallyViewController.privateValue=private_Value;
    StartRallyViewController.inviteUserArr = arr;
    [self.navigationController pushViewController:StartRallyViewController animated:NO];

}
-(void)selectAllUserAction:(UIButton *)sender
{
    if (![sender isSelected])
    {
        [selectAllBtn  setTitle:@"Unselect" forState:UIControlStateNormal];
        [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        selectAllBtn.tintColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        
        for (int i= 0; i<[packArr count]; i++)
        {
            cell = [[inviteTableview visibleCells] objectAtIndex:i];
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            NSString *str = [NSString stringWithFormat:@"%li",(long)[cell tag]];
            [arr addObject:str];
        }
        
        [sender setSelected:YES];
    }
    else
    {
        [selectAllBtn  setTitle:@"Select All" forState:UIControlStateNormal];
        [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        for (int i= 0; i<[packArr count]; i++)
        {
            cell = [[inviteTableview visibleCells] objectAtIndex:i];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            NSString *str = [NSString stringWithFormat:@"%li",(long)[cell tag]];
            [arr removeObject:str];
        }
        
        [sender setSelected:NO];
    }
}

-(void)callWebService_viewuserpacks
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }

    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                         
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];


}

-(void)JSONRecieved_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict1 = response;
//    NSLog(@"response12%@",response);
    if ([dict1 objectForKey:@"userfriends"] == [NSNull null])
    {
        
    }
    else
    {
        packArr = [[dict1 objectForKey:@"userfriends"]mutableCopy];
        [inviteTableview reloadData];
    }
           switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ViewUserInPackSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            //            break;
        }
        default:
            
            break;
            
    }
    
}

-(void)callWebService_rallyinvitation
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * arrstr = [NSString stringWithFormat:@"%@",arr];
  
    //arrstr = [arrstr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyinvitation.php?userid=%@&friendids=%@&rallyid=",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],arrstr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_rallyinvitation:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_rallyinvitation:(NSDictionary *)response
{
    NSDictionary *dict1 = response;
//    NSLog(@"response12%@",response);
    if ([dict1 objectForKey:@"userfriends"] == [NSNull null])
    {
        
    }
    else
    {
        packArr = [[dict1 objectForKey:@"userfriends"]mutableCopy];
        [inviteTableview reloadData];
    }
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ViewUserInPackSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            //            break;
        }
        default:
            
            break;
            
    }
    
}

- (void)datepickerChanged:(id)sender
{
   
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
}

- (void)timepickerChanged:(id)sender
{
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"HH:mm:ss"];
       time_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:timePicker.date]];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    searchAddress = NO;
     zoomSet = 0;
    [self callservice];
    [rallymapView becomeFirstResponder];
    rallymapView.userInteractionEnabled = YES;
    [rallymapView removeAnnotations:rallymapView.annotations];
    
  
    UIImage *serchImage = [UIImage imageNamed:@"greenSearch.png"];
    [addressSearchBar setImage:serchImage forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    self.navigationController.navigationBar.hidden = NO;
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 30)];
    UIImage *image = [UIImage imageNamed:@"rally1Logo.png"];
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:image];
    myImageView.frame = CGRectMake(65, -2 , 100, 35);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:myImageView];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];
    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    nextButton.hidden = YES;
    timepickerView.hidden = YES;
    timePicker.hidden = YES;
    nextaddressBtn.hidden = YES;
    addressView.hidden = YES;
    inviteView.hidden = YES;

    }
-(void)viewDidAppear:(BOOL)animated
{
    
    }
-(void)popToView
{
    if (calenderViewBool == YES)
    {
      [ self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
         [self.navigationController popViewControllerAnimated:YES];
    }
   
}

-(void)callservice
{
    NSString *urlStr1= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallymap.php"];
    NSURL *url = [NSURL URLWithString:urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        [self JSONRecievedData:responseObject];
         //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         //ProgressHUD = nil;
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}


-(void)JSONRecievedData:(NSDictionary *)response
{
    [businessArr removeAllObjects];
    dict = response;
//    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"rallies"]!= [NSNull null])
    {
        businessArr = [[dict objectForKey:@"rallies"]mutableCopy] ;
    }
    
    for ( int i= 0; i<[businessArr count];i++)
    {
        businesdic  = [businessArr objectAtIndex:i];
        annotation1 = [[MKPointAnnotation alloc] init];
        annotation1.coordinate = CLLocationCoordinate2DMake([businesdic[@"rallyLATITUDE"] doubleValue],
                                                           [businesdic[@"rallyLONGITUDE"] doubleValue]);
       
        nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
      //  _mapView.userLocation.title = nameSrr;
         nameSrr = [nameSrr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        //  _mapView.userLocation.title = nameSrr;
        if (rallynameStr != nil) {
            annotation1.title = rallynameStr;
        }
        else{
            annotation1.title = nameSrr;
        }

//        if ( ([businesdic[@"rallyLATITUDE"] doubleValue] >= -90)     && ([businesdic[@"rallyLATITUDE"] doubleValue] <= 90)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] >= -180)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] <= 180))
//        {
        [rallymapView addAnnotation:annotation1];
//        }
    }
    
    
}

#pragma mark Delegate Methods

/*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (searchAddress == NO)
    {
        
        if([annotation isKindOfClass:[MKUserLocation class]])
        {
            return nil;
        }
        
        if ([annotation isKindOfClass:[MKPointAnnotation class]])
        {
            static NSString *AnnotationViewID = @"annotationViewID";
            
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
            
            if (annotationView == nil)
            {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
                annotationView.image = [UIImage imageNamed:@"rlogo12.png"];
                annotationView.canShowCallout = YES;
                annotationView.enabled =YES;
                
                UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                annotationView.rightCalloutAccessoryView = detailButton;
            }
            else
            {
                annotationView.annotation = annotation;
            }
            return annotationView;
        }
    }
    else
        
    {
        if ([annotation isKindOfClass:[MKPointAnnotation class]])
        {
            MKPinAnnotationView*    pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
            
            if (!pinView)
            {
                pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:@"CustomPinAnnotationView"];
                if([[pinView.annotation title] isEqualToString:searchBarName])
                {
                    pinView.pinColor = MKPinAnnotationColorGreen;
                    pinView.canShowCallout = YES;
                    
                }
                else
                {
                   pinView.image = [UIImage imageNamed:@"rlogo1.png"];
                    
                }
               // searchAddress = NO;
                
            }
            else
            {
                pinView.annotation = annotation;
            }
            return pinView;
        }
    }
    return nil;
}
*/
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        static NSString *AnnotationViewID = @"annotationViewID";
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        
        
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        if([[annotationView.annotation title] isEqualToString:searchBarName])
        {
            annotationView.pinColor = MKPinAnnotationColorGreen;
            annotationView.tag = -10;
            annotationView.canShowCallout = YES;
            
        }
        else
        {
            static NSString *AnnotationViewID = @"annotationViewID";
            
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
            
            if (annotationView == nil)
            {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
                annotationView.image = [UIImage imageNamed:@"r.png"];
                annotationView.canShowCallout = YES;
                annotationView.enabled =YES;
                annotationView.tag = -101;
                UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                annotationView.rightCalloutAccessoryView = detailButton;
            }
            else
            {
                annotationView.annotation = annotation;
            }
            return annotationView;
            

//            annotationView.image = [UIImage imageNamed:@"R-logo72.png"];
//            annotationView.canShowCallout = YES;
//            annotationView.enabled =YES;
//            annotationView.tag = -101;
//            UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//            annotationView.rightCalloutAccessoryView = detailButton;
        }
        return annotationView;
    }
    
    
    return nil;
    
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if (view.tag ==-101)
    {
        
        id <MKAnnotation> annotation = [view annotation];
        
        if ([annotation isKindOfClass:[MKPointAnnotation class]])
        {
            
            for ( int j= 0; j<[businessArr count];j++)
            {
                businesdic  = [businessArr objectAtIndex:j];
                nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
                NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallynameStr1 = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                rallynameStr1 = [rallynameStr1  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data1 = [rallynameStr1 dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallyDataEncode = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
                NSString * rallyname_Str;
                if (rallyDataEncode == nil || [rallyDataEncode isEqualToString:@""])
                {
                    rallyname_Str = rallynameStr1;
                    
                }
                else
                {
                    rallyname_Str = rallyDataEncode;
                }
                
                
                
                if([[annotation title] isEqualToString:rallyname_Str])
                {
                    annotation = [mapView.selectedAnnotations objectAtIndex:0];
                    //CLLocationCoordinate2D coord = annotation.coordinate;
                    
                    if ( [annotation coordinate].latitude == [[businesdic valueForKeyPath:@"rallyLATITUDE"]doubleValue] || [annotation coordinate].longitude  == [[businesdic valueForKeyPath:@"rallyLONGITUDE"]doubleValue])
                    {
                        JoinRallyIDStr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyID"]];
                    }
                }
            }
            
            //}
        }
        JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
        JoinRallyViewController.rallyid= JoinRallyIDStr;
        
        [self.navigationController pushViewController:JoinRallyViewController animated:NO];
        
    }
    else
    {
        searchAddress = YES;
        [self DisplayDatePickerView];
        
    }
    //  NSInteger indexOfTheObject = [mapView.annotations  indexOfObject:view.annotation];
    //  NSLog(@"calloutAccessoryControlTapped: annotation = %d", [mapView.annotations indexOfObject:view.annotation]);
    
    //   businesdic  = [businessArr objectAtIndex:indexOfTheObject];
    
    
    
    /*  latitudeSrr = [NSString stringWithFormat:@"%f",[businesdic[@"rallyLATITUDE"] doubleValue]] ;
     longitudeSrr = [NSString stringWithFormat:@"%f",[businesdic[@"rallyLONGITUDE"] doubleValue]] ;
     nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
     JoinRallyIDStr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyID"]];
     titleSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
     JoinRallyViewController.joinlatitudeStr=latitudeSrr;
     JoinRallyViewController.joinlongitudeStr= longitudeSrr;
     JoinRallyViewController.joinnameStr=nameSrr;
     JoinRallyViewController.jointitleStr= titleSrr;*/
}

/*-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [mapView setCenterCoordinate:userLocation.coordinate animated:YES];
}*/
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    id <MKAnnotation> annotation = [view annotation];
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {

    if (view.tag ==-10)
    {
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        view.rightCalloutAccessoryView = rightButton;
        
    }
    else if(view.tag ==-101)
    {
        UIButton *detailButton = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
        view.rightCalloutAccessoryView = detailButton;
        
    }
    }
}
/*- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
   [mapView removeAnnotation:annotation1];

}*/

-(void)myShowDetailsMethod:(UIButton *)sender
{
   // NSLog(@"hello");
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    /*double miles = 50.0;
    double scalingFactor = ABS( (cos(2 * M_PI * userLocation.coordinate.latitude / 360.0) ));*/
 
   MKCoordinateSpan span;
   
   span.latitudeDelta = 10.725;
   span.longitudeDelta =10.725;
   
   MKCoordinateRegion region;
   region.span = span;
    region.center = userLocation.coordinate;
    
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1500,1500);
    
    if (zoomSet == 0)
    {
        [rallymapView setRegion:[rallymapView regionThatFits:region] animated:YES];
        zoomSet = 1;
    }
    
    for ( int i= 0; i<[businessArr count];i++)
    {
        businesdic  = [businessArr objectAtIndex:i];
        annotation1 = [[MKPointAnnotation alloc] init];
        annotation1.coordinate = CLLocationCoordinate2DMake([businesdic[@"rallyLATITUDE"] doubleValue],
                                                            [businesdic[@"rallyLONGITUDE"] doubleValue]);
        nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
        //  _mapView.userLocation.title = nameSrr;
        nameSrr = [nameSrr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        //  _mapView.userLocation.title = nameSrr;
        if (rallynameStr != nil) {
            annotation1.title = rallynameStr;
        }
        else{
            annotation1.title = nameSrr;
        }

//        if ( ([businesdic[@"rallyLATITUDE"] doubleValue] >= -90)     && ([businesdic[@"rallyLATITUDE"] doubleValue] <= 90)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] >= -180)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] <= 180))
//        {

        [rallymapView addAnnotation:annotation1];
//        }
    }
}



#pragma mark - ---------- TextFieldDelegate methods ------------

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    nameTxt.leftView = paddingView;
    nameTxt.leftViewMode = UITextFieldViewModeAlways;
   
    CGRect viewFrame = addressView.frame;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {

    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
         viewFrame.origin.y -=10;
        
    }
    else
    {
         viewFrame.origin.y -=40;
    }

    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y -=10;
            
        }
        else
        {
            viewFrame.origin.y -=40;
        }

    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
         viewFrame.origin.y -=10;
        }
        else
        {
            viewFrame.origin.y -=40;
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    [addressView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
//    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//    if([t1 length]>25)
//    {
//        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//    }
   /* CGRect viewFrame = addressView.frame;
    viewFrame.origin.y -= 10;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [addressView setFrame:viewFrame];
    
    [UIView commitAnimations];*/
    //[self animateTextView:NO];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGRect viewFrame = addressView.frame;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
      CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        viewFrame.origin.y =80;
        
    }
    else
    {
        viewFrame.origin.y =60;
    }
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y =80;
            
        }
        else
        {
            viewFrame.origin.y =60;
        }
  
    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
        viewFrame.origin.y =80;
        
        }
        else
        {
        viewFrame.origin.y = 60;
        }
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [addressView setFrame:viewFrame];
    
    [UIView commitAnimations];
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        CGRect viewFrame = addressView.frame;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
        {

        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y =80;
            
        }
        else
        {
            viewFrame.origin.y =60;
        }
        }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                viewFrame.origin.y =80;
                
            }
            else
            {
                viewFrame.origin.y =60;
            }
 
        }
        else
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
             viewFrame.origin.y =80;
            }
            else
            {
                viewFrame.origin.y =60;
            }
        }
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.2];
        [addressView setFrame:viewFrame];
        
        [UIView commitAnimations];

        [textView resignFirstResponder];
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:NO];
}

- (void) animateTextView:(BOOL) up
{
    int movementDistance;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {

    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
       movementDistance =20;
        
    }
    else
    {
       movementDistance =50;
    }
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance =20;
            
        }
        else
        {
            movementDistance =50;
        }
 
    }
    
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance =20;

        }
        else
        {
            movementDistance =50;

        }

       
    }

   // const int movementDistance =180; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
   // NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    if([t1 length]>25)
    //    {
    //        return NO;
    //    }
    if([[textField text] length] - range.length + string.length < 26){
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}

#pragma mark - ------------ Searchbar Delegate Methods ------------

//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self handleSearch:searchBar];
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self handleSearch:searchBar];
}

//do our search on the remote server using HTTP request
- (void)handleSearch:(UISearchBar *)searchBar
{
   // NSLog(@"User searched for %@", searchBar.text);
   
    [searchBar resignFirstResponder];
    searchBarName = [NSString stringWithFormat:@"%@",searchBar.text];
     Geocoder = [[CLGeocoder alloc] init];
    [Geocoder geocodeAddressString:addressSearchBar.text completionHandler:^(NSArray *placemarks, NSError *error) {
        //Error checking
        if(placemarks.count > 0 )
        {
            
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
            
      /*  MKCoordinateRegion region;
            if ( (region.center.latitude >= -90)     && (region.center.latitude <= 90)     && (region.center.longitude >= -180)     && (region.center.longitude <= 180))
                {
        region.center.latitude = placemark.region.center.latitude;
        region.center.longitude = placemark.region.center.longitude;
        MKCoordinateSpan span;
        double radius = placemark.region.radius / 1000; // convert to km
        
//        NSLog(@"[searchBarSearchButtonClicked] Radius is %f", radius);
        span.latitudeDelta = radius / 112.0;
        
        region.span = span;
        
        [rallymapView setRegion:region animated:YES];}*/
            searchAddress = YES;
//            float spanX = 0.00725;
//            float spanY = 0.00725;
            float spanX = 0.725; //added by swati 11-oct
            float spanY = 0.725; //added by swati 11-oct
            MKCoordinateRegion region;
            region.center.latitude = placemark.region.center.latitude;
            region.center.longitude = placemark.region.center.longitude;
            region.span = MKCoordinateSpanMake(spanX, spanY);
            
            MKPointAnnotation *annotation4 =
            [[MKPointAnnotation alloc]init];
            annotation4.coordinate = CLLocationCoordinate2DMake(placemark.region.center.latitude, placemark.region.center.longitude);
            annotation4.title = searchBarName;
            
            
            
            
            searchAddressStr = [NSString stringWithFormat:@"%@",[placemark.addressDictionary objectForKey:@"FormattedAddressLines"]];
            NSCharacterSet *Character = [NSCharacterSet characterSetWithCharactersInString:@"(\"\")"];
            searchAddressStr = [[searchAddressStr componentsSeparatedByCharactersInSet:Character] componentsJoinedByString:@""];
            
            NSString *squashed = [searchAddressStr stringByReplacingOccurrencesOfString:@"[ ]+\\  "
                                                                             withString:@" "
                                                                                options:NSRegularExpressionSearch
                                                                                  range:NSMakeRange(0, annTitle.length)];
            
            //loactionSearchAddressStr = [squashed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            loactionSearchAddressStr = searchBarName;
            map_latitudeSearchAddress = [NSString stringWithFormat:@"%f",placemark.region.center.latitude];
            map_longitudeSearchAddress = [NSString stringWithFormat:@"%f", placemark.region.center.longitude];
            
//            if ( (placemark.region.center.latitude >= -90)     && (placemark.region.center.latitude <= 90)     && (  placemark.region.center.longitude >= -180)     && (  placemark.region.center.longitude <= 180))
//            {

            [rallymapView addAnnotation:annotation4];
            [rallymapView  setRegion:region animated:YES];
//            }
                  }
    }];
    
}

//user tapped on the cancel button
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    
    [searchBar resignFirstResponder];
}

#pragma mark - ------------ UITableView Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [packArr count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    cell = nil;
    userImagView = nil;
    static NSString *TableViewCellIdentifier = @"Cell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
        cell.indentationLevel = 5;
        cell.indentationWidth = 12;
        userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(12,4,35 ,35)];
        userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
        userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
        userImagView.clipsToBounds = YES;
        userImagView.image = [UIImage imageNamed:@""];
        userImagView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:userImagView];
      }
    indexrows = indexPath.row;
    if([selectedRows containsObject:indexPath]){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    userFriendDic = [packArr objectAtIndex:indexPath.row];
    cell.tag = [[userFriendDic objectForKey:@"friendID"]integerValue];
    if ([[userFriendDic objectForKey:@"friendNAME"] isEqual:[NSNull null]])
    {
        cell.textLabel.text  = @" ";
    }
    else
    {
        cell.textLabel.textColor = [UIColor grayColor];
        
        NSString *  nameStr2 = [NSString stringWithFormat:@"%@",[userFriendDic objectForKey:@"friendNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            
            nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
          nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        cell.textLabel.text  = nameStr2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    }
    if ([[userFriendDic objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
    {
        userImagView.image =[UIImage imageNamed:@""];
    }
    else
    {
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userFriendDic objectForKey:@"userIMAGE"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        userImagView.imageURL = imageURL;
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *str = [NSString stringWithFormat:@"%i",[cell tag]];
  
    if([selectedRows containsObject:indexPath])
    {
        [selectedRows removeObject:indexPath];
        [arr removeObject:str];
        
    }
    else
    {
        [selectedRows addObject:indexPath];
        [arr addObject:str];
       
    }
    [tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
