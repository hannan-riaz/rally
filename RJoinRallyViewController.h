//
//  RJoinRallyViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface RJoinRallyViewController : UIViewController<UIScrollViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSArray * imageArray;
      NSArray * imageArray1;
}

@property (strong, nonatomic) IBOutlet UILabel *rallyNameLbl;
@property (nonatomic, strong) NSDictionary *rallyInfoDictFromCalender;
@property (strong, nonatomic) NSString *rallyPrivateFromSearch;
@property (nonatomic) BOOL calenderViewBoolForJoin;
@property (weak, nonatomic) IBOutlet UITextView *discriptionTxt;
@property (weak, nonatomic) IBOutlet UITextView *addressTxt;
@property (weak, nonatomic) NSDictionary * hubRallyDic;
@property (weak, nonatomic) IBOutlet UIView *addressview;
@property (weak, nonatomic) IBOutlet UIScrollView *businessScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *pastRallyScrollView;
@property (strong, nonatomic) IBOutlet MKMapView *Joinmapview;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) NSString *joinlatitudeStr;
@property (weak, nonatomic)  NSString *joinlongitudeStr;
@property (weak, nonatomic)  NSString *jointitleStr;
@property (weak, nonatomic)  NSString *joinnameStr;
@property (weak, nonatomic)  NSString *joinIDStr;
@property (strong, nonatomic)  NSString *hubIDStr;
@property (strong, nonatomic) IBOutlet UITableView *rallyersTableview;
@property (strong, nonatomic) IBOutlet UIButton *joinRallyBtn;
@property (strong, nonatomic) IBOutlet UIButton *leaveRallyBtn;
- (IBAction)leaveRallyaction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;

- (IBAction)joinRallyAction:(id)sender;
@property (strong, nonatomic)  NSString *rallyid;
@end
