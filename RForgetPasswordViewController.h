//
//  RForgetPasswordViewController.h
//  Rally
//
//  Created by Ambika on 7/24/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RForgetPasswordViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emilIdTxt;
- (IBAction)forgetAction:(id)sender;

@end
