//
//  RSignInViewController.m
//  Rally
//
//  Created by Ambika on 4/24/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RSignInViewController.h"
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import "RHomeViewController.h"
#import "STTwitter.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "RForgetPasswordViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RSignInViewController ()<GPPSignInDelegate>
{
    MBProgressHUD *ProgressHUD;
    BOOL FirstLogin;
    BOOL twitterLogin;
    BOOL Login;

    NSString *oauthAccessTokenSecret;
    NSString *oauthAccessToken;
}
@property (nonatomic, strong) STTwitterAPI *twitter;
@property (nonatomic, strong)GPPSignIn *signIn;

@end

@implementation RSignInViewController
@synthesize loginBtn,password,emailIDTxt,signIn,forgetPassBtn,emailBtn,twitterBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad

{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"id"]){
        
        RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
        [self.navigationController pushViewController:HomeViewController animated:NO];
        return;
    }

    
    FirstLogin = NO;
    twitterLogin = NO;
    Login = NO;
    FBLoginView *loginview = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email",@"user_about_me"]];
    for (id loginObject in loginview.subviews)
    {
        if ([loginObject isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  loginObject;
            loginButton.contentMode = UIViewContentModeScaleAspectFill;
            UIImage *loginImage = [UIImage imageNamed:@"f-green.png"];
            
            loginButton.opaque = YES;
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_gray.png"] forState:UIControlStateSelected];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_gray.png"] forState:UIControlStateHighlighted];
            
        }
        if ([loginObject isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  loginObject;
            loginLabel.text = @"";
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    
    if (height== 568) {
        
       loginview.frame = CGRectMake(66,247, 50, 47);
        //loginview.frame = CGRectMake(102,247, 50, 47);

    }
    
    else {
        
       // loginview.frame = CGRectMake(102,246, 50, 47);
        loginview.frame = CGRectMake(66,246, 50, 47);
    }
    //   loginview.frame = CGRectMake(68,376, 50, 90);
    
    
    loginview.delegate = self;
    loginview.layer.shadowColor = [UIColor clearColor].CGColor;
    loginview.layer.shadowOpacity = 0;
    
    [self.view addSubview:loginview];
    
    
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    // signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    
    // Do any additional setup after loading the view from its nib.

    
    emailIDTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    emailIDTxt.layer.borderWidth= 1.3f;
    password.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    password.layer.borderWidth= 1.3f;

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    emailIDTxt.leftView = paddingView;
    emailIDTxt.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    password.leftView = paddingView1;
    password.leftViewMode = UITextFieldViewModeAlways;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return  YES;
}
-(void)viewWillAppear:(BOOL)animated{
    forgetPassBtn.titleLabel.textColor = [UIColor lightGrayColor];
    self.navigationItem.title = @"Log In";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.f green:245.0/255.f blue:245.0/255.f alpha:1.0];
    self.navigationController.navigationBar.translucent =NO;
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 25.0f)];
    [backButton setImage:[UIImage imageNamed:@"back.png"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    UIBarButtonItem *submitBtn = [[UIBarButtonItem alloc] initWithTitle:@"Sign In" style:UIBarButtonItemStylePlain target:self action:@selector(logInAction:)];
    submitBtn.tintColor = [UIColor lightGrayColor];
    [submitBtn setTitleTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = submitBtn;
    
}
-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logInAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    [emailIDTxt resignFirstResponder];
    [password resignFirstResponder];
    if(emailIDTxt.text.length ==0 || password.text.length == 0 )
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"All Fields Mandatory!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
       if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
      //  NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        

        
        NSString *urlStr = [NSString stringWithFormat:loginURL,emailIDTxt.text,password.text,deviceToken];
             NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
           
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);

         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
    }
    
    
}


-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
   // NSLog(@"%@",response);
       switch ([dict[kRAPIResult] integerValue])
    
    {
           case APIResponseStatus_LoginSuccessfull:
            
        {
            
            if ([dict objectForKey:@"userID"] != [NSNull null])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
            }
            if([dict objectForKey:@"userNAME"] != [NSNull null])
            {
                
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
            }

            
            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatus_LoginFailed:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Login Failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
        case APIResponseStatus_NotRegistered:
            
            {
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Your username is not registered with Rally. Please Check and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
                break;

            
        }
            
        default:
            
            break;
            
    }
    
}



#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    // first get the buttons set for login mode
    
    
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
      NSString * facebook_userId = [NSString stringWithFormat:@"%@",user.id];
//    NSLog(@"%@",user.id);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FBlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"facebookLogin"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?facebookid=%@&deviceid=%@&devicetype=iPhone",facebook_userId,deviceToken];
        

        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
        
        
        RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
        [self.navigationController pushViewController:HomeViewController animated:NO];

    }
    else{
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
        NSString *username1 = [nameStr
                              stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        
        NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",user.birthday];
        
        NSDateFormatter *formate=[[NSDateFormatter alloc]init];
         [formate setDateFormat:@"MM/dd/yyyy"];
        // NSLog(@"value: %@",[formate dateFromString:birthdayDate_str]);
        NSDate * date=[formate dateFromString:birthdayDate_str];
        
        NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
        [formate1 setDateFormat:@"yyyy-MM-dd"];
        NSString * dobStr = [formate1 stringFromDate:date];
        
        NSString * imgStr  =[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",user.id];;
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        NSData * FacebookImageData = [NSData dataWithContentsOfURL:imageURL];
        NSString * urlStr1= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=%@&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&facebookid=%@",username1,dobStr,[user objectForKey:@"email"],user.id,deviceToken, dateStr,facebook_userId];
        
        
        //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
        
       
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr1]];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:FacebookImageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecievedData:responseObject];
             if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]){
                 [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"facebookLogin"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Error", error.localizedDescription);         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
    }
}
-(void)JSONRecievedData:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //NSLog(@"response12%@",response);
        switch ([dict[kRAPIResult] integerValue])
    {
            
        case APIResponseStatusContactInUse:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This email address is already registered. Please try with different email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
        case  APIResponseStatus_RegistrationSuccessfullWithImage:
        {
           //FirstLogin=NO;
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
        }
            
        case APIResponseStatus_RegistrationSuccessfullWithNoImage:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            break;
            
        }
            
        case APIResponseStatus_RegistrationFailed:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Registration Faileld! Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
        case APIResponseStatusFacebookIDInUse:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusFacebookIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"FacebookID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
            
        case APIResponseStatusTwitterIDInUse:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];


            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusTwitterIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"TwitterID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
        case APIResponseStatus_UserAlreadyExist:
            
        {
            
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];

            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusGoogleIDInUse:
            
        {
            
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            
            [namedefaults synchronize];
            
            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
            
        }
            
        case APIResponseStatusGoogleIDUnique:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"GoogleID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
            

        default:
            
            break;
            
    }
    
}




/*- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    
    // Called after logout
   // NSLog(@"Logged out");
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    
}*/
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook Error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
//        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
    
}


- (IBAction)twitterAction:(id)sender
{
    twitterBtn.tintColor = [UIColor clearColor];
    [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateSelected];
    [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_gray.png"] forState:UIControlStateHighlighted];
    self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"JdCjH0kAvlB7WWMFmP96d6eU3"    consumerSecret:@"aqiGPCsxIT4Do65Y0hpSP2RtJj4l16SdVTAedsEvmm8d0QeFHT"];

    
    [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
//        NSLog(@"-- url: %@", url);
//        NSLog(@"-- oauthToken: %@", oauthToken);
        
        [[UIApplication sharedApplication] openURL:url];
        
    }
                    forceLogin:@(YES)
                    screenName:nil
                 oauthCallback:@"rallyapp://twitter_access_tokens/"
                    errorBlock:^( NSError *error) {
//                        NSLog(@"-- error: %@", error);
                        //_loginStatusLabel.text = [error localizedDescription];
                        
                    }];
    

}
- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier {
    
    
    [_twitter postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
        
        
//        NSLog(@"-- screenName: %@", screenName);
//        NSLog(@"-- screenName: %@", userID);
        oauthAccessTokenSecret =[NSString stringWithFormat:@"%@",_twitter.oauthAccessTokenSecret ];
        oauthAccessToken =[NSString stringWithFormat:@"%@",_twitter.oauthAccessToken];
        NSString * twitter_userId = [NSString stringWithFormat:@"%@",userID];  
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TWitterlogged_in"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];

     /*   if ([[NSUserDefaults standardUserDefaults]objectForKey:@"twitterLogin"])
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults objectForKey:@"deviceToken"];
            NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
            deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
            
            NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?twitterid=%@&deviceid=%@&devicetype=iPhone",twitter_userId,deviceToken];

           
            
            NSURL *url = [NSURL URLWithString:facebookUrlStr];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [self JSONRecieved:responseObject];
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 
             }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 kRAlert(@"Whoops", error.localizedDescription);             }];
            [[NSOperationQueue mainQueue] addOperation:operation];
            

                   }
        else
        
        {
            */
            [_twitter getUsersShowForUserID:nil orScreenName:screenName includeEntities:nil successBlock:^(NSDictionary *user)
            {
                
                
//                NSLog(@"user--%@", user );
                
                NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
                [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
                NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
                NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
                NSString *username2 = [nameStr
                                      stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults objectForKey:@"deviceToken"];
                NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
                
                NSString * checkurl =[NSString stringWithFormat:@"http://abs.twimg.com/sticky/default_profile_images/default_profile_6.png"];
                NSString *code = [checkurl  substringWithRange:NSMakeRange(0, 50)];
                NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url"]];
                NSString *code1 = [imageURLString substringWithRange:NSMakeRange(0,50)];
                
                NSData *avatarData;
                if ([code1 isEqualToString:code]) {
                    UIImage *image=[UIImage imageNamed:@"profileImg.png"];
                    // profileImg.image = [UIImage imageNamed:@"profileImg.png"];
                    
                    avatarData =UIImageJPEGRepresentation(image, 0.1);
                    
                }
                else
                {
                    NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url_https"]];
                    NSString *imageUrlNew=[imageURLString stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
                    NSURL *imageURL = [NSURL URLWithString:imageUrlNew];
                    avatarData = [NSData dataWithContentsOfURL:imageURL];
//                    NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url"]];
//                    NSURL *imageURL = [NSURL URLWithString:imageURLString];
//                    avatarData = [NSData dataWithContentsOfURL:imageURL];
                }
                

                
//               NSString * urlStr2= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=123&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@",username2,[user objectForKey:@"screen_name"],userID,deviceToken, dateStr];
                
                
                 NSString * urlStr2= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=123&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&twitterid=%@",username2,[user objectForKey:@"screen_name"],userID,deviceToken, dateStr,twitter_userId];
                //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
//                NSLog(@"%@",dateStr);
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:urlStr2]];
                [request setHTTPMethod:@"POST"];
                
                NSMutableData *body = [NSMutableData data];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                // UIImage *image=avtarimage;
                //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:avatarData]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [request setHTTPBody:body];

//                NSURL *url = [NSURL URLWithString:urlStr2];
//                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                
                operation.responseSerializer = [AFJSONResponseSerializer serializer];
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     [self JSONRecievedData:responseObject];
                     if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]){
                         [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"twitterLogin"];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     ProgressHUD = nil;
                 
                 }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
                 {
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     ProgressHUD = nil;
                     kRAlert(@"Whoops", error.localizedDescription);                 }];
                [[NSOperationQueue mainQueue] addOperation:operation];
                //  NSLog(@"%@",[user valueForKey:@"profile_image_url"]);
            }errorBlock:^(NSError *error) {
//              NSLog(@"%@",[error localizedDescription]);
            }];
        }
    //}
                                 errorBlock:^(NSError *error)
     {
         
//         NSLog(@"-- %@", [error localizedDescription]);
         
         
     }];
    
}



- (IBAction)emailAction:(id)sender
{
    emailBtn.tintColor = [UIColor clearColor];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateSelected];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateHighlighted];
    [signIn authenticate];
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    //signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [[GPPSignIn sharedInstance] trySilentAuthentication];
    [self refreshUserInfo];

}
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth   error: (NSError *) error
{
    if (error)
    {
       // NSLog(@"Received error %@ and auth object %@",error, auth);
        return;
        
    }
    else
    {
        
       [self refreshUserInfo];
    }
}
- (void)didDisconnectWithError:(NSError *)error {
    if (error)
    {
        // NSLog( @"Status: Failed to disconnect");
    } else
    {
//        NSLog( @"Status: Disconnected");
        
    }
    [self refreshUserInfo];
    
}
- (void)reportAuthStatus {
        [self refreshUserInfo];
}
- (void)refreshUserInfo

{
    if ([GPPSignIn sharedInstance].authentication == nil)
    {
        return;
    }
else
{
    
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil)
    {
        return;
    }
    
    
//    NSLog(@"%@",person);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"googleLogin"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
    
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"googleLogin"])
    {
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?googleid=%@&deviceid=%@&devicetype=iPhone",person.identifier,deviceToken];
        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops",error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
        
        
    }
    else
    {
        
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * nameStr = [NSString stringWithFormat:@"%@",person.displayName];
        NSString *username = [nameStr
                              stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        
        //NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",person.birthday];
        
        NSString * checkurl =[NSString stringWithFormat:@"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50"];
        NSString *imageURLString = person.image.url;
        NSData *avatarData;
        if ([imageURLString isEqualToString:checkurl]) {
            UIImage *image=[UIImage imageNamed:@"profileImg.png"];
            // profileImg.image = [UIImage imageNamed:@"profileImg.png"];
            
            avatarData =UIImageJPEGRepresentation(image, 0.1);
            
        }
        else
        {
            NSString *imageURLString = person.image.url;
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }

        // UIImage * avtarimage1= [UIImage imageWithData:avatarData];
        
        
        NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=1&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&googleid=%@",username,[GPPSignIn sharedInstance].userEmail,person.identifier,deviceToken, dateStr,person.identifier];
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // UIImage *image=avtarimage;
        //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:avatarData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             
             if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]   )
             {
                 [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"googleLogin"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
    }
}
}




- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)forgetPasswordaction:(id)sender
{
    RForgetPasswordViewController *ForgetPasswordView = [[RForgetPasswordViewController alloc]initWithNibName:@"RForgetPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:ForgetPasswordView animated:NO];
}
@end
