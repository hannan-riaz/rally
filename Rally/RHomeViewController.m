//
//  RHomeViewController.m
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RHomeViewController.h"
#import "RLeaderBoardViewController.h"
#import "RMapViewController.h"
#import "RProfileViewController.h"
#import "RQrCodeViewController.h"
#import "RHubViewController.h"
#import "RCalenderViewController.h"
#import "RStartRallyViewController.h"
#import "REditProfileViewController.h"
#import "RChatViewController.h"
#import "CDCircleOverlayView.h"
#import "RSearchViewController.h"
#import "RNotificationViewController.h"
#import "RPostImageViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RCahtListViewController.h"
#import "MBProgressHUD.h"
#import "RAlbumViewController.h"
#import "THLabel.h"


#define METERS_PER_MILE 1609.344
#define kShadowColor1		[UIColor blackColor]
#define kShadowColor2		[UIColor colorWithWhite:0.0 alpha:0.75]
#define kShadowOffset1		CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)
#define kShadowOffset2		CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 2.0 : 1.0)
#define kShadowBlur1		(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 10.0 : 5.0)
#define kShadowBlur2		(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)

#define kStrokeColor		[UIColor whiteColor]
#define kStrokeSize			(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 1.0 : 1.1)

#define kGradientStartColor	[UIColor colorWithRed:255.0 / 255.0 green:193.0 / 255.0 blue:127.0 / 255.0 alpha:1.0]
#define kGradientEndColor	[UIColor colorWithRed:255.0 / 255.0 green:163.0 / 255.0 blue:64.0 / 255.0 alpha:1.0]



@interface RHomeViewController ()
{
	BOOL animating;
    BOOL searchAddress;
    BOOL clickCameratoPostImg;
    int count;
    BOOL camerahide;
    NSTimer *myTimer;
    CDCircle *circle;
    int tap ;
    NSArray *imageNames ;
    NSMutableArray *images;
    UIView *  loadingView;
     UIActivityIndicatorView *activityView;
    UILabel * loadingLabel;
    int movesegment;
    UIImageView *arrow;
    UIImageView *backgroundImg1;
    NSString *fullPath;
    MBProgressHUD *ProgressHUD;
    NSMutableArray * joinRalliesEventArr;
    NSMutableArray * lastRalliesEventArr;
    NSString *rallyIdStr;
    NSDictionary *Dict1 ;
    CLLocation *coord1;
    CLLocation *coord2;
    UIView * splashView;
    float DistanceInMeter ;
    UIImageView *MyImage;
    AsyncImageView * userImagView;
    NSString * rallyid_Str1;
    UITableViewCell *cell;
    int rbtnClick;
    NSString * map_latitudeSearchAddress;
    NSString * map_longitudeSearchAddress;
    NSString * searchAddressStr;
    NSString *loactionSearchAddressStr;
    NSString * searchBarName;
    NSString *searchString;
    NSDictionary *dict;
    NSMutableArray *businessArr;
    NSDictionary * businesdic;
    NSMutableArray * currentCategory;
    NSString * nameSrr;
    NSString * latitudeSrr;
    NSString * longitudeSrr;
    NSString * titleSrr;
    NSString * map_latitudeSrr;
    NSString *map_longitudeSrr;
    float latitude;
    NSString * adressGetFromServer;
    float longitude;
    UIView * infoview;
    UIView * pickerView;
    UIDatePicker *datePicker;
    UIButton *nextButton;
    UIView * timepickerView;
    UIDatePicker *timePicker;
    UIButton *nextaddressBtn;
    UIView *addressView;
    UITextField *nameTxt;
    UITextView *addressTxt;
    UIButton *publicRadioBtn;
    UIButton * selectAllBtn;
    UILabel * publicLbl;
    UILabel * privateLbl;
    UIButton *privateRadioBtn;
    UIButton *okBtn;
    NSString *name_str;
    NSString *date_str;
    NSString *address_str;
    int checkedIndex;
    NSString *title_str;
    NSString *time_str;
    NSString * JoinRallyIDStr;
    NSDictionary * userFriendDic;
    BOOL public;
    int private;
    NSString * idStr;
    NSArray *idArray;
    UIButton * closeBtn ;
    NSString *  annTitle;
    NSString *loactionAddressStr;
    UIView * inviteView;
    int zoomSet;
    NSMutableArray * packArr;
    NSMutableArray *selectedRows;
    NSMutableArray * arr ;
    NSInteger indexrows;
    BOOL selectBox;
    NSTimer * timmerRepeat1;
    CDCircleOverlayView *overlay ;
}
@property (nonatomic, assign) int group;
@end

@implementation RHomeViewController
@synthesize hubBtn,calenderBtn,mapBtn,valueLabel,tagLbl,postView,cameraImg,clickCamera,grayView,rallymapView,rallyLogoBtn,logoImageView,locationManager,Geocoder,addressSearchBar,inviteTableview,calenderViewBool,calenerDateStr,annotation1,JoinRallyViewController,removeWheelBtn,checkBtn,cropImage,cancelBtn,saveBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    tap = 0;
    selectBox =  NO;
    searchAddress = NO;
    grayView.backgroundColor = [UIColor lightGrayColor];
    grayView.alpha = 0;
     clickCameratoPostImg = NO;
    postView.hidden= YES;
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        self.logoImageView.frame = CGRectMake(38,155,244, 89);
        tagLbl.frame =CGRectMake(23,  self.logoImageView.frame.origin.y+ self.logoImageView.frame.size.height+3, 277, 76);
    }
    else
    {
        self.logoImageView.frame = CGRectMake(38,205,244, 89);
        tagLbl.frame =CGRectMake(23,  self.logoImageView.frame.origin.y+ self.logoImageView.frame.size.height+3, 277, 76);
    }
    
    
    lastRalliesEventArr = [[NSMutableArray alloc]init];
    
    joinRalliesEventArr = [[NSMutableArray alloc]init];
    /* CAGradientLayer *gradient = [CAGradientLayer layer];
     gradient.frame = self.view.bounds;
     gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0] CGColor], (id)[[UIColor colorWithRed:16.0f/255.0f green:167.0f/255.0f blue:45.0f/255.0f alpha:1.0] CGColor], nil];
     [self.view.layer insertSublayer:gradient atIndex:0];*/
    logoImageView.textColor =  [UIColor whiteColor];
    
    tagLbl.frame = CGRectMake(tagLbl.frame.origin.x,self.logoImageView.frame.origin.y+self.logoImageView.frame.size.height-30 , tagLbl.frame.size.width, tagLbl.frame.size.height);
   // UIImage *faceImage = [UIImage imageNamed:@"graySearch-1.png"];
    
    UIImage *faceImage = [UIImage imageNamed:@"cameragray1.png"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake(5, 0, 30, 24 );
    [face addTarget:self action:@selector(SearchAction1:) forControlEvents:UIControlEventTouchUpInside];
    [face setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
    self.navigationItem.rightBarButtonItem = backButton;
    if (height== 568)
    {
        tagLbl.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f];
        
    }
    else
    {
        
        tagLbl.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f];
    }
    
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    selectedRows = [[NSMutableArray alloc]init];
    
    businessArr                 = [[NSMutableArray alloc]init];
    packArr                     = [[NSMutableArray alloc]init];
    dict                        = [[NSDictionary alloc]init];
    arr                         = [[NSMutableArray alloc]init];
    userFriendDic               = [[NSDictionary alloc]init];
    businesdic                  = [[NSDictionary alloc]init];
    currentCategory             = [[NSMutableArray alloc]init];
    rallymapView.delegate       = self;
    rallymapView.userInteractionEnabled = YES;
    Geocoder = [[CLGeocoder alloc] init];
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5;
    [rallymapView addGestureRecognizer:lpgr];
    
    UIImage *chatImage = [UIImage imageNamed:@"chat_icon_grey-1.png"];
    UIButton *chatBttn = [UIButton buttonWithType:UIButtonTypeCustom];
    chatBttn.bounds = CGRectMake( 4, 0,35, 30 );
    [chatBttn addTarget:self action:@selector(ChatAction:) forControlEvents:UIControlEventTouchUpInside];
    [chatBttn setImage:chatImage forState:UIControlStateNormal];
    UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithCustomView:chatBttn];
    self.navigationItem.leftBarButtonItem = chatButton;
    rallymapView.delegate= self;
    rallymapView.showsUserLocation=YES;
    
   
    // CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    //NSLog(@"screen size is %f",height);
    // Do any additional setup after loading the view from its nib.
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self callWebsercice_rally];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
   // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fB_succesfully_lg"];
    [rallymapView removeAnnotations:rallymapView.annotations];
    
    cancelBtn.backgroundColor  =[UIColor colorWithRed:103.0f/255.0f green:215.0f/255.0f blue:103.0f/255.0f alpha:1.0];
    
    saveBtn.backgroundColor  =[UIColor colorWithRed:103.0f/255.0f green:215.0f/255.0f blue:103.0f/255.0f alpha:1.0];
   
    logoImageView.hidden = YES;
    
    tagLbl.hidden = YES;
    
    searchAddress = NO;
    // clickCamera = NO;
   
    zoomSet = 0;
    
    count = [[NSUserDefaults standardUserDefaults]integerForKey:@"countKey"];
    
    [self callservice];
    
    [self updateLabel];
    [self deselectButtons];
    [self callWebsercice_rally_latestrallylist];
    // grayView.backgroundColor = [UIColor clearColor];
    
    
    rallyIdStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults ]objectForKey:@"rallyIDStr"]];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 30)];
    UIImage *image1 = [UIImage imageNamed:@"rally1Logo.png"];
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:image1];
    myImageView.frame = CGRectMake(55,-2,100,35);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:myImageView];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Heavy" size:30.0],NSFontAttributeName , [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] ,NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.hidden = NO;
    testView.hidden = YES;
    arrow.hidden = YES;
    circle.hidden = YES;
    backgroundImg1.hidden = YES;
    valueLabel.hidden = YES;
    removeWheelBtn.hidden = YES;
    tagLbl.textColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    
    [rallymapView becomeFirstResponder];
    rallymapView.userInteractionEnabled = YES;
    
    
    
    UIImage *serchImage = [UIImage imageNamed:@"greenSearch.png"];
    [addressSearchBar setImage:serchImage forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
    
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    nextButton.hidden = YES;
    timepickerView.hidden = YES;
    timePicker.hidden = YES;
    nextaddressBtn.hidden = YES;
    addressView.hidden = YES;
    inviteView.hidden = YES;
    grayView.hidden = YES;
    timmerRepeat1 =  [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(timerCallback1) userInfo:nil repeats:YES];
   NSRunLoop *runner = [NSRunLoop currentRunLoop];
   [runner addTimer:timmerRepeat1 forMode: NSDefaultRunLoopMode];
    
    
}
- (void)DisplayDatePickerView
{
    [addressSearchBar resignFirstResponder];
    [arr removeAllObjects];
    [selectedRows removeAllObjects];
    
    pickerView = [[UIView alloc]init];
    pickerView.layer.cornerRadius = 5.0f;
    pickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    pickerView.layer.borderWidth = 1.0f;
    
    pickerView.backgroundColor = [UIColor whiteColor];
    rallymapView.userInteractionEnabled = NO;
    datePicker = [[UIDatePicker alloc] init];
    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    if (calenderViewBool == YES)
    {
        if (calenerDateStr != nil)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd LLLL yyyy"];
            NSDate *date1 = [dateFormat dateFromString:calenerDateStr];
            [datePicker setDate:date1 animated:NO];
        }
    }
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextButton addTarget:self  action:@selector(nextTimePickerAction:)forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    nextButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.layer.cornerRadius = 5.0f;
    UILabel * DateLabel = [[UILabel alloc]init];
    DateLabel.text = @"Pick a Date";
    DateLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    
    [rallymapView setExclusiveTouch:NO];
    [pickerView addSubview:datePicker];
    [self.view addSubview:pickerView];
    [self.view bringSubviewToFront:pickerView];
    pickerView.hidden = NO;
    [datePicker becomeFirstResponder];
    pickerView.userInteractionEnabled = YES;
    [pickerView setExclusiveTouch:YES];
    [datePicker setExclusiveTouch:YES];
    [pickerView addSubview:closeBtn];
    
    [pickerView addSubview:DateLabel];
    
    [pickerView addSubview:nextButton];
    
}
-(void)callWebservice_googleGecode
{
    
    
}
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    
    [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    searchAddress = NO;
    [addressSearchBar resignFirstResponder];
    [arr removeAllObjects];
    [selectedRows removeAllObjects];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:rallymapView];
    
    CLLocationCoordinate2D touchMapCoordinate = [rallymapView convertPoint:touchPoint toCoordinateFromView:rallymapView];
    
    map_latitudeSrr = [NSString stringWithFormat:@"%f",touchMapCoordinate.latitude];
    map_longitudeSrr = [NSString stringWithFormat:@"%f",touchMapCoordinate.longitude];
    
   
       NSString *urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getaddress.php?lat=%@&lng=%@",map_latitudeSrr,map_longitudeSrr];
       NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
      AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
   
       operation.responseSerializer = [AFJSONResponseSerializer serializer];
   
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    
           [self JSONRecieved_GetGoogleAddress:responseObject];
   
       }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
   
                                            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                             ProgressHUD = nil;
                                            kRAlert(@"Whoops", error.localizedDescription);
                                         }];
    
        [[NSOperationQueue mainQueue] addOperation:operation];
    CLLocation *currentLocation = [[CLLocation alloc]
                                   initWithLatitude:touchMapCoordinate.latitude
                                   longitude:touchMapCoordinate.longitude];
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([currentLocation coordinate], 1500, 1500);
    //[self.mapView setRegion:region animated:YES];
    if (!Geocoder)
        Geocoder = [[CLGeocoder alloc] init];
    
    [Geocoder reverseGeocodeLocation:currentLocation completionHandler:
     ^(NSArray* placemarks, NSError* error)
     {
         if ([placemarks count] > 0)
         {
             CLPlacemark *topResult = [placemarks objectAtIndex:0];
             annTitle = [NSString stringWithFormat:@"%@",[topResult.addressDictionary objectForKey:@"FormattedAddressLines" ]];
             NSCharacterSet *Character = [NSCharacterSet characterSetWithCharactersInString:@"(\"\")"];
             annTitle = [[annTitle componentsSeparatedByCharactersInSet:Character] componentsJoinedByString:@""];
             
             NSString *squashed = [annTitle stringByReplacingOccurrencesOfString:@"[ ]+\\  "
                                                                      withString:@" "
                                                                         options:NSRegularExpressionSearch
                                                                           range:NSMakeRange(0, annTitle.length)];
             
             
             
             loactionAddressStr = [squashed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
             
         }
     }];
    
    
    pickerView = [[UIView alloc]init];
    pickerView.layer.cornerRadius = 5.0f;
    pickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    pickerView.layer.borderWidth = 1.0f;
    
    pickerView.backgroundColor = [UIColor whiteColor];
    rallymapView.userInteractionEnabled = NO;
    datePicker = [[UIDatePicker alloc] init];
    [datePicker addTarget:self action:@selector(datepickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    if (calenderViewBool == YES)
    {
        if (calenerDateStr != nil)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd LLLL yyyy"];
            NSDate *date1 = [dateFormat dateFromString:calenerDateStr];
            [datePicker setDate:date1 animated:NO];
        }
    }
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextButton addTarget:self  action:@selector(nextTimePickerAction:)forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    nextButton.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.layer.cornerRadius = 5.0f;
    UILabel * DateLabel = [[UILabel alloc]init];
    DateLabel.text = @"Pick a Date";
    DateLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else
    {
        pickerView.frame = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        datePicker.frame = CGRectMake(0,35,pickerView.frame.size.width,pickerView.frame.size.height);
        closeBtn.frame   = CGRectMake(5, 5, 24, 24);
        nextButton.frame = CGRectMake(100, 250, 80, 30);
        DateLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    
    [rallymapView setExclusiveTouch:NO];
    [pickerView addSubview:datePicker];
    [self.view addSubview:pickerView];
    [self.view bringSubviewToFront:pickerView];
    pickerView.hidden = NO;
    [datePicker becomeFirstResponder];
    pickerView.userInteractionEnabled = YES;
    [pickerView setExclusiveTouch:YES];
    [datePicker setExclusiveTouch:YES];
    [pickerView addSubview:closeBtn];
    
    [pickerView addSubview:DateLabel];
    
    [pickerView addSubview:nextButton];
    
}
-(void)JSONRecieved_GetGoogleAddress:(NSDictionary *)response
{
    NSDictionary *Dict = response;
    //NSLog(@"response12%@",response);
    if (![[Dict objectForKey:@"address"] isEqual:(id)[NSNull null]]) {
  
    adressGetFromServer = [NSString stringWithFormat:@"%@",[Dict objectForKey:@"address"]];
    }
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case  APIResponseStatus_address:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }
        case  APIResponseStatus_AddressNotFound:
        {
            adressGetFromServer = @"";
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
       
        default:
            
            break;
            
            
    }
}

-(void)CloseAction:(UIButton *)sender
{
    [rallymapView becomeFirstResponder];
    rallymapView.userInteractionEnabled = YES;
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
    [addressView removeFromSuperview];
    [inviteView removeFromSuperview];
}

-(void)nextTimePickerAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    
    timepickerView = [[UIView alloc]init];
    timepickerView.layer.cornerRadius = 5.0f;
    timepickerView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    timepickerView.layer.borderWidth = 1.0f;
    timepickerView.backgroundColor = [UIColor whiteColor];
    timepickerView.userInteractionEnabled = NO;
    [rallymapView setExclusiveTouch:NO];
    
    timePicker = [[UIDatePicker alloc] init];
    
    [timePicker addTarget:self action:@selector(timepickerChanged:) forControlEvents:UIControlEventValueChanged];
    timePicker.datePickerMode = UIDatePickerModeTime;
    timepickerView.userInteractionEnabled = YES;
    [timepickerView setExclusiveTouch:YES];
    [timePicker setExclusiveTouch:YES];
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"HH:mm:ss"];
    time_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:timePicker.date]];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    closeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nextaddressBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextaddressBtn addTarget:self  action:@selector(nextAddressAction:)  forControlEvents:UIControlEventTouchUpInside];
    [nextaddressBtn setTitle:@"NEXT" forState:UIControlStateNormal];
    nextaddressBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [nextaddressBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextaddressBtn.layer.cornerRadius = 5.0f;
    
    UILabel * TimeLabel = [[UILabel alloc]init];
    TimeLabel.text = @"Pick a Time";
    TimeLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);
        
    }
    else
    {
        
        timepickerView.frame    =  CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        timePicker.frame        = CGRectMake(0,35,timepickerView.frame.size.width,timepickerView.frame.size.height);
        closeBtn.frame          = CGRectMake(5, 5, 24, 24);
        nextaddressBtn.frame    = CGRectMake(100, 250, 80, 30);
        TimeLabel.frame  = CGRectMake(93, 5, 100, 30);
        
    }
    
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    [timepickerView addSubview:timePicker];
    [self.view addSubview:timepickerView];
    [self.view bringSubviewToFront:timepickerView];
    [timepickerView addSubview:closeBtn];
    [timepickerView addSubview:nextaddressBtn];
    [timepickerView addSubview:TimeLabel];
    
}

-(void)nextAddressAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
    
    addressView = [[UIView alloc]init];
    addressView.layer.cornerRadius = 5.0f;
    addressView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
    addressView.layer.borderWidth = 1.0f;
    addressView.backgroundColor = [UIColor whiteColor];
    addressView.userInteractionEnabled = NO;
    [rallymapView setExclusiveTouch:NO];
    
    nameTxt = [[UITextField alloc] init];
    nameTxt.borderStyle = UITextBorderStyleNone;
    nameTxt.font = [UIFont systemFontOfSize:15];
    nameTxt.placeholder = @"  Name";
    nameTxt.autocorrectionType = UITextAutocapitalizationTypeWords;
    nameTxt.keyboardType = UIKeyboardTypeDefault;
    nameTxt.returnKeyType = UIReturnKeyDefault;
    nameTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    nameTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    nameTxt.delegate = self;
    nameTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    nameTxt.layer.borderWidth= 1.3f;
    
    addressTxt = [[UITextView alloc] init];
    addressTxt.font = [UIFont systemFontOfSize:14];
    // loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    adressGetFromServer = [adressGetFromServer stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    adressGetFromServer = [adressGetFromServer stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    adressGetFromServer = [adressGetFromServer stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    // loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    /*  NSString *input = @"How now brown cow";
     for (NSUInteger i = 0; i < [input length]; i++) {
     unichar c = [input characterAtIndex:i];
  //   NSLog(@"%04x", (unsigned)c);
     // or NSString *s = [NSString stringWithFormat:@"%04x", (unsigned)c];
     }
     
     NSString *input = @"How now brown cow";
     for (NSUInteger i = 0; i < [input length]; i++) {
     unichar c = [input characterAtIndex:i];
     //NSLog(@"%04x", (unsigned)c);
     // or NSString *s = [NSString stringWithFormat:@"%04x", (unsigned)c];
     }
    
     */
    NSMutableString *Mutastring1 = [NSMutableString string];
    NSArray *elements = [adressGetFromServer componentsSeparatedByString:@","];
    
    for (int i= 0;i<elements.count; i++) {
        
        if (i == elements.count-3)
        {
            [Mutastring1 appendString:[elements objectAtIndex:i]];
            [ Mutastring1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [Mutastring1 appendString:@","];
            
            
        }
        
        else
        {
            [Mutastring1 appendString:[elements objectAtIndex:i]];
            
            //[Mutastring1 appendString:@" "];
        }
    }
    
    
    
    
    //if (goodValue == nil)
    // {
    
    
    if (searchAddress == YES)
    {
        loactionAddressStr = [loactionAddressStr stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        loactionAddressStr = [loactionAddressStr stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        
        
        NSMutableString *Mutastring1 = [NSMutableString string];
        NSArray *elements = [loactionSearchAddressStr componentsSeparatedByString:@","];
        
        for (int i= 0;i<elements.count; i++) {
            if (i == elements.count-3)
            {
                [Mutastring1 appendString:[elements objectAtIndex:i]];
                [Mutastring1 appendString:@","];
                
                
            }
            else
            {
                [Mutastring1 appendString:[elements objectAtIndex:i]];
                
            }
        }
        NSData *data = [Mutastring1 dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        
        if (goodValue == nil || [goodValue isEqualToString:@""])
        {
            addressTxt.text =Mutastring1;
        }
        else
        {
            addressTxt.text =goodValue;
            //                          addressTxt.text =@"";
        }
        
    }
    else
    {
        if (adressGetFromServer)
        {//Mutastring1 = @"\\U0397\\U03bd\\U03c9\\U03bc\\U03ad\\U03bd\\U03b5\\U03c2\\U03a0\\U03bf\\U03bb\\U03b9\\U03c4\\U03b5\\U03af";
            // Mutastring1 = @"\\U037";
            
            NSData *data = [Mutastring1 dataUsingEncoding:NSUTF8StringEncoding];
            NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            
            if (goodValue == nil||[goodValue isEqualToString:@""])
            {
                addressTxt.text = Mutastring1;
            }
            else
            {
                addressTxt.text =goodValue;
                //  addressTxt.text =@"";
            }
            
            
            
        }
    }
    
  
    // {
    //    addressTxt.text =goodValue;
    // }
    addressTxt.autocorrectionType = UITextAutocapitalizationTypeWords;
    addressTxt.keyboardType = UIKeyboardTypeASCIICapable;
    addressTxt.returnKeyType = UIReturnKeyDefault;
    addressTxt.textContainerInset = UIEdgeInsetsZero;
    addressTxt.textContainer.lineFragmentPadding =10;
    addressTxt.delegate = self;
    addressTxt.layer.borderColor = [[UIColor colorWithRed:224.0/255.f green:224.0/255.f blue:224.0/255.f alpha:1.0]CGColor];
    addressTxt.layer.borderWidth= 1.3f;
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
    [closeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [addressView addSubview:closeBtn];
    addressView.userInteractionEnabled = YES;
    [addressView setExclusiveTouch:YES];
    
    publicRadioBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [publicRadioBtn addTarget:self  action:@selector(publicRadioBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [publicRadioBtn setImage:[UIImage imageNamed:@"radio-button_off1.png"]  forState:UIControlStateNormal];
    //[publicRadioBtn setTitle:@"Ok" forState:UIControlStateNormal];
    publicRadioBtn.tag=0;
    publicLbl = [[UILabel alloc]init ];
    publicLbl.text = @"Public";
    
    publicLbl.userInteractionEnabled = YES;
    publicLbl.font = [UIFont fontWithName:@"AvenirNext" size:16.0f];
    privateLbl = [[UILabel alloc]init];
    privateLbl.text =@"Private";
    
    privateLbl.userInteractionEnabled = YES;
    privateLbl.font = [UIFont fontWithName:@"AvenirNext" size:16.0f];
    privateRadioBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [privateRadioBtn addTarget:self  action:@selector(publicRadioBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    
    privateRadioBtn.tag=1;
    [privateRadioBtn setImage:[UIImage imageNamed:@"radio-button_off1.png"]  forState:UIControlStateNormal];
    
    okBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [okBtn addTarget:self  action:@selector( okAction:)  forControlEvents:UIControlEventTouchUpInside];
    [okBtn setTitle:@"Next" forState:UIControlStateNormal];
    okBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    okBtn.layer.cornerRadius = 5.0f;
    
    [publicRadioBtn  setImage:[UIImage imageNamed:@"radio-button_on1.png"]  forState:UIControlStateSelected];
    [privateRadioBtn  setImage:[UIImage imageNamed:@"radio-button_on1.png"]  forState:UIControlStateSelected];
    [publicRadioBtn setSelected:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);
        
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);
        
    }
    else
    {
        addressView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,300);
        nameTxt.frame          = CGRectMake(10,32,addressView.frame.size.width-20,40);
        addressTxt.frame       = CGRectMake(10,120,addressView.frame.size.width-20,70);
        closeBtn.frame         = CGRectMake(5, 5, 24, 24);
        publicRadioBtn.frame   = CGRectMake(30, 85, 20, 20);
        privateRadioBtn.frame  = CGRectMake(140, 85, 20, 20);
        okBtn.frame            = CGRectMake(100, 240, 80, 30);
        publicLbl.frame        = CGRectMake(70, 70, 80, 49);
        privateLbl.frame       = CGRectMake(180, 70, 80, 49);
        
    }
    
    closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
    closeBtn.clipsToBounds =YES;
    
    [addressView addSubview:nameTxt];
    [addressView addSubview:addressTxt];
    [self.view addSubview:addressView];
    [self.view bringSubviewToFront:addressView];
    [addressView addSubview:publicRadioBtn];
    [addressView addSubview:publicLbl];
    [addressView addSubview:privateLbl];
    [addressView addSubview:privateRadioBtn];
    [addressView addSubview:okBtn];
    [addressView addSubview:closeBtn];
    
}


-(void)publicRadioBtnAction:(id)sender
{
    if ([sender tag] == 0)
    {
        
        [publicRadioBtn setSelected:YES];
        [privateRadioBtn setSelected:NO];
        
    }
    else
    {
        
        [publicRadioBtn setSelected:NO];
        [privateRadioBtn setSelected:YES];
        
    }
    
    
}

-(void)okAction:(UIButton *)sender
{
    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([t1 length]>25)
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (nameTxt.text.length == 0  )
    {
        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Please enter a name for the Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        [self callWebService_viewuserpacks];
        [pickerView removeFromSuperview];
        [timepickerView removeFromSuperview];
        [addressView removeFromSuperview];
        
        
        [nameTxt resignFirstResponder];
        [addressTxt resignFirstResponder];
        
        
        inviteTableview = [[UITableView alloc]init ];
        [inviteTableview setSeparatorInset:UIEdgeInsetsZero];
        inviteTableview.allowsMultipleSelection = YES;
        
        inviteView = [[UIView alloc]init];
        inviteView.layer.cornerRadius = 5.0f;
        inviteView.layer.borderColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0].CGColor;
        inviteView.layer.borderWidth = 1.0f;
        inviteView.backgroundColor = [UIColor whiteColor];
        inviteView.userInteractionEnabled = YES;
        [rallymapView setExclusiveTouch:NO];
        
        UILabel * InviteUserLabel = [[UILabel alloc]init];
        InviteUserLabel.text = @"Invite Users";
        InviteUserLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [closeBtn addTarget:self  action:@selector(CloseAction:)  forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setTitle:@"X" forState:UIControlStateNormal];
        closeBtn.titleEdgeInsets=UIEdgeInsetsMake(0, 1, 0, 0);
        [closeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
        closeBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [okBtn addTarget:self  action:@selector(goToNextViewAction:)  forControlEvents:UIControlEventTouchUpInside];
        [okBtn setTitle:@"OK" forState:UIControlStateNormal];
        okBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okBtn.layer.cornerRadius = 5.0f;
        
        selectAllBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [selectAllBtn addTarget:self  action:@selector(selectAllUserAction:)  forControlEvents:UIControlEventTouchUpInside];
        [selectAllBtn setTitle:@"Select All" forState:UIControlStateNormal];
        selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectAllBtn.layer.cornerRadius = 5.0f;
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-100);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
            
        }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-100);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
        }
        else
        {
            inviteView.frame      = CGRectMake(20,self.navigationController.navigationBar.frame.origin.y+44,280,320);
            closeBtn.frame         = CGRectMake(5, 5, 24, 24);
            okBtn.frame            = CGRectMake(160, 270, 80, 30);
            selectAllBtn.frame     = CGRectMake(40, 270, 80, 30);
            inviteTableview.frame  = CGRectMake(0,32,inviteView.frame.size.width,inviteView.frame.size.height-100);
            InviteUserLabel.frame  = CGRectMake(93, 5, 100, 30);
            
        }
        
        
        closeBtn.layer.cornerRadius = closeBtn.bounds.size.width/2;
        closeBtn.clipsToBounds =YES;
        
        inviteTableview.dataSource = self;
        inviteTableview.delegate = self;
        [inviteView addSubview:inviteTableview];
        [inviteView addSubview:closeBtn];
        [inviteView addSubview:okBtn];
        [inviteView addSubview:selectAllBtn];
        [inviteView addSubview:InviteUserLabel];
        
        
        [self.view addSubview:inviteView];
        [self.view bringSubviewToFront:inviteView];
        if (privateRadioBtn.selected== YES)
        {
            private= 1;
            [privateRadioBtn setSelected:NO];
            
        }
        else
            
        {
            [privateRadioBtn setSelected:YES];
            private = 0;
        }
        
        
        /* NSDateFormatter *formate=[[NSDateFormatter alloc]init];
         [formate setDateFormat:@"MM-dd-yyyy"];
         //  NSLog(@"value: %@",[formate stringFromDate:datePicker.date]);
         NSString *datepickerDate=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
         NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
         [formate1 setDateFormat:@"HH:mm:ss"];
         // NSLog(@"value: %@",[formate stringFromDate:timePicker.date]);
         NSString *timepickerDate=[NSString stringWithFormat:@"%@",[formate1 stringFromDate:timePicker.date]];*/
        
    }
}

-(void)goToNextViewAction:(UIButton *)sender
{
    [pickerView removeFromSuperview];
    [timepickerView removeFromSuperview];
    [addressView removeFromSuperview];
    NSString * private_Value = [NSString stringWithFormat:@"%d",private];
    RStartRallyViewController *StartRallyViewController = [[RStartRallyViewController alloc]initWithNibName:@"RStartRallyViewController" bundle:nil];
    name_str=[NSString stringWithFormat:@"%@",nameTxt.text];
    address_str =[NSString stringWithFormat:@"%@",addressTxt.text];
    
    if (searchAddress == YES)
    {
        StartRallyViewController.latitudeStr=map_latitudeSearchAddress;
        StartRallyViewController.longitudeStr=map_longitudeSearchAddress;
    }
    else
    {
        StartRallyViewController.latitudeStr=map_latitudeSrr;
        StartRallyViewController.longitudeStr=map_longitudeSrr;
    }
    
    
    StartRallyViewController.dateStr= date_str;
    StartRallyViewController.timeStr= time_str;
    StartRallyViewController.tileStr= title_str;
    StartRallyViewController.addressStr= address_str;
    StartRallyViewController.nameStr= name_str;
    StartRallyViewController.privateValue=private_Value;
    StartRallyViewController.inviteUserArr = arr;
    [self.navigationController pushViewController:StartRallyViewController animated:NO];
    
}


-(void)callWebService_viewuserpacks
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict1 = response;
    //    NSLog(@"response12%@",response);
    if ([dict1 objectForKey:@"userfriends"] == [NSNull null])
    {
        
    }
    else
    {
        packArr = [[dict1 objectForKey:@"userfriends"]mutableCopy];
        [inviteTableview reloadData];
    }
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ViewUserInPackSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            //            break;
        }
        default:
            
            break;
            
    }
    
}

-(void)callWebService_rallyinvitation
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * arrstr = [NSString stringWithFormat:@"%@",arr];
    
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
    arrstr = [arrstr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyinvitation.php?userid=%@&friendids=%@&rallyid=",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],arrstr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_rallyinvitation:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_rallyinvitation:(NSDictionary *)response
{
    NSDictionary *dict1 = response;
    //    NSLog(@"response12%@",response);
    if ([dict1 objectForKey:@"userfriends"] == [NSNull null])
    {
        
    }
    else
    {
        packArr = [[dict1 objectForKey:@"userfriends"]mutableCopy];
        [inviteTableview reloadData];
    }
    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ViewUserInPackSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            //            break;
        }
        default:
            
            break;
            
    }
    
}

- (void)datepickerChanged:(id)sender
{
    
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"MM-dd-yyyy"];
    
    date_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker.date]];
}

- (void)timepickerChanged:(id)sender
{
    NSDateFormatter *formate=[[NSDateFormatter alloc]init];
    [formate setDateFormat:@"HH:mm:ss"];
    time_str=[NSString stringWithFormat:@"%@",[formate stringFromDate:timePicker.date]];
}
-(void)callservice
{
    NSString *urlStr1= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallymap.php"];
    NSURL *url = [NSURL URLWithString:urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecievedData:responseObject];
         //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         //ProgressHUD = nil;
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         kRAlert(@"Error", error.localizedDescription);
     }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}


-(void)JSONRecievedData:(NSDictionary *)response
{
    [businessArr removeAllObjects];
    [rallymapView removeAnnotations:rallymapView.annotations];
    dict = response;
    //    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"rallies"]!= [NSNull null])
    {
        businessArr = [[dict objectForKey:@"rallies"]mutableCopy] ;
    }
    
    for ( int i= 0; i<[businessArr count];i++)
    {
        businesdic  = [businessArr objectAtIndex:i];
        annotation1 = [[MKPointAnnotation alloc] init];
        annotation1.coordinate = CLLocationCoordinate2DMake([businesdic[@"rallyLATITUDE"] doubleValue],
                                                            [businesdic[@"rallyLONGITUDE"] doubleValue]);
        
        nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
        nameSrr = [nameSrr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        //  _mapView.userLocation.title = nameSrr;
        if (rallynameStr != nil) {
            // annotation1.title = @" ";
            annotation1.title = rallynameStr;
            
            
        }
        else{
            
            // annotation1.title = @" ";
            annotation1.title = nameSrr;
            
            
        }
        
        //        if ( ([businesdic[@"rallyLATITUDE"] doubleValue] >= -90)     && ([businesdic[@"rallyLATITUDE"] doubleValue] <= 90)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue]>= -180)     && ([businesdic[@"rallyLONGITUDE"] doubleValue]<= 180))
        //        {
        //
        
        [rallymapView addAnnotation:annotation1];
        //        }
    }
    
    
}

#pragma mark Delegate Methods

/*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
 {
 if (searchAddress == NO)
 {
 
 if([annotation isKindOfClass:[MKUserLocation class]])
 {
 return nil;
 }
 
 if ([annotation isKindOfClass:[MKPointAnnotation class]])
 {
 static NSString *AnnotationViewID = @"annotationViewID";
 
 MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
 
 if (annotationView == nil)
 {
 annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
 annotationView.image = [UIImage imageNamed:@"rlogo12.png"];
 annotationView.canShowCallout = YES;
 annotationView.enabled =YES;
 annotationView.tag = -101;
 UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
 annotationView.rightCalloutAccessoryView = detailButton;
 }
 else
 {
 annotationView.annotation = annotation;
 }
 return annotationView;
 }
 }
 else
 
 
 {
 if ([annotation isKindOfClass:[MKPointAnnotation class]])
 {
 MKPinAnnotationView*    pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
 
 if (!pinView)
 {
 pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
 reuseIdentifier:@"CustomPinAnnotationView"];
 if([[pinView.annotation title] isEqualToString:searchBarName])
 {
 pinView.pinColor = MKPinAnnotationColorGreen;
 pinView.tag = -10;
 pinView.canShowCallout = YES;
 
 }
 else
 {
 pinView.image = [UIImage imageNamed:@"rlogo1.png"];
 
 }
 //searchAddress = NO;
 
 }
 else
 {
 pinView.annotation = annotation;
 }
 return pinView;
 }
 }
 return nil;
 
 }
 */

//- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
//    if (self)
//    {
//        // Set the frame size to the appropriate values.
//        CGRect  myFrame = self.frame;
//        myFrame.size.width = 40;
//        myFrame.size.height = 40;
//        self.frame = myFrame;
//
//        // The opaque property is YES by default. Setting it to
//        // NO allows map content to show through any unrendered parts of your view.
//        self.opaque = NO;
//    }
//    return self;
//}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        static NSString *AnnotationViewID = @"annotationViewID";
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:nil];
        
        
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        if([[annotationView.annotation title] isEqualToString:searchBarName])
        {
            annotationView.pinColor = MKPinAnnotationColorGreen;
            annotationView.tag = -10;
            annotationView.canShowCallout = YES;
            
        }
        else
        {
            static NSString *AnnotationViewID = @"annotationViewID";
            
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
            
            if (annotationView == nil)
            {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
                UIImageView *myCustomImage = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 5,5) ]; myCustomImage.image = [UIImage imageNamed:@"r.png"];
                annotationView.image = myCustomImage.image;
                annotationView.enabled = YES;
                annotationView.canShowCallout = YES;
                annotationView.centerOffset = CGPointMake(0,0);
                annotationView.tag = -101;
                UILabel *lblTitolo = [[UILabel alloc] initWithFrame:CGRectMake(0,-40,250,40)];
                lblTitolo.text = [NSString stringWithString:nameSrr];
                lblTitolo.backgroundColor = [UIColor whiteColor];
                lblTitolo.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
                lblTitolo.lineBreakMode = NSLineBreakByWordWrapping;
                lblTitolo.adjustsFontSizeToFitWidth=TRUE;
                lblTitolo.numberOfLines = 0;
                // annotationView.leftCalloutAccessoryView = lblTitolo;
                
                annotationView.opaque = YES;
                UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                annotationView.rightCalloutAccessoryView = detailButton;
            }
            else
            {
                annotationView.annotation = annotation;
            }
            return annotationView;
            
            
            //            annotationView.image = [UIImage imageNamed:@"Rpiece2.png"];
            //            annotationView.canShowCallout = YES;
            //
            //            annotationView.enabled =YES;
            //            annotationView.tag = -101;
            //            UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            //            annotationView.rightCalloutAccessoryView = detailButton;
        }
        
       // annotationView.annotation = annotation;
        
        return annotationView;
    }
    
    
    return nil;
    
}
- (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    return returnValue;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{  //if (searchAddress == NO)
    //{
    if (view.tag ==-101)
    {
        
        id <MKAnnotation> annotation = [view annotation];
        
        if ([annotation isKindOfClass:[MKPointAnnotation class]])
        {
            
            for ( int j= 0; j<[businessArr count];j++)
            {
                businesdic  = [businessArr objectAtIndex:j];
                nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];

                //                NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
//                NSString *rallynameStr1 = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding]; //changed by swati 2 Dec
                nameSrr = [nameSrr  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                NSData *data1 = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
                NSString *rallyDataEncode = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
                NSString * rallyname_Str;
                if (rallyDataEncode == nil  || [rallyDataEncode isEqualToString:@""])
                {
                    rallyname_Str = nameSrr;
                    
                }
                else
                {
                    rallyname_Str = rallyDataEncode;
                }
                
                
                
                if([[annotation title] isEqualToString:rallyname_Str])
                {
                    annotation = [mapView.selectedAnnotations objectAtIndex:0];
                    //CLLocationCoordinate2D coord = annotation.coordinate;
                    
                    if ( [annotation coordinate].latitude == [[businesdic valueForKeyPath:@"rallyLATITUDE"]doubleValue] || [annotation coordinate].longitude  == [[businesdic valueForKeyPath:@"rallyLONGITUDE"]doubleValue])
                    {
                        JoinRallyIDStr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyID"]];
                    }
                }
            }
            
            //}
        }
        JoinRallyViewController = [[RJoinRallyViewController alloc]initWithNibName:@"RJoinRallyViewController" bundle:nil];
        JoinRallyViewController.rallyid= JoinRallyIDStr;
        
        [self.navigationController pushViewController:JoinRallyViewController animated:NO];
        
    }
    else
    {
        searchAddress = YES;
        [self DisplayDatePickerView];
        
    }
    //  NSInteger indexOfTheObject = [mapView.annotations  indexOfObject:view.annotation];
    //  NSLog(@"calloutAccessoryControlTapped: annotation = %d", [mapView.annotations indexOfObject:view.annotation]);
    
    //   businesdic  = [businessArr objectAtIndex:indexOfTheObject];
    
    
    
    /*  latitudeSrr = [NSString stringWithFormat:@"%f",[businesdic[@"rallyLATITUDE"] doubleValue]] ;
     longitudeSrr = [NSString stringWithFormat:@"%f",[businesdic[@"rallyLONGITUDE"] doubleValue]] ;
     nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
     JoinRallyIDStr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyID"]];
     titleSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
     JoinRallyViewController.joinlatitudeStr=latitudeSrr;
     JoinRallyViewController.joinlongitudeStr= longitudeSrr;
     JoinRallyViewController.joinnameStr=nameSrr;
     JoinRallyViewController.jointitleStr= titleSrr;*/
}

/*-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
 [mapView setCenterCoordinate:userLocation.coordinate animated:YES];
 }*/
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    id <MKAnnotation> annotation = [view annotation];
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        
        if (view.tag ==-10)
        {
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            view.rightCalloutAccessoryView = rightButton;
            
        }
        else if(view.tag ==-101)
        {
            
            // view.image = [UIImage imageNamed:@"Rpiece2.png"];
            UIButton *detailButton = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
            view.rightCalloutAccessoryView = detailButton;
            
        }
    }
    // [_mapView deselectAnnotation:view.annotation animated:YES];
    // NSInteger indexOfTheObject = [_mapView.annotations indexOfObject:view.annotation];
    // businesdic  = [businessArr objectAtIndex:indexOfTheObject];
    
    //    nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
    //    annotation1.title = nameSrr;
    //  NSLog(@"%ld",(long)indexOfTheObject);
    //  NSLog(@"%ld",(long)view.tag);
    
    /* infoview = [[UIView alloc]initWithFrame:CGRectMake(0,80, 120, 70)];
     infoview.userInteractionEnabled = YES;
     infoview.backgroundColor = [UIColor whiteColor];
     
     UILabel * infoLbl = [[UILabel alloc]init];
     infoLbl.frame =CGRectMake(10, infoview.frame.origin.x, 70, 49);
     
     infoLbl.userInteractionEnabled = YES;
     infoLbl.font = [UIFont fontWithName:@"AvenirNext-DemiBoldItalic" size:16.0f];
     [infoview addSubview:infoLbl];
     
     UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     infoBtn.frame = CGRectMake(0, infoview.frame.origin.x, infoview.frame.size.width
     ,infoview.frame.size.height);
     [infoBtn addTarget:self action:@selector(gotoJoinRally:) forControlEvents:UIControlEventTouchUpInside];
     [infoview addSubview:infoBtn];
     
     [view addSubview:infoview];
     // [view addSubview:infoview];
     [view bringSubviewToFront:infoview];
     [infoview becomeFirstResponder];
     [_mapView setExclusiveTouch:NO];
     [infoview setExclusiveTouch:YES];
     [infoBtn setExclusiveTouch :YES];
     */
    
    // businesdic  = [businessArr objectAtIndex:view.tag];
    
    //nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
    //  infoLbl.text = nameSrr;
}

//- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
// {
//   [mapView removeAnnotations:[mapView annotations]];
//
// }

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.725;
    span.longitudeDelta =0.725;
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = userLocation.coordinate;
//    region.center=CLLocationCoordinate2DMake(39.676,-74.990);
//    double miles = 50.0;
//    double scalingFactor = ABS( (cos(2 * M_PI * userLocation.coordinate.latitude / 360.0) ));
//    
//    MKCoordinateSpan span;
//    
//    span.latitudeDelta = miles/69.0;
//    span.longitudeDelta = miles/(scalingFactor * 69.0);
//    
//    MKCoordinateRegion region;
//    region.span = span;
//    region.center = userLocation.coordinate;
    
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate,1000,1000);
    coord1 = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
    if (zoomSet == 0)
    {
        [rallymapView setRegion:[rallymapView regionThatFits:region] animated:YES];
        zoomSet = 1;
    }
    
    for ( int i= 0; i<[businessArr count];i++)
    {
        businesdic  = [businessArr objectAtIndex:i];
        annotation1 = [[MKPointAnnotation alloc] init];
        
        
        annotation1.coordinate = CLLocationCoordinate2DMake([businesdic[@"rallyLATITUDE"] doubleValue],
                                                            [businesdic[@"rallyLONGITUDE"] doubleValue]);
        nameSrr = [NSString stringWithFormat:@"%@",[businesdic valueForKeyPath:@"rallyNAME"]];
        //  _mapView.userLocation.title = nameSrr;
        nameSrr = [nameSrr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [nameSrr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallynameStr = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        //  _mapView.userLocation.title = nameSrr;
        
        
        if (rallynameStr != nil) {
            annotation1.title = rallynameStr;
//            if (rallynameStr.length>23) {
//                
//                rallynameStr = [rallynameStr substringWithRange:NSMakeRange(23, rallynameStr.length-23)];
//                annotation1.title = rallynameStr;
//                
//            }
            
        }
        else{
            annotation1.title = nameSrr;
//            if (nameSrr.length>23) {
//                
//                nameSrr = [nameSrr substringWithRange:NSMakeRange(23, nameSrr.length-23)];
//                annotation1.title = nameSrr;
//                
//            }
        }
        
        //        if ( ([businesdic[@"rallyLATITUDE"] doubleValue] >= -90)     && ([businesdic[@"rallyLATITUDE"] doubleValue] <= 90)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] >= -180)     && ( [businesdic[@"rallyLONGITUDE"] doubleValue] <= 180))
        //        {
        //
        [rallymapView addAnnotation:annotation1];
        //        }
    }
}



#pragma mark - ---------- TextFieldDelegate methods ------------

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    nameTxt.leftView = paddingView;
    nameTxt.leftViewMode = UITextFieldViewModeAlways;
    
    CGRect viewFrame = addressView.frame;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y -=10;
            
        }
        else
        {
            viewFrame.origin.y -=40;
        }
        
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y -=10;
            
        }
        else
        {
            viewFrame.origin.y -=40;
        }
        
    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y -=10;
        }
        else
        {
            viewFrame.origin.y -=40;
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    [addressView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    if([t1 length]>25)
    //    {
    //        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    
    //    }
    
    /* CGRect viewFrame = addressView.frame;
     viewFrame.origin.y -= 10;
     
     [UIView beginAnimations:nil context:NULL];
     [UIView setAnimationBeginsFromCurrentState:YES];
     [UIView setAnimationDuration:0.2];
     
     [addressView setFrame:viewFrame];
     
     [UIView commitAnimations];*/
    //[self animateTextView:NO];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    CGRect viewFrame = addressView.frame;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y =80;
            
        }
        else
        {
            viewFrame.origin.y =60;
        }
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y =80;
            
        }
        else
        {
            viewFrame.origin.y =60;
        }
        
    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            viewFrame.origin.y =80;
            
        }
        else
        {
            viewFrame.origin.y = 60;
        }
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    
    [addressView setFrame:viewFrame];
    
    [UIView commitAnimations];
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    if([t1 length]>25)
    //    {
    //        return NO;
    //    }
    if([[textField text] length] - range.length + string.length < 26) //Added by swati from 26 to 21
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        CGRect viewFrame = addressView.frame;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
        {
            
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                viewFrame.origin.y =80;
                
            }
            else
            {
                viewFrame.origin.y =60;
            }
        }
        else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                viewFrame.origin.y =80;
                
            }
            else
            {
                viewFrame.origin.y =60;
            }
            
        }
        else
        {
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            if (height== 568)
            {
                viewFrame.origin.y =80;
            }
            else
            {
                viewFrame.origin.y =60;
            }
        }
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.2];
        [addressView setFrame:viewFrame];
        
        [UIView commitAnimations];
        
        [textView resignFirstResponder];
    }
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    
    [self animateTextView:NO];
}

- (void) animateTextView:(BOOL) up
{
    int movementDistance;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance =20;
            
        }
        else
        {
            movementDistance =50;
        }
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance =20;
            
        }
        else
        {
            movementDistance =50;
        }
        
    }
    
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
            movementDistance =20;
            
        }
        else
        {
            movementDistance =50;
            
        }
        
        
    }
    
    // const int movementDistance =180; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    // NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - ------------ Searchbar Delegate Methods ------------

//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self handleSearch:searchBar];
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self handleSearch:searchBar];
}


//do our search on the remote server using HTTP request
- (void)handleSearch:(UISearchBar *)searchBar
{
    // NSLog(@"User searched for %@", searchBar.text);
    
    [searchBar resignFirstResponder];
    searchBarName = [NSString stringWithFormat:@"%@",searchBar.text];
    Geocoder = [[CLGeocoder alloc] init];
    [Geocoder geocodeAddressString:addressSearchBar.text completionHandler:^(NSArray *placemarks, NSError *error) {
        //Error checking
        if(placemarks.count > 0 )
        {
            
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            
            /* MKCoordinateRegion region1;
             
             if ( (region1.center.latitude >= -90)     && (region1.center.latitude <= 90)     && (region1.center.longitude >= -180)     && (region1.center.longitude <= 180))
             {
             region1.center.latitude = placemark.region.center.latitude;
             region1.center.longitude = placemark.region.center.longitude;
             MKCoordinateSpan span;
             double radius = placemark.region.radius / 1000; // convert to km
             
             //        NSLog(@"[searchBarSearchButtonClicked] Radius is %f", radius);
             span.latitudeDelta = radius / 112.0;
             
             region1.span = span;
             
             [rallymapView setRegion:region1 animated:YES];
             }*/
            searchAddress = YES;
//            float spanX = 0.00725;
//            float spanY = 0.00725;
            float spanX = 0.005; //added by swati_11-oct
            float spanY = 0.005;
            MKCoordinateRegion region;
            region.center.latitude = placemark.region.center.latitude;
            region.center.longitude = placemark.region.center.longitude;
            region.span = MKCoordinateSpanMake(spanX, spanY);
            
            MKPointAnnotation *annotation4 =
            [[MKPointAnnotation alloc]init];
            annotation4.coordinate = CLLocationCoordinate2DMake(placemark.region.center.latitude, placemark.region.center.longitude);
            annotation4.title = searchBarName;
            
            
            
            
            searchAddressStr = [NSString stringWithFormat:@"%@",[placemark.addressDictionary objectForKey:@"FormattedAddressLines"]];
            NSCharacterSet *Character = [NSCharacterSet characterSetWithCharactersInString:@"(\"\")"];
            searchAddressStr = [[searchAddressStr componentsSeparatedByCharactersInSet:Character] componentsJoinedByString:@""];
            
            NSString *squashed = [searchAddressStr stringByReplacingOccurrencesOfString:@"[ ]+\\  "
                                                                             withString:@" "
                                                                                options:NSRegularExpressionSearch
                                                                                  range:NSMakeRange(0, annTitle.length)];
            
            //loactionSearchAddressStr = [squashed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            loactionSearchAddressStr = searchBarName;
            map_latitudeSearchAddress = [NSString stringWithFormat:@"%f",placemark.region.center.latitude];
            map_longitudeSearchAddress = [NSString stringWithFormat:@"%f", placemark.region.center.longitude];
            
            //            if ( (placemark.region.center.latitude >= -90)     && (placemark.region.center.latitude <= 90)     && ( placemark.region.center.longitude>= -180)     && (placemark.region.center.longitude<= 180))
            //            {
            
            [rallymapView addAnnotation:annotation4];
            [rallymapView  setRegion:region animated:YES];
            //            }
        }
    }];
}

//user tapped on the cancel button
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    
    [searchBar resignFirstResponder];
}

#pragma mark - ------------ UITableView Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [packArr count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
   
    cell = nil;
    userImagView = nil;
//    UILabel *nameLabel;
    static NSString *TableViewCellIdentifier = @"Cell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
        if (iOSVersion>=8.0)
        {
             cell.indentationLevel = 4.0; //Added by swati 27Oct
        }
        else
        {
            cell.indentationLevel = 5.0;
        }
       
        cell.indentationWidth = 10;
        userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(12,4,35 ,35)];
        userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
        userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
        userImagView.clipsToBounds = YES;
        userImagView.tag = -34;
        
        userImagView.image = [UIImage imageNamed:@""];
        userImagView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:userImagView];
        
//        nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(53,6,220,30)];
//        nameLabel.backgroundColor=[UIColor clearColor];
//        .nameLabel.tag=100;
//        nameLabel.textColor=[UIColor grayColor];
//        nameLabel.font =[UIFont fontWithName:@"HelveticaNeue" size:16];
//        nameLabel.adjustsFontSizeToFitWidth=TRUE;
//        [cell.contentView addSubview:nameLabel];
    }
    indexrows = indexPath.row;
    if([selectedRows containsObject:indexPath])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    userFriendDic = [packArr objectAtIndex:indexPath.row];
    cell.tag = [[userFriendDic objectForKey:@"friendID"]integerValue];
    if ([[userFriendDic objectForKey:@"friendNAME"] isEqual:[NSNull null]])
    {
        //nameLabel.text=@"";
        cell.textLabel.text = @" ";
    }
    else
    {
        cell.textLabel.textColor = [UIColor grayColor];
        
        NSString *  nameStr2 = [NSString stringWithFormat:@"%@",[userFriendDic objectForKey:@"friendNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            
            nameStr2 = [nameStr2   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"  " withString:@" "];

//        nameLabel.text=nameStr2;
        cell.textLabel.text = nameStr2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    }
    if ([[userFriendDic objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
    {
        userImagView.image =[UIImage imageNamed:@""];
    }
    else
    {
        userImagView = (AsyncImageView *)[cell viewWithTag:-34];
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userFriendDic objectForKey:@"userIMAGE"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        userImagView.imageURL = imageURL;
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *str = [NSString stringWithFormat:@"%i",[cell tag]];
    
    if([selectedRows containsObject:indexPath])
    {
        [selectedRows removeObject:indexPath];
        [arr removeObject:str];
        
    }
    else
    {
        [selectedRows addObject:indexPath];
        [arr addObject:str];
        
    }
    [tableView reloadData];
    
}
-(void)selectAllUserAction:(UIButton *)sender
{
    if (packArr.count>0)
    {
        
        if (![sender isSelected])
        {
            [selectAllBtn  setTitle:@"Unselect" forState:UIControlStateNormal];
            [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            selectAllBtn.tintColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            
            for (int i= 0; i<[packArr count]; i++)
            {
                NSIndexPath *index=[NSIndexPath indexPathForRow:i inSection:0];
//                NSArray *indexArr=[inviteTableview indexPathsForSelectedRows];
//                cell = [[inviteTableview visibleCells] objectAtIndex:i];
                cell=[inviteTableview cellForRowAtIndexPath:index];
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//                NSString *str = [NSString stringWithFormat:@"%li",(long)[cell tag]];//changed val 4dec
                 NSString *str =[[packArr objectAtIndex:i]valueForKey:@"friendID"] ;
                [arr addObject:str];
                [selectedRows addObject:index];
            }
            
            [sender setSelected:YES];
        }
        else
        {
            [selectAllBtn  setTitle:@"Select All" forState:UIControlStateNormal];
            [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            selectAllBtn.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            for (int i= 0; i<[packArr count]; i++)
            {
//                cell = [[inviteTableview visibleCells] objectAtIndex:i];
                NSIndexPath *index=[NSIndexPath indexPathForRow:i inSection:0];
                cell=[inviteTableview cellForRowAtIndexPath:index];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                NSString *str = [NSString stringWithFormat:@"%li",(long)[cell tag]];
                [arr removeObject:str];
                [selectedRows removeObject:index];
            }
            
            [sender setSelected:NO];
        }
    }
}



-(void)callWebsercice_rally_latestrallylist
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/latestrallylist.php?userid=%@",[defaults objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_latestrallylist:responseObject];
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         kRAlert(@"Whoops", error.localizedDescription);
         [timmerRepeat1 invalidate];
         timmerRepeat1 = nil;
         if ([error.localizedDescription isEqualToString:@"Could not connect to the server."])
         {
             [timmerRepeat1 invalidate];
             timmerRepeat1 = nil;
         }
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}

-(void)JSONRecieved_latestrallylist:(NSDictionary *)response
{
    camerahide = clickCamera;
    rallyid_Str1 =@"";
    NSDictionary *Dict = response;
    // NSLog(@"response12%@",response);
    
    if ([Dict objectForKey:@"joinedrallies"] == [NSNull null])
    {
        
        // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else
    {
        
        lastRalliesEventArr = [Dict objectForKey:@"joinedrallies"];
        if ([lastRalliesEventArr count]>0)
        {
            for (int check = 0; check<[lastRalliesEventArr count]; check++)
            {
                //                if ([[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"checkin"] isEqualToString:@"0"])
                //                {
                NSString * rallyDateStr =[NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyDATE"]];
                NSString * rallyTimeStr =[NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyTIME"]];
                NSString * appendRallyDateStr = [rallyDateStr stringByAppendingString:rallyTimeStr];
                NSString *dateString = appendRallyDateStr;
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                [dateFormatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                NSDate * dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:dateString];
                NSInteger timeUntilEnd = (NSInteger)[dateFromString timeIntervalSinceDate:[NSDate date]];
                
                NSString * chatdateStr =[NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"chatdate"]];
                if ([chatdateStr isEqual:(id)[NSNull null]] ||[chatdateStr isEqualToString:@"<null>"])
                {
                    chatdateStr = @"0";
                }
                
                
                
                NSComparisonResult result;
                if ([[NSUserDefaults standardUserDefaults]objectForKey:[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyID"]] != nil) {
                    result = [chatdateStr compare:[[NSUserDefaults standardUserDefaults]objectForKey:[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyID"]]]; // comparing two dates
                }
                else
                {
                    if ([chatdateStr isEqual:(id)[NSNull null]] ||[chatdateStr isEqualToString:@"<null>"]) {
                        chatdateStr = [NSString stringWithFormat:@"%@",[NSDate date]];
                    }
                    
                    else
                    {
                        
                        [[NSUserDefaults standardUserDefaults]setValue:chatdateStr forKey:[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyID"]];
                    }
                    
                    result = [chatdateStr compare:[[NSUserDefaults standardUserDefaults]objectForKey:[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyID"]]]; // comparing two dates
                }
                
                
                if(result==NSOrderedSame)
                {
                    
                    
                    
                }
                
                else if(result == NSOrderedDescending)
                {
                    //                if (chatdateStr>[[NSUserDefaults standardUserDefaults]valueForKey:@"seeLastTimeView"])
                    //                {
                    //UIImage *chatImage = [UIImage imageNamed:@"chat_icon.png"];
                    UIImage *chatImage = [UIImage imageNamed:@"chat_icon_grey-1.png"];
                    UIButton *chatBttn = [UIButton buttonWithType:UIButtonTypeCustom];
                    chatBttn.bounds = CGRectMake( 4, 0,35,30 );
                    [chatBttn addTarget:self action:@selector(ChatAction:) forControlEvents:UIControlEventTouchUpInside];
                    [chatBttn setImage:chatImage forState:UIControlStateNormal];
                    UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithCustomView:chatBttn];
                    self.navigationItem.leftBarButtonItem = chatButton;
                    
                    //                }
                    
                }
                else if(result ==  NSOrderedAscending)
                {
                    UIImage *chatImage = [UIImage imageNamed:@"chat_icon_grey-1.png"];
                    UIButton *chatBttn = [UIButton buttonWithType:UIButtonTypeCustom];
                    chatBttn.bounds = CGRectMake( 4, 0,35, 30 );
                    [chatBttn addTarget:self action:@selector(ChatAction:) forControlEvents:UIControlEventTouchUpInside];
                    [chatBttn setImage:chatImage forState:UIControlStateNormal];
                    UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithCustomView:chatBttn];
                    self.navigationItem.leftBarButtonItem = chatButton;
                }
                
                if (timeUntilEnd <= 0)
                {
                    NSString *latitude_Str = [NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyLATITUDE"]];
                    NSString * longitude_Str1 = [NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyLONGITUDE"]];
                    
                    
                    coord2  = [[CLLocation alloc] initWithLatitude:[latitude_Str floatValue] longitude:[longitude_Str1 floatValue]];
                    //}
                                 NSInteger nRadius = 6371; // Earth's radius in Kilometers
                    double latDiff = (coord2.coordinate.latitude - coord1.coordinate.latitude) * (M_PI/180);
                    double lonDiff = (coord2.coordinate.longitude - coord1.coordinate.longitude) * (M_PI/180);
                    double lat1InRadians = coord1.coordinate.latitude * (M_PI/180);
                    double lat2InRadians = coord2.coordinate.latitude * (M_PI/180);
                 //   NSLog(@"coord2%@",coord2);
                    // NSLog(@"coord1%@",coord1);
                    double nA = pow ( sin(latDiff/2), 2 ) + cos(lat1InRadians) * cos(lat2InRadians) * pow ( sin(lonDiff/2), 2 );
                    double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
                    double nD = nRadius * nC;
                    // convert to meters
                    // return @(nD*1000);
                    //  NSLog(@"nd  %f",nD*1000);
                    DistanceInMeter = nD*1000;
                    if (DistanceInMeter < 200.0) //added by swati 27Oct from 100 to 200
                        //                        if (DistanceInMeter < 7)
                    {
                        rallyid_Str1 = [NSString stringWithFormat:@"%@",[[lastRalliesEventArr objectAtIndex:check] objectForKey:@"rallyID"]];
                        clickCamera = YES;
                    }
//                    else
//                    {
//                        //                            if (rbtnClick == 1)
//                        //                            {
//                        clickCamera = NO;
//                        
//                    }
//                    if (camerahide != clickCamera)
//                    {
//                        
//                        
//                        if (rbtnClick == 1)
//                            
//                        {
//                         // [self callwhell];
//                            //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defaultsChanged1:) name:NSUserDefaultsDidChangeNotification object:nil];
//                            
//                        }
                    
                        
                    //}
                }
            }
            
        }
      if (rallyid_Str1.length >0)
        {
           
                //clickCamera = YES;
            clickCameratoPostImg =YES;
            UIImage *faceImage = [UIImage imageNamed:@"camera_Green.png"];
            UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
            face.bounds = CGRectMake(5, 0, 30, 24 );
            [face addTarget:self action:@selector(SearchAction1:) forControlEvents:UIControlEventTouchUpInside];
            [face setImage:faceImage forState:UIControlStateNormal];
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
            self.navigationItem.rightBarButtonItem = backButton;

            }
            else
            {
                //                            if (rbtnClick == 1)
                //                            {
               // clickCamera = NO;
                 clickCameratoPostImg =NO;
                        UIImage *faceImage = [UIImage imageNamed:@"cameragray1.png"];
                UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
                face.bounds = CGRectMake(5, 0, 30, 24 );
                [face addTarget:self action:@selector(SearchAction1:) forControlEvents:UIControlEventTouchUpInside];
                [face setImage:faceImage forState:UIControlStateNormal];
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
                self.navigationItem.rightBarButtonItem = backButton;

                
            }
            //if (camerahide != clickCamera)
         //   {
                
                
               // if (rbtnClick == 1)
                    
               // {
                    //[self callwhell];
                    //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defaultsChanged1:) name:NSUserDefaultsDidChangeNotification object:nil];
                    
              //  }
                
                
            //}
//
     
        //     if ([lastRalliesEventArr count]>0)
        {
            //            UIImage *chatImage = [UIImage imageNamed:@"chat_icon.png"];
            //            UIButton *chatBttn = [UIButton buttonWithType:UIButtonTypeCustom];
            //            chatBttn.bounds = CGRectMake( 4, 0,35, 30 );
            //            [chatBttn addTarget:self action:@selector(ChatAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [chatBttn setImage:chatImage forState:UIControlStateNormal];
            //            UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithCustomView:chatBttn];
            //            self.navigationItem.leftBarButtonItem = chatButton;
            
        }
        // [chatTableView reloadData];
    }
    
    
    
    switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallyListViewFailed:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
        case APIResponseStatus_RallendarViewSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
            
        default:
            
            break;
            
            
    }
}
-(void)callwhell
{
    self.view.userInteractionEnabled=NO;
    
   // loadingView = [[UIView alloc] initWithFrame:CGRectMake(80, 165, 165, 90)];
    loadingView = [[UIView alloc] init];
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.opaque = NO;
    loadingView.alpha = 0.8;
    loadingView.clipsToBounds = YES;
    loadingView.layer.cornerRadius = 10.0;
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingLabel = [[UILabel alloc] init];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.adjustsFontSizeToFitWidth = YES;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.text = @"Loading...";
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        loadingView.frame = CGRectMake(80, 165, 165, 90);
        activityView.frame = CGRectMake(65, 15, activityView.bounds.size.width, activityView.bounds.size.height);
         loadingLabel.frame = CGRectMake(20, 55, 130, 22);
    }
    else
    {
        loadingView.frame = CGRectMake(80, 135, 165, 90);
        activityView.frame = CGRectMake(65, 15, activityView.bounds.size.width, activityView.bounds.size.height);
         loadingLabel.frame = CGRectMake(20, 55, 130, 22);
        }
    
       
    [loadingView addSubview:activityView];
    
   // loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 55, 130, 22)];
   
    [loadingView addSubview:loadingLabel];
    
    [self.view addSubview:loadingView];
    [activityView startAnimating];
    
//      ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//
//        ProgressHUD.labelText = @"Loading...";
//        ProgressHUD.dimBackground = YES;
   
    [self removeWheelAction:nil];
    [self RallyAction:nil];
    //[self.view performSelector:@selector(defaultsChanged1) withObject:nil afterDelay:10];
    [self performSelector:@selector(progessBarHide)
               withObject:nil
               afterDelay:2.0];
  
}

-(void)progessBarHide
    {
        
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        ProgressHUD = nil;
        self.view.userInteractionEnabled=YES;
       
        [activityView stopAnimating];
        [loadingView removeFromSuperview];
}

-(void)callWebsercice_rally
{
    //    if (ProgressHUD == nil)
    //    {
    //        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //        ProgressHUD.labelText = @"Loading...";
    //        ProgressHUD.dimBackground = YES;
    //    }
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallendar.php?userid=%@",[defaults objectForKey:@"id"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_rally:responseObject];
        //        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        //        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         //         ProgressHUD = nil;
         kRAlert(@"Error", error.localizedDescription);               }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved_rally:(NSDictionary *)response
{
    Dict1 = response;
    //  NSLog(@"response12%@",response);
    if ([Dict1 objectForKey:@"joinedrallies"] == [NSNull null])
    {
        
        // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else
        
    {
        joinRalliesEventArr = [Dict1 objectForKey:@"joinedrallies"];
        [[NSUserDefaults standardUserDefaults] setObject:[Dict1 objectForKey:@"joinedrallies"] forKey:@"DicKey"];
        
    }
    
    
    
    switch ([Dict1[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallendarViewFailed:
        {
            
            // UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            // [alert show];
            
            break;
        }
        case APIResponseStatus_RallendarViewSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
            
        default:
            
            break;
            
            
    }
}

-(void)deselectButtons
{
    for(UIView *vw in [self.view subviews])
    {
        if([vw isKindOfClass:([UIButton class])])
        {
            UIButton *btn=(UIButton *)vw;
            btn.selected=FALSE;
        }
    }
}

- (void) updateLabel
{
    count ++;
    
    if(count == 1)
    {
        tagLbl.text = @"Coffee is good. Coffee with people is better";
        
        count = 1;
    }
    else if(count == 2)
    {
        tagLbl.text = @"It’s time for the world to Rally";
        
        count = 2;
    }
    else if(count == 3)
    {
        tagLbl.text = @"No plans for the weekend? Your life just got easier";
        
        count = 3;
    }
    else if(count == 4)
    {
        tagLbl.text = @"Do the opposite of sitting at home by yourself tonight";
        
        count = 4;
    }
    else if(count == 5)
    {
        tagLbl.text = @"Your social life, in the palm of your hands";
        
        count = 5;
    }
    else if(count == 6)
    {
        tagLbl.text =  @"Check the hub to see what everyone is doing";
        
        count = 6;
    }
    else if(count == 7)
    {
        tagLbl.text =  @"Start a Rally anywhere on the map";
        
        count = 7;
    }
    else if(count == 8)
    {
        tagLbl.text =  @"What’s going on tonight? Hint: You’re in the right place";
        
        count = 8;
    }
    else if(count == 9)
    {
        tagLbl.text =  @"Let’s Rally!";
        
        count = 9;
    }
    else if(count == 10)
    {
        tagLbl.text =  @"Life is about having a good time with friends";
        
        count = 10;
    }
    
    else if(count == 11)
    {
        tagLbl.text =  @"We knew you’d be back";
        
        count = 11;
    }
    else if(count == 12)
    {
        tagLbl.text =  @"Just join a Rally already!";
        
        count = 12;
    }
    
    else if(count == 13)
    {
        tagLbl.text =  @"What happens after midnight? Stories. Stories happen.";
        
        count = 13;
    }
    
    else if(count == 14)
    {
        tagLbl.text =  @"Don’t be left behind tonight";
        
        count = 14;
    }
    else if(count == 15)
    {
        tagLbl.text =  @"This app doesn’t work without your friends (ok it might still work)";
        
        count = 15;
    }
    else if(count == 16)
    {
        tagLbl.text =  @"Making new friends isn't weird, we promise";
        
        count = 0;
    }
    [[NSUserDefaults standardUserDefaults]setInteger:count forKey:@"countKey"];
    
    
}
-(void)SearchAction1:(UIButton *)sender
{
    [self removeWheelAction : nil];
    if ( clickCameratoPostImg  == YES)
    {
        
            
            valueLabel.text = @"Camera";
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                                     delegate: self
                                                            cancelButtonTitle: @"Cancel"
                                                       destructiveButtonTitle: nil
                                                            otherButtonTitles: @"Camera", nil];
            [actionSheet showInView:self.view];
            
        
    }
    else
    {
       // [self removeWheelAction:nil];
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Go to a Rally to use the camera! Then you can take all the selfies you want." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
    }
    
}
-(void)ChatAction:(UIButton *)sender
{
    RCahtListViewController *CahtListViewController = [[RCahtListViewController alloc]initWithNibName:@"RCahtListViewController" bundle:nil];
    [self.navigationController pushViewController:CahtListViewController animated:NO];
    
    //    RChatViewController *ChatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
    //    [self.navigationController pushViewController:ChatViewController animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)removeViewLoop
{
    
    [splashView removeFromSuperview];
}

-(void)loop
{
    
    //    [AnimationImageView stopAnimating];
    //    removeWheelBtn.hidden = NO;
//      timmerRepeat1 =  [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(timerCallback1) userInfo:nil repeats:YES];
//       NSRunLoop *runner = [NSRunLoop currentRunLoop];
//       [runner addTimer:timmerRepeat1 forMode: NSDefaultRunLoopMode];
 //[self startTimer];
   
    [AnimationImageView stopAnimating];
    [myTimer invalidate];
    myTimer = nil;
    grayView.hidden = NO;
    logoImageView.hidden = NO;
    tagLbl.hidden = NO;
    arrow = [[UIImageView alloc]init];
    backgroundImg1 = [[UIImageView alloc]init];
    valueLabel = [[UILabel alloc] init];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 568)
    {
        valueLabel.frame = CGRectMake(0,tagLbl.frame.origin.y+tagLbl.frame.size.height+91,320, 20);
        valueLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0f];
        arrow.frame =CGRectMake(150, valueLabel.frame.origin.y+valueLabel.frame.size.height+3, 20, 17);
        backgroundImg1.frame =CGRectMake(0, self.view.frame.size.height-142, 320, 152);
    }
    else
    {
        valueLabel .frame = CGRectMake(0, tagLbl.frame.origin.y+tagLbl.frame.size.height+43,320, 20);
        valueLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0f];
        arrow.frame =CGRectMake(150, valueLabel.frame.origin.y+valueLabel.frame.size.height+3,20, 17);
        backgroundImg1.frame =  CGRectMake(0, self.view.frame.size.height-140, 320, 150);
    }
    //   if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    //    {
    //        valueLabel .frame = CGRectMake(102, tagLbl.frame.origin.y+tagLbl.frame.size.height+30, 120, 30);
    //        valueLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:18.0f];
    //        arrow.frame =CGRectMake(150, valueLabel.frame.origin.y+valueLabel.frame.size.height,20, 17);
    //        backgroundImg1.frame =  CGRectMake(0, self.view.frame.size.height-140, 320, 150);
    //
    //        }
    valueLabel.text =@"The Hub";
    arrow.image =[UIImage imageNamed:@"rArrow12.png"];
    backgroundImg1.image =[UIImage imageNamed:@"green-ring123.png"];
    [valueLabel setTextAlignment:NSTextAlignmentCenter];
    valueLabel.textColor = [UIColor whiteColor];
    valueLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:valueLabel];
    [self.view addSubview:backgroundImg1];
    // [self.view sendSubviewToBack:backgroundImg1];
    [self.view addSubview:arrow];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped)];
    
    CGFloat height1 = [UIScreen mainScreen].bounds.size.height;
    if (height1== 568)
    {
        circle = [[CDCircle alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height-198, 300, 450) numberOfSegments:12 ringWidth:100.0f];
    }
    else
    {
        circle = [[CDCircle alloc] initWithFrame:CGRectMake(10 , self.view.frame.size.height-197, 300, 450) numberOfSegments:12 ringWidth:100.0f];
    }
    circle.dataSource = self;
    circle.delegate = self;
       overlay = [[CDCircleOverlayView alloc] initWithCircle:circle];
    [self.view addSubview:circle];
    //    circle.alpha = 0;
    //    overlay.alpha = 0;
    circle.alpha = 1.0;
    overlay.alpha = 1.0;
    [circle addGestureRecognizer:tapGesture];
    //Overlay cannot be subview of a circle because then it would turn around with the circle
    [self.view addSubview:overlay];
   
    /*[UIView animateWithDuration:0.0 animations:^{
     [AnimationImageView stopAnimating];
     circle.alpha = 1.0;
     overlay.alpha = 1.0;
     AnimationImageView.alpha = 0.0;
     }];*/
    removeWheelBtn.hidden = NO;
    
    
}

-(void)tapped
{
//    [timmerRepeat1 invalidate];
//    
//    timmerRepeat1 = nil;
    tap = 1;
}

- (IBAction)RallyAction:(id)sender
{
    rbtnClick = 0;
    if (![sender isSelected])
    {
       
        rbtnClick = 1;
        
        grayView.backgroundColor = [UIColor lightGrayColor];
        grayView.alpha = 0.8;
        tagLbl.textColor = [UIColor whiteColor];
        removeWheelBtn.hidden = YES;
        [self.view bringSubviewToFront:rallyLogoBtn];
        // NSArray *imageNames = @[@"icons1.png", @"icons2.png", @"icons3.png",@"icons4.png"];
        // NSArray *imageNames = @[@"icons1.png", @"icons2.png", @"icons3.png"];
       
      imageNames = nil;
   images =nil;
       // AnimationImageView= nil;
        imageNames = @[@"wheel1.png",@"wheel2.png",@"wheel3.png",@"wheel4.png",@"wheel5.png", @"wheel6.png",@"wheel7.png",@"wheel8.png",@"wheel11.png",@"wheel12.png"];
        images = [[NSMutableArray alloc] init];
        for (int i = 0; i < imageNames.count; i++)
        {
            [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        }
        
        // Normal Animation
        AnimationImageView = [[UIImageView alloc] init];
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        //NSLog(@"screen soze is %f",height);
        if (height== 568) {
            rallyLogoBtn.frame =CGRectMake(126,self.view.frame.size.height-65, 68, 63);
            AnimationImageView.frame =CGRectMake(0, self.view.frame.size.height-168, 320, 193);
        }
        
        else
        {
            rallyLogoBtn.frame =CGRectMake(126,self.view.frame.size.height-64, 68, 63);
            AnimationImageView.frame =CGRectMake(0, self.view.frame.size.height-165, 320, 190);
        }
        AnimationImageView.contentMode = UIViewContentModeScaleAspectFit;
        AnimationImageView.animationImages = images;
        // AnimationImageView.animationDuration = 1.5;
        AnimationImageView.animationDuration = 1.0;
        AnimationImageView.animationRepeatCount = 0;
        [self.view addSubview:AnimationImageView];
        //[self.view sendSubviewToBack:AnimationImageView];
        [AnimationImageView startAnimating];
        myTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self
                                                 selector:@selector(loop)
                                                 userInfo:nil
                                                  repeats:NO];
        [sender setSelected:YES];
    }
    else
    {
        rbtnClick = 0;
        [AnimationImageView stopAnimating];
        [myTimer invalidate];
        myTimer= nil;
        [sender setSelected:NO];
    }
}
-(void)timerCallback1
{
    
    lastRalliesEventArr = nil;
    
    [self callWebsercice_rally_latestrallylist];
    
    //  [bubbleTable scrollBubbleViewToBottomAnimated:YES];
    //[bubbleTable reloadData];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [myTimer invalidate];
    myTimer = nil;
    [timmerRepeat1 invalidate];
    timmerRepeat1 = nil;
}


#pragma mark - Circle delegate & data source

-(void) circle:(CDCircle *)circle didMoveToSegment:(NSInteger)segment thumb:(CDCircleThumb *)thumb
{
    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User did select item" message:[NSString stringWithFormat:@"Segment's tag: %i", segment] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //    [alert show];
    //   [timmerRepeat1 invalidate];
    //    timmerRepeat1 = nil;
    //    if(movesegment == 0)
    //    {
    if (segment == 0 || segment == 6)
    {
        if (tap == 0) {
           
            valueLabel.text = @"The Hub";
            //tap = 1;
        }
        else if (tap == 1)
        {
            [self removeWheelAction:nil];
            valueLabel.text = @"The Hub";
            RHubViewController *HubViewController = [[RHubViewController alloc]initWithNibName:@"RHubViewController" bundle:nil];
            [self.navigationController pushViewController:HubViewController animated:NO];
            tap = 0;
        }
    }
    if (segment == 1 || segment == 7)
    {
        if (tap == 0)
        {
           
            valueLabel.text = @"Notifications";
            // tap = 1;
        }
        else if (tap == 1)
        {
            [self removeWheelAction:nil];
            valueLabel.text = @"Notifications";
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            [self.navigationController pushViewController:NotificationViewController animated:NO];
            
            tap = 0;
        }
    }
    if (segment == 2 || segment == 8)
    {
        if (tap == 0)
        {
           
            valueLabel.text = @"Search";
            // tap = 1;
        }
        
        
        /* if ([joinRalliesEventArr count]>0)
         {
         if (tap == 1)
         {
         valueLabel.text = @"Camera";
         UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
         delegate: self
         cancelButtonTitle: @"Cancel"
         destructiveButtonTitle: nil
         otherButtonTitles: @"Camera", nil];
         [actionSheet showInView:self.view];
         tap = 0;
         }
         }*/
        
        else if (tap == 1)
        {
            RSearchViewController *SearchViewController = [[RSearchViewController alloc]initWithNibName:@"RSearchViewController" bundle:nil];
            [self.navigationController pushViewController:SearchViewController animated:NO];
        }
      /*  if ( clickCameratoPostImg  == YES)
        {
            if (tap == 1)
            {
                
                
                valueLabel.text = @"Camera";
                
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                                         delegate: self
                                                                cancelButtonTitle: @"Cancel"
                                                           destructiveButtonTitle: nil
                                                                otherButtonTitles: @"Camera", nil];
                [actionSheet showInView:self.view];
                
                tap = 0;
            }
            
        }
        else
        {
            if (tap == 1)
            { [self removeWheelAction:nil];
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Go to a Rally to use the camera! Then you can take all the selfies you want." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        */
        
        
    }
    if (segment == 3 || segment == 9)
    {
        if (tap == 0)
        {
//                        valueLabel.text = @"Rallendar";
            //tap = 1;
        }
        else if (tap == 1)
        {
            [self removeWheelAction:nil];
//            valueLabel.text = @"Rallendar";
            
//            RCalenderViewController *calendarController = [[RCalenderViewController alloc] initWithNibName:@"RCalenderViewController" bundle:[NSBundle mainBundle]];
//            [self.navigationController pushViewController:calendarController animated:NO]
            
            RCalenderViewController *rcontroller = [RCalenderViewController new];
            self.viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            // self.viewController.ralliesEventArr12 = [Dict1 objectForKey:@"joinedrallies"];
            [self presentViewController:rcontroller animated:YES completion:nil];
            
            tap = 0;
        }
    }
    
    if (segment == 5 || segment == 11)
    {
        if (tap == 0) {
            valueLabel.text = @"Profile";

            
            //tap = 1;
        }
        else if (tap == 1) {
            [self removeWheelAction:nil];
            self.title = @"";
            valueLabel.text = @"Profile";
            RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
            ProfileViewController.homeView1 = YES;
            [self.navigationController pushViewController:ProfileViewController animated:NO];
            

            
            tap = 0;
        }
    }
    if (segment == 4 || segment == 10)
    {
        if (tap == 0) {
           
            valueLabel.text = @"Photo Albums";
            
            //tap = 1;
        }
        else if (tap == 1) {
            [self removeWheelAction:nil];
            valueLabel.text = @"Photo Albums";
            RAlbumViewController *AlbumViewController = [[RAlbumViewController alloc]initWithNibName:@"RAlbumViewController" bundle:nil];
            AlbumViewController.profileUSerID = [[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
            [self.navigationController pushViewController:AlbumViewController animated:NO];
            tap = 0;
        }
    }
    //        movesegment = 1;
    //    }
    
}

-(UIImage *) circle:(CDCircle *)circle iconForThumbAtRow:(NSInteger)row
{
    //NSString *fileString = [[[NSBundle mainBundle] pathsForResourcesOfType:@"png" inDirectory:nil] lastObject];
    
    //return [UIImage imageWithContentsOfFile:fileString];
    UIImage *img;
    
    
    if (row == 0 || row == 6)
    {
        return  [UIImage imageNamed:@"home1.png"];
    }
    
    else if (row == 1 || row== 7)
    {
        UIImage * im= [UIImage imageNamed:@"noti-icon_2.png"];
        
        return im;
        
        //return [im imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else if (row == 2 || row == 8)
    {
        return  [UIImage imageNamed:@"he3.png"];
    }
   /* else if (row == 2 || row == 8)
    {
        if (clickCamera == YES)
        {
            //clickCamera = NO;
            //  [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"testCamera"];
            clickCameratoPostImg =YES;
            return  [UIImage imageNamed:@"camera.png"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"testCamera"];
            // clickCamera = NO;
            clickCameratoPostImg =NO;
          //  return  [UIImage imageNamed:@"camera1.png"];
             return  [UIImage imageNamed:@"search.png"];
        }
        
        
        
        // if ([rallyIdStr isEqualToString:@"(null)"] || [rallyIdStr isEqual:[NSNull null]] ||  rallyIdStr.length == 0 || rallyIdStr == nil )
        
        
    }*/
    else if (row == 3 || row == 9)
    {
        return  [UIImage imageNamed:@"calendar.png"];
           }
    else if (row == 5 || row == 11)
    {
        return  [UIImage imageNamed:@"profile.png"];

    }
    else if (row == 4 || row== 10 )
    {
        return  [UIImage imageNamed:@"albumPhoto.png"];
    }
    
    return img;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
            //        case 1:
            //            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}

# pragma Mark ----------- MapDelegate ------------



# pragma Mark ----------- CLRegion ------------


/*- (void)initializeLocationManager
 {
 // Check to ensure location services are enabled
 if(![CLLocationManager locationServicesEnabled]) {
 // [self showAlertWithMessage:@"You need to enable location services to use this app."];
 return;
 }
 
 locationManager = [[CLLocationManager alloc] init];
 locationManager.delegate = self;
 }
 - (CLRegion*)dictToRegion:(NSDictionary*)dictionary
 {
 NSString *identifier = [dictionary valueForKey:@"identifier"];
 CLLocationDegrees latitude = [[dictionary valueForKey:@"latitude"] doubleValue];
 CLLocationDegrees longitude =[[dictionary valueForKey:@"longitude"] doubleValue];
 CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
 CLLocationDistance regionRadius = [[dictionary valueForKey:@"radius"] doubleValue];
 
 if(regionRadius > locationManager.maximumRegionMonitoringDistance)
 {
 regionRadius = locationManager.maximumRegionMonitoringDistance;
 }
 
 NSString *version = [[UIDevice currentDevice] systemVersion];
 CLRegion * region =nil;
 
 if([version floatValue] >= 7.0f) //for iOS7
 {
 region =  [[CLCircularRegion alloc] initWithCenter:centerCoordinate
 radius:regionRadius
 identifier:identifier];
 }
 else // iOS 7 below
 {
 region = [[CLRegion alloc] initCircularRegionWithCenter:centerCoordinate
 radius:regionRadius
 identifier:identifier];
 }
 return  region;
 }
 - (void) initializeRegionMonitoring:(NSArray*)geofences
 {
 if(![CLLocationManager regionMonitoringAvailable])
 {
 //[self showAlertWithMessage:@"This app requires region monitoring features which are unavailable on this device."];
 return;
 }
 
 for(CLRegion *geofence in geofences)
 {
 [locationManager startMonitoringForRegion:geofence];
 }
 }
 - (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
 //[self showRegionAlert:@"Entering Region" forRegion:region.identifier];
 //    NSLog(@"Entering Region %@ region", region.identifier);
 
 }
 
 - (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
 //  [self showRegionAlert:@"Exiting Region" forRegion:region.identifier];
 //    NSLog(@"Exiting Region %@ region", region.identifier);
 
 }
 
 - (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
 //    NSLog(@"Started monitoring %@ region", region.identifier);
 }
 
 
 - (NSNumber*)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2
 {
 NSInteger nRadius = 6371; // Earth's radius in Kilometers
 double latDiff = (coord2.latitude - coord1.latitude) * (M_PI/180);
 double lonDiff = (coord2.longitude - coord1.longitude) * (M_PI/180);
 double lat1InRadians = coord1.latitude * (M_PI/180);
 double lat2InRadians = coord2.latitude * (M_PI/180);
 double nA = pow ( sin(latDiff/2), 2 ) + cos(lat1InRadians) * cos(lat2InRadians) * pow ( sin(lonDiff/2), 2 );
 double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
 double nD = nRadius * nC;
 // convert to meters
 return @(nD*1000);
 }*/
- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = NO;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showImageView];
    image = [info valueForKey: UIImagePickerControllerOriginalImage];
    //   NSData *imageData = UIImagePNGRepresentation(image);
    cameraImg.image = image;
    // imageCropView.image= image;
    //    if(image != nil){
    //        ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:image];
    //        controller.delegate = self;
    //        [[self navigationController] pushViewController:controller animated:YES];
    //    }
    
}
-(void)showImageView

{
    removeWheelBtn.hidden = YES;
    grayView.alpha=1;
    grayView.backgroundColor = [UIColor blackColor];
    tagLbl.hidden = YES;
    logoImageView.hidden = YES;
    postView.hidden= NO;
    rallyLogoBtn.hidden= YES;
}
- (void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    
    image = croppedImage;
    cameraImg.image = croppedImage;
    [[self navigationController] popViewControllerAnimated:YES];
    [self showImageView];
}

- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller
{
    
    cameraImg.image = image;
    postView.hidden = NO;
    grayView.alpha=1;
    removeWheelBtn.hidden = YES;
    grayView.backgroundColor = [UIColor blackColor];
    tagLbl.hidden = YES;
    tagLbl.textColor = [UIColor whiteColor];
    logoImageView.hidden = YES;
    rallyLogoBtn.hidden= YES;
    [checkBtn setSelected:NO];
    [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    selectBox = NO;
    [[self navigationController] popViewControllerAnimated:YES];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    
    postView.hidden = YES;
    grayView.alpha=0.8;
    removeWheelBtn.hidden = NO;
    grayView.backgroundColor = [UIColor lightGrayColor];
    tagLbl.hidden = YES;
    tagLbl.textColor = [UIColor whiteColor];
    logoImageView.hidden = YES;
    rallyLogoBtn.hidden= NO;
    [checkBtn setSelected:NO];
    [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    selectBox = NO;

    //cameraImg.image = image;
   // postView.hidden = NO;
   // grayView.alpha=1;
    //removeWheelBtn.hidden = YES;
   // grayView.backgroundColor = [UIColor blackColor];
    //tagLbl.hidden = YES;
    //tagLbl.textColor = [UIColor whiteColor];
    //logoImageView.hidden = YES;
    //rallyLogoBtn.hidden= YES;
   // [checkBtn setSelected:NO];
   // [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    //selectBox = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)postImageAction:(id)sender

{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
    [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
    NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
    
    rallyIdStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults ]objectForKey:@"rallyIDStr"]];
    NSString *urlStr;
    if (selectBox == YES)
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages.php?rallyid=%@&userid=%@&date=%@&showonhub=1",rallyid_Str1,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"], dateStr];
        // NSData *imgData=UIImagePNGRepresentation(profileImg.image);
    }
    else
    {
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallyimages.php?rallyid=%@&userid=%@&date=%@&showonhub=0",rallyid_Str1,[[NSUserDefaults standardUserDefaults] objectForKey:@"id"], dateStr];
    }
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    UIImage *image=cameraImg.image;
    NSData *imageData =UIImageJPEGRepresentation(image, 0.1);
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagename\"; filename=\"profile.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    //   NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //   NSString*s11=   [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    
    // NSLog(@"%@",dateStr);
    
    //   NSURL *url = [NSURL URLWithString:urlStr];
    //   NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         
         
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}



-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict3 = response;
    // NSLog(@"response12%@",response);
    switch ([dict3[kRAPIResult] integerValue])
    
    {
        case  APIResponseStatus_ImageUploadSuccessfull:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"Image Uploaded Successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag =-3;
            [alert show];
            break;
        }
        case  APIResponseStatus_ImageUploadFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Image Upload Failed. Please try  again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
            
        default:
            
            break;
            
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == -3)
    {
        if (buttonIndex ==0) {
            postView.hidden =YES;
            removeWheelBtn.hidden = NO;
            grayView.alpha=0.8;
            grayView.backgroundColor = [UIColor lightGrayColor];
            tagLbl.hidden = YES;
            logoImageView.hidden = YES;
            tagLbl.textColor = [UIColor whiteColor];
            rallyLogoBtn.hidden= NO;
            [checkBtn setSelected:NO];
            [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            selectBox = NO;
            
        }
    }
    
}
- (IBAction)CancelAction:(id)sender
{
    //[postView removeFromSuperview];
    
    postView.hidden = YES;
    grayView.alpha=0.8;
    removeWheelBtn.hidden = NO;
    grayView.backgroundColor = [UIColor lightGrayColor];
    tagLbl.hidden = YES;
    tagLbl.textColor = [UIColor whiteColor];
    logoImageView.hidden = YES;
    rallyLogoBtn.hidden= NO;
    [checkBtn setSelected:NO];
    [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    selectBox = NO;
}

- (IBAction)removeWheelAction:(id)sender
{
    logoImageView.hidden = YES;
    tagLbl.hidden = YES;
    [rallyLogoBtn setSelected:NO];
    [circle removeFromSuperview];
    [overlay removeFromSuperview];
    grayView.backgroundColor = [UIColor lightGrayColor];
    grayView.alpha = 0;
//    tagLbl.textColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    [valueLabel removeFromSuperview];
    [arrow removeFromSuperview];
    [backgroundImg1 removeFromSuperview];
    AnimationImageView =nil;
    [AnimationImageView removeFromSuperview];
    [AnimationImageView stopAnimating];
    [myTimer invalidate];
    myTimer = nil;
       imageNames = nil;
    images = nil;
    circle = nil;

    overlay = nil;

    //    [timmerRepeat1 invalidate];
    //    timmerRepeat1 = nil;
}
- (IBAction)checkBtnAction:(id)sender
{
    
    if (![sender isSelected])
    {
        selectBox = YES;
        [checkBtn setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [checkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        checkBtn.tintColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        
        
        [sender setSelected:YES];
        
    }
    else
    {
        selectBox = NO;
        [checkBtn setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }
}


@end
