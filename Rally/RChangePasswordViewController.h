//
//  RChangePasswordViewController.h
//  Rally
//
//  Created by Hannan Khan on 3/1/15.
//  Copyright (c) 2015 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RChangePasswordViewController : UIViewController
@property ( strong , nonatomic ) IBOutlet UITextField *currPassField;
@property ( strong , nonatomic ) IBOutlet UITextField *nPassTxtField;
@property ( strong , nonatomic ) IBOutlet UITextField *confirmPassField;
@property ( strong , nonatomic ) IBOutlet UIButton *saveBtn;

@end
