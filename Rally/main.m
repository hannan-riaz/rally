                                                                                                                                                                //  main.m
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
     // [[NSUserDefaults standardUserDefaults]setObject:[NSArray arrayWithObject:@"en"]forKey:@"AppleLanguages"];

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RAppDelegate class]));
    }
}
