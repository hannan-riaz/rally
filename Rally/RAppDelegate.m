//
//  RAppDelegate.m
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "STTwitter.h"
#import <Parse/Parse.h>
#import "RChatViewController.h"
#import "RFullImageViewController.h"
#import "RNotificationViewController.h"
#import "RProfileViewController.h"
#import "RHubViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "RSliderViewController.h"
#import "AGPushNoteView.h"

static NSInteger pushCounter = 0;

@implementation RAppDelegate
{
    UIView *splashView;
    UIImageView * MyImage;
    
}

//static NSString * const kClientID =@"581762919327-9and13ete497jpb89egjmb13ddt3u43q.apps.googleusercontent.com";
static NSString * const kClientID =@"343453764761-pnnt4cif2e0osifkpjdfa38ceptee8cv.apps.googleusercontent.com";


@synthesize loginView,navigationView,homeView,netStatus;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GPPSignIn sharedInstance].clientID = kClientID;
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ( [CLLocationManager locationServicesEnabled] )
    {
        self.locationManager.delegate = self;
//        self.locationManager.distanceFilter = kdistance;
        if(IS_OS_8_OR_LATER)
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    }
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings1 = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings1];
        [application registerForRemoteNotifications];
    }
    else
    {
        // Register for Push Notifications before iOS 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    }
    
   
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)]; //Added by swati_9-Oct
    
//    UIRemoteNotificationType enabledTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    [Parse setApplicationId:@"YmaitL0ahJI8rhUWCWZ46ubG8bOwVVVUflWWHJdz"
               clientKey:@"hhmL2h7DuRUl8YweHTQkxyXtSZOpKtHOJy5MEdSU"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
   // [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"slideView"];


    [self ShowSplashView];
    // Override point for customization after application launch.
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [self CopyHtmlFiles];
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
       return YES;
}
- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}
-(void)CopyHtmlFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    
    NSArray *FilesName;
    NSString *SourceFilePath;
    NSString *DestFilePath;
    FilesName = [NSArray arrayWithObjects:
                 @"terms.html",
                 @"privacy.html",nil];
    
    for (int i=0; i<FilesName.count; i++) {
        SourceFilePath=[documentsPath stringByAppendingPathComponent:[FilesName objectAtIndex:i]];
        // check to see current file exists in documents
        if (![[NSFileManager defaultManager] fileExistsAtPath:SourceFilePath])
        {
            DestFilePath=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[FilesName objectAtIndex:i]];
            [[NSFileManager defaultManager] copyItemAtPath:DestFilePath toPath:SourceFilePath error:nil];
        }
        
    }
    
}

-(void)ShowSplashView
{
    splashView = [[UIView alloc]initWithFrame:CGRectMake(0,0, 320,568)];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    if (height== 480)
    {
        
        MyImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320,480)];
        MyImage.image=[UIImage imageNamed:@"Default@2x.png"];
    }
    else
    {
        MyImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 40, 320,490)];
        MyImage.image=[UIImage imageNamed:@"Default~iphone@2x.png"];
    }
    MyImage.userInteractionEnabled = YES;
    [splashView addSubview:MyImage];
    //  [splashView bringSubviewToFront:MyImage];
    
    //[splashView addSubview:MyImage];
    
    [UIView animateWithDuration:0.2 animations:^{
        [splashView setAlpha:0.1];
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(removeViewLoop)
                                   userInfo:nil
                                    repeats:NO];
    [self.window addSubview:splashView];
    [self.window bringSubviewToFront:splashView];

}
-(void)removeViewLoop
{
    [splashView removeFromSuperview];
    
       if ([[NSUserDefaults standardUserDefaults]boolForKey:@"slideView"] == NO)
    {
   
        RSliderViewController *SliderViewController = [[RSliderViewController alloc]initWithNibName:@"RSliderViewController" bundle:nil];
        navigationView = [[UINavigationController alloc]initWithRootViewController:SliderViewController];
        self.window.rootViewController = navigationView;
      [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"slideView"];
        
    }
    else
   {
       loginView = [[RLogInViewController alloc]initWithNibName:@"RLogInViewController" bundle:nil];
        navigationView = [[UINavigationController alloc]initWithRootViewController:loginView];
        self.window.rootViewController = navigationView;
    }
    [self navcolor];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	//NSLog(@"My token is: %@", deviceToken);
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
   [currentInstallation saveInBackground];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *idstr = [NSString stringWithFormat:@"%@",deviceToken];
    [defaults setObject:idstr forKey:@"deviceToken"];
    [defaults synchronize];
//    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	//NSLog(@"Failed to get token, error: %@", error);
}
//+ (void)Generalstyle {
//    //navigationbar
//    UINavigationBar *navBar = [UINavigationBar appearance];
//    
//    navBar =na .navigationBar;
//    int borderSize = 3;
//    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navBar.frame.size.height-borderSize,navBar.frame.size.width, borderSize)];
//    [navBorder setBackgroundColor:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0]];
//    [self.navigationView.navigationBar addSubview:navBorder];
//    
//}
- (NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString
{
    
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
    NSArray *queryComponents = [queryString componentsSeparatedByString:@"&"];
    
    for(NSString *s in queryComponents)
    {
        NSArray *pair = [s componentsSeparatedByString:@"="];
        if([pair count] != 2) continue;
        
        NSString *key = pair[0];
        NSString *value = pair[1];
        
        md[key] = value;
    }
    
    return md;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
   
    NSMutableArray * arrData = [[userInfo objectForKey:@"aps"] objectForKey:@"data"];
    NSDictionary * dic = [userInfo valueForKey:@"data"];
    NSLog(@"Dict %@",dic);
   [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
   //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        [AGPushNoteView showWithNotificationMessage:[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]];
//        [AGPushNoteView setMessageAction:^(NSString *message) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PUSH"
//                                                            message:message
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"Close"
//                                                  otherButtonTitles:nil];
//            [alert show];
//        }];

         //[PFPush handlePush:userInfo];
//        }];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [self callWebsercice_rally];

          }
    else
    {
        
        
        if([[dic valueForKey:@"message"] isEqual:@"message"])
        {
            RChatViewController *ChatViewController = [[RChatViewController alloc]initWithNibName:@"RChatViewController" bundle:nil];
            
            ChatViewController.RallyIDStr =[dic objectForKey:@"rallyid"];
            [self.navigationView pushViewController:ChatViewController animated:YES];
        }
        else if([[dic valueForKey:@"like"] isEqual:@"like"])
            
        {
           
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
            
            [self.navigationView pushViewController:NotificationViewController animated:YES];
//            RFullImageViewController *FullImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
//            
//            FullImageViewController.PushDataDic =dic;
//            [self.navigationView pushViewController:FullImageViewController animated:YES];
            
        }
        else if([[dic valueForKey:@"comment"] isEqual:@"comment"])
            
        {
            RFullImageViewController *FullImageViewController = [[RFullImageViewController alloc]initWithNibName:@"RFullImageViewController" bundle:nil];
            
            FullImageViewController.PushDataDic =dic;
            [self.navigationView pushViewController:FullImageViewController animated:YES];
            
        }

        else if([[dic valueForKey:@"rallyrequest"] isEqual:@"rallyrequest"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
          //  NotificationViewController.RallyIDStr =[dic objectForKey:@"rallyid"];
            [self.navigationView pushViewController:NotificationViewController animated:YES];
            
        }

        else if([[dic valueForKey:@"packrequest"] isEqual:@"packrequest"])
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
            [self.navigationView pushViewController:NotificationViewController animated:YES];
        }

        else if([[dic valueForKey:@"rallyrequestaccepted"] isEqual:@"rallyrequestaccepted"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
                      [self.navigationView pushViewController:NotificationViewController animated:YES];
            
        }

        else if([[dic valueForKey:@"packrequestaccepted"] isEqual:@"packrequestaccepted"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
          
            [self.navigationView pushViewController:NotificationViewController animated:YES];
        }

        else if([[dic valueForKey:@"invitedecline"] isEqual:@"invitedecline"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
                       [self.navigationView pushViewController:NotificationViewController animated:YES];
            
        }

        else if([[dic valueForKey:@"invite"] isEqual:@"invite"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
                   [self.navigationView pushViewController:NotificationViewController animated:YES];
        }
        else if([[dic valueForKey:@"rallydecline"] isEqual:@"rallydecline"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
                  [self.navigationView pushViewController:NotificationViewController animated:YES];
        }
        else if([[dic valueForKey:@"declinerallyrequest"] isEqual:@"declinerallyrequest"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
    
            [self.navigationView pushViewController:NotificationViewController animated:YES];
        }
        else if([[dic valueForKey:@"packdecline"] isEqual:@"packdecline"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
            
            [self.navigationView pushViewController:NotificationViewController animated:YES];
        }
        else if([[dic valueForKey:@"inviteaccepted"] isEqual:@"inviteaccepted"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
            
            [self.navigationView pushViewController:NotificationViewController animated:YES];
        }
        
        else if([[dic valueForKey:@"actlike"] isEqual:@"actlike"])
            
        {
            RNotificationViewController *NotificationViewController = [[RNotificationViewController alloc]initWithNibName:@"RNotificationViewController" bundle:nil];
            
            
            [self.navigationView pushViewController:NotificationViewController animated:YES];

//            RHubViewController *HubViewController = [[RHubViewController alloc]initWithNibName:@"RHubViewController" bundle:nil];
//            
//            [self.navigationView pushViewController:HubViewController animated:YES];
        }

        

    }
}


-(void)navcolor
{
    UINavigationBar* navBar = self.navigationView.navigationBar;
    int borderSize = 2;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navBar.frame.size.height-borderSize,navBar.frame.size.width, borderSize)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0]];
    [self.navigationView.navigationBar addSubview:navBorder];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    
    NSLog(@"annotation %@",annotation);
     NSLog(@"annotation %@",sourceApplication);
      NSLog(@"annotation %@",url);
    //if ([[url scheme] isEqualToString:@"rallyapp"] == NO) return NO;
    if ([[url scheme] isEqualToString:@"rallyapp"] != NO){
    NSDictionary *d = [self parametersDictionaryFromQueryString:[url query]];
    NSString *token = d[@"oauth_token"];
    NSString *verifier = d[@"oauth_verifier"];
   //loginView = [[RLogInViewController alloc]init];
     RLogInViewController *vc = (RLogInViewController *)[[self window] rootViewController];
        NSArray *currentControllers = navigationView.viewControllers;
       // NSLog(@"currentControllers%@",currentControllers);
//      RLogInViewController *viewController = [[RLogInViewController alloc] initWithNibName:@"RLogInViewController" bundle:nil];
//      navigationView = [[UINavigationController alloc]initWithRootViewController:viewController];
       
        [[currentControllers lastObject] setOAuthToken:token oauthVerifier:verifier];
//               self.window.rootViewController=navigationView;
       
   
        
           return YES;
       
    }

     // attempt to extract a token from the url
    else if ([[url scheme] isEqualToString:@"fb658759617492747"] != NO)
    {

    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
       // NSLog(@"In fallback handler");
    }];
       
    }
    else
    {
    return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
}
       

}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
   [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self callWebsercice_rally];

    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self callWebsercice_rally];
    [FBAppEvents activateApp];
    
    [FBAppCall handleDidBecomeActive];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     [FBSession.activeSession close];
}
-(void)callWebsercice_rally
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/resetbadges.php?userid=%@",[defaults objectForKey:@"id"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
       
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         
         kRAlert(@"Whoops", error.localizedDescription);               }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved:(NSDictionary *)response
{         NSDictionary *Dict = response;
    //    NSLog(@"response12%@",response);
    
    
    if ([Dict objectForKey:@"joinedrallies"] == [NSNull null])
    {
        
        // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }
    else
    {
        
    }
    
    switch ([Dict[kRAPIResult] integerValue])
    
    {
            
        case APIResponseStatus_ResetBadgesSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
            
            break;
        }
        case APIResponseStatus_BadgeResetFailed:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            
            
            break;
        }
            
        default:
            
            break;
            
            
    }
}

@end
