//
//  RAppDelegate.h
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "RLogInViewController.h"
#import "RHomeViewController.h"
#import <GooglePlus/GooglePlus.h>

@interface RAppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,GPPDeepLinkDelegate,CLLocationManagerDelegate>


{
    // where the save file is located
    NSString* savePath;
    
    // app settings
    NSMutableDictionary* settings;

}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)RLogInViewController *loginView;
@property (strong, nonatomic)RHomeViewController *homeView;
@property (strong, nonatomic)NSString *netStatus;
@property (strong, nonatomic)UINavigationController *navigationView;
@property(strong,nonatomic)CLLocationManager *locationManager;

@end
