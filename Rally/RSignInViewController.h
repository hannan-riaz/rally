//
//  RSignInViewController.h
//  Rally
//
//  Created by Ambika on 4/24/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface RSignInViewController : UIViewController<UITextFieldDelegate,FBLoginViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)logInAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)twitterAction:(id)sender;
- (IBAction)emailAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verfier;
@property (strong, nonatomic) IBOutlet UIButton *forgetPassBtn;
- (IBAction)forgetPasswordaction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
@property (strong, nonatomic) IBOutlet UITextField *emailIDTxt;
@end
