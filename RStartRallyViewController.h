//
//  RStartRallyViewController.h
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RCalenderViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "HPGrowingTextView.h"

@interface RStartRallyViewController : UIViewController<UIScrollViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UITextViewDelegate,UIAlertViewDelegate,HPGrowingTextViewDelegate>
{
    NSArray * imageArray;
    NSArray * imageArray1;
    HPGrowingTextView *growingtextView;
    UIView *customView;
    UIView *settingView;
    UIView *tempView;
    
}
@property(nonatomic,strong)IBOutlet UIBarButtonItem *crossBtn;
@property(nonatomic,strong)IBOutlet UIBarButtonItem *nextBtn;
@property(nonatomic,strong)IBOutlet UIToolbar* toolbar;
@property (weak, nonatomic) IBOutlet UIScrollView *businessScrollview;
@property (weak, nonatomic) IBOutlet UIScrollView *pastRScrollView;
@property (weak, nonatomic) NSString *latitudeStr;
@property (weak, nonatomic)  NSString *longitudeStr;
@property (weak, nonatomic) NSString *nameStr;
@property (weak, nonatomic)  NSString *addressStr;
@property (weak, nonatomic)  NSString *discriptionStr;
@property (weak, nonatomic) NSString *tileStr;
@property (weak, nonatomic)  NSString *dateStr;
@property (weak, nonatomic)  NSString *timeStr;
@property (weak, nonatomic)  NSMutableArray *inviteUserArr;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UITextView *addressTxt;
@property (weak, nonatomic) IBOutlet UITextView *discriptionTxt;
@property (weak, nonatomic) IBOutlet UIView *addressView;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIButton *stratRallyBtn;
@property (strong, nonatomic) NSString * privateValue ;
@property (strong, nonatomic)RCalenderViewController *viewController ;
- (IBAction)startRallyAction:(id)sender;

@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (strong, nonatomic) IBOutlet UIButton *resignBtn;

- (IBAction)resignAction:(id)sender;
-(IBAction)nextBtnAction:(id)sender;
-(IBAction)closeBtnAction:(id)sender;

@end
