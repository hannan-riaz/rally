//
//  RProfileViewController.m
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RProfileViewController.h"
#import "REditProfileViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "RSearchViewController.h"
#import "FXBlurView.h"
#import "RMoreViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RImageViewController.h"
#import "AsyncImageView.h"
#import "RAlbumViewController.h"

@interface RProfileViewController ()

{
    NSMutableArray *userinfoArr;
    NSDictionary * userInfoDic;
    NSDictionary * userPackDic;
    NSString * namestr;
    NSMutableArray *dataArray;
    NSMutableArray * packArr;
    NSDictionary * userFriendDic;
    NSString * userpackID;
    NSString * friendId;
    MBProgressHUD *ProgressHUD;
    NSDictionary * userinfo;
    NSDictionary *responseDict ;
    FXBlurView *transView;
    AsyncImageView *fullImage;
    UIButton *  backtoProfileBtn;
    UIScrollView * zoomScrollView ;
     NSMutableArray * arr_user ;
    NSTimer * timerFor_Cancel ;
    int remove ;
   
    UILabel*  lblTitle;
}

@end

@implementation RProfileViewController
@synthesize RallyPointLbl,scrollView,arrowImg,packTableview,aboutBtn,packBtn,searchuserid,schoolLbl,HomeLbl,BioLbl,serachView1,profileImg,addUser,packLbl,homeView1,profileCircleImage,ageLbl,profileNameLbl,hubUserIDStr,frienduserId,JoinRallyersuserId,deleteBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.aboutBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Regular" size:14.0]];
    [self.photoAlbumBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Regular" size:14.0]];
    [self.packBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Regular" size:14.0]];
    [self.BioLbl setFont:[UIFont fontWithName:@"Raleway-Regular" size:12.0]];
    [self.profileNameLbl setFont:[UIFont fontWithName:@"Raleway-SemiBold" size:18.0]];
    [self.countryAddress setFont:[UIFont fontWithName:@"Raleway-Regular" size:13.0]];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    
    transView = [[FXBlurView alloc]init ];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            transView.frame=CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            ageLbl.frame = CGRectMake(15,20, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);
            
        
        }
        else
        {
//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//             transView.frame= CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            
//            ageLbl.frame = CGRectMake(15,20, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);
            

        }

    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {

//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            transView.frame=CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            ageLbl.frame = CGRectMake(15,20, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);
        }
        else
        {
//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64,profileImg.frame.size.width,profileImg.frame.size.height);
//              transView.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            ageLbl.frame = CGRectMake(15,ageLbl.frame.size.width, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);

        }

    }
    else
    {
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {
//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            transView.frame=CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            ageLbl.frame = CGRectMake(15,20, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);

        }
        else
        {
//            profileImg.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            transView.frame= CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height-64, profileImg.frame.size.width, profileImg.frame.size.height);
//            profileCircleImage.frame = CGRectMake(110, profileImg.frame.origin.y+profileImg.frame.size.height-125, profileCircleImage.frame.size.width, profileCircleImage.frame.size.height);
//            profileNameLbl.frame = CGRectMake(0,profileCircleImage.frame.origin.y+profileCircleImage.frame.size.height+3, profileNameLbl.frame.size.width, profileNameLbl.frame.size.height);
//            ageLbl.frame = CGRectMake(15,20, ageLbl.frame.size.width, ageLbl.frame.size.height);
//            
//            deleteBtn.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+23, deleteBtn.frame.size.width, deleteBtn.frame.size.height);
//            addUser.frame = CGRectMake(48, profileImg.frame.origin.y+profileImg.frame.size.height+24, addUser.frame.size.width, addUser.frame.size.height);
//            aboutBtn.frame = CGRectMake(130,profileImg.frame.origin.y+profileImg.frame.size.height+28, aboutBtn.frame.size.width, aboutBtn.frame.size.height);
//            packLbl.frame = CGRectMake(225, profileImg.frame.origin.y+profileImg.frame.size.height+28, packLbl.frame.size.width, packLbl.frame.size.height);
//            packBtn.frame = CGRectMake(248,profileImg.frame.origin.y+profileImg.frame.size.height+28, packBtn.frame.size.width, packBtn.frame.size.height);
//            arrowImg.frame = CGRectMake(0,aboutBtn.frame.origin.y+aboutBtn.frame.size.height+3, arrowImg.frame.size.width, arrowImg.frame.size.height);
//            scrollView.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, scrollView.frame.size.width, scrollView.frame.size.height);

        }
       }
    deleteBtn.hidden = YES;
    
    //FXBlurView *transView = [[FXBlurView alloc]initWithFrame:profileImg.frame];
    transView.alpha = 0.9;
    //transView.dynamic = NO;
    //transView.tintColor = [UIColor colorWithRed:0 green:0.5 blue:0.5 alpha:1];
    [profileImg addSubview:transView];
    profileCircleImage.layer.cornerRadius = profileCircleImage.bounds.size.width/2;
    profileCircleImage.clipsToBounds = YES;
   
//    [BioLbl setFont:[UIFont fontWithName:@"Helvetica Neue" size:16.0f]];
//    BioLbl.textColor = [UIColor darkGrayColor];
//        ageLbl.layer.borderColor = [UIColor whiteColor].CGColor;
//    ageLbl.layer.borderWidth = 2.0;
//    [ageLbl.layer setCornerRadius:ageLbl.bounds.size.width/2];
//    ageLbl.layer.masksToBounds = YES;
    
    userinfoArr = [[NSMutableArray alloc]init];
    packArr = [[NSMutableArray alloc]init];
    userInfoDic = [[NSMutableDictionary alloc]init];
    userPackDic = [[NSMutableDictionary alloc]init];
    userFriendDic = [[NSMutableDictionary alloc]init];
    arr_user = [[NSMutableArray alloc]init];
    userpackID = searchuserid;

    dataArray = [[NSMutableArray alloc] initWithObjects:@"Adam", @"Smith", @"Adam Smiuth",@"Mac",@"Deniel", nil];
    // Do any additional setup after loading the view from its nib.
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [fullImage removeFromSuperview];
    [backtoProfileBtn removeFromSuperview];

}


-(void)callWebServiceForUser_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    if (remove == 0)
    {
     if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    }
    NSString * urlStr;
    

    if (serachView1 == YES)
    {
        if ([searchuserid isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],searchuserid];
        }
        else
        {
          urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],searchuserid];

        }
       
    }
    else if(hubUserIDStr)
    {
        if ([hubUserIDStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],hubUserIDStr];
        }
        else
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],hubUserIDStr];
            
        }

       
    }
    else if(frienduserId )
    {
        if ([frienduserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],frienduserId];
        }
        else
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],frienduserId];
            
        }

       

    }
    else if (JoinRallyersuserId )
    {
        if ([JoinRallyersuserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],JoinRallyersuserId];
        }
        else
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],JoinRallyersuserId];
            
        }
        
    }
    else
    {
       
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@&mainuserid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"id"],[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
        
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Userid:responseObject];
        if (remove == 0)
        {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        }
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Error", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_Userid:(NSDictionary *)response
{
    responseDict = response;
//    NSLog(@"response12%@",response);
   // NSLog(@"response12%@",[dict objectForKey:@"packcount"]);
    
    userinfo =response;
    if ([responseDict objectForKey:@"userinfo"] != [NSNull null])
    {
         userInfoDic = [responseDict objectForKey:@"userinfo"];
    }
   
    [self getUserData];
    switch ([responseDict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ProfileviewSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
                       break;
        }
                default:
            
            break;
            
    }
}

-(void)getUserData
{
    if ([[userInfoDic objectForKey:@"userBIO"] isEqual:[NSNull null]])
    {
        BioLbl.text = @"";
    }
    else
    {
        NSString *  BioStr = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userBIO"]];
        
        BioStr = [BioStr stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSData *data = [BioStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];         //BioLbl.lineBreakMode = NSLineBreakByWordWrapping;
      
        
       /// CGSize expectedLabelSize = [BioStr sizeWithFont:BioLbl.font
                                    //constrainedToSize:BioLbl.frame.size
                                       // lineBreakMode:NSLineBreakByWordWrapping];
        
       // CGRect newFrame = BioLbl.frame;
       // newFrame.size.height = expectedLabelSize.height;
      //  BioLbl.frame = newFrame;
        
       // BioLbl.numberOfLines = 0;
        if (goodValue == nil||[goodValue isEqualToString:@""])
        {
            if ([BioStr isEqualToString:@""])
            {
            BioLbl.text=@"Entreprenuer";
            
            }
            else
            {
            BioLbl.text =BioStr;
            }

        }
        else
            
        {
             BioLbl.text =goodValue;
        }
            }
    
    if ([[userInfoDic objectForKey:@"userSCHOOL"] isEqual:[NSNull null]])
    {
        schoolLbl.text = @"";
    }
    else
        
    {
        NSString *  schoolStr = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userSCHOOL"]];
       
        if ([schoolStr isEqualToString:@""])
        {
        schoolLbl.text=@"School";
        }
        else
        {
        schoolLbl.text = schoolStr;
        }
    }
    if ([[userInfoDic objectForKey:@"userTOWN"] isEqual:[NSNull null]])
    {
        HomeLbl.text = @"";
    }
    else
        
    {
      NSString *  HomeStr = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userTOWN"]];
        
        if ([HomeStr isEqualToString:@""])
        {
         HomeLbl.text=@"Hometown";
        }
        else
        {
         HomeLbl.text  =HomeStr;
        }
    }
    
    if ([[userInfoDic objectForKey:@"userNAME"] isEqual:[NSNull null]])
    {
        lblTitle.text = @"";
        profileNameLbl.text = @"";
    }
    else
        
    {
        namestr = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
        namestr = [namestr   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        namestr = [namestr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
         namestr = [namestr  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
       lblTitle.text = @"Profile";
        profileNameLbl.text =[NSString stringWithFormat:@"%@, 26",namestr];
    }
    
    if ([[userInfoDic objectForKey:@"packcount"] isEqual:[NSNull null]])
    {
         packLbl.text = @"";
    }
    else
        
    {
        NSString * packString = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"packcount"]];
        packLbl.text = packString;
    }
    if ([[userInfoDic objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
    {
        profileImg.image =[UIImage imageNamed:@""];
            }
    else
    {
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userInfoDic objectForKey:@"userIMAGE"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
       // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
       // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        profileImg.imageURL = imageURL;
        profileCircleImage.imageURL = imageURL;
         }
    
    if ([[userInfoDic objectForKey:@"userDOB"] isEqual:[NSNull null]])
    {
        ageLbl.text = @"";
    }
    else
        
    {
        NSString *  brithdaydate = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userDOB"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:brithdaydate];
        NSDateComponents* agecalcul = [[NSCalendar currentCalendar]components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:dateFromString toDate:[NSDate date] options:0];
        NSInteger age = [agecalcul year];
       // NSLog(@"the age of user==%ld",(long)age);
        
        NSString * ageStr = [NSString stringWithFormat:@"%ld",(long)age];
        ageLbl.text = ageStr;
    }
    NSString *  statusString = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"status"]];

    if ([statusString isEqualToString:@"2"])
    {
        deleteBtn.hidden = NO;
        deleteBtn.userInteractionEnabled = YES;
        [deleteBtn setImage:[UIImage imageNamed:@"minusGray.png"] forState:UIControlStateNormal];
        addUser.hidden = YES;
        
    }
    else if ([statusString isEqualToString:@"1"])
    {
        deleteBtn.hidden = NO;
        deleteBtn.userInteractionEnabled = YES;
        [deleteBtn setImage:[UIImage imageNamed:@"redMinus.png"] forState:UIControlStateNormal];
        addUser.hidden = YES;
        
    }

}

-(void)callWebServiceForUser_Pack
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
     NSString * urlStr;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
   
    if (serachView1 == YES)
    {
     
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",userpackID];
    }
    else if(hubUserIDStr)
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",hubUserIDStr];

    }
    else if (frienduserId)
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",frienduserId];

    }
    else if (JoinRallyersuserId)
    {
          urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",JoinRallyersuserId];
    }
     else
    {
        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewuserpacks.php?userid=%@",[defaults objectForKey:@"id"]];
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    if ([dict objectForKey:@"userfriends"] == [NSNull null])
    {
        
    }
    else
    {
        packArr = [[dict objectForKey:@"userfriends"]mutableCopy];
      
        [packTableview reloadData];
    }
    if (serachView1 == YES)
    {
        for (int j= 0 ; j< [packArr count]; j++)
        {
            if ([[[packArr objectAtIndex:j ] objectForKey:@"friendID"]  isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            deleteBtn.hidden = NO;
            [deleteBtn setImage:[UIImage imageNamed:@"redMinus.png"] forState:UIControlStateNormal];
            addUser.hidden = YES;
            //[addUser setBackgroundImage:[UIImage imageNamed:@"sign_minus.png"] forState:UIControlStateNormal];
        }
        }
    }
   /* else if (hubUserIDStr)
    {
        if ([hubUserIDStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            for (int j= 0 ; j< [packArr count]; j++)
            {
                if ([[[packArr objectAtIndex:j ] objectForKey:@"friendID"]  isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
                {
                    deleteBtn.hidden = NO;
                    addUser.hidden = YES;
                    //[addUser setBackgroundImage:[UIImage imageNamed:@"sign_minus.png"] forState:UIControlStateNormal];
                }
            }

        }
    else if(frienduserId)
            {
                if ([frienduserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
                {
                    
                }
                    else if (JoinRallyersuserId)
                    {
                        if ([JoinRallyersuserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
                        {*/
            switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_UserIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_ViewUserInPackSuccessfull:
        {
            
            //            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert show];
            //            break;
        }
        default:
            
            break;
            
    }

}
-(void)checkUserId
{
                            
}
-(void)popToView
{
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
//    self.navigationController.navigationBar.hidden = NO;
//    self.title = @"Profile";
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [self callWebServiceForUser_id];
    [self callWebServiceForUser_Pack];
    remove =0;
    schoolLbl.adjustsLetterSpacingToFitWidth = YES;
    schoolLbl.adjustsFontSizeToFitWidth = YES;

    HomeLbl.adjustsLetterSpacingToFitWidth = YES;
    HomeLbl.adjustsFontSizeToFitWidth = YES;

 
    self.navigationController.navigationBar.hidden = NO;
        lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Profile";
    self.navigationItem.titleView = lblTitle;

//        self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];

//        RallyPointLbl.backgroundColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//        self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//               UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22.0f, 18.0f)];
//        [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//        [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    
    
    
    if (serachView1 == YES)
    {
        if ([userpackID isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            [addUser removeFromSuperview];
            [self dispalySettingBtn];
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
                //serachView1=NO;
    }
    else if (hubUserIDStr)
    {
        if ([hubUserIDStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            [addUser removeFromSuperview];
            [self dispalySettingBtn];
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
    else if(frienduserId)
    {
        if ([frienduserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            [addUser removeFromSuperview];
            [self dispalySettingBtn];
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
    else if (JoinRallyersuserId)
    {
        if ([JoinRallyersuserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            [addUser removeFromSuperview];
            [self dispalySettingBtn];
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
    else
    {

        [addUser removeFromSuperview];
        [self dispalySettingBtn];
    }
    
   // UIBarButtonItem * [settingBtn = [UIBarButtonItem alloc]ini
        scrollView.scrollEnabled = YES;
        scrollView.contentSize = CGSizeMake(320, 500);
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.delegate = self;
    
           packTableview.hidden =NO;
    
}
- (void)menuAction: (id)sender{
    
}
-(void)dispalySettingBtn
{
    UIImage *settingImage = [UIImage imageNamed:@"menu-new.png"];
    UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    settingBtn.bounds = CGRectMake( 10, 0, 30, 27 );
    [settingBtn addTarget:self action:@selector(settingAction:) forControlEvents:UIControlEventTouchUpInside];
    [settingBtn setImage:settingImage forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
    self.navigationItem.rightBarButtonItem = backButton;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Raleway-Regular" size:20],
      NSFontAttributeName, nil]];
}
-(void)settingAction:(UIButton *)sender
{
    RMoreViewController *MoreViewController = [[RMoreViewController alloc]initWithNibName:@"RMoreViewController" bundle:nil];
    MoreViewController.getuserInfoDictionary= [userinfo objectForKey:@"userinfo"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"updateImage"];
    [self.navigationController pushViewController:MoreViewController animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)aboutAction:(id)sender
{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UITableView class]])
        {
            [view removeFromSuperview];
            arrowImg.image  = [UIImage imageNamed:@"back_about.png"];
            scrollView.hidden =NO;
            [aboutBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [packBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

        }
    }
    if ([packTableview superview])
    {
        arrowImg.image  = [UIImage imageNamed:@"back_about.png"];
        scrollView.hidden =NO;
        [aboutBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [packBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [packTableview removeFromSuperview];

    }
              // packBtn.selected = YES;
       
}

- (IBAction)packAction:(id)sender
{
    packTableview = [[UITableView alloc]init ];
   [packTableview setSeparatorInset:UIEdgeInsetsZero];
   arrowImg.image  = [UIImage imageNamed:@"back_pack.png"];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
       packTableview.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, self.view.frame.size.width,  self.view.frame.size.height-240);//changed
       
    }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
        packTableview.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3, self.view.frame.size.width,  self.view.frame.size.height-240);//changed
    }
    else
    {
        packTableview.frame = CGRectMake(0,arrowImg.frame.origin.y+arrowImg.frame.size.height+3,self.view.frame.size.width,  self.view.frame.size.height-240);//changed
        
    }
   
        if ([scrollView superview])
    {
        
        scrollView.hidden =YES;
        [packBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [aboutBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
              // packTableview.style : UITableViewStylePlain;
        packTableview.dataSource = self;
        packTableview.delegate = self;
        
        //  [self.view addSubview:packTableview];
        
        
        
        [UIView transitionWithView:self.view duration:0.5
                           options:(UIViewAnimationOptionTransitionNone|UIViewAnimationOptionAllowAnimatedContent)
                        animations:^ { [self.view addSubview:packTableview]; }
                        completion:nil];
        
        [self callWebServiceForUser_Pack];

    }
  
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (serachView1 == YES)
    {
        if ([userpackID isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
           return YES;
        }
        else
        {
        return NO;
        }
    }
    else if (hubUserIDStr)
    {
        
        if ([hubUserIDStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if (frienduserId)
    {
        if ([frienduserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            return YES;
        }
        else
        {
            return NO;
        }

    }
    else if (JoinRallyersuserId)
    {
        if ([JoinRallyersuserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            return YES;
        }
        else
        {
            return NO;
        }

    }
    else
    {
        return YES;
    }
}
- (IBAction)addUserAction:(id)sender
{
   //  arrowImg.image  = [UIImage imageNamed:@"back_join.png"];
   if (serachView1 == YES)
    {
        if ([userpackID isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            //[self gotoSearchView];
        }
        else
        {
            [self callWebServiceAddPack];
        }
        
    }
    else if (frienduserId)
    {
        if ([frienduserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
           // [self gotoSearchView];
        }
        else
        {
            [self callWebServiceAddPack];
        }
    }
    else if (hubUserIDStr)
    {
        if ([hubUserIDStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            //[self gotoSearchView];
        }
        else
        {
            [self callWebServiceAddPack];
        }
    }
    else if (JoinRallyersuserId)
    {
        if ([JoinRallyersuserId isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
        {
            //[self gotoSearchView];
        }
        else
        {
            [self callWebServiceAddPack];
        }
    }

    else
    {
         //[self gotoSearchView];
 
    }

}
-(void)gotoSearchView
{
    RSearchViewController *SearchViewController = [[RSearchViewController alloc]initWithNibName:@"RSearchViewController" bundle:nil];
    [self.navigationController pushViewController:SearchViewController animated:NO];
}
-(void)callWebServiceAddPack
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
    
    // urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/adduserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],searchuserid];
    
    if (serachView1 == YES)
    {
        urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addpackrequest.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],searchuserid];
    }
    else if(hubUserIDStr)
    {
        urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addpackrequest.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],hubUserIDStr];
    }
    else if(frienduserId )
    {
        urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addpackrequest.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],frienduserId];
    }
    else if (JoinRallyersuserId )
    {
        urlStr  = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/addpackrequest.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],JoinRallyersuserId];
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self JSONRecieved_Add_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops",error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

- (IBAction)photoAlbumAction:(id)sender

{
//    RAlbumViewController *ImageViewController = [[RAlbumViewController alloc]initWithNibName:@"RAlbumViewController" bundle:nil];
//    [self.navigationController pushViewController:ImageViewController animated:NO];

    
    RAlbumViewController *ImageViewController = [[RAlbumViewController alloc]initWithNibName:@"RAlbumViewController" bundle:nil];
    if (hubUserIDStr)
    {
        
        ImageViewController.profileUSerID = hubUserIDStr;
        
    }
    else if (userpackID)
    {
        
        ImageViewController.profileUSerID = userpackID;
        
    }
    
    else if (frienduserId)
    {
        ImageViewController.profileUSerID = frienduserId;
    }
      else if (JoinRallyersuserId)
    {
        
        ImageViewController.profileUSerID = JoinRallyersuserId;
    }
    else
    {
        ImageViewController.profileUSerID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        
    }
    [self.navigationController pushViewController:ImageViewController animated:NO];

   }

-(void)JSONRecieved_Add_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    //packArr = [dict objectForKey:@"userfriends"];
    [packTableview reloadData];
    switch ([dict[kRAPIResult] integerValue])
    
    {
            case APIResponseStatus_PackReuestSentSuccessfully:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"Your request to join this user's pack has been successfully sent." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            addUser.hidden = YES;
            deleteBtn.hidden = NO;
            [deleteBtn setImage:[UIImage imageNamed:@"minusGray.png"] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"userAddIntoPack"];
            deleteBtn.userInteractionEnabled = YES;
            break;
        }
        case   APIResponseStatus_PackReuestFailed
            :
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please try Again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case   APIResponseStatus_PackRequestAlreadySent:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have already sent a request to join this users pack." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            

    /* case APIResponseStatus_FriendIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Friend ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case   APIResponseStatus_AlreadyHaveFiveFriends
:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"You have Already Five Friends Add into your pack" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case   APIResponseStatus_FriendAlreadyExist:
         {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Friend Already Add into your pack" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }

        
        case   APIResponseStatus_UserPackSuccessfull:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User Add into Your Pack" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
        case   APIResponseStatus_UserPackFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Edit User Pack Not Delete" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            */
               default:
            
            break;
            
    }
    
}

#pragma mark - ------------ Table View Delegate Methods ------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [packArr count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell *cell = nil;
    //UIButton *button;
    AsyncImageView * userImagView = nil;
//    UILabel *nameLabel;
    static NSString *TableViewCellIdentifier = @"Cell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
            if (iOSVersion>=8.0)
            {
                cell.indentationLevel = 4.0; //Added by swati 27Oct
            }
            else
            {
                cell.indentationLevel = 5.0;
            }
            cell.indentationWidth = 10;
            userImagView = [[AsyncImageView alloc]initWithFrame:CGRectMake(12,4,35,35)];
            userImagView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImagView.layer.cornerRadius = userImagView.bounds.size.width/2;
            userImagView.clipsToBounds = YES;
            userImagView.tag =  -12;
            userImagView.image = [UIImage imageNamed:@""];
            userImagView.backgroundColor = [UIColor clearColor];
            userImagView.contentMode=UIViewContentModeScaleToFill;
            [cell.contentView addSubview:userImagView];
            
//            nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(53,6,250,30)];
//            nameLabel.backgroundColor=[UIColor clearColor];
//            nameLabel.tag=100;
//            nameLabel.textColor=[UIColor grayColor];
//            nameLabel.font =[UIFont fontWithName:@"HelveticaNeue" size:16];
//            nameLabel.adjustsFontSizeToFitWidth=TRUE;
//            [cell.contentView addSubview:nameLabel];
            



        }
    
        userFriendDic = [packArr objectAtIndex:indexPath.row];
    if ([[userFriendDic objectForKey:@"friendNAME"] isEqual:[NSNull null]])
    {
        cell.textLabel.text  = @" ";
//        nameLabel.text=@"";
    }
    else
    {
        cell.textLabel.textColor = [UIColor grayColor];
        NSString *  nameStr2 = [NSString stringWithFormat:@"%@",[userFriendDic objectForKey:@"friendNAME"]];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
        {
            
            nameStr2 = [nameStr2   stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        nameStr2 = [nameStr2  stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        
//        nameLabel.text=nameStr2; //Added by swati

        cell.textLabel.text  = nameStr2;
        //cell.textLabel.text  = [userFriendDic objectForKey:@"friendNAME"];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16]; //added by swati
    }
    if ([[userFriendDic objectForKey:@"userIMAGE"] isEqual:[NSNull null]])
    {
        userImagView.image =[UIImage imageNamed:@""];
    }
    else
    {
         userImagView = (AsyncImageView *)[cell viewWithTag:-12];
        NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userFriendDic objectForKey:@"userIMAGE"]];
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        // UIImage *imageLoad = [[UIImage alloc] initWithData:imageData];
        userImagView.imageURL = imageURL;
        
    }
            return cell;
    
}

- (void)tableView:(UITableView*)tableView  didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //frienduserId =[userFriendDic objectForKey:@"friendID"];
    // [self callWebServiceForUser_id];
   
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    userFriendDic = [packArr objectAtIndex:indexPath.row];
    ProfileViewController.frienduserId =[userFriendDic objectForKey:@"friendID"];
    packTableview.hidden =YES;
    [self.navigationController pushViewController:ProfileViewController animated:NO];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    userFriendDic = [packArr objectAtIndex:indexPath.row] ;
    friendId = [NSString stringWithFormat:@"%@",[userFriendDic objectForKey:@"friendID"]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],friendId];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecievedEdit_User_Pack:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;

    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    [packArr removeObjectAtIndex:indexPath.row];
   // [packTableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [packTableview reloadData];

}

-(void)JSONRecievedEdit_User_Pack:(NSDictionary *)response
{
    NSDictionary *dict = response;
//   NSLog(@"response12%@",response);
    
  //packArr = [dict objectForKey:@"userfriends"];
            switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_FriendIDRequired:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_EditUserPackSuccessfull:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Name Required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                       [alert show]
            addUser.hidden = NO;
            deleteBtn.hidden = YES;
            [packTableview reloadData];

            [self callWebServiceForUser_id];
            break;
        }
           
        case  APIResponseStatus_EditUserPackFailed:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Edit User Pack Not Delete" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                              [alert show];
//                             break;
        }
        default:
            
            break;
            
    }

}
- (IBAction)joinPackAction:(id)sender
{
    
}
- (IBAction)FullImageAction:(id)sender
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

     zoomScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    zoomScrollView.scrollEnabled = YES;
    zoomScrollView.bouncesZoom = YES;
    zoomScrollView.delegate = self;
    zoomScrollView.clipsToBounds = YES;

    
    [self centerScrollViewContents];

   [self.view addSubview:zoomScrollView];
   
    fullImage = [[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height)];

    fullImage.userInteractionEnabled = YES;
    //fullImage.image = profileImg.image; //change by swati 28Oct
    fullImage.backgroundColor = [UIColor blackColor];
    fullImage.contentMode =  UIViewContentModeScaleAspectFit;
    //fullImage.clipsToBounds=YES; //added by swati 31Oct
//    fullImage.contentMode=UIViewContentModeRedraw;
    
    
    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[userInfoDic objectForKey:@"userIMAGE"]];
   
    NSURL *imageURL = [NSURL URLWithString:imgStr];
   // NSData * imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
//    
   // UIImage *newImage=[self normalResImage:[UIImage imageWithData:imageData] withResizingFactor:320];
    
    fullImage.imageURL = imageURL;
    
   // fullImage.image=newImage;
    
  //  fullImage.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=fullImage.frame.size};
    [zoomScrollView addSubview:fullImage];
    
    zoomScrollView.contentSize = CGSizeMake(fullImage.bounds.size.width, fullImage.bounds.size.height);
    zoomScrollView.decelerationRate = UIScrollViewDecelerationRateFast;

    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [fullImage addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [fullImage addGestureRecognizer:twoFingerTapRecognizer];
    float minimumScale = 1.0;//This is the minimum scale, set it to whatever you want. 1.0 = default
    zoomScrollView.maximumZoomScale = 4.0;
    zoomScrollView.minimumZoomScale = minimumScale;
    zoomScrollView.zoomScale = minimumScale;
    [zoomScrollView setContentMode:UIViewContentModeScaleAspectFit];
   // [fullImage sizeToFit];
    [zoomScrollView setContentSize:CGSizeMake(fullImage.frame.size.width, fullImage.frame.size.height)];
   
    backtoProfileBtn =[[UIButton alloc]init];
    backtoProfileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backtoProfileBtn.frame = CGRectMake(fullImage.frame.origin.x, fullImage.frame.origin.y,fullImage.frame.size.width,fullImage.frame.size.height);
    [backtoProfileBtn addTarget:self  action:@selector(backToprofile:) forControlEvents:UIControlEventTouchUpInside];
    [fullImage addSubview:backtoProfileBtn];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = zoomScrollView.bounds.size;
    CGRect contentsFrame = fullImage.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    fullImage.frame = contentsFrame;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    // Get the location within the image view where we tapped
    CGPoint pointInView = [recognizer locationInView:fullImage];
    
    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.scrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, zoomScrollView.maximumZoomScale);
    
    // Figure out the rect we want to zoom to, then zoom to it
    CGSize scrollViewSize = zoomScrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    [zoomScrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = zoomScrollView.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, zoomScrollView.minimumZoomScale);
    [zoomScrollView setZoomScale:newZoomScale animated:YES];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return fullImage;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}
-(void)backToprofile:(UIButton *)sender
{
    [zoomScrollView removeFromSuperview];
    [fullImage removeFromSuperview];
    [backtoProfileBtn removeFromSuperview];
    
}
- (IBAction)removeUserFromPackAction:(id)sender
{
  remove = 1;
    [self callWebServiceForUser_id];
     if ([deleteBtn.currentImage isEqual:[UIImage imageNamed:@"minusGray.png"]])
        {
           
       timerFor_Cancel = [NSTimer scheduledTimerWithTimeInterval: 1.0f target:self selector:@selector(timerFired_For_Cancel:) userInfo:nil repeats:NO];
              }
    else if([deleteBtn.currentImage isEqual:[UIImage imageNamed:@"redMinus.png"]])
        
    {
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    NSString * urlStr;
        
        if (serachView1 == YES)
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],userpackID];
           
        }
        else if(hubUserIDStr)
        {
             urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],hubUserIDStr];
            
            
        }
        else if (frienduserId)
        {
             urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],frienduserId];
          
            
        }
        else if (JoinRallyersuserId)
        {
             urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],JoinRallyersuserId];
           
        }
        else
        {
            urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/edituserpacks.php?userid=%@&friendid=%@",[defaults objectForKey:@"id"],[defaults objectForKey:@"id"]];
            
        }

        
        
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecievedEdit_User_Pack:responseObject];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    }
}
-(void)timerFired_For_Cancel:(NSTimer*)timer
{
    NSString *notificationIDStr;
    if (![[userInfoDic objectForKey:@"notificationID"]isEqual:[NSNull null]])
    {
        notificationIDStr =[NSString stringWithFormat:@"%@", [userInfoDic objectForKey:@"notificationID"]];
    }
    
    
    if (notificationIDStr)
    {
        
        if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
        NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/cancelpackrequest.php?notificationid=%@",notificationIDStr];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self JSONRecievedCancel_PackRequest:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
            
        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                             ProgressHUD = nil;
                                             kRAlert(@"Whoops", error.localizedDescription);
                                         }];
        
        [[NSOperationQueue mainQueue] addOperation:operation];
        
    }
    

}
-(void)JSONRecievedCancel_PackRequest:(NSDictionary *)response
{
    NSDictionary *dict = response;
    //   NSLog(@"response12%@",response);
   
    //packArr = [dict objectForKey:@"userfriends"];
   

    switch ([dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_NotAlreadyDeleted:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Pack request has been already cancel" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case  APIResponseStatus_PackRequestCanceledSuccessfully:
        {
            
           UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"Pack request has been canceled" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            addUser.hidden = NO;
            deleteBtn.hidden = YES;
           
            break;
        }
            
               default:
            
            break;
            
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    
}
-(UIImage *)normalResImage:(UIImage*)raw_image withResizingFactor:(CGFloat)factor;
{
    // Convert ALAsset to UIImage
    UIImage *image = raw_image;
    NSLog(@"%f---%f",image.size.width,image.size.height);
    // Determine output size
    CGFloat maxSize = factor;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat newWidth = width;
    CGFloat newHeight = height;
    CGSize newSize=CGSizeMake(newWidth, newHeight);
    // If any side exceeds the maximun size, reduce the greater side to 1200px and proportionately the other one
    if (width > maxSize) {
        if (width > height) {
            newWidth = maxSize;
            newHeight = (height*maxSize)/width;
            
        }
        else {
            newHeight = maxSize;
            newWidth = (width*maxSize)/height;
        }
        newSize=CGSizeMake(newWidth, newHeight);
    }
    UIGraphicsBeginImageContextWithOptions(newSize, YES, image.scale);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(newImage, 1.0f);
    UIImage *processedImage = [UIImage imageWithData:imageData];
    
    return processedImage;
}


@end
