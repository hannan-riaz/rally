//
//  RHomeViewController.h
//  Rally
//
//  Created by Ambika on 4/2/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDCircle.h"
#import <MapKit/MapKit.h>
#import "CalendarKit/CalendarKit.h"
#import <CoreLocation/CoreLocation.h>
#import "RJoinRallyViewController.h"
#import "RJoinRallyViewController.h"
#import "RStartRallyViewController.h"
#import "AsyncImageView.h"
#import "THLabel.h"
#import "ImageCropView.h"

@interface RHomeViewController : UIViewController<CDCircleDelegate,CDCircleDataSource,UISearchDisplayDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,ImageCropViewControllerDelegate>

{
    
    UIView * testView ;
    NSArray *dataArray;
    BOOL clockwise;
    UIImageView *AnimationImageView;
    CLLocationManager *locationManager;
    ImageCropView* imageCropView;
    UIImage *image;
    UIView *settingView;
    UIView *tempView;

}
@property (strong, nonatomic) IBOutlet UIButton *removeWheelBtn;
- (IBAction)removeWheelAction:(id)sender;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIButton *rallyLogoBtn;
@property (strong, nonatomic) IBOutlet MKMapView *rallymapView;
- (IBAction)postImageAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *postView;
- (IBAction)CancelAction:(id)sender;
@property(nonatomic) BOOL clickCamera;
@property (strong, nonatomic) IBOutlet UIImageView *cameraImg;
@property (strong, nonatomic) IBOutlet UIView *grayView;
@property (weak, nonatomic) IBOutlet THLabel *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *hubBtn;
- (IBAction)RallyAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *calenderBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (nonatomic, strong) UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLbl;
@property (strong, nonatomic) CKCalendarViewController *viewController;

@property (nonatomic) BOOL calenderViewBool;
@property (strong, nonatomic)UITableView * inviteTableview;
@property (strong, nonatomic) IBOutlet CLGeocoder *Geocoder;
@property (strong, nonatomic) NSString * calenerDateStr;
@property (strong, nonatomic) MKPointAnnotation *annotation1;
@property (strong, nonatomic) IBOutlet UISearchBar *addressSearchBar;
@property (strong, nonatomic) RJoinRallyViewController *JoinRallyViewController;
@property (strong, nonatomic) IBOutlet UIButton *checkBtn;
- (IBAction)checkBtnAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *cropImage;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;

@property (strong, nonatomic) IBOutlet UIButton *saveBtn;

@end
