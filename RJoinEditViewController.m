//
//  RJoinEditViewController.m
//  Rally
//
//  Created by Ambika on 6/13/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RJoinEditViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "RProfileViewController.h"
#import "RHomeViewController.h"
@interface RJoinEditViewController ()
{
    MBProgressHUD *ProgressHUD;
    NSString *currentdate_Str;
    NSString * adressStr;
    NSString *userIdStr;
    
    UIView  * cellView;
    UIButton *acceptBtn;
    UILabel *titleLbl;
    UIButton *rejectBtn;
    NSMutableArray * rallyrequestArray;
    NSString *  rallyIdStr;
    NSString *  sentfromStr;
    NSString *  senttoStr;
    NSString *  notificationidStr;
    AsyncImageView *userImage;
    UIButton * userNameBtn;
    UIButton *userImage1Btn1;
}
@end

@implementation RJoinEditViewController
@synthesize adressTxtView,discriptionTxtView,addressView,dicriptionView,dicriptionString,nameString,saveBtn,JoinRallyId,joinRallyTableView,userIdStr,rallyIdStr,resignBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
    {
//        addressView .frame = CGRectMake(-2, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+20, addressView.frame.size.width, addressView.frame.size.height);
//        adressTxtView .frame = CGRectMake(0, addressView.frame.origin.y-84, addressView.frame.size.width, addressView.frame.size.height);
//        dicriptionView.frame = CGRectMake(-2, addressView.frame.origin.y+addressView.frame.size.height+20, dicriptionView.frame.size.width, dicriptionView.frame.size.height);
//
//        discriptionTxtView .frame = CGRectMake(0, dicriptionView.frame.origin.y-157, dicriptionView.frame.size.width, dicriptionView.frame.size.height);
            }
    else if ([[NSUserDefaults standardUserDefaults]  boolForKey:@"TWitterlogged_in"])
    {
//        addressView .frame = CGRectMake(-2, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+20, addressView.frame.size.width, addressView.frame.size.height);
//        adressTxtView .frame = CGRectMake(0, addressView.frame.origin.y-84, addressView.frame.size.width, addressView.frame.size.height);
//        dicriptionView.frame = CGRectMake(-2, addressView.frame.origin.y+addressView.frame.size.height+20, dicriptionView.frame.size.width, dicriptionView.frame.size.height);
//        
//        discriptionTxtView .frame = CGRectMake(0, dicriptionView.frame.origin.y-157, dicriptionView.frame.size.width, dicriptionView.frame.size.height);
//
    }
    else
    {
//        CGFloat height = [UIScreen mainScreen].bounds.size.height;
//        if (height== 480)
//        {
//       addressView .frame = CGRectMake(-2, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+40, addressView.frame.size.width, addressView.frame.size.height);
//        adressTxtView .frame = CGRectMake(0, addressView.frame.origin.y-104, adressTxtView.frame.size.width, adressTxtView.frame.size.height-10);
//        dicriptionView.frame = CGRectMake(-2, addressView.frame.origin.y+addressView.frame.size.height+20, dicriptionView.frame.size.width, dicriptionView.frame.size.height);
//        
//       discriptionTxtView .frame = CGRectMake(0, dicriptionView.frame.origin.y-169, discriptionTxtView.frame.size.width, discriptionTxtView.frame.size.height);
//        }
    }
    adressTxtView.textContainerInset = UIEdgeInsetsZero;
    adressTxtView.textContainer.lineFragmentPadding =6;
    discriptionTxtView.textContainerInset = UIEdgeInsetsZero;
    discriptionTxtView.textContainer.lineFragmentPadding =6;
    discriptionTxtView.textColor = [UIColor darkGrayColor];
    adressTxtView.textColor = [UIColor darkGrayColor];
    rallyrequestArray =[[NSMutableArray alloc]init];
    

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
   //[addressView addGestureRecognizer:lpgr];
    //[dicriptionView addGestureRecognizer:lpgr];
    
        // Do any additional setup after loading the view from its nib.
}
- (void)handleLongPress1:(UIGestureRecognizer *)gestureRecognizer
{
    [adressTxtView resignFirstResponder];
    [discriptionTxtView resignFirstResponder];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    self.toolbar.hidden=NO;
    NSDictionary* info = [aNotification userInfo];
    CGRect kbRect=[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (kbRect.size.height==253)
    {
        
        if (IS_IPHONE_5)
        {
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,208,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
        }
        else
        {
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,123,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
        }
        
    }
    else if (kbRect.size.height==224)
    {
        if (IS_IPHONE_5)
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,243,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,148,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
    }
    else
    {
        
        
        if (IS_IPHONE_5)
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,246,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
        else
        {
            
            
            [UIView beginAnimations:@"Start" context:nil];
            
            [UIView setAnimationDuration:0.2];
            
            self.toolbar.frame =CGRectMake(0,160,self.toolbar.frame.size.width, self.toolbar.frame.size.height);
            
            [UIView commitAnimations];
            
            
        }
    }
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.toolbar.hidden=YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.title = @"Edit Rally";
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0] forKey:NSForegroundColorAttributeName];
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor ];
//    UIButton *backButton1 = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 22.0f, 18.0f)];
//    [backButton1 setImage:[UIImage imageNamed:@"arrow.png"]  forState:UIControlStateNormal];
//    [backButton1 addTarget:self action:@selector(popToView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    
    self.navigationController.navigationBar.hidden = NO;
    //    self.navigationItem.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Edit Rally";
    self.navigationItem.titleView = lblTitle;
    
    if ([userIdStr isEqual:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]])
    {
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelRallyAction)];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        saveBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
         saveBtn.userInteractionEnabled = YES;
      //  [self callWebSercvice_getrallyrequest];

    }
//    else
//    {
//        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Leave" style:UIBarButtonItemStyleBordered target:self action:@selector(LeaveRallyAction)];
//        self.navigationItem.rightBarButtonItem = rightBarButton;
//        self.navigationItem.rightBarButtonItem.tintColor=[UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
//        saveBtn.backgroundColor = [UIColor lightGrayColor];
//        saveBtn.userInteractionEnabled = NO;
//        
//    }
  
    addressView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baground.png"]];
    dicriptionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baground.png"]];
    addressView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    addressView.layer.borderWidth= 1.5f;
    dicriptionView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    dicriptionView.layer.borderWidth= 1.5f;
    
    nameString = [nameString stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"`" withString:@"\n"];
    NSData *data1 = [nameString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *goodValue1 = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
    
    
    if (goodValue1 == nil || [goodValue1 isEqualToString:@""])
    {
        adressTxtView.text = nameString;
    }
    else
    {
         adressTxtView.text = goodValue1;
    }
    

   
    adressTxtView.textColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
    dicriptionString = [dicriptionString stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    dicriptionString = [dicriptionString stringByReplacingOccurrencesOfString:@"%60" withString:@"\n"];
    dicriptionString = [dicriptionString stringByReplacingOccurrencesOfString:@"`" withString:@"\n"];
    NSData *data = [dicriptionString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    if (goodValue == nil)
    {
         discriptionTxtView.text = dicriptionString;
    }
    else
    {
        discriptionTxtView.text = goodValue;
    }

   
    resignBtn.hidden = YES;

}
-(void)cancelRallyAction
{
    [self callwebService_DeleteRally];
}
//-(void)LeaveRallyAction
//{
//    [self callwebService_LeaveRally];
//}
-(void)callwebService_DeleteRally
{
    NSString * urlStr;
    
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterally.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],rallyIdStr];
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_DeleteRally:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    

}
-(void)JSONRecieved_DeleteRally:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    switch ([dict[kRAPIResult] integerValue])
    {
           
        case  APIResponseStatus_NotAuthorisedToDeleteRally:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"You have not authorised to delete Rally."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case  APIResponseStatus_RallyDeletedSuccessfully:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"The Rally has been deleted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = -1;
            [alert show];
            break;
        }
        case  APIResponseStatus_DeleteRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
            
        default:
            
        break;
            
    }
    

}
-(void)callwebService_LeaveRally
{
    NSString * urlStr;
    
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/leaverally.php?userid=%@&rallyid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],rallyIdStr];
    
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_LeaveRally:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    

}
-(void)JSONRecieved_LeaveRally:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    switch ([dict[kRAPIResult] integerValue])
    {
        case  APIResponseStatus_RallyLeavedSuccessfully:
        {
            
           UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You have left this Rally"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
          [alert show];
            break;
        }
        case  APIResponseStatus_LeaveRallyFailed:
        {
            
           UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong.Please Try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [alert show];
            break;
        }

            
        default:
            
            break;
            
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
   }
/*-(void)callWebSercvice_getrallyrequest
{
    NSString * urlStr;
    
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/getrallyrequest.php?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_getrallyrequest:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}

-(void)JSONRecieved_getrallyrequest:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
   
    NSString *strrallyrequest = [dict objectForKey:@"rallyrequest"];
    
    if (strrallyrequest ==(id)[NSNull null] )
    {
        rallyrequestArray= nil;
        [joinRallyTableView reloadData];

        // [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You didn't have any notification at the moment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if([dict objectForKey:@"rallyrequest"] != [NSNull null])
    {
        rallyrequestArray = [dict objectForKey:@"rallyrequest"];
        
        [joinRallyTableView reloadData];

    }
   
    
        switch ([dict[kRAPIResult] integerValue])
    {
        case  APIResponseStatus_UserIDRequired:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rally Join Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            break;
        }
            
        default:
            
            break;
            
    }
    
    
    
}*/
-(IBAction)closeBtnAction:(id)sender
{
    [self.discriptionTxtView resignFirstResponder];
    [self.adressTxtView resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
//    self.toolbar.hidden=NO;
    if ([textView.text isEqualToString:@"Description "])
    {
        textView.text = @"";
    }
       resignBtn.hidden= NO;
    [textView becomeFirstResponder];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
//     self.toolbar.hidden=YES;
//    NSString *t1= [adressTxtView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//    if([t1 length]>25)
//    {
//        UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//    }
  
}
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//   
//    return NO;
//}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;{
    //    NSString *t1= [nameTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    if([t1 length]>25)
    //    {
    //        return NO;
    //    }
    if(textView ==discriptionTxtView )
    {
        return YES;
    }
    if([[adressTxtView text] length] - range.length + text.length < 26){
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}

-(void)popToView
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)saveAction:(id)sender
{
    NSString *t1= [adressTxtView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([t1 length]>25)
    {
        
           UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally name should not be greater than 25 charactes" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
else
{
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    
    
    NSDateFormatter *dateFormat1  = [[NSDateFormatter alloc]init];
    [dateFormat1  setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    currentdate_Str = [NSString stringWithFormat:@"%@",[dateFormat1 stringFromDate:[NSDate date]]];
    currentdate_Str = [currentdate_Str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *spaceRmoveadressStr =[ NSString stringWithFormat:@"%@",adressTxtView.text];
   // spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    spaceRmoveadressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@",\\" withString:@""];
   // adressStr = [spaceRmoveadressStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
   
    NSString *uniText1 = [NSString stringWithUTF8String:[spaceRmoveadressStr UTF8String]];
    NSData *goodMsg1 = [uniText1 dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString*discriptionStr = [NSString stringWithFormat:@"%@",discriptionTxtView.text];
    discriptionStr = [discriptionStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    //discriptionStr = [discriptionStr stringByReplacingOccurrencesOfString:@"\n" withString:@"%60"];
    

    NSString *uniText = [NSString stringWithUTF8String:[discriptionStr UTF8String]];
    NSData *goodMsg = [uniText dataUsingEncoding:NSUTF8StringEncoding];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSLog(@"%@", [defaults objectForKey:@"id"]);
    userIdStr = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"id"]];
    NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/editrally.php?userid=%@&rallyid=%@&updatedate=%@",userIdStr,rallyIdStr,currentdate_Str];
    
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rallydesc\";"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:goodMsg]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rallyname\";"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:goodMsg1]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    

    // NSConnection *
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
     
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Error", error.localizedDescription);
     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
}

- (IBAction)resignBtnAction:(id)sender
{
    if (![sender isSelected])
    {
        [adressTxtView resignFirstResponder];
        [discriptionTxtView resignFirstResponder];
        [sender setSelected:YES];
        resignBtn.hidden = YES;
    }
    else
    {
        resignBtn.hidden = NO;
        [sender setSelected:NO];
  }
    

}

-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
    
    
    switch ([dict[kRAPIResult] integerValue])
    
    {
            
        case  APIResponseStatus_RallyDescRequired:
            
        {
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Rally Description Required." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            

            break;
        }
        case  APIResponseStatus_EditRallyFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Failed to update Rally info. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
        }
            
        case  APIResponseStatus_EditRallySuccessfull:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Success" message:@"Rally info has been updated successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.tag = -4;
            [alert show];
            
            break;
        }
            
            
        default:
            
            break;
            
    }
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if ([text isEqualToString:@"\n"])
//    {
//        [textView resignFirstResponder];
//    }
//    return YES;
//}

/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    return [rallyrequestArray count];
    
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell *cell = nil;
   
  
    static NSString *TableViewCellIdentifier = @"Cell";
     cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (cell == nil)
    {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
            
            cellView = [[UIView alloc]initWithFrame: CGRectMake(cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,cell.frame.size.height+54)];
            cellView.backgroundColor = [UIColor clearColor];
            cellView.tag = 50;
            cellView.userInteractionEnabled = YES;
            
        
            userImage = [[AsyncImageView alloc]init];
            //userImage.image = [UIImage imageNamed:@""];
            userImage.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
            userImage.backgroundColor = [UIColor clearColor];
            userImage.tag = 54;
            userImage.frame = CGRectMake(10, 9, 40, 40);
            [cellView addSubview:userImage];
        
        
            userImage1Btn1 =[[UIButton alloc]init];
            userImage1Btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            userImage1Btn1.frame = CGRectMake(10, 10, 40, 40);
            userImage1Btn1.tag = 168;
            [userImage1Btn1 addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            userImage1Btn1.backgroundColor = [UIColor clearColor];
            [cellView addSubview:userImage1Btn1];

            titleLbl = [[UILabel alloc]init];
            titleLbl.frame = CGRectMake(0, 0, cell.frame.size.width, 30);
            titleLbl.textColor = [UIColor darkGrayColor];
            titleLbl.tag = 51;
            titleLbl.textAlignment = NSTextAlignmentLeft;
            titleLbl.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
            //startRallyLbl.text = @"started a Rally at";
            [cellView addSubview:titleLbl];
        
            userNameBtn =[[UIButton alloc]init];
            userNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //userNameBtn.frame = CGRectMake(55,15,0,30);
            userNameBtn.tag = 55;
            [userNameBtn addTarget:self  action:@selector(userBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            // [userNameBtn setTitle:@"amy" forState:UIControlStateNormal];
            userNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            userNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            userNameBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
            [userNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            // userNameBtn.titleLabel.adjustsLetterSpacingToFitWidth = YES;
            // userNameBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [cellView addSubview:userNameBtn];
            
            //acceptBtn =[[UIButton alloc]init];
            acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            acceptBtn.userInteractionEnabled = YES;
            acceptBtn.frame = CGRectMake(cellView.frame.origin.x+60,titleLbl.frame.origin.y+titleLbl.frame.size.height+20,90,30);
            acceptBtn.tag = 52;
            acceptBtn.layer.cornerRadius = 10.0f;
            acceptBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            acceptBtn.layer.borderWidth = 0.5;
           [acceptBtn addTarget:self  action:@selector(acceptBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
            acceptBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
        
            [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
            acceptBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [acceptBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cellView addSubview:acceptBtn];
        
            rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rejectBtn.frame = CGRectMake(cellView.frame.origin.x+170,titleLbl.frame.origin.y+titleLbl.frame.size.height+20,90,30);
            [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
            rejectBtn.backgroundColor = [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];
            rejectBtn.layer.cornerRadius = 10.0f;
            rejectBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            rejectBtn.layer.borderWidth = 0.5;
        
            rejectBtn.tag = 53;
            rejectBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
            [rejectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [rejectBtn addTarget:self action:@selector(rejectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
            [cellView addSubview:rejectBtn];
            [cell.contentView  addSubview:cellView];

}
    
    if ([[[rallyrequestArray objectAtIndex:indexPath.row ] objectForKey:@"request"] isEqual:[NSNull null]])
    {
        titleLbl.text = @"";
        
    }
    else
        
    {
        NSString *  requestStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:indexPath.row ] objectForKey:@"request"]];
        
         titleLbl.text = requestStr;
    }
    
    if ([[[rallyrequestArray objectAtIndex:indexPath.row ] objectForKey:@"userimage"] isEqual:[NSNull null]])
    {
        userImage.image = [UIImage imageNamed:@""];
        
    }
    else
    {
    NSString *imgStr =[NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/uploads/%@",[[rallyrequestArray objectAtIndex:indexPath.row]objectForKey:@"userimage"]];
    NSURL *imageURL = [NSURL URLWithString:imgStr];
    userImage.imageURL = imageURL;
    }
    if ([[[rallyrequestArray objectAtIndex:indexPath.row ] objectForKey:@"username"] isEqual:[NSNull null]])
    {
        userNameBtn.titleLabel.text =@"";
    }
    else
    {
        NSString *  nameStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:indexPath.row ] objectForKey:@"username"]];
 if ([[NSUserDefaults standardUserDefaults] boolForKey:@"googleLogin"])
 {
 

//        nameStr = [nameStr  stringByReplacingOccurrencesOfString:@"_" withString:@" "];
 }
        [userNameBtn setTitle:nameStr forState:UIControlStateNormal];
        CGSize stringsize = [nameStr sizeWithFont:[UIFont systemFontOfSize:14]];
        //or whatever font you're using
        [userNameBtn setFrame:CGRectMake(55,11,stringsize.width,stringsize.height)];
        titleLbl.frame = CGRectMake(stringsize.width+47,10,cell.frame.size.width, stringsize.height);
        
    }
    


    cell.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0] ;
     return cell;
    
}

- (void)userBtnAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:userNameBtn.bounds toView:joinRallyTableView];
    NSIndexPath *indexPath = [joinRallyTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:joinRallyTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    //[joinBtn setTag:rowNumber];
    
    RProfileViewController *ProfileViewController = [[RProfileViewController alloc]initWithNibName:@"RProfileViewController" bundle:nil];
    
    NSString *  userProfileIdStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    ProfileViewController.hubUserIDStr=userProfileIdStr;
    [self.navigationController pushViewController:ProfileViewController animated:NO];
    
    
}


- (void)acceptBtnAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:acceptBtn.bounds toView:joinRallyTableView];
    NSIndexPath *indexPath = [joinRallyTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:joinRallyTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [acceptBtn setTag:rowNumber];
    rallyIdStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber ] objectForKey:@"rallyid"]];
    sentfromStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber ] objectForKey:@"sentfrom"]];
    senttoStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber ] objectForKey:@"sentto"]];
    [self callWebSercvice_JoinRally];
    
}
- (void)rejectBtnAction:(UIButton*)sender
{
    
    CGRect buttonFrameInTableView = [sender convertRect:rejectBtn.bounds toView:joinRallyTableView];
    NSIndexPath *indexPath = [joinRallyTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSInteger rowNumber = 0;
    
    for(NSInteger i = 0; i < indexPath.section ; i++)
    {
        rowNumber += [self tableView:joinRallyTableView numberOfRowsInSection:i];
    }
    
    rowNumber += indexPath.row;
    
    [rejectBtn setTag:rowNumber];
    
    notificationidStr = [NSString stringWithFormat:@"%@",[[rallyrequestArray objectAtIndex:rowNumber] objectForKey:@"notificationid"]];
    [self callWebSercvice_deleterallyrequest];
    
}

-(void)callWebSercvice_JoinRally
{
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * urlStr;
        urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/joinrally.php?userids=%@&rallyid=%@&addedby=%@&joindate=%@",sentfromStr,rallyIdStr,senttoStr,dateStr];
            
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self JSONRecieved_joinRally:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                             ProgressHUD = nil;
                                             kRAlert(@"Error", error.localizedDescription);
                                         }];
        
        [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_joinRally:(NSDictionary *)response
 {
        NSDictionary *dict = response;
//        NSLog(@"response12%@",response);
     
            //userInfoDic = [dict objectForKey:@"userinfo"];
        // [self getUserData];
        switch ([dict[kRAPIResult] integerValue])
        
        
        {          case  APIResponseStatus_UserAlreadyJoinedRally:
            {
                
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"You Have Already Joined this Rally." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                break;
            }
            case APIResponseStatus_JoinRallySuccessfull:
            {
                
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Congrats" message:@"You have Joined a  Rally Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [self callWebSercvice_getrallyrequest];
                [joinRallyTableView reloadData];
                
                break;
            }
            case  APIResponseStatus_JoinRallyFailed:
            {
                
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please Try again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                break;
            }
                
                
            default:
                
                break;
                
        }
        
           
    }


-(void)callWebSercvice_deleterallyrequest
{
   
    NSString * urlStr;
    urlStr = [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/deleterallyrequest.php?notificationid=%@",notificationidStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_deleterallyrequest:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}
-(void)JSONRecieved_deleterallyrequest:(NSDictionary *)response
{
    NSDictionary *dict = response;
//    NSLog(@"response12%@",response);
    
   
    //userInfoDic = [dict objectForKey:@"userinfo"];
    // [self getUserData];
    switch ([dict[kRAPIResult] integerValue])
        {
     
        case  APIResponseStatus_RallyRequestDeletedSuccessfully:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"Rally has been deleted Successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag =1;
            [alert show];
            [self callWebSercvice_getrallyrequest];
            [joinRallyTableView reloadData];
            break;
        }
        case APIResponseStatus_DeleteRallyRequestFailed:
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Something happened wrong. Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
              default:
            
            break;
            
    }
    
    
    
}*/
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == -1)
   {
      
           RHomeViewController *homeVC = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
           [self.navigationController pushViewController:homeVC animated:YES];

     
    }
    if (alertView.tag == -4)
    {
        
        if (buttonIndex == 0)
        {
            RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            [self.navigationController pushViewController:HomeViewController animated:NO];
            
            
        }
    }

}


@end
