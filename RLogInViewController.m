//
//  RLogInViewController.m
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import "RLogInViewController.h"
#import "RSignUpViewController.h"
#import "RLeaderBoardViewController.h"
#import "RHomeViewController.h"
#import "REditProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "RAppDelegate.h"
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import "RSignInViewController.h"
#import "STTwitter.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "RHubViewController.h"

#import <QuartzCore/QuartzCore.h>
@interface RLogInViewController ()<GPPSignInDelegate>
{
    MBProgressHUD *ProgressHUD;
    int one;
    int count;
    NSTimer *myTimer;
    BOOL FirstLogin;
    BOOL twitterLogin;
    NSString *oauthAccessTokenSecret;
    NSString *oauthAccessToken;
    UIImage * avtarimage;
    BOOL * fbBtncick;

}
@property (nonatomic, strong) STTwitterAPI *twitter_api;

@property (nonatomic, strong)GPPSignIn *signIn;
@end
// https://dev.twitter.com/docs/auth/implementing-sign-twitter
@implementation RLogInViewController
static NSString * const kClientID = @"581762919327-9and13ete497jpb89egjmb13ddt3u43q.apps.googleusercontent.com";

@synthesize urlStr,tagLbl,signIn,twitterBtn,emailBtn,FirstTimelogout;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.alreadySignupLbl setTextColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0]];
    
    [self.signupBtn setTitleColor:[UIColor colorWithRed:0.40 green:0.84 blue:0.40 alpha:1.0] forState:UIControlStateNormal];
    
    [self.alreadySignupLbl setFont:[UIFont fontWithName:@"Raleway-Medium" size:16.0]];
    [self.signupBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-SemiBold" size:16.0]];
    [self.signinBtn.titleLabel setFont:[UIFont fontWithName:@"Raleway-Regular" size:16.0]];
       if (FirstTimelogout == YES)
    {
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
//        {
//            self.edgesForExtendedLayout = UIRectEdgeNone;
//            self.automaticallyAdjustsScrollViewInsets = NO;
//        }
//        CAGradientLayer *gradient = [CAGradientLayer layer];
//        gradient.frame = self.view.bounds;
//        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0] CGColor], (id)[[UIColor colorWithRed:16.0f/255.0f green:167.0f/255.0f blue:45.0f/255.0f alpha:1.0] CGColor], nil];
//        [self.view.layer insertSublayer:gradient atIndex:0];
        
        self.navigationController.navigationBar.hidden = YES;
        
        
        
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        if (height== 568)
        {

        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
        else
        {
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

        }
    }
      FirstLogin = NO;
    twitterLogin=NO;
     twitterBtn.tintColor = [UIColor clearColor];
    [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_g.png"] forState:UIControlStateSelected];
    [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_g.png"] forState:UIControlStateHighlighted];
    

    emailBtn.tintColor = [UIColor clearColor];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_g.png"] forState:UIControlStateSelected];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_g.png"] forState:UIControlStateHighlighted];
      FBLoginView *loginview = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email",@"user_about_me"]];   // FBLoginView *loginview = [[FBLoginView alloc] initWithReadPermissions:@[@"user_birthday"]];

    for (id loginObject in loginview.subviews)
    {
        
        if ([loginObject isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  loginObject;
            
          
            loginButton.opaque = YES;
          //  [loginButton setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
            UIImage *loginImage = [UIImage imageNamed:@"facebook.png"];
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_g.png"] forState:UIControlStateSelected];
            [loginButton setBackgroundImage:[UIImage imageNamed:@"facebook_g.png"] forState:UIControlStateHighlighted];
            
            
        }
        if ([loginObject isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  loginObject;
            loginLabel.text = @"";
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
            loginLabel.hidden = YES;
            loginLabel.opaque = NO;
            loginLabel.tintColor = [UIColor clearColor];
        }
    }
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    //NSLog(@"screen size is %f",height);
    
    if (height== 568) {
        
        loginview.frame = CGRectMake(48,375, 46, 46);
//        loginview.frame = CGRectMake(103,375, 50, 90);

        //tagLbl.font = [UIFont fontWithName:@"AvenirNext-DemiBoldItalic" size:24.0f];
    }
    
    else {
        //loginview.frame = CGRectMake(103,285, 50, 90);
       loginview.frame = CGRectMake(68,285, 50, 90);
       // tagLbl.font = [UIFont fontWithName:@"AvenirNext-DemiBoldItalic" size:22.0f];
    }
 //   loginview.frame = CGRectMake(68,376, 50, 90);
    
    loginview.delegate = self;
    
    [loginview setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
    
    
    [self.view addSubview:loginview];
    
    
      tagLbl.textColor =  [UIColor colorWithRed:89.0f/255.0f green:197.0f/255.0f blue:102.0f/255.0f alpha:1.0];

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"id"]){
        
//        RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
//        [self.navigationController pushViewController:HomeViewController animated:NO];
        RHubViewController *hubController = [[RHubViewController alloc] initWithNibName:@"RHubViewController" bundle:[NSBundle mainBundle]];
        [self.navigationController pushViewController:hubController animated:NO];
        return;
    }
   [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fB_succesfully_lg"];
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
   signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
  //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
   signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
        
    // Do any additional setup after loading the view from its nib.
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"";
      //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-320.png"]];
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.view.bounds;
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0] CGColor], (id)[[UIColor colorWithRed:16.0f/255.0f green:167.0f/255.0f blue:45.0f/255.0f alpha:1.0] CGColor], nil];
//    [self.view.layer insertSublayer:gradient atIndex:0];
    
        self.navigationController.navigationBar.hidden = YES;
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SigninAction:(id)sender {
    self.title = @"";
    RSignInViewController *SignInViewController  = [[RSignInViewController alloc]initWithNibName:@"RSignInViewController" bundle:nil];
    [self.navigationController pushViewController:SignInViewController  animated:NO];
}

- (IBAction)FacebookAction:(id)sender
{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [myTimer invalidate];
     myTimer = nil;
}


#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    // first get the buttons set for login mode
    
      //[[NSUserDefaults standardUserDefaults]objectForKey:@"facebookLogin"];
 //  NSLog(@"already logged in ");
    
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
   //NSLog(@"%@",user);
     NSString * facebook_userId = [NSString stringWithFormat:@"%@",user.id];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FBlogged_in"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];
    [[NSUserDefaults standardUserDefaults] setObject:facebook_userId  forKey:@"facebook_userId"];
  /* if ([[NSUserDefaults standardUserDefaults]objectForKey:@"facebookLogin"])
    {
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?facebookid=%@&deviceid=%@&devicetype=iPhone",facebook_userId,deviceToken];
        
      
        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
            // NSLog(@"ERROR%@",error);
             
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);

             
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
  

        
    }
    else{
        */
    

        if (ProgressHUD == nil)
        {
            ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ProgressHUD.labelText = @"Loading...";
            ProgressHUD.dimBackground = YES;
        }
        
        NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
        [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
        NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
        NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
        NSString *username = [nameStr
                              stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];

        
        NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",user.birthday];
        
        NSDateFormatter *formate=[[NSDateFormatter alloc]init];
        [formate setDateFormat:@"MM/dd/yyyy"];
        // NSLog(@"value: %@",[formate dateFromString:birthdayDate_str]);
        NSDate * date=[formate dateFromString:birthdayDate_str];
        
        NSDateFormatter *formate1=[[NSDateFormatter alloc]init];
        [formate1 setDateFormat:@"yyyy-MM-dd"];
        NSString * dobStr = [formate1 stringFromDate:date];
        NSString * imgStr  =[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",user.id];;
        NSURL *imageURL = [NSURL URLWithString:imgStr];
        NSData * FacebookImageData = [NSData dataWithContentsOfURL:imageURL];
        
       

        urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=%@&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&facebookid=%@",username,dobStr,[user objectForKey:@"email"],user.id,deviceToken, dateStr,facebook_userId];
        
        //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
       
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlStr]];
        
            [request setHTTPMethod:@"POST"];
            
            NSMutableData *body = [NSMutableData data];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:FacebookImageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:body];
       

        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
//             if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]  )
//             {
//                 [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"facebookLogin"];
//                 [[NSUserDefaults standardUserDefaults]synchronize];
//             }
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
                  }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops", error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
   // }
}
-(void)JSONRecieved:(NSDictionary *)response
{
    NSDictionary *dict = response;
   // NSLog(@"response12%@",response);
    
       switch ([dict[kRAPIResult] integerValue])
        {
            
        case APIResponseStatusContactInUse:
        {
          
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"This email address already exit. Please try with different email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
           

            break;
        }
        case  APIResponseStatus_RegistrationSuccessfullWithImage:
        {
           // FirstLogin=NO;
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            [namedefaults synchronize];
            NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
            NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
            [emailDefaults setObject:emailStr forKey:@"email"];
            [emailDefaults synchronize];
            
            [self callWebServiceForUser_id];

           // RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
            //[self.navigationController pushViewController:HomeViewController animated:NO];
            
            
            break;
        }
            
        case APIResponseStatus_RegistrationSuccessfullWithNoImage:
            
        {
            if ([dict[@"userID"]integerValue])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                [defaults setObject:idstr forKey:@"id"];
                [defaults synchronize];
                
            }
            else if([dict[@"userid"]integerValue])
            {
                
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                [defaults1 synchronize];
                
            }
            
            
            NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
            NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
            [namedefaults setObject:userNameStr forKey:@"userNAME"];
            [namedefaults synchronize];
            NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
            NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
            [emailDefaults setObject:emailStr forKey:@"email"];
            [emailDefaults synchronize];
            
            [self callWebServiceForUser_id];

           // RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
           // [self.navigationController pushViewController:HomeViewController animated:NO];
            break;
            
        }
            
        case APIResponseStatus_RegistrationFailed:
            
        {
            
            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Whoops" message:@"Registration Faileld!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            break;
            
        }
                
            case APIResponseStatus_UserAlreadyExist:
                
            {
                if ([dict[@"userID"]integerValue])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                    [defaults setObject:idstr forKey:@"id"];
                    [defaults synchronize];
                    
                }
                else if([dict[@"userid"]integerValue])
                {
                    
                    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                    NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                    [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                    [defaults1 synchronize];
                    
                }
                
                
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
                [namedefaults synchronize];
                NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                [emailDefaults setObject:emailStr forKey:@"email"];
                [emailDefaults synchronize];

                RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                [self.navigationController pushViewController:HomeViewController animated:NO];
                
                
                break;
                
            }

           case APIResponseStatusFacebookIDInUse:
                
            {
                if ([dict[@"userID"]integerValue])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                    [defaults setObject:idstr forKey:@"id"];
                    [defaults synchronize];
                    
                }
                else if([dict[@"userid"]integerValue])
                {
                    
                    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                    NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                    [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                    [defaults1 synchronize];
                    
                }
                
                
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
                [namedefaults synchronize];
                NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                [emailDefaults setObject:emailStr forKey:@"email"];
                [emailDefaults synchronize];
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FBlogged_in"])
                {

                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fB_succesfully_lg"]== NO)
                {
                   RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                                [self.navigationController pushViewController:HomeViewController animated:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fB_succesfully_lg"];
                }
                }
                else
                {
                   RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                    [self.navigationController pushViewController:HomeViewController animated:NO];

                }
//                [self callWebServiceForUser_id];
                RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                [self.navigationController pushViewController:HomeViewController animated:NO];
                
                
                break;
                
            }
                
            case APIResponseStatusFacebookIDUnique:
                
            {
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"FacebookID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
                
                
                break;
                
            }
                
            case APIResponseStatusTwitterIDInUse:
                
            {
                if ([dict[@"userID"]integerValue])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                    [defaults setObject:idstr forKey:@"id"];
                    [defaults synchronize];
                    
                }
                else if([dict[@"userid"]integerValue])
                {
                    
                    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                    NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                    [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                    [defaults1 synchronize];
                    
                }
                
                
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
                [namedefaults synchronize];
                NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                [emailDefaults setObject:emailStr forKey:@"email"];
                [emailDefaults synchronize];
                
              //  [self callWebServiceForUser_id];
               
                [self callhomeview];
                
                break;
                
            }
                
            case APIResponseStatusTwitterIDUnique:
                
            {
               UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"TwitterID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
               
                
                break;
                
            }
                
                
            case APIResponseStatusGoogleIDInUse:
                
            {
                if ([dict[@"userID"]integerValue])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                    [defaults setObject:idstr forKey:@"id"];
                    [defaults synchronize];
                    
                }
                else if([dict[@"userid"]integerValue])
                {
                    
                    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                    NSString *twitter_idstr1 = [NSString stringWithFormat:@"%ld",(long)[dict[@"userid"]integerValue]];
                    [defaults1 setObject:twitter_idstr1 forKey:@"id"];
                    [defaults1 synchronize];
                    
                }
                
                
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
                [namedefaults synchronize];
                NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                [emailDefaults setObject:emailStr forKey:@"email"];
                [emailDefaults synchronize];
                
                //[self callWebServiceForUser_id];

                RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
                [self.navigationController pushViewController:HomeViewController animated:NO];
                
                
                break;
                
            }
                
            case APIResponseStatusGoogleIDUnique:
                
            {
                UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Rally" message:@"GoogleID Unique" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
                break;
                
            }
            case APIResponseStatusUsernameUnique:{
                if ([dict[@"userID"]integerValue])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *idstr = [NSString stringWithFormat:@"%ld",(long)[dict[@"userID"]integerValue]];
                    [defaults setObject:idstr forKey:@"id"];
                    [defaults synchronize];
                    
                }
                NSUserDefaults *namedefaults = [NSUserDefaults standardUserDefaults];
                NSString *userNameStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"userNAME"]];
                [namedefaults setObject:userNameStr forKey:@"userNAME"];
                [namedefaults synchronize];
                NSUserDefaults *emailDefaults = [NSUserDefaults standardUserDefaults];
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                [emailDefaults setObject:emailStr forKey:@"email"];
                [emailDefaults synchronize];
                
                [self callWebServiceForUser_id];
                
               // RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
               // [self.navigationController pushViewController:HomeViewController animated:NO];
                

                
            }
                break;
                
            default:
            
            break;
            
    }
    
}

-(void)callhomeview
{
    RHomeViewController *HomeViewController = [[RHomeViewController alloc]initWithNibName:@"RHomeViewController" bundle:nil];
   [self.navigationController pushViewController:HomeViewController animated:YES];
  //  [self presentViewController:HomeViewController animated:YES completion:nil];
}

-(void)callWebServiceForUser_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
    
    if (ProgressHUD == nil) {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSString * urlStrViewProfile;
    
    urlStrViewProfile= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/viewprofile.php?userid=%@",[defaults objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:urlStrViewProfile];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved_Userid:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"Whoops", error.localizedDescription);
                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
    
}

-(void)JSONRecieved_Userid:(NSDictionary *)response
{
    NSDictionary * responseDict = response;
    
    if ([responseDict objectForKey:@"userinfo"] != [NSNull null])
    {
        
        [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"PushFromSignUP"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        userInfoDict = [responseDict objectForKey:@"userinfo"];
        
        REditProfileViewController *editprofileController=[[REditProfileViewController alloc]initWithNibName:@"REditProfileViewController" bundle:nil];
        
        editprofileController.userInfoDictionary=userInfoDict;
        
        [self.navigationController pushViewController:editprofileController animated:NO];
        
    }
}

/*- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    
    // Called after logout
//      NSLog(@"Logged out");
    
        [FBSession.activeSession closeAndClearTokenInformation];
   
    
}*/
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook Error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    }
    else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
    {
//        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    }
    else
    {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
//        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage)
    {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }

}

- (IBAction)TwitterAction:(id)sender
{
    //    self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:_consumerKeyTextField.text
    //                                                 consumerSecret:_consumerSecretTextField.text];
    //
    //    _loginStatusLabel.text = @"Trying to login with Safari...";
    //    _loginStatusLabel.text = @"";
    //
    //    [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
    //        NSLog(@"-- url: %@", url);
    //        NSLog(@"-- oauthToken: %@", oauthToken);
    //
    //        [[UIApplication sharedApplication] openURL:url];
    //    } forceLogin:@(YES)
    //                    screenName:nil
    //                 oauthCallback:@"myapp://twitter_access_tokens/"
    //                    errorBlock:^(NSError *error) {
    //                        NSLog(@"-- error: %@", error);
    //                        _loginStatusLabel.text = [error localizedDescription];
    //                    }];

    
    
    //self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"Vw5wHLBwcR3vcwHgfbPUV9rpk" consumerSecret:@"RUdwnzlhTx5rDUL9hOiYWIeV6V0jzCHQ1N4l7HhdjgTL9IDvUg"];
   twitterBtn.tintColor = [UIColor clearColor];
   [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_g.png"] forState:UIControlStateSelected];
   [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitter_g.png"] forState:UIControlStateHighlighted];
   
 // self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"PUCpsqBIqJUTord0zmWGQ2PW0"    consumerSecret:@"XnL9R3V4XWys7qlCj7pc3bcT5pui12lniFqY7CsPYQzfNmYyx3"];
    
  self.twitter_api = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"JdCjH0kAvlB7WWMFmP96d6eU3"    consumerSecret:@"aqiGPCsxIT4Do65Y0hpSP2RtJj4l16SdVTAedsEvmm8d0QeFHT"];
   // NSLog(@"twitterrrrrr %@",self.twitter_api);
    
    //self.twitter_api = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"4iFyURzqFVzvR8WoIPSRfWILT"    consumerSecret:@"NBLAlMFj1XpS00j7DIxMlZqSHLtTW4HJng7auEneYQiZF0X6Bc"];

    
    
    [_twitter_api postTokenRequest:^(NSURL *url, NSString *oauthToken) {
        NSLog(@"-- url: %@", url);
        NSLog(@"-- oauthToken: %@", oauthToken);
       
        [[UIApplication sharedApplication] openURL:url];
        
             }
                    forceLogin:@(YES)
                    screenName:nil
                 oauthCallback:@"rallyapp://twitter_access_tokens/"
                    errorBlock:^( NSError *error) {
                        NSLog(@"-- error: %@", error);
                        NSLog(@"-- error: %@", [error localizedDescription]);
                        //_loginStatusLabel.text = [error localizedDescription];
                        [[[UIAlertView alloc]initWithTitle:@"Whoops" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                        
                    }];
    
    
}



- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier
{
    //self.twitter_api = [STTwitterAPI twitterAPIWithOAuthConsumerKey:@"JdCjH0kAvlB7WWMFmP96d6eU3"    consumerSecret:@"aqiGPCsxIT4Do65Y0hpSP2RtJj4l16SdVTAedsEvmm8d0QeFHT"];


       [_twitter_api postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName)
     
    {
//        NSLog(@"-- screenName: %@", screenName);
//        NSLog(@"-- screenName: %@", userID);
        
        oauthAccessTokenSecret =[NSString stringWithFormat:@"%@",_twitter_api.oauthAccessTokenSecret ];
        oauthAccessToken =[NSString stringWithFormat:@"%@",_twitter_api.oauthAccessToken];
        NSString * twitter_userId = [NSString stringWithFormat:@"%@",userID];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TWitterlogged_in"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"googleLogin"];

 /*if ([[NSUserDefaults standardUserDefaults]objectForKey:@"twitterLogin"])
   {
     
       if (ProgressHUD == nil)
       {
           ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
           ProgressHUD.labelText = @"Loading...";
           ProgressHUD.dimBackground = YES;
       }

       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       [defaults objectForKey:@"deviceToken"];
       NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
       deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
       deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
       deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];

       NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?twitterid=%@&deviceid=%@&devicetype=iPhone",twitter_userId,deviceToken];

       
       NSURL *url = [NSURL URLWithString:facebookUrlStr];
       NSURLRequest *request = [NSURLRequest requestWithURL:url];
       AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
       
       operation.responseSerializer = [AFJSONResponseSerializer serializer];
       
       [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            [self JSONRecieved:responseObject];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
            
            
            
        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ProgressHUD = nil;
            kRAlert(@"Whoops",error.localizedDescription);
        }];
       [[NSOperationQueue mainQueue] addOperation:operation];
       
       
     }
        else
       {
  */

   
            [_twitter_api getUsersShowForUserID:nil orScreenName:screenName includeEntities:nil successBlock:^(NSDictionary *user)
         
            {
                if (ProgressHUD == nil)
                {
                    ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    ProgressHUD.labelText = @"Loading...";
                    ProgressHUD.dimBackground = YES;
                }
            
//             NSLog(@"user--%@", user );
//            NSLog(@"%@", [user objectForKey:@"screen_name"]);

            NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
            [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
            NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
            NSString * emailStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"screen_name"]];

            NSString * nameStr = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
            NSString *username = [nameStr
                                  stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults objectForKey:@"deviceToken"];
            NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
                deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
                
                
                
                NSString * checkurl =[NSString stringWithFormat:@"http://abs.twimg.com/sticky/default_profile_images/default_profile_6.png"];
                NSString *code = [checkurl  substringWithRange:NSMakeRange(0, 50)];
                NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url"]];
                NSString *code1 = [imageURLString substringWithRange:NSMakeRange(0, 50)];

                NSData *avatarData;
                if ([code1 isEqualToString:code])
                {
                    UIImage *image=[UIImage imageNamed:@"profileImg.png"];
                    // profileImg.image = [UIImage imageNamed:@"profileImg.png"];
                    
                    avatarData =UIImageJPEGRepresentation(image, 0.1);
                    
                }
                else
                {
                    NSString *imageURLString = [NSString stringWithFormat:@"%@",[user objectForKey:@"profile_image_url_https"]];
                    NSString *imageUrlNew=[imageURLString stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
                    NSURL *imageURL = [NSURL URLWithString:imageUrlNew];
                    avatarData = [NSData dataWithContentsOfURL:imageURL];
                }

                
              
                
                
                urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=123&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&twitterid=%@",username,emailStr,userID,deviceToken, dateStr,twitter_userId];
            
 //    NSString *urlStr = [NSString stringWithFormat:registerURL,nameText.text,phonenoText.text,emailText.text,passwordText.text,dateStr];
//            NSLog(@"%@",dateStr);
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:urlStr]];
                [request setHTTPMethod:@"POST"];
                
                NSMutableData *body = [NSMutableData data];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                // UIImage *image=avtarimage;
                //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:avatarData]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [request setHTTPBody:body];

           // NSURL *url = [NSURL URLWithString:urlStr];
           // NSURLRequest *request = [NSURLRequest requestWithURL:url];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [self JSONRecieved:responseObject];
//                 if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]){
//                     [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"twitterLogin"];
//                     [[NSUserDefaults standardUserDefaults]synchronize];
//                 }
                 
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 
                 
                 
             }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 ProgressHUD = nil;
                 kRAlert(@"Whoops", error.localizedDescription);
             }];
            [[NSOperationQueue mainQueue] addOperation:operation];
          //  NSLog(@"%@",[user valueForKey:@"profile_image_url"]);
        }
                             errorBlock:^(NSError *error)
        {
//            NSLog(@"%@",[error localizedDescription]);
        }];
        }

  // }else close
     
        
                                        errorBlock:^(NSError *error)
    {
        
       NSLog(@"-- %@", [error localizedDescription]);
        
        
    }];
    


}

- (IBAction)MessageAction:(id)sender
{
    
    emailBtn.tintColor = [UIColor clearColor];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateSelected];
    [emailBtn setBackgroundImage:[UIImage imageNamed:@"email_gray.png"] forState:UIControlStateHighlighted];

    
    
    
   [signIn authenticate];
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    //  signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [[GPPSignIn sharedInstance] trySilentAuthentication];
    [self refreshUserInfo];
}
- (void)signOut
{
    [[GPPSignIn sharedInstance] signOut];
}
- (IBAction)SignUpAction:(id)sender
{
    RSignUpViewController * SignUpViewController = [[RSignUpViewController alloc]initWithNibName:@"RSignUpViewController" bundle:nil];
    [self.navigationController pushViewController:SignUpViewController animated:YES];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth   error: (NSError *) error
{
    if (error)
    {
//    NSLog(@"Received error %@ and auth object %@",error, auth);
        return;

    }
    else
    {
    
   [self refreshUserInfo];
    }
}
- (void)didDisconnectWithError:(NSError *)error
{
    if (error)
    {
        // NSLog( @"Status: Failed to disconnect");
    } else
    {
//        NSLog( @"Status: Disconnected");
        
    }
    
    
}

- (void)refreshUserInfo

{
   if ([GPPSignIn sharedInstance].authentication == nil)
    {
              return;
    }
    
    else
    {
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil) {
        return;
    }
    
   
//     NSLog(@"%@",person);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"googleLogin"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TWitterlogged_in"];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FBlogged_in"];
    

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"google_Login"])
    {
       

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults objectForKey:@"deviceToken"];
        NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        NSString * facebookUrlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/login.php?googleid=%@&deviceid=%@&devicetype=iPhone",person.identifier,deviceToken];
        
        NSURL *url = [NSURL URLWithString:facebookUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [self JSONRecieved:responseObject];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             
             
             
         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             ProgressHUD = nil;
             kRAlert(@"Whoops",error.localizedDescription);
         }];
        [[NSOperationQueue mainQueue] addOperation:operation];
        
        
    }
else
{
    
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    NSDateFormatter *dateFormat  = [[NSDateFormatter alloc]init];
    [dateFormat  setDateFormat:@"yyyy-MM-dd%20HH:mm:ss"];
    NSString *dateStr = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:[NSDate date]]];
    NSString * nameStr = [NSString stringWithFormat:@"%@",person.displayName];
    NSString *username = [nameStr
                          stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"deviceToken"];
    NSString * deviceToken = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"deviceToken"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    
    //NSString * birthdayDate_str=[NSString stringWithFormat:@"%@",person.birthday];
    NSString * checkurl =[NSString stringWithFormat:@"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50"];
    NSString *code = [checkurl substringFromIndex:[checkurl length] - 2];
     NSString *imageURLString = person.image.url;
      NSString *code1 = [imageURLString substringFromIndex: [checkurl length] - 2];
    NSData *avatarData;
    if ([code1 isEqualToString:code]) {
        UIImage *image=[UIImage imageNamed:@"profileImg.png"];
       // profileImg.image = [UIImage imageNamed:@"profileImg.png"];

      avatarData =UIImageJPEGRepresentation(image, 0.1);

    }
    else
    {
    NSString *imageURLString = person.image.url;
    NSURL *imageURL = [NSURL URLWithString:imageURLString];
    avatarData = [NSData dataWithContentsOfURL:imageURL];
    }
   // UIImage * avtarimage1= [UIImage imageWithData:avatarData];

    
    urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/register.php?username=%@&dob=1&email=%@&password=%@&devicetype=iPhone&deviceid=%@&date=%@&googleid=%@",username,[GPPSignIn sharedInstance].userEmail,person.identifier,deviceToken, dateStr,person.identifier];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
   // UIImage *image=avtarimage;
  //  NSData *imageData =UIImageJPEGRepresentation(avtarimage1, 0.1);
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n",@"profile"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:avatarData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self JSONRecieved:responseObject];
         
         if([[responseObject objectForKey:@"result"] isEqualToString:@"114"]   )
         {
             [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:@"google_Login"];
             [[NSUserDefaults standardUserDefaults]synchronize];
         }
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         ProgressHUD = nil;
         kRAlert(@"Whoops", error.localizedDescription);
     }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}
    }
}

- (void)presentSignInViewController:(UIViewController *)viewController
{
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}

@end
