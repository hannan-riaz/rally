//
//  RSearchViewController.h
//  Rally
//
//  Created by Ambika on 5/22/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSearchViewController : UIViewController<UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *searchTableview;
@property (nonatomic, strong) NSMutableArray *searchResultArray;
@property (weak, nonatomic) IBOutlet UISearchBar *serachBar;
@property (strong, nonatomic) IBOutlet UIView *searchRView;



@end
