//
//  RPrivacyPolicyViewController.h
//  Rally
//
//  Created by Ambika on 9/11/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPrivacyPolicyViewController : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *plicyWebView;

@end
