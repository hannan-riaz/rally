//
//  RCalenderViewController.m
//  Rally
//
//  Created by Ambika on 4/3/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//
/*
 
 tapku || https://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "RCalenderViewController.h"
#import "RJoinRallyViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"

#pragma mark - CalendarMonthViewController

@interface RCalenderViewController ()
{
   // NSDictionary *_eventsDict;
   // NSMutableArray *eventsArray;
  //  NSMutableArray *_infoDict;
    NSMutableDictionary *eventsDict;
    NSMutableArray * startRalliesEventArr;
     NSMutableArray * joinRalliesEventArr;
     NSMutableArray * ralliesEventArr;
    UILongPressGestureRecognizer *longPressGesture;
    NSMutableArray *myeventsArray;
    BOOL dateExists;
    NSMutableDictionary *dict;
}
  @property (nonatomic, strong) NSMutableDictionary *data;
 @property (nonatomic, strong) MBProgressHUD *ProgressHUD ;
@end

@implementation RCalenderViewController
@synthesize aCKCalendarEvent,ProgressHUD,data,calendarView1,calendarView2;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
            [self setDataSource:self];
            [self setDelegate:self];
      
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    //calendarView1 = [[CKCalendarViewController alloc]init];
    calendarView2 = [[CKCalendarView alloc]init];
    [calendarView2 setDelegate:self];
    [calendarView2 setDataSource:self];
    // [self presentViewController:calendar animated:YES completion:nil];
    //[self.view addSubview:calendar];
    
    startRalliesEventArr = [[NSMutableArray alloc] init];
    joinRalliesEventArr = [[NSMutableArray alloc] init];
    ralliesEventArr = [[NSMutableArray alloc] init];
    
//    longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
//    longPressGesture.minimumPressDuration = 0.5; //user needs to press for 2 seconds
   // [calendarView1 addGestureRecognizer:longPressGesture];
  
//    NSLog(@"dic%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"]);

    
    ralliesEventArr = [[NSUserDefaults standardUserDefaults] objectForKey:@"DicKey"];
    data = [[NSMutableDictionary alloc] init];
    eventsDict = [[NSMutableDictionary alloc] init];
    dict = [[NSMutableDictionary alloc]init];
    // ralliesEventArr = [[startRalliesEventArr arrayByAddingObjectsFromArray:joinRalliesEventArr] mutableCopy];
    
    myeventsArray = [[NSMutableArray alloc] init];
    
    
    for (int i =0; i< [ralliesEventArr count] ;i++)
    {
        eventsDict = [ralliesEventArr objectAtIndex:i];
        aCKCalendarEvent = [[CKCalendarEvent alloc] init];
        NSString * rallyNAME_Str = [NSString stringWithFormat:@"%@",[eventsDict  objectForKey:@"rallyNAME"]];
        rallyNAME_Str = [rallyNAME_Str  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        
        NSData *rallydata = [rallyNAME_Str dataUsingEncoding:NSUTF8StringEncoding];
        NSString *rallyDataEncode = [[NSString alloc] initWithData:rallydata encoding:NSNonLossyASCIIStringEncoding];
        if (rallyDataEncode == nil)
        {
            aCKCalendarEvent.title = rallyNAME_Str;
        }
        else
        {
            aCKCalendarEvent.title = rallyDataEncode;
        }
        

        
        NSString * rallyDate = [NSString stringWithFormat:@"%@",[eventsDict  objectForKey:@"rallyDATE"]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:rallyDate];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        rallyDate = [dateFormat stringFromDate:date];
        NSString * rallyTime = [NSString stringWithFormat:@"%@",[eventsDict  objectForKey:@"rallyTIME"]];
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HH:mm:ss"];
        NSDate *dateTime = [timeFormat dateFromString:rallyTime];
        [timeFormat setDateFormat:@"h:mm a"];
        rallyTime = [timeFormat stringFromDate:dateTime];
        aCKCalendarEvent.date = [dateFormat dateFromString:rallyDate];
        aCKCalendarEvent.Time = rallyTime;
        aCKCalendarEvent.info = eventsDict;
        NSMutableArray *arr = [dict objectForKey:aCKCalendarEvent.date];
        if (arr ==  nil)
        {
            [arr addObject:aCKCalendarEvent];
            [dict setObject:[NSMutableArray arrayWithObject:aCKCalendarEvent] forKey:aCKCalendarEvent.date];
            
        }
        else
        {
            
            [arr addObject:aCKCalendarEvent];
            [dict setObject:arr forKey:aCKCalendarEvent.date];
        }
        
        [myeventsArray addObject: aCKCalendarEvent];
        
    }
    data = dict;
    
//    [calendarView1 reloadInputViews];
//    [calendarView2 reload];
//    [calendarView2 reloadAnimated:YES];
//    [calendarView2 reloadInputViews];
    
    	//[self.monthView selectDate:[NSDate date]];
    
    
    
    /*NSMutableArray *myeventsArray = [[NSMutableArray alloc] init];
   // NSMutableDictionary *evenDict = [[NSMutableDictionary alloc] init];
    aCKCalendarEvent = [[CKCalendarEvent alloc] init];
    aCKCalendarEvent.title = @"email";
    aCKCalendarEvent.date =[dateformatter dateFromString: @"06/10/2014"]; //[eventsArray  objectForKey:@"phone"];
    [myeventsArray addObject: aCKCalendarEvent];
    [data setObject:myeventsArray forKey:aCKCalendarEvent.date];*/
    
    self.navigationController.navigationBar.hidden = NO;
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    //    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Rallendar";
    self.navigationItem.titleView = lblTitle;
    

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.navigationController.navigationBar.hidden=YES;
    //   [self callWebsercice_rally];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;

    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"action_bar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    //    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(0,0, 220, 40) ;
    // lblTitle.text =nameStr;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
    //lblTitle.backgroundColor = [UIColor redColor];
    lblTitle.textColor = [UIColor whiteColor];
    [lblTitle setFont:[UIFont fontWithName:@"Raleway-Medium" size:20.0]];
    //[lblTitle sizeToFit];
    lblTitle.text = @"Rallendar";
    self.navigationItem.titleView = lblTitle;
}

-(void)popToView
{
   // [self.navigationController popViewControllerAnimated:YES];
}
/*-(void)callWebsercice_rally
    {
    if (ProgressHUD == nil)
    {
        ProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ProgressHUD.labelText = @"Loading...";
        ProgressHUD.dimBackground = YES;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"id"];
       NSString * urlStr= [NSString stringWithFormat:@"http://198.1.114.240/~jointhg8/rallyv2/Rally_API/rallendar.php?userid=%@",[defaults objectForKey:@"id"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self JSONRecieved:responseObject];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        ProgressHUD = nil;
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                         ProgressHUD = nil;
                                         kRAlert(@"error", error.localizedDescription);                                     }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}


-(void)JSONRecieved:(NSDictionary *)response
{
        NSDictionary *Dict = response;
       // NSLog(@"response12%@",response);
      // startRalliesEventArr = [Dict objectForKey:@"startedrallies"];
     ralliesEventArr = [Dict objectForKey:@"joinedrallies"];
        data = [[NSMutableDictionary alloc] init];
        eventsDict = [[NSMutableDictionary alloc] init];
        dict = [[NSMutableDictionary alloc]init];
     // ralliesEventArr = [[startRalliesEventArr arrayByAddingObjectsFromArray:joinRalliesEventArr] mutableCopy];

        myeventsArray = [[NSMutableArray alloc] init];
    
    
       for (int i =0; i< [ralliesEventArr count] ;i++)
        {
            eventsDict = [ralliesEventArr objectAtIndex:i];
            aCKCalendarEvent = [[CKCalendarEvent alloc] init];
            
            aCKCalendarEvent.title = [eventsDict  objectForKey:@"rallyNAME"];
            NSString * rallyDate = [NSString stringWithFormat:@"%@",[eventsDict  objectForKey:@"rallyDATE"]];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormat dateFromString:rallyDate];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            rallyDate = [dateFormat stringFromDate:date];
            NSString * rallyTime = [NSString stringWithFormat:@"%@",[eventsDict  objectForKey:@"rallyTIME"]];
            NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
            [timeFormat setDateFormat:@"HH:mm:ss"];
            NSDate *dateTime = [timeFormat dateFromString:rallyTime];
            [timeFormat setDateFormat:@"HH:mm"];
            rallyTime = [timeFormat stringFromDate:dateTime];
            aCKCalendarEvent.date = [dateFormat dateFromString:rallyDate];
            aCKCalendarEvent.Time=rallyTime;
            aCKCalendarEvent.info =eventsDict;
            NSMutableArray *arr = [dict objectForKey:aCKCalendarEvent.date];
            if (arr ==  nil)
            {
                [arr addObject:aCKCalendarEvent];
                [dict setObject:[NSMutableArray arrayWithObject:aCKCalendarEvent] forKey:aCKCalendarEvent.date];
    
            }
            else
            {
                
                [arr addObject:aCKCalendarEvent];
                [dict setObject:arr forKey:aCKCalendarEvent.date];
            }

            [myeventsArray addObject: aCKCalendarEvent];
            
    }
       data = dict;
    
      [calendarView1 reloadInputViews];
      [calendarView2 reload];
      [calendarView2 reloadAnimated:YES];
      [calendarView2 reloadInputViews];
        //   }

            switch ([Dict[kRAPIResult] integerValue])
    
    {
        case APIResponseStatus_RallendarViewFailed:
        {
            
          UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"Rallendar View Failed...Please Try Again Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case APIResponseStatus_RallendarViewSuccessfull:
        {
            
//            UIAlertView * alert =[[UIAlertView alloc ]initWithTitle:@"Alert" message:@"User ID Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            break;
        }

              default:
            
            break;
            
            
    }
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - CKCalendarViewDataSource

- (NSArray *)calendarView:(CKCalendarView *)calendarView eventsForDate:(NSDate *)date
{
      // NSLog(@"%@",self.data[date]);
   return self.data[date];
    
}
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
//    RMapViewController *MapViewController  = [[RMapViewController alloc] init];
//    UINavigationController *navcont = [[UINavigationController alloc] initWithRootViewController:MapViewController];
//    MapViewController.calenderViewBool = YES;
//   [self presentViewController:navcont animated:YES completion:nil];
    RMapViewController *MapViewController  = [[RMapViewController alloc] initWithNibName:@"RMapViewController" bundle:nil];
   [self.navigationController pushViewController:MapViewController animated:YES];
}
#pragma mark - CKCalendarViewDelegate

// Called before/after the selected date changes
- (void)calendarView:(CKCalendarView *)CalendarView willSelectDate:(NSDate *)date
{
   // RMapViewController *MapViewController  = [[RMapViewController alloc] init];
    //UINavigationController *navcont = [[UINavigationController alloc] initWithRootViewController:MapViewController];
  //  MapViewController.calenderViewBool = YES;
    //[self presentViewController:navcont animated:YES completion:nil];

}

- (void)calendarView:(CKCalendarView *)CalendarView didSelectDate:(NSDate *)date
{
   
    
      if (self.data[date])
    {
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"dateevent"];
       
           }
    else

    {
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"dateevent"];
       
        
    }
    
        
}

- (void)calendarView:(CKCalendarView *)CalendarView didSelectEvent:(CKCalendarEvent *)event;
{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"MM/dd/yyyy"];
    
    RJoinRallyViewController *JoinRallyViewController  = [[RJoinRallyViewController alloc] init];
    UINavigationController *navcont = [[UINavigationController alloc] initWithRootViewController:JoinRallyViewController];
    JoinRallyViewController.rallyInfoDictFromCalender = event.info;
    JoinRallyViewController.calenderViewBoolForJoin = YES;
    [self presentViewController:navcont animated:YES completion:nil];

}


@end
