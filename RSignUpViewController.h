//
//  RSignUpViewController.h
//  Rally
//
//  Created by Ambika on 4/1/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "REditProfileViewController.h"

@interface RSignUpViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,FBLoginViewDelegate>
{
    NSDictionary *userInfoDict;
    UIImage *new_image; //added by swati
    int updatingYear;
    int dayUpdate;
    int monthUpdate;
    int updatingWeekDay;
    NSString *monthName;
    int monthRange;
}
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *dobText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (nonatomic, assign) float verticalPadding;
@property (nonatomic, assign) float horizontalPadding;
- (IBAction)imageTapped:(id)sender;
- (IBAction)twitterAction:(id)sender;
- (IBAction)messageAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mailBtn;
- (IBAction)dobAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
@property (strong, nonatomic) IBOutlet UIButton *twittetBtn;
@property ( strong , nonatomic ) IBOutlet UILabel *addPhotosLbl;
@property ( strong , nonatomic ) IBOutlet UIButton *saveBtn;
@property ( strong , nonatomic ) IBOutlet UILabel *fullDateLbl;
@property ( strong , nonatomic ) IBOutlet UILabel *monthLbl;
@property ( strong , nonatomic ) IBOutlet UILabel *dayLbl;
@property ( strong , nonatomic ) IBOutlet UILabel *yearLbl;
@property ( strong , nonatomic ) IBOutlet UIButton *doneBtn;
@property ( strong , nonatomic ) IBOutlet UIButton *cancelBtn;
@property ( strong , nonatomic ) IBOutlet UIView *datePopupView;
@property ( strong , nonatomic ) IBOutlet UIView *overlayView;


- (IBAction)monthUpBtnAction:(id)sender;
- (IBAction)monthDownBtnAction:(id)sender;
- (IBAction)dayUpBtnAction:(id)sender;
- (IBAction)dayDownBtnAction:(id)sender;
- (IBAction)yearUpBtnAction:(id)sender;
- (IBAction)yearDownBtnAction:(id)sender;

- (IBAction)dobPopupViewDoneBtnAction:(id)sender;
- (IBAction)dobPopupViewCancelBtnAction:(id)sender;
-(IBAction)submitBtn:(id)sender;

@end
