//
//  RImageViewController.h
//  Rally
//
//  Created by Ambika on 6/7/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RImageViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (strong, nonatomic) NSString * friendUserId_serch;
@property(strong,nonatomic)NSString *rallyid_str;
@property(strong,nonatomic)NSString *albumId_str;
@property(strong,nonatomic)NSString *albumname_str;

@end
