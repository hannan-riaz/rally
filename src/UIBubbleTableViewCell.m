//
//  UIBubbleTableViewCell.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <QuartzCore/QuartzCore.h>
#import "UIBubbleTableViewCell.h"
#import "NSBubbleData.h"
#import "AsyncImageView.h"
#import "RProfileViewController.h"
@interface UIBubbleTableViewCell ()
{
    CGFloat avatarX;
    CGFloat avatarY;
}
@property (nonatomic, retain) UIView *customView;
@property (nonatomic, retain) UIImageView *bubbleImage;
@property (nonatomic, retain) UIImageView *HatImage;
@property (nonatomic, retain) AsyncImageView *avatarImage;
@property (nonatomic, strong) UIButton *userName;
@property (nonatomic, strong) UIButton *avtarbtn;
@property (nonatomic, strong) NSString *userProfileName;
@property(nonatomic,strong)UIImageView *arrowImage; //changed


- (void) setupInternalData;

@end

@implementation UIBubbleTableViewCell

@synthesize data = _data;
@synthesize customView = _customView;
@synthesize bubbleImage = _bubbleImage;
@synthesize showAvatar = _showAvatar;
@synthesize avatarImage = _avatarImage;
@synthesize arrowImage=_arrowImage;

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}

#if !__has_feature(objc_arc)
- (void) dealloc
{
    self.data = nil;
    self.customView = nil;
    self.bubbleImage = nil;
    self.avatarImage = nil;
    [super dealloc];
}
#endif

- (void)setDataInternal:(NSBubbleData *)value
{
	self.data = value;
	[self setupInternalData];
}

- (void) setupInternalData
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!self.bubbleImage)
    {
#if !__has_feature(objc_arc)
        self.bubbleImage = [[[UIImageView alloc] init] autorelease];
    
#else
        self.bubbleImage = [[UIImageView alloc] init];        
#endif
        [self addSubview:self.bubbleImage];
    }
    
    NSBubbleType type = self.data.type;
    
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;
    

    CGFloat x = (type == BubbleTypeSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
  //  x = (type == BubbleTypeRallyOwner) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    if (type == BubbleTypeRallyOwner) {
        x = (type == BubbleTypeRallyOwner) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    }
    
   
    CGFloat y = 0;
    
    // Adjusting the x coordinate for avatar
    if (self.showAvatar)
    {
        [self.avatarImage removeFromSuperview];
#if !__has_feature(objc_arc)
        self.avatarImage = [[[AsyncImageView alloc] initWithImage:(self.data.avatar ? self.data.avatar : [UIImage imageNamed:@"missingAvatar.png"])] autorelease];
#else
        if (self.data.imageURL != nil) {
            self.avatarImage = [[AsyncImageView alloc] init];
            self.avatarImage.imageURL = self.data.imageURL;
        }
        else
        self.avatarImage = [[AsyncImageView alloc] initWithImage:(self.data.avatar ? self.data.avatar : [UIImage imageNamed:@"missingAvatar.png"])];
#endif
        
        avatarX = (type == BubbleTypeSomeoneElse) ? 8 : self.frame.size.width - 52;
       // avatarX = (type == BubbleTypeRallyOwner) ? 6 : self.frame.size.width - 52;
         if (type == BubbleTypeRallyOwner) {
        avatarX = (type == BubbleTypeRallyOwner) ? 6 : self.frame.size.width - 52;
         }
         avatarY = self.frame.size.height - 100;
        avatarY = 5;
        //avatarY = (type == BubbleTypeRallyOwner) ? 15 : self.frame.size.width - 52;
        if (type == BubbleTypeRallyOwner) {
            avatarY = (type == BubbleTypeRallyOwner) ? 15 : self.frame.size.width - 52;
        }
        
        self.avatarImage.frame = CGRectMake(avatarX, avatarY+11,35, 35);
        self.avatarImage.layer.cornerRadius = self.avatarImage.bounds.size.width / 2.0;
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarImage.layer.borderColor = [UIColor colorWithWhite:0.0 alpha:0.2].CGColor;
        self.avatarImage.layer.borderWidth = 1.0;
        self.avatarImage.userInteractionEnabled = YES;
        self.avatarImage.contentMode = UIViewContentModeScaleAspectFill;
        self.avatarImage.frame = CGRectMake(avatarX, avatarY+11,35, 35);

        
        self.avtarbtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        self.avtarbtn.frame = CGRectMake(avatarX, avatarY+11,35, 40);
        

        [self.avtarbtn addTarget:self  action:@selector(UserNameAction:)  forControlEvents:UIControlEventTouchUpInside];

        
       // self.userName = [[UIButton alloc]initWithFrame:CGRectMake(7, self.avatarImage.frame.origin.y+self.avatarImage.frame.size.height-10, 50, 50)];
        self.userName = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.userName setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0,  0.0,  0.0)];
        self.userName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.userName.frame = CGRectMake(5, self.avatarImage.frame.origin.y+self.avatarImage.frame.size.height, 53, 10);
        [self.userName addTarget:self  action:@selector(UserNameAction:)  forControlEvents:UIControlEventTouchUpInside];
        self.userName.userInteractionEnabled = YES;
        [self.userName setTitle:self.data.profileName forState:UIControlStateNormal];
        [self.userName setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        self.arrowImage=[[UIImageView alloc]init];
        
        //self.arrowImage.frame=CGRectMake(x,self.userName.frame.origin.y,30,10);
        
        
        
        
        self.HatImage = [[UIImageView alloc]initWithFrame:CGRectMake(6, 11, self.avatarImage.frame.size.width, 30)];
        
        if (type == BubbleTypeSomeoneElse)
        {
            
            
            [self addSubview:self.avatarImage];
            self.userName.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9.0f];
            self.userName.titleLabel.numberOfLines =0;
            self.userName.frame = CGRectMake(5, self.avatarImage.frame.origin.y+self.avatarImage.frame.size.height, 53, 10);
            
           // self.arrowImage.frame=CGRectMake(x,self.userName.frame.origin.y,30,10);//changed
            
            [self addSubview:self.userName];
            
            //[self addSubview:self.arrowImage]; //changed
            CGRect currentFrame = self.userName.frame;
            CGSize max = CGSizeMake(self.userName.frame.size.width, 50);
            CGSize expected = [self.data.profileName sizeWithFont:self.userName.titleLabel.font constrainedToSize:max lineBreakMode:self.userName.titleLabel.lineBreakMode];
            currentFrame.size.height = expected.height;
            self.userName .frame = currentFrame;
            self.avtarbtn.frame = CGRectMake(avatarX, avatarY+11,40,35);
            [self addSubview:self.avtarbtn];
        }
        
         CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
        if (delta > 0) y = delta;
        
        if (type == BubbleTypeSomeoneElse)  y = delta-30;//changed
        if (type == BubbleTypeRallyOwner)   y = delta-30;//changed
        if (type == BubbleTypeRallyOwner)   x += 54;
        if (type == BubbleTypeSomeoneElse)  x += 54;
        if (type == BubbleTypeMine)         x -= 5;
    }

    [self.customView removeFromSuperview];
    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + self.data.insets.left, y + self.data.insets.top, width, height);
    [self.contentView addSubview:self.customView];
  
    if (type == BubbleTypeRallyOwner)
    {
        
        
            self.HatImage.image = [UIImage imageNamed:@"kinghat.png"];
            [self addSubview:self.HatImage];
            [self addSubview:self.avatarImage];
            self.userName.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9.0f];
        self.userName.titleLabel.numberOfLines =0;
        
        self.userName.frame = CGRectMake(5, self.avatarImage.frame.origin.y+self.avatarImage.frame.size.height, 53, 10);
        
       // self.arrowImage.frame=CGRectMake(x,self.userName.frame.origin.y,30,10);//changed
        
        [self addSubview:self.userName];
        //[self addSubview:self.arrowImage];
        CGRect currentFrame = self.userName.frame;
        CGSize max = CGSizeMake(self.userName.frame.size.width, 50);
        CGSize expected = [self.data.profileName sizeWithFont:self.userName.titleLabel.font constrainedToSize:max lineBreakMode:self.userName.titleLabel.lineBreakMode];
        currentFrame.size.height = expected.height;
        self.userName.frame = currentFrame;
        self.avtarbtn.frame = CGRectMake(avatarX, avatarY+11,40,35);
      [self addSubview:self.avtarbtn];
    
    CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
    if (delta > 0) y = delta;
    
    if (type == BubbleTypeSomeoneElse)  y = delta-30;//changed
    if (type == BubbleTypeRallyOwner)   y = delta-30;//changed
    if (type == BubbleTypeRallyOwner)   x += 0;
    if (type == BubbleTypeSomeoneElse)  x += 0;
    if (type == BubbleTypeMine)         x -= 5;
}

    [self.customView removeFromSuperview];
    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + self.data.insets.left, y + self.data.insets.top, width, height);
    [self.contentView addSubview:self.customView];
    
     self.bubbleImage.frame = CGRectMake(x, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom); //shift from bottom to top changed

    if (type == BubbleTypeSomeoneElse)
    {
       
        self.arrowImage.image=[UIImage imageNamed:@"grey-arrow.png"];
        
        self.arrowImage.frame=CGRectMake(x-0.5,self.userName.frame.origin.y,17,15);

        self.bubbleImage.image = [[UIImage imageNamed:@"bubbleSomeone_new.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];

        
    }
    else if (type == BubbleTypeRallyOwner)
    
    {
    
       // self.bubbleImage.image = [[UIImage imageNamed:@"blueOwner1.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        self.arrowImage.image=[UIImage imageNamed:@"bubble-blue_new-arrow.png"];
        
        self.arrowImage.frame=CGRectMake(x+2.1,self.userName.frame.origin.y,11.5,9);
        
        self.bubbleImage.image = [[UIImage imageNamed:@"Bubble-Blue-1_new.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];

    }
    else
    {
        self.bubbleImage.image = [[UIImage imageNamed:@"bubbleMine.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:14];
    }

   
    
    if (type == BubbleTypeSomeoneElse || type == BubbleTypeRallyOwner)
    {
        if (self.bubbleImage.frame.size.height>55)
        {
        [self addSubview:self.arrowImage];
        }
        else
        {
            if (type==BubbleTypeSomeoneElse)
            {
                self.bubbleImage.image = [[UIImage imageNamed:@"bubbleSomeone.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            }
            else
            {
                self.bubbleImage.image = [[UIImage imageNamed:@"Bubble-Blue-1.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            }
          
        }
    }
    
    NSLog(@"Bubble Image Frame %f",self.bubbleImage.frame.size.height);
}
-(void)UserNameAction:(UIButton *)sender
{
     [[ NSUserDefaults standardUserDefaults] setValue:self.data.userIdStr forKey:@"userid_key"];
   // NSLog(@"%@",self.data.userIdStr);
    [[ NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profile"];

}

@end
