//
//  REditPackViewController.h
//  Rally
//
//  Created by Ambika on 4/4/14.
//  Copyright (c) 2014 Binney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface REditPackViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchDisplayDelegate>
{
    NSMutableArray *dataArray;
    UIButton * button;
    NSInteger * index;
     
}
@property (strong, nonatomic) IBOutlet UITableView *editpackTableview;
@property (strong, nonatomic) IBOutlet UITableView *deletePackTableview;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (nonatomic, strong) NSArray *searchResult;
@property (nonatomic, strong) NSArray *searchResult1;
@end
